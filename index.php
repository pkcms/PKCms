<?php

$path_root = __DIR__;
$path_install = '/index.php/install/install';
if (!file_exists($path_root . DIRECTORY_SEPARATOR . 'Cache')
    && !strstr(strtolower($_SERVER['REQUEST_URI']), 'install')) {
    header('location: ' . $path_install);
}
require_once 'Code/index.php';