;!function (win, doc) {

    var PKUser = function () {
            win.language = document.getElementsByTagName('html')[0].getAttribute('lang');
            if (win.language !== 'en') {
                win.language = 'zhCn';
            }
        },
        mAlert = function (str) {
            layer.msg(str, {offset: 't', anim: 5});
        },
        apiPostAndResult = function (tips, path, params, callback) {
            PKCms.apiPost('/index.php/' + path, params, function (error, result) {
                if (error) {
                    callback(error);
                } else {
                    PKCms.apiResultHandler(result, function (error, code, result, count) {
                        if (error) {
                            mAlert(tips + '出错：' + error);
                        } else {
                            callback(result, count);
                        }
                    });
                }
            });
        }, checkApiParams = function (list_field, params, fn) {
            let is_fn = true;
            if (typeof params == "object") {
                for (let listFieldKey in list_field) {
                    if (!params.hasOwnProperty(listFieldKey)) {
                        is_fn = false;
                        mAlert('缺少【' + list_field[listFieldKey] + '】参数');
                        break;
                    }
                }
                if (is_fn) {
                    fn();
                }
            } else {
                mAlert('您提交一个空对象');
            }
        }, userOnlineParams = function () {
            return JSON.parse(PKCms.localStorage_get('userLogin'));
        };

    PKUser.prototype.getInfoOFUser = function (fn) {
        let params = userOnlineParams();
        apiPostAndResult('获取会员信息', 'Member/ApiGetInfoOfUser', params, function (res, count) {
            fn(res);
        });
    }

    PKUser.prototype.login = function (params, fn) {
        let list_field = {login: '用户名、电子邮箱或手机号', password: '登陆密码'};
        checkApiParams(list_field, params, function () {
            apiPostAndResult('登陆会员', 'Member/ApiLogin', params, function (res, count) {
                if (typeof res == "object" && res.hasOwnProperty('userKey')
                    && res.hasOwnProperty('userSign')) {
                    console.log('会员登陆成功');
                    PKCms.localStorage_set({userLogin: JSON.stringify(res)});
                    fn();
                } else {
                    mAlert('登陆接口返回的数据结构出错');
                }
            });
        });
    }

    PKUser.prototype.loginOut = function (fn) {
        let params = userOnlineParams();
        apiPostAndResult('退出登陆', 'Member/ApiLoginOut', params, function (res, count) {
            PKCms.localStorage_del('userLogin');
            fn();
        });
    }

    PKUser.prototype.register = function (params, fn) {
        let list_field = {userName: '用户名', email: '电子邮箱', password: '用户密码', confirmPwd: '确认密码'};
        checkApiParams(list_field, params, function () {
            apiPostAndResult('注册新会员', 'Member/ApiRegister', params, function (res, count) {
                fn();
            });
        });
    };

    PKUser.prototype.sendVerifyOfEmail = function (params, fn) {
        let list_field = {email: '电子邮箱'};
        checkApiParams(list_field, params, function () {
            apiPostAndResult('注册新会员', 'Member/ApiSendVerifyOfEmail',
                params, function (res, count) {
                fn();
            });
        });
    }

    win.PKUser = new PKUser();
}(window, document);
