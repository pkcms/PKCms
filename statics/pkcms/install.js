;!function (win, doc) {
    var PKInstall = function () {
        this.params_api = {
            db: {},
            site: {}
        };
        this.is_next = true;
        win.speed = {
            db: {},
            site: {},
            list: [],
            count: 0,
            index: 0
        };
    }, loadingJS = function (src) {
        var sobj = doc.createElement('script');
        sobj.type = "text/javascript";
        sobj.src = src;
        var headobj = doc.getElementsByTagName('head')[0];
        headobj.appendChild(sobj);
    }, localPKCMS = function () {
        loadingJS('/statics/pkcms/app/layui.js')
        loadingJS('/statics/pkcms/app/common.js')
    }, goToSpeed = function (element_speed, path_api, callback) {
        setTimeout(function () {
            var tmp_length = win.speed.list.length;
            element_speed.progress('demo', PKCms.Percentage(win.speed.index, tmp_length) + '%');
            var tmp_step = win.speed.list[win.speed.index];
            PKLayer.apiPost(path_api, win.speed, function (params_result, count) {
                PKLayer.useTplPrepend({text: tmp_step}, 'tpl-log', 'view-log');
                win.speed.index += 1;
                if (win.speed.index < tmp_length) {
                    goToSpeed(element_speed, path_api, callback);
                } else {
                    element_speed.progress('demo', PKCms.Percentage(win.speed.index, tmp_length) + '%');
                    callback();
                }
            });
        }, 1000);
    };

    PKInstall.prototype.ready = function (fn) {
        if (doc.addEventListener) {
            doc.addEventListener('DOMContentLoaded', function () {
                //注销事件, 避免反复触发
                doc.removeEventListener('DOMContentLoaded', arguments.callee, false);
                //执行函数
                if (typeof PKCms == "undefined" && !win.hasOwnProperty('is_load_core')) {
                    win.is_load_core = true;
                    localPKCMS();
                }
                setTimeout(function () {
                    fn();
                }, 500);
            }, false);
        } else if (doc.attachEvent) {        //IE
            doc.attachEvent('onreadystatechange', function () {
                if (doc.readyState == 'complete') {
                    doc.detachEvent('onreadystatechange', arguments.callee);
                    //函数执行
                    if (typeof PKCms == "undefined" && !win.hasOwnProperty('is_load_core')) {
                        win.is_load_core = true;
                        localPKCMS();
                    }
                    setTimeout(function () {
                        fn();
                    }, 500);
                }
            });
        }
    };

    PKInstall.prototype.api = function (path_api, params_api, callback) {
        PKLayer.apiPost(
            'Install/' + path_api, params_api, function (params_res, count) {
                callback(params_res, count);
            }
        )
    };

    PKInstall.prototype.serviceHandler = function (params_service) {

        var drive_mysql = [];
        // 操作系统的判断
        if (params_service.hasOwnProperty('OS')) {
            params_service['isOK_os'] = params_service.OS.indexOf('windows') > -1 || params_service.OS.indexOf('linux') > -1;
            if (params_service['isOK_os'] === false) {
                this.is_next = false;
            }
        }
        console.log(this.is_next);
        if (params_service.hasOwnProperty('web_soft')) {
            var web_soft = params_service.web_soft.toLowerCase();
            params_service['isOK_ServerSoftWare'] = web_soft.indexOf('iis') > -1
                || web_soft.indexOf('nginx') > -1 || web_soft.indexOf('apache') > -1;
            if (params_service['isOK_ServerSoftWare'] === false && this.is_next) {
                this.is_next = false;
            }

        }
        console.log(this.is_next);
        // console.log(params_service);
        PKLayer.useTpl(params_service, 'tpl-server', 'show-server');
        if (params_service.hasOwnProperty('php_extension')) {
            var php_extension = params_service.php_extension;
            // 必要扩展的判断
            if (php_extension.hasOwnProperty('must')) {
                var list_must = php_extension.must;

                for (var listMustElement of list_must) {
                    var name = listMustElement.name;
                    var is_loaded = listMustElement.isLoaded;
                    if ((['mysqli', 'pdo_mysql'].indexOf(name) > 0) && !is_loaded) {
                        drive_mysql.push(name);
                    } else if (!is_loaded && this.is_next) {
                        this.is_next = false;
                    }
                }
                console.log(this.is_next);
                console.log(drive_mysql);
                if (!this.is_next || (drive_mysql.length >= 1)) {
                    $('#stepBtn-service').attr('disabled', true);
                    PKLayer.tipsMsg('环境检测能不过');
                } else {
                    $('#stepBtn-service').removeAttr('disabled');
                }
            }
        }
    };

    PKInstall.prototype.checkDB = function (params_db) {
        this.params_api.db = params_db;
        PKLayer.apiPost('Install/ApiDataBase/GetNameIsEqual', this.params_api, function (params_res, count) {
            if (params_res.hasOwnProperty('is_exists')) {
                var msg = '数据库名不重复，请放心使用！';
                if (params_res.is_exists) {
                    msg = '数据库名重复，请谨慎使用！';
                }
                PKLayer.tipsMsg(msg);
            } else {
                PKLayer.tipsMsg('接口返回参数不正确');
            }
        });
    };

    PKInstall.prototype.goToSpeed = function (element_speed, path_api, callback) {
        goToSpeed(element_speed, path_api, function () {
            callback();
        });
    };

    win.PKInstall = new PKInstall();

}(window, document);
