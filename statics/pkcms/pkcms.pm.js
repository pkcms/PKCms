;!function (win, doc) {

    var PKPM = function () {
    }, is_pc = function (call) {
        if (window.navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i)) {
            call(false); // 移动端
        } else {
            call(true); // PC端
        }
    };

    PKPM.prototype.to = function (pc, mobile) {
        var host_now = location.host.toLowerCase(),
            href = location.href,
            host_m = mobile.toLowerCase(),
            host_p = pc.toLowerCase();
        is_pc(function (is_pc) {
            if (is_pc && host_now === host_m) {
                win.location.href = href.replace(host_now, host_p);
            } else if (!is_pc && host_now === host_p) {
                win.location.href = href.replace(host_now, host_m);
            }
        });
    }

    win.PKPM = new PKPM();
}(window, document);