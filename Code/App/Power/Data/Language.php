<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return array(
    'Menu_Empty' => array(
        'zh-cn' => '菜单信息不存在，或为空',
        'en' => 'Menu information does not exist or is empty'
    ),
    'Menu_IdEmpty' => array(
        'zh-cn' => '菜单 id 不能为空',
        'en' => 'Menu ID cannot be empty'
    ),
    'Menu_NameEmpty' => array(
        'zh-cn' => '菜单名称不能为空',
        'en' => 'Menu ID cannot be empty'
    ),
    'Menu_NameExists' => array(
        'zh-cn' => '菜单名已经存在',
        'en' => 'Menu name already exists'
    ),
    'Menu_Form_ParentName' => array(
        'zh-cn' => '表单管理',
        'en' => 'form management'
    ),
    'Menu_Other_ParentName' => array(
        'zh-cn' => '其他管理',
        'en' => 'form management'
    ),
    'Menu_Other_ParentName_NoExists' => array(
        'zh-cn' => '系统级菜单名【其他管理】找不到了',
        'en' => '系统级菜单名【其他管理】找不到了'
    ),
    'Menu_sceneEmpty' => array(
        'zh-cn' => '菜单的场景为空',
        'en' => 'The scene of the menu is empty'
    ),
    'Menu_DelError' => array(
        'zh-cn' => '该菜单下有子菜单的存在，请清空子菜单后再进行些操作',
        'en' => 'There are submenus under this menu. Please clear the submenus before proceeding with any further operations'
    ),
);