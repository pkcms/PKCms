<?php


namespace PKApp\Power;


use PKCommon\Controller\AdminGetController;

class AdminGetScene extends AdminGetController
{

    public function ApiByLists()
    {
    }

    public function ApiBySelect()
    {
        $lists = dict('menu_scene');
        $result = [];
        foreach ($lists as $key => $value) {
            $result[] = [
                'id' => $key, 'name' => $value,
                'status' => in_array($key, ['system']) ? 0 : 1
            ];
        }
        $this->json($result);
    }

    public function Main()
    {
    }
}