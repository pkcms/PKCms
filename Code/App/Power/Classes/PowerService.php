<?php


namespace PKApp\Power\Classes;


use PKApp\Member\Classes\GroupService;
use PKFrame\DataHandler\Numbers;
use PKFrame\Service;

class PowerService extends Service
{

    protected $service_group;

    public function __construct()
    {
        parent::__construct();
        $this->service_group = new GroupService();
    }

    public function GetListByUser($groupId, $field = '*')
    {
        $groupEntity = $this->service_group->interface_getEntityById($groupId);
        // 区分是后台管理员，还是前台会员
        $option = [
            'scene' => $groupEntity['modelId'] == 1 ? ['system', 'admin'] : ['member'],
        ];
        if (request()->action() == 'ApiByMenuInUser') {
            $option['id'] = explode(',', $groupEntity['powerById']);
            $option['parentId'] = Numbers::To(request()->post('id'));
            $option['powerType'] = 'menu';
        }
        return $this->GetList($option, $field);
    }

    protected function db()
    {
        return new PowerDataBase();
    }

    public function GetEntity($viewParams = [], $viewField = '*')
    {
        return $this->db()->Where($viewParams)->Select($viewField)->First();
    }

    public function GetList($viewParams = [], $viewField = '*')
    {
        return $this->db()
            ->Where($viewParams)
            ->OrderBy('listSort', 'ASC')
            ->OrderBy('id', 'ASC')
            ->Select($viewField)->ToList();
    }

    public function GetAdminActionList($id_list)
    {
        return $this->GetList(
            ['id' => $id_list, 'scene' => ['system', 'admin']],
            ['action']
        );
    }

    public function interface_getEntityById($id, $viewField = '*')
    {
        return $this->getEntityById($id, 'Menu_IdEmpty', 'Menu_Empty', $viewField);
    }
}