<?php


namespace PKApp\Power;

use PKApp\Model\Classes\ModelService;
use PKApp\Power\Classes\PowerService;
use PKCommon\Controller\AdminSetController;
use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\Numbers;

class AdminSetPower extends AdminSetController
{
    protected $service;

    public function __construct()
    {
        parent::__construct();
        $this->service = new PowerService();
    }

    public function Main()
    {
        $this->assign([
            'id' => request()->has('id', 'get') ? request()->get('id') : 0
        ])->fetch('menu_form');
    }

    public function ApiByCreate()
    {
        $post = $this->model();
        $this->_isExists([
            'name' => $post['name'],
            'scene' => $post['scene'],
            'parentId' => $post['parentId'],
        ]);
        $this->service->Add($post);
        $this->inputSuccess();
    }

    public function ApiByChange()
    {
        $post = $this->model();
        $id = $this->checkPostFieldIsEmpty('id', 'Menu_IdEmpty');
        $this->_isExists([
            'name' => $post['name'],
            'scene' => $post['scene'],
            'parentId' => $post['parentId'],
            'id <> ' . $id,
        ]);
        $this->service->UpdateById($id, $post);
        $this->inputSuccess();
    }

    private function _isExists($params)
    {
        $params['powerType'] = 'menu';
        $entity = $this->service->GetEntity($params);
        !Arrays::Is($entity) ?: $this->noticeByJson('Menu_NameExists');
    }

    public function ApiByDel()
    {
        $id = request()->post('id');
        Numbers::IsId($id) ?: $this->noticeByJson('Menu_IdEmpty');
        $list = $this->service->GetList(['parentId'=>$id]);
        !Arrays::Is($list) ?: $this->noticeByJson('Menu_DelError');
        $this->service->DeleteById($id);
        $this->inputSuccess();
    }

    public function ApiByAddModelForm()
    {
        $modelId = request()->get('id');
        Numbers::IsId($modelId) ?: $this->noticeByJson('Model_IdEmpty', 'Model');
        $service_model = new ModelService();
        $modelEntity = $service_model->interface_getEntityById($modelId);
        $data = ['app' => 'form', 'c' => 'AdminGetData', 'scene' => 'admin', 'powerType' => 'menu',
            'name' => $modelEntity['name'], 'data' => 'modelId=' . $modelId];
        $menuEntity = $this->service->GetEntity(array('name' => $data['name']));
        if (empty($menuEntity)) {
            // 还没有添加到导航
            // 检查父级是否已经存在
            $parentName = language('Menu_Form_ParentName');
            $parentEntity = $this->service->GetEntity(array('name' => $parentName));
            if (!Arrays::Is($parentEntity)) {
                // 父级导航不存在
                $parentData = ['name' => $parentName, 'scene' => 'admin'];
                $parentId = $this->service->Add($parentData);
            } else {
                $parentId = $parentEntity['id'];
            }
            $data['parentId'] = $parentId;
            $this->service->Add($data);
            $this->inputSuccess();
        } else {
            // 已经添加到导航
            $this->service->UpdateById($menuEntity['id'], $data);
            $this->inputSuccess();
        }
    }

    protected function model()
    {
        $post = $this->checkModel(['name', 'parentId', 'app', 'c', 'action', 'data', 'scene']);
        $post['name'] = $this->checkPostFieldIsEmpty('name', 'Menu_NameEmpty');
        $post['parentId'] = Numbers::To($post['parentId']);
        $lists = dict('menu_scene');
        array_key_exists($post['scene'], $lists) ?: $this->noticeByJson('Menu_sceneEmpty');
        return $post;
    }
}