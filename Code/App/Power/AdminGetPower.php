<?php


namespace PKApp\Power;

use PKApp\Power\Classes\PowerService;
use PKCommon\Controller\AdminGetDetailController;
use PKFrame\DataHandler\Arrays;

class AdminGetPower extends AdminGetDetailController
{

    protected $service, $service_group;

    public function __construct()
    {
        parent::__construct();
        $this->service = new PowerService();
    }

    public function Main()
    {
        $this->assign([
            'menu_scene' => 'admin',
        ]);
        $this->fetch('menu_list');
    }

    public function ApiByLists()
    {
        $lists = $this->service->GetList(['scene' => $this->_getScene()]);
        $lists_type = dict('menu_scene');
        foreach ($lists as $index => $item) {
            $item['typeName'] = $lists_type[$item['powerType']];
            $lists[$index] = $item;
        }
        $this->json($lists);
    }

    public function ApiBySelect()
    {
        $this->json($this->service->GetList(
            ['scene' => ['admin', 'user'], 'parentId' => 0]
        ));
    }

    public function GetPowerMenu()
    {
        $this->json(
            $this->_treeMenuChildren(
                $this->service->GetList(
                    ['scene' => ['admin', 'system']],
                    ['id', 'parentId', 'name']
                )
            )
        );
    }

    private function _treeMenuChildren($menuList, $parentId = 0)
    {
        $result = [];
        $nowChildrenList = array_filter($menuList, function ($item) use ($parentId) {
            if (isset($item['parentId'])) {
                return $item['parentId'] == $parentId ? $item : [];
            }
            return [];
        });
        if (Arrays::Is($nowChildrenList)) {
            foreach ($nowChildrenList as $index => $item) {
                if (isset($item['id'])) {
                    $children = $this->_treeMenuChildren($menuList, $item['id']);
                }
                $data = [
                    'id' => $item['id'],
                    'name' => $item['name'],
                    'checkArr' => [
                        'type' => 0,
                        'checked' => 0,
                    ],
                ];
                if (isset($children) && Arrays::Is($children) > 0) {
                    $data['children'] = $children;
                    $data['last'] = false;
                } else {
                    $data['last'] = true;
                }
                $result[] = $data;
            }
        }
        return $result;
    }

    public function ApiByMenuInUser()
    {
        $list = $this->service->GetListByUser($this->loginUser()->GroupId);
        if (Arrays::Is($list)) {
            foreach ($list as $index => $item) {
                $path = '';
                $tmp_app = Arrays::GetKey('app', $item);
                $tmp_c = Arrays::GetKey('c', $item);
                $tmp_action = Arrays::GetKey('action', $item);
                $tmp_data = Arrays::GetKey('data', $item);
                if (!empty($tmp_app) && !empty($tmp_c)) {
                    $path = $tmp_app . '/' . $tmp_c;
                }
                empty($tmp_action) ?: $path .= '/' . $tmp_action;
                empty($tmp_data) ?: $path .= '?' . $tmp_data;
                $list[$index] = [
                    'id' => $item['id'],
                    'text' => $item['name'],
                    'href' => empty($path) ? '' : self::ToMeUrl($path),
                    'icon' => '',
                    'hasChildren' => $item['parentId'] == 0 ? 1 : 0,
                ];
            }
        }
        $this->json($list);
    }

    private function _getScene()
    {
        $scene = request()->get('menu_scene');
        !empty($scene) ?: $scene = ['admin','user'];
        return $scene;
    }

    public function ApiGetDetail()
    {
        $this->json(
            $this->service->interface_getEntityById(
                request()->get('id')
            )
        );
    }
}