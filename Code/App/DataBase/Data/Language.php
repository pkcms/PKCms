<?php
return [
    'backup_sql_no' => [
        'zh-cn' => '要导入的备份SQL文件，在物理路径中不存在，请核实文件存在，或者重新进行备份数据库后再尝试操作。',
        'en' => 'The backup SQL file to be imported does not exist in the physical path. Please verify the existence of the file or try backing up the database again before attempting the operation.'
    ],
    'sqlite_noExists' => [
        'zh-cn' => '要导入 SQLite 的 db 文件，在物理路径中不存在，请核实文件存在后再尝试操作。',
        'en' => 'The db file to import SQLite does not exist in the physical path. Please verify the existence of the file before attempting the operation.'
    ],
    'sqlite_siteEmpty' => [
        'zh-cn' => '要导入 SQLite 的 db 文件，站点信息找不到，或者不存在。',
        'en' => "To import SQLite's db file, the site information cannot be found or does not exist."
    ],
    'sqlite_categoryEmpty' => [
        'zh-cn' => '要导入 SQLite 的 db 文件，栏目信息找不到，或者不存在。',
        'en' => "To import SQLite's db file, the column information cannot be found or does not exist."
    ],
    'sqlite_posterEmpty' => [
        'zh-cn' => '要导入 SQLite 的 db 文件，广告信息找不到，或者不存在。',
        'en' => "To import SQLite's db file, the advertising information cannot be found or does not exist."
    ],
    'sqlite_model_indexTable_noExists' => [
        'zh-cn' => '执行导入数据模型的数据表指针为空，或不存在。',
        'en' => 'The data table pointer for importing the data model is empty or does not exist.'
    ],
    'sqlite_model_tableList_noExists' => [
        'zh-cn' => '执行导入数据模型的数据表列表为空，或不存在。',
        'en' => 'The list of data tables for importing the data model is empty or does not exist.'
    ],
    'sqlite_model_tableEach_error' => [
        'zh-cn' => '执行导入数据模型的数据表列表遍历操作出错，指针指向的表不存在。',
        'en' => 'An error occurred during the data table list traversal operation of importing the data model. The table pointed to by the pointer does not exist.'
    ],
    'sqlite_model_index_empty' => [
        'zh-cn' => '执行导入数据模型的数据表列表遍历操作出错，请求参数【指针】为空。',
        'en' => "An error occurred during the data table list traversal operation of importing the data model, and the request parameter [pointer] is empty."
    ],
    'sqlite_model_table_empty' => [
        'zh-cn' => '执行导入数据模型的数据表列表遍历操作出错，请求参数【表】为空。',
        'en' => "An error occurred during the data table list traversal operation of importing the data model, and the request parameter 'Table' is empty."
    ],
    'sqlite_field_cache_empty' => [
        'zh-cn' => '执行导入数据模型的数据表列表遍历操作出错，字段的实体参数为空。',
        'en' => 'An error occurred during the data table list traversal operation of the imported data model, and the entity parameter of the field is empty.'
    ],
];