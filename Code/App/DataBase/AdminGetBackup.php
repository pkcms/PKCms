<?php
/**
 * 备份数据库
 */

namespace PKApp\DataBase;

use PKApp\DataBase\Classes\DataBaseController;
use PKFrame\DataHandler\Arrays;

class AdminGetBackup extends DataBaseController
{

    public function Main()
    {
        $this->fetch('backup');
    }

    public function ApiByLists()
    {
        $list_file = $this->getAllBackup();
        $this->json(Arrays::Is($list_file) ? $this->filePathHandler($list_file) : [], count($list_file));
    }
}