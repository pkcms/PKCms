<?php


namespace PKApp\DataBase;


use PKApp\DataBase\Classes\DataBaseController;
use PKFrame\DataHandler\Arrays;

class AdminGetSQLite extends DataBaseController
{

    public function Main()
    {
        $this->fetch('sqlite_list');
    }

    public function ApiByList()
    {
        $file_list = fileHelper()->OpenFolder($this->getPathSQLite(), 'db');
        $this->json(Arrays::Is($file_list) ? $this->filePathHandler($file_list) : []);
    }

}