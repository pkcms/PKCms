<?php
/**
 * 导入数据库
 */

namespace PKApp\DataBase;

use PKApp\DataBase\Classes\DataBaseController;
use PKApp\DataBase\Classes\TraitDataBase;
use PKFrame\DataHandler\Arrays;

class AdminImportBackup extends DataBaseController
{
    use TraitDataBase;

    public function Main()
    {
        $list_table = request()->post('list');
        $index_table = request()->post('index');
        $this->handler($list_table[$index_table]);
    }

    public function ImportByFirst()
    {
        $name_file = request()->post('file_name');
        $this->handler($name_file);
    }

    public function CountBackupFile()
    {
        $list_file = $this->getAllBackup();
        ($file_size = count($list_file)) > 0 ?: $this->noticeByJson('backup_sql_no');
        $this->serviceOfDataBase()->RemoveOfDataBaseByAll();
        $list_fileName = Arrays::Column($list_file, 'file_name');
        sort($list_fileName);
        $this->json($list_fileName, $file_size);
    }

    protected function handler(string $name_file)
    {
        $this->serviceOfDataBase()->ExecSQL(
            fileHelper()->GetContentsByDisk(
                $this->getPathBackup() . DS . $name_file,
                'backup_sql_no')
        );
        $this->json();
    }

}