<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
    <link rel="stylesheet" href="/statics/pkcms/icons/iconfont.css" media="all">
</head>

<body>

<div class="layui-fluid" lay-filter="tpl-element">
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <fieldset class="layui-elem-field layui-field-title">
                <legend>导入 SQLite 数据库</legend>
            </fieldset>
            <div class="layui-field-box" style="font-weight: bold;color: red;">
                <h2>操作前请先阅读（已明白操作流程可直接略过）:</h2>
                <ul>
                    <li>1、支持的 SQLite 的版本系列是 3.x.x 。</li>
                    <li>2、将 SQLite 的 *.db 数据库文件放置在站点根目录下的 Tmp/SQLite 文件夹中，如果没有相对应的文件夹则手动创建即可。</li>
                    <li>3、导入前请先检验 SQLite 的 *.db 数据库文件的文件无损（能够正常打开）。</li>
                    <li>4、本导入过程不会将原来数据库中的全部数据都导入，只会导入重点性数据，如站点、（部分）数据模型、栏目及内容等</li>
                    <li>5、为避免在导入中出错，请在纯净（不含有项目的数据）的项目中进行。</li>
                    <li>6、检查【pk_model_field】表中的字段数据与实体表中的字段一一对应，导入程序以【pk_model_field】为基准建立。</li>
                </ul>
            </div>
        </div>

        <div class="layui-card-body">
            <table class="layui-hide PK-table" id="table_adminList" lay-filter="table_adminList"
                   data-get="DataBase/AdminGetSQLite/ApiByList"></table>

        </div>
    </div>
</div>
<script type="text/html" id="table_tool">
    <button type="button" class="layui-btn layui-btn-normal layui-btn-xs btn-import" lay-tips="导入该db"
            data-file="{{  d.file_name  }}">
        <i class="iconfont pkcms-icon-import"></i></button>
</script>

<script type="text/html" id="tpl-log">
    <li>
        完成进度： -&gt;【{{ d.text }}】
    </li>
</script>

<script type="text/html" id="tpl-table">
    <div class="layui-card">
        <div class="layui-card-body">数据模型个数：{{ d.table.count }}</div>
    </div>
    <div class="layui-card">
        <div class="layui-card-header">功能模型导入数据进度：</div>
        <div class="layui-card-body">
            <div class="layui-progress layui-progress-big" lay-filter="import-module" lay-showpercent="true">
                <div class="layui-progress-bar layui-bg-blue" lay-percent="0%">
                    <span class="layui-progress-text"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="layui-card">
        <div class="layui-card-header">数据模型导入数据进度：</div>
        <div class="layui-card-body">
            <div class="layui-progress layui-progress-big" lay-filter="import-model" lay-showpercent="true">
                <div class="layui-progress-bar layui-bg-blue" lay-percent="0%">
                    <span class="layui-progress-text"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="layui-card">
        <div class="layui-card-header">数据模型的正文内容导入数据进度</div>
        <div class="layui-card-body">
            <div class="layui-progress layui-progress-big" lay-filter="import-content" lay-showpercent="true">
                <div class="layui-progress-bar layui-bg-blue" lay-percent="0%">
                    <span class="layui-progress-text"></span>
                </div>
            </div>
        </div>
        <div class="layui-card-body">
            <div class="layui-border-box" style="height: 300px;padding: 5px;overflow: auto;">
                <ul id="view-log"></ul>
            </div>
        </div>
    </div>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

</body>
<script type="text/javascript">
    var data = {
        table: {
            count: 0,
            lists: []
        },
        index: {
            model: 0,
            module: 0,
            table: 0
        },
        count_model: 0,
        count_content: 0,
        page_index: 0,
        page_line: 20,
        file_name: '',
        list_field:[],
        steps_module: [
            {
                name: '导入站点数据',
                action: 'Site'
            },
            {
                name: '导入栏目数据',
                action: 'Category'
            },
            {
                name: '导入单页数据',
                action: 'Page'
            },
            {
                name: '导入模型的选项数据',
                action: 'ModelOptions'
            },
            {
                name: '导入广告数据',
                action: 'Poster'
            }
        ],
        steps_model: [
            {
                name: '检查模型字段',
                action: 'CheckField'
            },
            {
                name: '导入数据',
                action: 'ImportData'
            }
        ]
    };

    // ========== 显示 db 列表
    function renderTable() {
        PKAdmin.table({
            cols: [[
                {field:'file_name', title:'备份文件名'},
                {field:'size', title:'文件大小'},
                {field:'perms', title:'权限', width: 60},
                {field:'file_path', title:'文件路径'},
                {field:'c_time', title:'创建时间',templet:"<div>{{dateFormat(d.c_time)}}</div>"},
                {field:'m_time', title:'修改时间',templet:"<div>{{dateFormat(d.m_time)}}</div>"},
                {
                    title: "操作",
                    align: "center",
                    width: 60,
                    fixed: "right",
                    toolbar: "#table_tool"
                }
            ]],
            initSort: {
                field: 'file_name',
                type: 'asc'
            }
        });
    }

    PKAdmin.ready(function () {
        renderTable();
        setTimeout(function () {
            layui.use(['element'], function () {
                var $ = layui.$
                    , element_speed = layui.element;

                // =========== 执行导入
                function ImportByDB(file_name) {
                    data.file_name = file_name;
                    PKLayer.apiPost(
                        'DataBase/AdminImportSQLite', data,
                        function (apiResult, count) {
                            data.table.lists = apiResult;
                            data.table.count = count;
                            data.count_model = count * data.steps_model.length;
                            data.index.module = 0;
                            data.index.model = 0;
                            data.index.table = 0;
                            element_speed.progress('import-module', PKCms.Percentage(data.index.module, data.steps_module.length) + '%');
                            element_speed.progress('import-model', PKCms.Percentage(data.index.model, data.count_model) + '%');
                            element_speed.progress('import-content', PKCms.Percentage(data.page_index, data.count_content) + '%');
                            PKLayer.useModelByTpl('执行导入 SQLite 流程', data, 'tpl-table', function (index) {
                                setTimeout(function () {
                                    ImportModule();
                                }, 1000);
                            });
                        }
                    );
                }

                function ImportModule() {
                    var action = data.steps_module[data.index.module].action;
                    PKLayer.apiPost(
                        'DataBase/AdminImportSQLite/ApiImportModuleBy' + action, data,
                        function (apiResult, count) {
                            PKLayer.useTplPrepend({text:data.steps_module[data.index.module].name}, 'tpl-log', 'view-log');
                            data.index.module += 1;
                            element_speed.progress('import-module', PKCms.Percentage(data.index.module, data.steps_module.length) + '%');
                            if (data.index.module < data.steps_module.length) {
                                setTimeout(function () {
                                    ImportModule();
                                }, 1000);
                            } else {
                                setTimeout(function () {
                                    ImportModel();
                                }, 1000);
                            }
                        }
                    );
                }

                function ImportModel() {
                    if (data.index.table < data.table.count) {
                        var action = data.steps_model[0].action;
                        PKLayer.apiPost(
                            'DataBase/AdminImportSQLite/ApiImportModelBy' + action, data,
                            function (apiResult, count) {
                                data.list_field = apiResult;
                                data.index.model += 1;
                                element_speed.progress('import-model', PKCms.Percentage(data.index.model, data.count_model) + '%');
                                PKLayer.useTplPrepend({
                                    text: data.table.lists[data.index.table].name + ' ' + data.steps_model[0].name
                                }, 'tpl-log', 'view-log');
                                data.page_index = 1;
                                data.count_content = data.table.lists[data.index.table].count_content;
                                setTimeout(function () {
                                    ImportContent();
                                }, 1000);
                            }
                        );
                    } else {
                        PKLayer.useTplPrepend({
                            text: '导入流程已完成！'
                        }, 'tpl-log', 'view-log');
                    }
                }

                function ImportContent() {
                    var action = data.steps_model[1].action;
                    PKLayer.apiPost(
                        'DataBase/AdminImportSQLite/ApiImportModelBy' + action, data,
                        function (apiResult, count) {
                            PKLayer.useTplPrepend({
                                text: data.table.lists[data.index.table].name + ' ' + data.steps_model[1].name
                            }, 'tpl-log', 'view-log');
                            if (data.page_index * data.page_line >= data.count_content) {
                                data.index.model += 1;
                                data.index.table += 1;
                                element_speed.progress('import-content', PKCms.Percentage(data.count_content, data.count_content) + '%');
                                element_speed.progress('import-model', PKCms.Percentage(data.index.model, data.count_model) + '%');
                                setTimeout(function () {
                                    ImportModel();
                                }, 1000);
                            } else {
                                element_speed.progress('import-content', PKCms.Percentage(data.page_index * data.page_line, data.count_content) + '%');
                                data.page_index += 1;
                                setTimeout(function () {
                                    ImportContent();
                                }, 1000);
                            }
                        }
                    );
                }

                $('.btn-import').on('click', function () {
                    ImportByDB($(this).data('file'));
                });
            });
        }, 1000);
    });
</script>
</html>
