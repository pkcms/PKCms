<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
    <link rel="stylesheet" href="/statics/pkcms/icons/iconfont.css" media="all">
</head>

<body>

<div class="layui-fluid" lay-filter="tpl-element">
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <fieldset class="layui-elem-field layui-field-title">
                <legend>备份（或导入）数据库</legend>
            </fieldset>
            <div class="layui-field-box" style="font-weight: bold;">
                <h2>操作前请先阅读（已明白操作流程可直接略过）:</h2>
                <p>导入备份的操作会将现运行的数据表的数据进行重写，导入后的数据将以备份的数据为准。</p>
            </div>
            <div class="layui-form layui-card-header layuiadmin-card-header-auto">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <button class="layui-btn layuiadmin-btn-admin layui-btn-normal" id="btnBackup">
                            <i class="layui-icon layui-icon-upload layuiadmin-button-btn"></i>备份数据库
                        </button>
                    </div>
                    <div class="layui-inline">
                        <button class="layui-btn layuiadmin-btn-admin layui-btn" id="btnImport">
                            <i class="iconfont pkcms-icon-import layuiadmin-button-btn"></i> 全部导入
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="layui-card-body">
            <table class="layui-hide PK-table" id="table_adminList" lay-filter="table_adminList"
                   data-get="DataBase/AdminGetBackup" data-set="DataBase/AdminSetBackup"></table>

        </div>
    </div>
</div>

<script type="text/html" id="tpl-backup">
    <div class="layui-card">
        <div class="layui-card-header">操作进度：</div>
        <div class="layui-card-body">
            <div style="margin: 0 auto;max-height: 300px;">
                <div class="layui-progress layui-progress-big" lay-filter="demo" lay-showpercent="true">
                    <div class="layui-progress-bar layui-bg-blue" lay-percent="0%">
                        <span class="layui-progress-text"></span>
                    </div>
                </div>
                <div class="layui-border-box" style="height: 300px;padding: 5px;overflow: auto;">
                    <ul id="view-log"></ul>
                </div>
            </div>
        </div>
    </div>
</script>

<script type="text/html" id="tpl-log">
    <li>
        完成进度： -&gt;【{{ d.text }}】
    </li>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">
    var data = {
        params_api: {
            list: null,
            index: 0,
            count: 0
        }
    };

    function renderTable() {
        PKAdmin.table({
            cols: [[
                {field:'file_name', title:'备份文件名'},
                {field:'size', title:'文件大小'},
                {field:'perms', title:'权限', width: 60},
                {field:'file_path', title:'文件路径'},
                {field:'c_time', title:'创建时间',templet:"<div>{{dateFormat(d.c_time)}}</div>"},
                {field:'m_time', title:'修改时间',templet:"<div>{{dateFormat(d.m_time)}}</div>"}
            ]],
            initSort: {
                field: 'file_name',
                type: 'asc'
            }
        });
    }

    PKAdmin.ready(function () {
        layui.use(['element'], function () {
            var $ = layui.$
                , element_speed = layui.element;

            function backup_speed(api_path) {
                setTimeout(function () {
                    element_speed.progress('demo', PKCms.Percentage(data.params_api.index, data.params_api.count) + '%');
                    var tmp_step = data.params_api.list[data.params_api.index];
                    PKLayer.apiPost(api_path, data.params_api, function (params_result, count) {
                        PKLayer.useTplPrepend({text:tmp_step}, 'tpl-log', 'view-log');
                        data.params_api.index += 1;
                        if (data.params_api.index < data.params_api.count) {
                            backup_speed(api_path);
                        } else {
                            element_speed.progress('demo', PKCms.Percentage(data.params_api.index, data.params_api.count) + '%');
                            PKLayer.useTplPrepend({text:'任务已完成'}, 'tpl-log', 'view-log');
                            renderTable();
                        }
                    });
                }, 1000)
            }

            $('#btnBackup').on('click', function () {
                PKLayer.useModelByTpl('备份数据库的进度',{}, 'tpl-backup'
                    , function (index) {
                        $('#view-log').empty();
                        setTimeout(function () {
                            PKLayer.apiPost('DataBase/AdminSetBackup/GetListByTableName',{}, function (result, count) {
                                PKLayer.useTplPrepend({text:'获取所有的数据表名'}, 'tpl-log', 'view-log');
                                data.params_api.list = result;
                                data.params_api.count = count;
                                data.params_api.index = 0;
                                backup_speed('DataBase/AdminSetBackup');
                            });
                        }, 1000);
                    });
                return false;
            });

            $('#btnImport').on('click', function () {
                PKLayer.useModelByTpl('全部导入数据库的进度',{}, 'tpl-backup', function (index) {
                    $('#view-log').empty();
                    setTimeout(function () {
                        PKLayer.apiPost('DataBase/AdminImportBackup/CountBackupFile',{}, function (result, count) {
                            PKLayer.useTplPrepend({text:'获取所有的备份SQL文件'}, 'tpl-log', 'view-log');
                            data.params_api.list = result;
                            data.params_api.count = count;
                            data.params_api.index = 0;
                            backup_speed('DataBase/AdminImportBackup');
                        });
                    }, 1000);
                });
                return false;
            });

        });
        renderTable();
    });
</script>
</body>
</html>
