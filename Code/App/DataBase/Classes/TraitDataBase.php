<?php


namespace PKApp\DataBase\Classes;

trait TraitDataBase
{

    final protected function serviceOfDataBase(): DataBaseService
    {
        static $cls;
        !empty($cls) ?: $cls = new DataBaseService();
        return $cls;
    }

}