<?php


namespace PKApp\DataBase\Classes;

use PKCommon\Controller\AdminController;

abstract class DataBaseController extends AdminController
{

    protected const FileName_Table = 'dataStructure.sql';

    final protected function getPathBackup(): string
    {
        return PATH_CACHE . dict('disk_dir_name');
    }

    final protected function getPathSQLite(): string
    {
        return PATH_TMP . 'SQLite' . DS;
    }

    final protected function getAllBackup(): array
    {
        return fileHelper()->OpenFolder($this->getPathBackup(), 'sql');
    }

    final protected function filePathHandler(array $list_file): array
    {
        foreach ($list_file as $index => $item) {
            $item['file_path'] = urldecode($item['file_path']);
            $list_file[$index] = $item;
        }
        return $list_file;
    }

}