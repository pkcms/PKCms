<?php


namespace PKApp\DataBase\Classes;


use PKFrame\DataHandler\Arrays;
use PKFrame\Driver\PDO_Sqlite;
use PKFrame\Driver\Sqlite;
use PKFrame\Lib\DataBase;

class SQLiteService
{

    private $_path;

    public function __construct($path)
    {
        $this->_path = $path;
    }

    protected function dbBySQLite()
    {
        static $cls;
        if (empty($cls)) {
            if (extension_loaded('pdo_sqlite')) {
                $cls = new PDO_Sqlite($this->_path);
            } elseif (extension_loaded('sqlite3')) {
                $cls = new Sqlite($this->_path);
            } else {
                out()->noticeByJson('Sqlite3 or pdo_Sqlite extension not open');
            }
        }
        return $cls;
    }

    protected function checkTableName(string $table_name)
    {
        $sql = <<<SQL
select * from sqlite_master where type = 'table' and name = '{$table_name}'
SQL;
        return $this->dbBySQLite()->fetchAssoc($sql, true);
    }

    public function getCategoryByPage()
    {
        $sql = <<<sql
SELECT * FROM [pk_content_page]
sql;
        return $this->dbBySQLite()->fetchAssoc($sql, true);
    }

    public function getModelByOptions()
    {
        $table = 'pk_model_options';
        $entity = $this->checkTableName($table);
        if (Arrays::Is($entity)) {
            $sql = <<<SQL
SELECT * FROM [pk_model_options]
SQL;
            return $this->dbBySQLite()->fetchAssoc($sql, true);
        } else {
            return null;
        }
    }

    public function getModelByContent()
    {
        $sql = <<<SQL
SELECT [tablename],[name],[id] FROM [pk_model] WHERE [type] = 0
SQL;
        return $this->dbBySQLite()->fetchAssoc($sql, true);
    }

    public function getField($model_id)
    {
        $sql = <<<SQL
SELECT * FROM [pk_model_field] WHERE [modelid]={$model_id}
SQL;
        return $this->dbBySQLite()->fetchAssoc($sql, true);
    }

    public function countContentByModel(string $model_name)
    {
        $sql = <<<SQL
SELECT COUNT(*) AS [count] FROM [pk_content_{$model_name}] WHERE [status]=1
SQL;
        return $this->dbBySQLite()->fetchAssoc($sql);
    }

    public function getContentByModel(string $model_name, int $index)
    {
        $model_name = strtolower($model_name);
        $index = ($index - 1) * 20;
        $sql = <<<SQL
SELECT * FROM [pk_content_{$model_name}] WHERE [status]=1 ORDER BY [id] DESC LIMIT {$index},20
SQL;
        return $this->dbBySQLite()->fetchAssoc($sql, true);
    }

    public function getContentByData(string $model_name, string $id_list)
    {
        $sql = <<<SQL
SELECT * FROM [pk_content_{$model_name}_data] WHERE [id] IN ({$id_list}) GROUP BY [id]
SQL;
        return $this->dbBySQLite()->fetchAssoc($sql, true);
    }

    public function getContentByHits(string $model_name, string $id_list)
    {
        $sql = <<<SQL
SELECT [id],[hits] FROM [pk_content_{$model_name}_count] WHERE [id] IN ({$id_list})
SQL;
        $list = $this->dbBySQLite()->fetchAssoc($sql, true);
        !Arrays::Is($list) ?: $list = Arrays::Column($list, 'hits', 'id');
        return $list;
    }

    public function getSite()
    {
        $sql = <<<SQL
SELECT * FROM [pk_site]
SQL;
        return $this->dbBySQLite()->fetchAssoc($sql, true);
    }

    public function getCategory()
    {
        $sql = <<<SQL
SELECT * FROM [pk_category] WHERE [status] != 4
SQL;
        return $this->dbBySQLite()->fetchAssoc($sql, true);
    }

    public function getPoster()
    {
        $sql = <<<SQL
SELECT * FROM [pk_poster]
SQL;
        return $this->dbBySQLite()->fetchAssoc($sql, true);
    }

}