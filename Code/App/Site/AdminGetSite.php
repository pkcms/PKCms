<?php

namespace PKApp\Site;

use PKApp\Member\Classes\TraitMember;
use PKCommon\Controller\AdminController;
use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\MatchHelper;
use PKFrame\DataHandler\Numbers;

class AdminGetSite extends AdminController
{

    use TraitMember;

    public function Main()
    {
        if ($this->loginUser()->GroupId == 1) {
            $this->assign('groupId', $this->loginUser()->GroupId);
            $this->fetch('site_list');
        } else {
            header('location:/index.php/Site/AdminSetSite');
        }
    }

    private function _handler($siteList, $userList)
    {
        if (Arrays::Is($siteList)) {
            foreach ($siteList as $index => $item) {
                if (is_array($item) && array_key_exists('id', $item)) {
                    $siteId = $item['id'];
                    $nowUserList = array_filter($userList, function ($user) use ($siteId) {
                        if (isset($user['siteIdByAdmin'])) {
                            return $user['siteIdByAdmin'] == $siteId ? $user : [];
                        }
                        return [];
                    });
                    if (Arrays::Is($nowUserList)) {
                        $nowUserList = Arrays::GetArrayByKey($nowUserList, 'userName');
                    }
                    $item['admin'] = isset($nowUserList) && is_array($nowUserList) ? implode(',', $nowUserList) : '';
                }
                $siteList[$index] = $item;
            }
        }
        return $siteList;
    }

    public function ApiByLists()
    {
        $siteList = $this->serviceOfSite()->GetList(
            $this->loginUser()->GroupId == 1 ? [] : ['id' => $this->loginUser()->SiteId],
            ['id', 'Site_Name', 'Site_Domain']
        );
        $siteIdList = array_column($siteList, 'id');
        $userList = $this->serviceByUser()->GetList(['siteIdByAdmin' => $siteIdList], ['siteIdByAdmin', 'userName']);
        $result = $this->_handler($siteList, $userList);
        $this->json($result);
    }

    public function ApiByCount()
    {
        $this->json([], $this->serviceOfSite()->GetCount());
    }

    public function ApiBySelect()
    {
        $siteList = $this->serviceOfSite()->GetList([], ['id', 'Site_Name']);
        $result = [];
        if (Arrays::Is($siteList)) {
            foreach ($siteList as $item) {
                $result[] = ['id' => $item['id'], 'name' => $item['Site_Name']];
            }
        }
        $this->json($result);
    }

    public function ApiGetDetail()
    {
        $siteId = request()->get('id');
        Numbers::IsId($siteId) ?: $this->noticeByJson('Site_IdEmpty');
        if ($this->loginUser()->GroupId > 1) {
            $siteId == $this->getIdOfSiteByAdmin() ?: $this->noticeByPage('Site_NoPower_Edit');
        }
        $this->json($this->serviceOfSite()->interface_getEntityById($siteId));
    }

    public function ApiGetDomain()
    {
        $entity_site = $this->serviceOfSite()->interface_getEntityById($this->loginUser()->SiteId, ['Site_Domain']);
        MatchHelper::MatchFormat('url', $entity_site['Site_Domain'], 'Site_Domain_Error');
        $array_url = parse_url($entity_site['Site_Domain']);
        $this->json($array_url);
    }
}
