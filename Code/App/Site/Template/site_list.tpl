<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
    <link rel="stylesheet" href="/statics/pkcms/icons/iconfont.css" media="all">
</head>

<body>

<div class="layui-fluid">

    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <fieldset class="layui-elem-field layui-field-title">
                <legend>站点管理</legend>
            </fieldset>
            <div class="layui-card layui-box layui-border-box">
                <div class="layui-card-header"><h3>注意点:</h3></div>
                <div class="layui-card-body">
                    <ul>
                        <li>站点域名在新版系统中与多个功能模块相挂钩，请认真填写上。</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="layui-card-body">
            <table class="layui-hide PK-table" id="table_adminList" lay-filter="table_adminList"
                   data-get="Site/AdminGetSite"
                   data-set="Site/AdminSetSite"></table>

            <{if $groupId == 1}>
            <script type="text/html" id="table_toolBar">
                <div class="layui-btn-container">
                    <button class="layui-btn layui-btn-normal layui-btn-sm" lay-event="add" lay-tips="添加新的站点">
                        <i class="layui-icon layui-icon-add-circle"></i>
                    </button>
                </div>
            </script>
            <{/if}>

            <script type="text/html" id="table_tool">
                <button class="layui-btn layui-btn-normal layui-btn-xs"
                        lay-event="edit" lay-tips="设置站点的信息">
                    <i class="layui-icon layui-icon-set"></i></button>
                <a class="layui-btn layui-btn-primary layui-btn-xs" lay-tips="预览该站点的前台"
                   href="/index.php/Site/Index?siteId={{ d.id }}&preView=1" target="_blank">
                    <i class="iconfont pkcms-icon-view"></i></a>
            </script>
        </div>
    </div>
</div>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">
    function renderTable() {
        PKAdmin.table({
            cols: [[
                {field:'id', title:'ID'},
                {field:'Site_Name', title:'站点名称'},
                {field:'Site_Domain', title:'站点域名'},
                {field:'admin', title:'关联站点管理员'},
                {
                    title: "操作",
                    align: "center",
                    width: 250,
                    fixed: "right",
                    toolbar: "#table_tool"
                }
            ]]
        });
    }

    PKAdmin.ready(function () {
        renderTable();
    });
</script>
</body>
</html>

