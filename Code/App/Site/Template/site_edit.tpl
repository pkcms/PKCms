<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
</head>

<body>

<div class="layui-fluid">

    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <fieldset class="layui-elem-field layui-field-title">
                <legend>站点编辑</legend>
            </fieldset>
        </div>

        <div class="layui-card-body layui-card-body-mb">
            <form class="layui-form" action="Admin/SetSite/Api" lay-filter="<{$lay_filter}>">
                <div id="view-form"></div>

                <div class="layui-form-footer-fixed">
                    <div class="layui-input-block">
                        <div class="layui-footer">
                            <button class="layui-btn" lay-submit lay-filter="<{$lay_filter}>">立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/html" data-id="<{$id}>" data-api="Site/AdminGetSite" id="tpl-form">
    <input type="hidden" name="id" value="{{#  if(d.id){ }}{{  d.id  }}{{#  } }}"/>

    <div class="layui-card">
        {{#
        var power = PKCms.localStorage_get('powerByAction');
        }}
        {{#  if (power.indexOf('changeSiteBaseSetPower') >= 0){ }}
        <div class="layui-card-header layui-bg-gray">站点基本信息</div>
        <div class="layui-card-body">
            <div class="layui-form-item">
                <label class="layui-form-label">站点名：</label>
                <div class="layui-input-block">
                    <input type="text" name="Site_Name" lay-verify="required"
                           autocomplete="off" class="layui-input"
                           value="{{#  if(d.Site_Name){ }}{{  d.Site_Name  }}{{#  } }}">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">站点域名：</label>
                <div class="layui-input-block">
                    <input type="text" name="Site_Domain" lay-verify="required"
                           autocomplete="off" class="layui-input"
                           value="{{#  if(d.Site_Domain){ }}{{  d.Site_Domain  }}{{#  } }}">
                    <div class="layui-form-mid layui-word-aux">
                        <b style="color: red">填写时请带上协议（http:// 或 https://）</b>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">备案号：</label>
                <div class="layui-input-block">
                    <input type="text" name="Site_Tcp" class="layui-input"
                           value="{{#  if(d.Site_Tcp){ }}{{  d.Site_Tcp  }}{{#  } }}">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">LOGO:：</label>
                <div class="layui-input-block">
                    <input type="text" class="layui-input" style="width: 60%;float: left;"
                           id="Site_logo" name="Site_logo"
                           value="{{#  if(d.Site_logo){ }}{{  d.Site_logo  }}{{#  } }}"/>
                    <button type="button" class="layui-btn pkcms_upload" id="uploadBtn_Site_logo" data-field="Site_logo">
                        <i class="layui-icon layui-icon-upload"></i>选择
                    </button>
                    <button type="button" class="layui-btn" onclick="PreViewImage('Site_logo')">预览
                    </button>
                </div>
            </div>
        </div>
        {{#  } }}

        {{#  if (power.indexOf('changeSiteSEO') >= 0){ }}
        <div class="layui-card-header layui-bg-gray">SEO 设置</div>
        <div class="layui-card-body">
            <div class="layui-form-item">
                <label class="layui-form-label">Title：</label>
                <div class="layui-input-block">
                    <input type="text" name="Site_Title" class="layui-input"
                           value="{{#  if(d.Site_Title){ }}{{  d.Site_Title  }}{{#  } }}">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">KeyWords：</label>
                <div class="layui-input-block">
                    <textarea name="Site_Keyword"
                              class="layui-textarea">{{#  if(d.Site_Keyword){ }}{{  d.Site_Keyword  }}{{#  } }}</textarea>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">Description：</label>
                <div class="layui-input-block">
                                <textarea name="Site_Description"
                                          class="layui-textarea">{{#  if(d.Site_Description){ }}{{  d.Site_Description  }}{{#  } }}</textarea>
                </div>
            </div>
        </div>
        {{#  } }}

        {{#  if (power.indexOf('changeSiteContact') >= 0){ }}
        <div class="layui-card-header layui-bg-gray">企业客服联系信息</div>
        <div class="layui-card-body">
            <div class="layui-form-item">
                <label class="layui-form-label">百度定位：</label>
                <div class="layui-input-block">
                    <div class="pkcms_BaiDuMaps" data-id="BaiDuMaps"
                         data-name="Site_Contact[BaiDuMaps]"
                         data-value="{{#  if(d.Site_Contact && d.Site_Contact.BaiDuMaps){ }}{{  d.Site_Contact.BaiDuMaps  }}{{#  } }}"></div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">客服电话：</label>
                <div class="layui-input-block">
                    <input type="text" name="Site_Contact[tel]" class="layui-input"
                           value="{{#  if(d.Site_Contact && d.Site_Contact.tel){ }}{{  d.Site_Contact.tel  }}{{#  } }}">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">QQ 客服：</label>
                <div class="layui-input-block">
                    <input type="text" name="Site_Contact[qq]" class="layui-input"
                           value="{{#  if(d.Site_Contact && d.Site_Contact.qq){ }}{{  d.Site_Contact.qq  }}{{#  } }}">
                    <div class="layui-form-mid layui-word-aux">多个QQ号码之间用英文逗号（,）分隔</div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">微信客服二维码：</label>
                <div class="layui-input-block">
                    <input type="text" class="layui-input" style="width: 60%;float: left;"
                           id="WeChat" name="Site_Contact[WeChat]"
                           value="{{#  if(d.Site_Contact && d.Site_Contact.WeChat){ }}{{  d.Site_Contact.WeChat  }}{{#  } }}"/>
                    <button type="button" class="layui-btn pkcms_upload"
                            id="uploadBtn_WeChat" data-field="WeChat">
                        <i class="layui-icon layui-icon-upload"></i>选择
                    </button>
                    <button type="button" class="layui-btn" onclick="PreViewImage('WeChat')">预览
                    </button>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">联系方式：</label>
                <div class="layui-input-block">
                    <textarea class="layui-textarea"
                              name="Site_Contact[detailes]">{{#  if(d.Site_Contact && d.Site_Contact.detailes){ }}{{  d.Site_Contact.detailes  }}{{#  } }}</textarea>
                </div>
            </div>
        </div>
        {{#  } }}

        {{#  if (power.indexOf('changeSiteHeightSetting') >= 0){ }}
        <div class="layui-card-header layui-bg-gray">高级属性</div>
        <div class="layui-card-body">
            <div class="layui-form-item">
                <label class="layui-form-label">站点语言包：</label>
                <div class="layui-input-block">
                    <div class="pkcms_select" id="DefaultLang"
                         data-name="setting[Default_Lang]"
                         data-value="{{#  if(d.setting){ }}{{  d.setting.Default_Lang  }}{{#  } }}"
                         data-api="Site/AdminApi/GetLanguage"></div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">搜索页返回每页结果数：</label>
                <div class="layui-input-block">
                    <input type="number" name="setting[searchItemSize]" lay-verify="required"
                           autocomplete="off" class="layui-input"
                           value="{{#  if(d.setting){ }}{{  d.setting.searchItemSize  }}{{#  }else{ }}10{{#  } }}">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">站点模板主题：</label>
                <div class="layui-input-block">
                    <div class="pkcms_select" id="SiteThemes"
                         data-name="setting[Site_Themes]"
                         data-value="{{#  if(d.setting){ }}{{  d.setting.Site_Themes  }}{{#  } }}"
                         data-api="Template/AdminGetThemes"></div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">电脑版静态页路径：</label>
                <div class="layui-input-block">
                    <input type="text" name="Site_Path" class="layui-input"
                           value="{{#  if(d.Site_Path){ }}{{  d.Site_Path  }}{{#  } }}">
                </div>
                <div class="layui-form-mid layui-word-aux">
                    留空，则默认将静态页保存到站点的根目录
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">是否启用手机版：</label>
                <div class="layui-input-block">
                    <input type="checkbox" lay-skin="switch" id="IsMobileStatics" lay-filter="IsMobileStatics"
                           name="setting[IsMobileStatics]"
                           {{#  if(d.setting && d.setting.IsMobileStatics){ }} value="1" checked{{#  } }}/>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">移动域名是否绑定【移动版静态页路径】：</label>
                <div class="layui-input-block">
                    <div>
                        <input type="checkbox" lay-skin="switch" id="IsMobileDomain" lay-filter="IsMobileDomain"
                               name="setting[IsMobileDomain]"
                               {{#  if(d.setting && d.setting.IsMobileDomain){ }} value="1" checked{{#  } }}/>
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        移动域名绑定【移动版静态页路径】后，栏目（或内容）中过滤 url 中的【移动版静态页路径】的依据
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">移动版静态页路径：</label>
                <div class="layui-input-block">
                    <input type="text" name="setting[MobileStaticsPath]" class="layui-input"
                           value="{{#  if(d.setting){ }}{{  d.setting.MobileStaticsPath  }}{{#  } }}">
                    <div class="layui-form-mid layui-word-aux">
                        手机版关闭，无需设置
                    </div>
                </div>
            </div>
        </div>
        {{#  } }}
    </div>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">
    PKAdmin.ready(function () {
        PKAdmin.tplFormBySubmit({});
    });
</script>
</body>
</html>

