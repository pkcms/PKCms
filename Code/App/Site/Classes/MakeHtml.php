<?php


namespace PKApp\Site\Classes;

class MakeHtml extends SiteController
{

    public function Main()
    {
        request()->get('siteId', $this->getIdOfSite());
        $html_file = 'index.htm';
        $tpl_file = 'index';
        $html_pc = $this->_makeHtml($tpl_file);
        $this->_createhtml($this->diskPathByPc(), $html_file, $html_pc);
        if ($isOpen_mobile = $this->isOpenMobile()) {
            self::$makeHtml_is_mobile = true;
            $html_mobile = $this->_makeHtml($tpl_file . '_m');
            $this->_createhtml($this->diskPathByMobile(), $html_file, $html_mobile);
        }
    }

    private function _makeHtml($tpl_file, $tpl_file_dir = 'content')
    {
        ob_start();
        $this->getSiteEntity();
        $this->_fetch($tpl_file, $tpl_file_dir);
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }

    private function _fetch($template = '', $dir = null)
    {
        is_null($dir) ?: $this->view()->SetTplDir($dir);
        echo $this->view()->TplDisplay($template);
    }

    private function _createhtml($dir, $file, $data)
    {
        fileHelper()->PutContents($dir, $file, $data);
    }

}