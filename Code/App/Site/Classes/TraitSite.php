<?php


namespace PKApp\Site\Classes;

use PKFrame\DataHandler\Numbers;

trait TraitSite
{

    protected function getIdOfSite()
    {
        static $id;
        if (!Numbers::IsId($id)) {
            $id = Numbers::To(request()->has('siteId', 'post')
                ? request()->post('siteId') : request()->get('siteId'));
            Numbers::IsId($id) ?: out()->noticeByLJson('Site_IdEmpty', 'site');
        }
        return $id;
    }

    final protected function serviceOfSite(): SiteService
    {
        static $cls;
        !empty($cls) ?: $cls = new SiteService();
        return $cls;
    }

    protected function configFileName($prefix_name): string
    {
        switch ($prefix_name) {
            case 'BaiDu':
                return 'BaiDu_' . $this->getIdOfSiteByAdmin();
                break;
            case 'Config':
                return $prefix_name;
                break;
        }
        return '';
    }

    protected function readConfigFileName($prefix_name)
    {
        $config = cache()->Disk()->ReadByConfig($this->configFileName($prefix_name));
        return !is_array($config) ? [] : $config;
    }

}