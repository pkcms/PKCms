<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/5/27
 * Time: 10:37
 */

namespace PKApp\Site\Classes;

use PKFrame\DataHandler\Arrays;
use PKFrame\Lib\DataBase;

class SiteDataBase extends DataBase
{

    protected $table = 'site';

    public function GetSite($options, $field): ?array
    {
        $entity = $this->Where($options)->Select($field)->First();
        if (is_array($entity) && array_key_exists('setting', $entity)) {
            $entity['setting'] = Arrays::Unserialize($entity['setting']);
        }
        if (is_array($entity) && array_key_exists('Site_Contact', $entity)) {
            $entity['Site_Contact'] = Arrays::Unserialize($entity['Site_Contact']);
        }
        return $entity;
    }

}
