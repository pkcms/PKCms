<?php


namespace PKApp\Site\Classes;

use PKApp\Site\Model\SiteModel;
use PKFrame\Controller;
use PKFrame\DataHandler\Arrays;

abstract class SiteController extends Controller
{

    use TraitSite;

    protected static $makeHtml_is_mobile;

    final protected function getSiteEntity(): SiteModel
    {
        static $m;
        if (empty($m)) {
            $cache_file = dict('cache_name', 'site') . $this->getIdOfSite();
            $site_entity = cache()->Disk()->ReadByData($cache_file);
            if (empty($site_entity)) {
                $site_entity = $this->serviceOfSite()->interface_getEntityById(
                    $this->getIdOfSite()
                );
                cache()->Disk()->WriteByData($cache_file, $site_entity);
            }
            $m = new SiteModel();
            $m->Id = Arrays::GetKey('id', $site_entity);
            $m->PathSite = Arrays::GetKey('Site_Path', $site_entity);
            $m->Setting = Arrays::GetKey('setting', $site_entity);
            $m->Contact = Arrays::GetKey('Site_Contact', $site_entity);
            $m->IsOpenMobile = (bool)Arrays::GetKey('IsMobileStatics', $m->Setting);
            $m->PathMobile = Arrays::GetKey('MobileStaticsPath', $m->Setting);
            if (!request()->isAjax() || (request()->controller() == 'AdminMakeHtml')) {
                $this->view()->SetTplThemes($m->Setting['Site_Themes']);
                $this->_setTemplateParamsBySite($site_entity);
            }
        }
        return $m;
    }

    final public function diskPathByPc(): string
    {
        return PATH_ROOT . (empty($this->getSiteEntity()->PathSite) ? null : $this->getSiteEntity()->PathSite . DS);
    }

    final public function diskPathByMobile(): string
    {
        return PATH_ROOT . (empty($this->getSiteEntity()->PathMobile) ? null : $this->getSiteEntity()->PathMobile . DS);
    }

    final public function isOpenMobile(): bool
    {
        return $this->getSiteEntity()->IsOpenMobile && !empty($this->getSiteEntity()->PathMobile);
    }

    final protected function getSitePath()
    {
        return self::$makeHtml_is_mobile || ($this->isOpenMobile() && request()->isMobile())
            ? $this->getSiteEntity()->PathMobile : $this->getSiteEntity()->PathSite;
    }

    final private function _setTemplateParamsBySite($site_entity)
    {
        if (array_key_exists('Site_Contact', $site_entity)) {
            $siteContact = $site_entity['Site_Contact'];
            unset($site_entity['Site_Contact']);
            foreach ($siteContact as $index => $item) {
                switch ($index) {
                    case 'qq';
                        $item = explode(',', $item);
                        break;
                    case 'detailes':
                        $item = str_replace(["\r\n","\n"],'<br/>',$item);
                        break;
                }
                $this->assign('siteContact_' . strtolower($index), $item);
            }
        }
        $this->assign('Site_Id', $site_entity['id']);
        unset($site_entity['setting'], $site_entity['id']);
        $site_entity = array_change_key_case($site_entity, CASE_LOWER);
        $this->assign($site_entity);
    }

    /**
     * 处理自定义的模板文件名
     * @param string $file_name
     * @return false|string
     */
    protected function getTplFileOfCustomize(string $file_name)
    {
        return substr($file_name, 0, strlen($file_name) - 4);
    }

    final protected function GuestIP_isExpire($name_file = '', $timeCell = 0): bool
    {
        $ip = request()->GuestIP();
        $name_dir = 'GuestIP';
        $is_expire = true;
        if ($arr_data = cache()->Tmp()->ReadByJSON($name_file, $name_dir)) {
            if (array_key_exists($ip, $arr_data)) {
                // 如果有过 IP 记录，则检查行动的时间间隔
                $is_expire = (TIMESTAMP - $arr_data[$ip]) >= $timeCell;
            }
        }
        $arr_data = !is_array($arr_data) ? [$ip => TIMESTAMP] : array_merge($arr_data, [$ip => TIMESTAMP]);
        cache()->Tmp()->WriteByJSON($name_file, $arr_data, $name_dir);
        return $is_expire;
    }

}