<?php
return [
    'language_select' => ['en-us' => '英语（美国）', 'zh-cn' => '简体中文'],
    'cache_name' => 'siteEntity_',
    'cache_name_baiduMaps'=>'BaiDuMapsByLocal_',
];