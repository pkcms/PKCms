<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return array(
    'Site_Empty' => array(
        'zh-cn' => '站点信息不存在，或为空',
        'en' => 'The site information does not exist or is empty'
    ),
    'Site_IdEmpty' => array(
        'zh-cn' => '站点ID不能为空',
        'en' => 'Site ID cannot be empty'
    ),
    'Site_SettingEmpty' => array(
        'zh-cn' => '站点配置数据不能为空',
        'en' => 'Site configuration data cannot be empty'
    ),
    'Site_LanguageEmpty' => array(
        'zh-cn' => '站点语言包不能为空',
        'en' => 'The site language pack cannot be empty'
    ),
    'Site_ThemesEmpty' => array(
        'zh-cn' => '站点模板主题不能为空',
        'en' => 'The site template theme cannot be empty'
    ),
    'Site_NoPower_base' => array(
        'zh-cn' => '您的用户名没有修改站点信息的权限！请联系超级管理员开启。',
        'en' => 'Your username does not have permission to modify site information! Please contact the super administrator to enable it.'
    ),
    'Site_NoPower_Edit' => array(
        'zh-cn' => '您不是该站点的管理员，无权修改站点信息',
        'en' => 'You are not the administrator of this site and do not have permission to modify site information'
    ),
    'Site_BaiDuMaps_Empty' => array(
        'zh-cn' => '百度定位为空，请在站点编辑中进行设置',
        'en' => 'Baidu positioning is empty, please set it in the site editor'
    ),
    'Site_Domain_Error' => array(
        'zh-cn' => '站点的域名格式不正确，格式：协议+主机域名（如：http://xxx/）',
        'en' => 'The domain name format of the site is incorrect, format: protocol+host domain name (such as: http://xxx/ ）'
    ),
    'cn' => array(
        'zh-cn' => '中国',
        'en' => 'cn'
    ),
    'intranet' => array(
        'zh-cn' => '内网',
        'en' => 'Intranet'
    ),
);