<?php


namespace PKApp\Site;

use PKCommon\Controller\AdminController;
use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\MatchHelper;
use PKFrame\DataHandler\Numbers;

class AdminSetSite extends AdminController
{

    public function Main()
    {
        if ($this->loginUser()->GroupId == 1) {
            $id_site = Numbers::To(request()->get('id'));
            $lay_filter = 'form-frame';
        } else {
            $id_site = $this->loginUser()->SiteId;
            $lay_filter = 'form-page';
        }
        $this->assign([
            'groupId' => $this->loginUser()->GroupId,
            'id' => $id_site,
            'lay_filter' => $lay_filter,
        ]);
        $this->fetch('site_edit');
    }

    public function ApiByCreate()
    {
        $post = $this->model();
        $siteId = $this->serviceOfSite()->Add($post);
        $this->_delCache($siteId);
        $this->inputSuccess();
    }

    public function ApiByChange()
    {
        $post = $this->model();
        $siteId = Numbers::To(request()->post('id'));
        Numbers::IsId($siteId) ?: $this->noticeByJson('Site_IdEmpty');
        $this->serviceOfSite()->UpdateById($siteId, $post);
        $this->_delCache($siteId);
        $this->inputSuccess();
    }

    public function ApiByDel()
    {
    }

    protected function model(): array
    {
        in_array('changeSiteBaseSetPower', $this->loginUser()->PowerByAction) ?: $this->noticeByJson('Site_NoPower_base');
        $post = $this->checkModel(array(
            'Site_Name', 'Site_Domain', 'Site_Path', 'Site_Tcp', 'Site_logo',
        ));
        !array_key_exists('Site_Domain', $post) ?: ($post['Site_Domain'] = rtrim($post['Site_Domain'], '/') . '/');
        MatchHelper::checkUrlOfScheme($post['Site_Domain']) ?: $this->noticeByJson('Site_Domain_Error');
        // 判断是否有修改站点SEO的权限
        if (in_array('changeSiteHeightSetting', $this->loginUser()->PowerByAction)) {
            $post['Site_Title'] = trim(request()->post('Site_Title'));
            $post['Site_Keyword'] = trim(request()->post('Site_Keyword'));
            $post['Site_Description'] = trim(request()->post('Site_Description'));
        }
        // 判断是否有修改站点高级设置的权限
        if (in_array('changeSiteHeightSetting', $this->loginUser()->PowerByAction)) {
            // 检查站点配置信息中的默认语言、站点模板主题是否已经设置
            $site_setting = $this->checkPostFieldIsEmpty('setting', 'Site_SettingEmpty');
            Arrays::GetKey('Default_Lang', $site_setting, 'Site_LanguageEmpty');
            Arrays::GetKey('Site_Themes', $site_setting, 'Site_ThemesEmpty');
            $post['setting'] = serialize($site_setting);
        }
        // 判断是否有修改站点联系方式的权限
        if (in_array('changeSiteContact', $this->loginUser()->PowerByAction)) {
            $post['Site_Contact'] = serialize(
                request()->post('Site_Contact')
            );
        }
        return $post;
    }

    private function _delCache($siteId)
    {
        cache()->Disk()->DelByData(dict('cache_name') . $siteId);
        cache()->Disk()->DelByData(dict('cache_name_baiduMaps') . $siteId);
    }
}
