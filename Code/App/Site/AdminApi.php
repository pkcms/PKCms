<?php


namespace PKApp\Site;


use PKCommon\Controller\AdminController;

class AdminApi extends AdminController
{

    public function Main()
    {
    }

    public function GetLanguage()
    {
        $this->json($this->getDictToArray('language_select'));
    }

}