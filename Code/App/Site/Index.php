<?php

namespace PKApp\Site;

use PKApp\Site\Classes\SiteController;
use PKFrame\DataHandler\Arrays;

class Index extends SiteController
{

    public function Main()
    {
        if (request()->has('perView', 'get')) {
            $this->getSiteEntity();
            $this->fetch('index', 'content');
        } else {
            $config = $this->readConfigFileName('Config');
            $path = '';
            // 判断是否开启了根据IP自动切换站点
            if (array_key_exists('isAutoSwitchAccorToIP', $config)) {
                $ip = request()->GuestIP();
                try {
                    $location = getLocationByIp($ip);
                    $cn = language('cn');
                    $intranet = language('intranet');
                    if (!stristr($location, $intranet)) {
                        $list_site = $config['list_site'];
                        if (Arrays::Is($list_site)) {
                            $is_cn = stristr($location, $cn);
                            foreach ($list_site as $item) {
                                $path = $item['path'];
                                if ($is_cn && $item['lang'] == 'zh-cn') {
                                    break;
                                } elseif (!$is_cn && $item['lang'] == 'en') {
                                    break;
                                }
                            }
                        }
                    }
                } catch (\Exception $e) {
                }
            }
            $path = ($path != '' ? $path . DS : '') . 'index.htm';
            if (file_exists(PATH_ROOT . $path)) {
                header('location:/' . str_replace(DS, '/', $path));
            } else {
                $this->noticeByPage('not page: ' . $path);
            }
        }
    }

    public function GetBaiDuMapsByLocal()
    {
        $cache_file = dict('cache_name_baiduMaps') . $this->getSiteEntity()->Id;
        $cache_data = cache()->Disk()->ReadByData($cache_file);
        if (empty($cache_data)) {
            $data_BaiDuMaps = Arrays::GetKey('BaiDuMaps',$this->getSiteEntity()->Contact,'Site_BaiDuMaps_Empty');
            $cache_data = ['lng'=>'','lat'=>''];
            list($cache_data['lng'], $cache_data['lat'])=explode(',',$data_BaiDuMaps);
            cache()->Disk()->WriteByData($cache_file,$cache_data);
        }
        $this->json($cache_data);
    }

}