<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return array(
    'Antispam_close' => array(
        'zh-cn' => '内容安全检查处于关闭状态',
        'en' => 'Content security check is off'
    ),
    'AntispamApi_Tips' => array(
        'zh-cn' => '您提交的信息中存在敏感词信息，检查结果：',
        'en' => 'There are sensitive words in the information you submitted'
    ),
    'AntispamApi_Error' => array(
        'zh-cn' => '内容安全远程接口出错，提示：',
        'en' => 'Content security remote interface error, prompt:'
    ),
    'TemplateTag_ContentListError' => array(
        'zh-cn' => '请注意！该模板标签只能在栏目页中使用',
        'en' => 'Please note that! This template label can only be used in column pages'
    ),
    'TemplateTag_RelatedError' => array(
        'zh-cn' => '请注意！该模板标签只能在内容页中使用',
        'en' => 'Please note that! This template label can only be used in content pages'
    ),
    'TemplateTag_listNumber_zero' => array(
        'zh-cn' => '请注意！该模板标签中没有设置返回数量，或者数量不能为0，请加上返回数量参数 num 并设置好返回的数量',
        'en' => 'Please note that! The returned quantity is not set in the template label, or the quantity cannot be 0. Please add the returned quantity parameter num and set the returned quantity'
    ),
    'template_typeEmpty' => array(
        'zh-cn' => '选择模板文件时，模板类型不能为空',
        'en' => 'When selecting a template file, the template type cannot be empty'
    ),
    'Site_idEmpty' => array(
        'zh-cn' => '站点ID不能为空',
        'en' => 'Site ID cannot be empty'
    ),
    'Model_Empty' => array(
        'zh-cn' => '模型信息找不到或不存在',
        'en' => 'Model information cannot be found or does not exist'
    ),
    'Model_idEmpty' => array(
        'zh-cn' => '请选择模型',
        'en' => 'Please select model'
    ),
    'modelError_content' => array(
        'zh-cn' => '该数据模型不为内容类型',
        'en' => 'The data model is not a content type'
    ),
    'Category_Empty' => array(
        'zh-cn' => '栏目信息找不到或不存在',
        'en' => 'Column information cannot be found or does not exist'
    ),
    'Category_tplEmpty' => array(
        'zh-cn' => '栏目的模板文件信息为空',
        'en' => 'The template file information of the column is empty'
    ),
    'Category_idEmpty' => array(
        'zh-cn' => '栏目ID不能为空',
        'en' => 'Column ID cannot be empty'
    ),
    'Category_model_notEqual' => array(
        'zh-cn' => '栏目的模型ID不是同一个',
        'en' => 'The model ID of multiple columns is not the same'
    ),
    'Category_NameEmpty' => array(
        'zh-cn' => '栏目名称不能为空',
        'en' => 'Column name cannot be empty'
    ),
    'Category_TypeEmpty' => array(
        'zh-cn' => '栏目类型不能为空',
        'en' => 'Column type cannot be empty'
    ),
    'CategoryParent_noExists' => array(
        'zh-cn' => '该栏目的父级的数据找不到',
        'en' => 'The parent data of this column cannot be found'
    ),
    'category_PCModel_Error' => array(
        'zh-cn' => '该栏目的数据模型与父级的数据模型不一致',
        'en' => 'The data model of this column is inconsistent with the data model of the parent level'
    ),
    'Category_Type_NotList' => array(
        'zh-cn' => '该栏目类型不是列表',
        'en' => 'The column type is not a list'
    ),
    'Category_BatchDelError' => array(
        'zh-cn' => '批量删除的栏目列表中存在父级栏目，并且其的子栏目没有全部选中，删除请谨慎！',
        'en' => 'There are parent columns in the batch deleted column list, and not all of its sub columns are selected. Please be careful when deleting!'
    ),
    'Category_DelError' => array(
        'zh-cn' => '该栏目为父级栏目，其下有子级栏目存在，请先删除或转移其下的子级栏目再试，或者使用批量删除（这种会带上子栏目一同删除）。',
        'en' => 'This column is a parent column, and there are child columns under it. Please delete or transfer the child columns first and try again, or use batch deletion (this method will delete the child columns together).'
    ),
    'Category_seo_Empty' => array(
        'zh-cn' => '没有接收SEO相关的参数',
        'en' => 'No SEO related parameters received'
    ),
    'Category_power_Empty' => array(
        'zh-cn' => '没有接收权限相关的参数',
        'en' => 'No parameters related to receiving permissions'
    ),
    'Category_PowerError' => array(
        'zh-cn' => '您的用户名没有栏目的编辑权限！请联系超级管理员开通权限。',
        'en' => 'Your user name does not have the edit right of column! Please contact the super administrator to open permissions.'
    ),
    'category_order_empty' => array(
        'zh-cn' => '栏目的内容列表排序方式为空',
        'en' => "The sorting method of the column's content list is empty"
    ),
    'Category_listNumber_zero' => array(
        'zh-cn' => '列表栏目的列表显示数量为0，请在栏目编辑里面将数量改为大于0',
        'en' => 'The list display quantity of the list column is 0, please change the quantity to greater than 0 in the column editor'
    ),
    'Content_idEmpty' => array(
        'zh-cn' => '内容ID不能为空',
        'en' => 'Content ID cannot be empty'
    ),
    'Content_titleEmpty' => array(
        'zh-cn' => '内容标题不能为空',
        'en' => 'Content title cannot be empty'
    ),
    'Content_Empty' => array(
        'zh-cn' => '内容信息找不到，或者是您的用户名在这个栏目的阅读受限',
        'en' => 'The content information cannot be found, or your user name is limited in this column'
    ),
    'Content_mobileCategory_Error' => array(
        'zh-cn' => '被转移内容的目标栏目的数据模型不相同，所以不能移动',
        'en' => 'The data model of the target column of the transferred content is different, so it cannot be moved'
    ),
    'Content_attribute_Empty' => array(
        'zh-cn' => '您要筛选的内容属性的值为空',
        'en' => 'The value of the content property you want to filter is empty'
    ),
    'Content_attribute_Error' => array(
        'zh-cn' => '您操作的内容属性不在允许的属性列表中',
        'en' => 'The content property you are operating on is not in the list of allowed properties'
    ),
    'Content_power_field' => array(
        'zh-cn' => '该字段的内容设有阅读的权限，默认的情况下不会显示。',
        'en' => 'The content of this field is set with reading permission and will not be displayed by default.'
    ),
    'Content_createTime_Error' => array(
        'zh-cn' => '内容的发布时间格式不正确',
        'en' => 'The content was not published in the correct format'
    ),
    'Content_PowerError' => array(
        'zh-cn' => '您的用户名没有内容的编辑权限！请联系上级管理员开通权限。',
        'en' => 'Your user name does not have permission to edit the content! Please contact the superior administrator to open the permission.'
    ),
    'CreateToOtherCategory_CatIdEmpty' => array(
        'zh-cn' => '内容的发布到其他栏目的栏目ID为空',
        'en' => 'The column ID of content published to other columns is empty'
    ),
    'CreateToOtherCategory_ModelIdEmpty' => array(
        'zh-cn' => '内容的发布到其他栏目的模型ID为空',
        'en' => 'The model ID for publishing content to other columns is empty'
    ),
    'CreateToOtherCategory_ContentEmpty' => array(
        'zh-cn' => '内容的发布到其他栏目的内容实体为空',
        'en' => 'The content entity for publishing to other columns is empty'
    ),
    'CreateToOtherCategory_categoryEmpty' => array(
        'zh-cn' => '内容的发布到其他栏目的内容所属栏目实体为空',
        'en' => 'The entity of the column to which the content belongs when published to other columns is empty'
    ),
    'CreateToOtherCategory_IdError' => array(
        'zh-cn' => '被发到的栏目ID与本内容的栏目ID相同，请重新选择',
        'en' => 'The column ID sent to is the same as the column ID of this content. Please reselect'
    ),
    'CreateToOtherCategory_ModelIdError' => array(
        'zh-cn' => '被发到的栏目的模型ID与本内容的模型ID不一致，请重新选择',
        'en' => 'The model ID of the column being sent does not match the model ID of this content. Please reselect'
    ),
    'Exists_title_unique' => array(
        'zh-cn' => '存在相同的内容标题',
        'en' => 'Same Content Title Exists'
    ),
    'Empty_Import_fromCatId' => array(
        'zh-cn' => '找不到导入的源栏目ID',
        'en' => 'Unable to find the imported source column ID'
    ),
    'Empty_Import_targetCatId' => array(
        'zh-cn' => '找不到导入的目标栏目ID',
        'en' => 'Unable to find the imported target column ID'
    ),
    'Empty_import_fromCat' => array(
        'zh-cn' => '找不到导入的源栏目',
        'en' => 'Unable to find the imported source column'
    ),
    'Empty_translate' => array(
        'zh-cn' => '找不到翻译后的内容',
        'en' => 'Unable to find translated content'
    ),
    'Empty_translate_to' => array(
        'zh-cn' => '找不到翻译的目标语言',
        'en' => 'Unable to find the target language for translation'
    ),
    'Error_import_model' => array(
        'zh-cn' => '只支持列表类型的栏目',
        'en' => 'Only columns of list type are supported'
    ),
    'Err_match' => array(
        'zh-cn' => '没有找到匹配的字段：',
        'en' => 'No matching fields found:'
    ),
    'Err_url' => array(
        'zh-cn' => 'URL的必须是数字、字母的组合',
        'en' => 'The URL must be a combination of numbers and letters'
    ),
    'Page_Empty' => array(
        'zh-cn' => '单页信息找不到或不存在',
        'en' => 'Single page information cannot be found or does not exist'
    ),
    'SearchQuery_Empty' => array(
        'zh-cn' => '请输入搜索关键字',
        'en' => 'Please enter search keywords'
    ),
    'Search_ParamEmpty' => array(
        'zh-cn' => '没有检测到 query 请求参数，或者其他筛选参数',
        'en' => 'No query request parameters or other filtering parameters were detected'
    ),
    'Search_Error' => array(
        'zh-cn' => '请选择被搜索的数据模型，并输入搜索关键字',
        'en' => 'Please select the data model to be searched for and enter the search keywords'
    ),
    'Search_Category_TypeError' => array(
        'zh-cn' => '请选择被搜索的栏目的栏目类型必须是【内容列表】类型',
        'en' => "Please select the column type of the searched column that must be of the 'Content List' type"
    ),
    'Search_Category_IdExists' => array(
        'zh-cn' => '栏目ID的在被搜索的列表中不存在',
        'en' => 'The column ID does not exist in the searched list'
    ),
    'Search_LineSize_Empty' => array(
        'zh-cn' => '内容搜索要显示的数量要大于0，请在站点编辑的高级设置里面进行修改',
        'en' => 'The number of content searches to display must be greater than 0. Please modify it in the advanced settings of site editing'
    ),
    'Make_Type_Error' => array(
        'zh-cn' => '发布静态的类型不在支持范围',
        'en' => 'Publishing static types is not within the supported range'
    ),
    'MakeHtml_Index_Error' => array(
        'zh-cn' => '发布静态出错：栏目遍历指针已经超越总数！',
        'en' => 'Static publishing error: column traversal pointer has exceeded the total number!'
    ),
    'mirroring_content_FailChange' => array(
        'zh-cn' => '镜像内容不可编辑',
        'en' => 'Mirror content cannot be edited'
    ),
    'PushBD_ErrApiCode' => array(
        'zh-cn' => '百度 API 通信的状态码：',
        'en' => 'Status code of Baidu API communication:'
    ),
    'PushBD_ErrApiToken' => array(
        'zh-cn' => '百度普通收录 API 的 TOKEN 码没有设置',
        'en' => "The TOKEN code for Baidu's general indexing API is not set"
    ),
    'PushBD_EmptyUrl' => array(
        'zh-cn' => '没有新的url需要提交',
        'en' => 'No new URLs need to be submitted'
    ),
    'PushBD_ErrUrl' => array(
        'zh-cn' => '不合法的url列表',
        'en' => 'Illegal URL list'
    ),
    'PushBD_ErrSiteUrl' => array(
        'zh-cn' => '由于不是本站url而未处理的url列表',
        'en' => 'List of unprocessed URLs due to non local URLs'
    ),
    'PushBD_ErrToken' => array(
        'zh-cn' => '验证不通过！请检查站点的域名与TOKEN的数据是否正确！',
        'en' => 'Verification failed! Please check if the domain name and TOKEN data of the site are correct!'
    ),
);