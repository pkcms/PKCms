<?php
return [
    'category_typeList' => [
        'link' => '链接',
        'page' => '单页',
        'list' => '列表'
    ],
    'category_content_orderList' => [
        [
            'name' => '内容编号的降序方式', 'id' => 0,
            'field' => 'id', 'mode' => 'DESC'
        ],
        [
            'name' => '内容排序值的降序方式', 'id' => 1,
            'field' => 'listSort', 'mode' => 'DESC'
        ],
        [
            'name' => '内容的置顶方式（该方式只对列表页展示起作用）', 'id' => 2,
            'field' => 'toTop', 'mode' => 'DESC'
        ],
        [
            'name' => '栏目的排序方式', 'id' => 3,
            'field' => 'catListSort', 'mode' => 'ASC'
        ],
        [
            'name' => '内容创建时间的降序方式', 'id' => 4,
            'field' => 'createTime', 'mode' => 'DESC'
        ],
    ],
    'content_attribute' => [
        'toHead' => '头条',
        'toPush' => '推荐',
        'toTop' => '置顶'
    ],
    'tplConfig_ofList' => ['categoryTpl', 'contentTpl', 'isRewrite', 'categoryHtml', 'contentHtml',
        'listDataSize', 'isClosePageByFirst', 'isFilterContentMirror', 'orderList', 'categoryUrl', 'contentUrl'],
    'tplConfig_ofPage' => ['pageTpl', 'isRewrite', 'pageHtml', 'pageUrl'],
    'import_lang' => ['en' => '英语', 'jp' => '日语', 'kor' => '韩语', 'fra' => '法语',
        'spa' => '西班牙语', 'th' => '泰语', 'ara' => '阿拉伯语', 'ru' => '俄语',
        'pt' => '葡萄牙语', 'de' => '德语', 'it' => '意大利语', 'el' => '希腊语', 'nl' => '荷兰语',
        'pl' => '波兰语', 'bul' => '保加利亚语', 'est' => '爱沙尼亚语', 'dan' => '丹麦语',
        'fin' => '芬兰语', 'cs' => '捷克语', 'rom' => '罗马尼亚语', 'slo' => '斯洛文尼亚语',
        'swe' => '瑞典语', 'hu' => '匈牙利语', 'vie' => '越南语'],
    'export_excel_lang' => ['id'=>'编号', 'title'=>'标题', 'url'=>'页面地址']
];