<?php


namespace PKApp\Content;

use PKCommon\Controller\AdminController;

class AdminApi extends AdminController
{

    public function Main()
    {
    }

    /**
     * 栏目内容列表的排序方式
     */
    public function GetSortModeList()
    {
        $this->json(
            dict('category_content_orderList')
        );
    }

    /**
     * 内容的属性
     * 如：头条、置顶、推荐
     */
    public function GetContentAttribute()
    {
        $this->json(
            $this->getDictToArray('content_attribute')
        );
    }

    /**
     * 栏目类型
     */
    public function GetCategoryTypeList()
    {
        $this->json(
            $this->getDictToArray('category_typeList')
        );
    }
}
