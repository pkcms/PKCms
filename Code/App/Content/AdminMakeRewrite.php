<?php


namespace PKApp\Content;

use PKApp\Content\Classes\TraitContent;
use PKCommon\Controller\AdminController;

class AdminMakeRewrite extends AdminController
{
    use TraitContent;

    protected const UrlRewrite_category = '/index.php/content/category?';
    protected const UrlRewrite_content = '/index.php/content/content?';
    protected $list_category;
    protected $list_rewrite;

    public function Main()
    {
        $this->list_rewrite = [];
        $id_site = $this->loginUser()->SiteId;
        $service_category = $this->serviceOfCategory($id_site);
        $this->list_category = $service_category->GetList(['categoryType' => 'list']);
        $soft_web = strtolower(request()->server('SERVER_SOFTWARE'));
        $str = $this->webSoft();
        fileHelper()->PutContents(PATH_ROOT, '.htaccess', $str);
        $this->json();
    }

    protected function webSoft(): string
    {
        $str = "RewriteEngine On\r\nRewriteBase /\r\nRewriteCond %{QUERY_STRING} ^(.*)$\r\n";
        foreach ($this->list_category as $item) {
            $tpl = $item['template'];
            $this->rewriteListCategory($tpl);
            $this->rewriteContent($tpl);
        }
        $str .= implode("\r\nRewriteCond %{QUERY_STRING} ^(.*)$\r\n", $this->list_rewrite);
        return $str;
    }

    protected function rewriteListCategory(array $tpl)
    {
        $index = 1;
        $url_rewrite = $tpl['categoryUrl'];
        $url_md5 = md5($url_rewrite);
        if (!array_key_exists($url_md5, $this->list_rewrite)) {
            $url_php = self::UrlRewrite_category;
            $url_arr = explode('.', $url_rewrite);
            $url_ext = end($url_arr);
            $url_rewrite = str_replace('.' . $url_ext, '', $url_rewrite);
            if (stristr($url_rewrite, '[id]')) {
                $url_rewrite = str_replace('[id]', '([0-9]+)', $url_rewrite);
                $url_php .= 'id=$' . $index . '&';
                $index += 1;
            }
            if (stristr($url_rewrite, '[page]')) {
                $url_rewrite = str_replace('[page]', '([0-9]+)', $url_rewrite);
                $url_php .= 'page=$' . $index;
            }
            $this->list_rewrite[$url_md5] = 'RewriteRule ^/' . $url_rewrite . '\.' . $url_ext . '$ ' . $url_php . ' [L]';
        }
    }

    protected function rewriteContent(array $tpl)
    {
        $index = 1;
        $url_rewrite = $tpl['contentUrl'];
        $url_md5 = md5($url_rewrite);
        if (!array_key_exists($url_md5, $this->list_rewrite)) {
            $url_php = self::UrlRewrite_content;
            $url_arr = explode('.', $url_rewrite);
            $url_ext = end($url_arr);
            $url_rewrite = str_replace('.' . $url_ext, '', $url_rewrite);
            if (stristr($url_rewrite, '[catId]')) {
                $url_rewrite = str_replace('[catId]', '([0-9]+)', $url_rewrite);
                $url_php .= 'catId=$' . $index . '&';
                $index += 1;
            }
            if (stristr($url_rewrite, '[id]')) {
                $url_rewrite = str_replace('[id]', '([0-9]+)', $url_rewrite);
                $url_php .= 'id=$' . $index;
            }
            $this->list_rewrite[$url_md5] = 'RewriteRule ^/' . $url_rewrite . '\.' . $url_ext . '$ ' . $url_php . ' [L]';
        }
    }
}