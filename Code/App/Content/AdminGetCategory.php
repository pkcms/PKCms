<?php


namespace PKApp\Content;

use PKApp\Content\Classes\TraitContent;
use PKCommon\Controller\AdminController;
use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\Numbers;

class AdminGetCategory extends AdminController
{
    use TraitContent;
    protected $_serviceOfCategory;

    public function __construct()
    {
        parent::__construct();
        $this->_serviceOfCategory = $this->serviceOfCategory($this->loginUser()->SiteId);
    }

    public function Main()
    {
        $this->assign(['categoryTypeList' => $this->_getDictTypeList()])->fetch('category_lists');
    }

    private function _getDictTypeList()
    {
        return dict('category_typeList');
    }

    private function _treeCategoryList($entityList, $parentId = null): array
    {
        $result = [];
        $nowChild = array_filter($entityList, function ($entity) use ($parentId) {
            return $entity['parentId'] == $parentId ? $entity : [];
        });
        if (Arrays::Is($nowChild)) {
            $action = request()->action();
            foreach ($nowChild as $item) {
                $children = $this->_treeCategoryList($entityList, $item['id']);
                switch ($action) {
                    case 'ApiBySelect':
                        $tmp = ['name' => $item['name'], 'value' => $item['id']];
                        !Arrays::Is($children) ?: $tmp['children'] = $children;
                        $result[] = $tmp;
                        break;
                    case 'ApiByListsOfContent':
                        if (is_array($item) && array_key_exists('template', $item)) {
                            $item['url'] = self::UrlPathOfCategory($item, $item['template'], $this->siteEntity['Site_Path']);
                            if (Arrays::Is($children)) {
                                $item['children'] = $children;
                                $item['last'] = false;
                            } else {
                                $item['last'] = true;
                            }
                            $item['disabled'] = $item['categoryType'] == 'link';
                        }
                        $result[] = $item;
                        break;
                }
            }
        }
        return $result;
    }

    public function ApiByLists()
    {
        $entityList = $this->_serviceOfCategory->GetList(
            [],
            ['id', 'siteId', 'childrenIdList', 'parentId', 'categoryType', 'name', 'url',
                'template', 'countContent', 'listSort']
        );
        if (Arrays::Is($entityList)) {
            $siteEntity = $this->serviceOfSite()->interface_getEntityById($this->loginUser()->SiteId, ['Site_Path']);
            $typeList = $this->_getDictTypeList();
            foreach ($entityList as $key => $item) {
                if (is_array($item)) {
                    if (array_key_exists('template', $item)) {
                        empty($item['template']) ?: $item['template'] = Arrays::Unserialize($item['template']);
                        $item['url'] = self::UrlPathOfCategory($item, $item['template'], $siteEntity['Site_Path']);
                    }
                    if (array_key_exists('categoryType', $item)) {
                        // 预览URL
                        if ($item['categoryType'] == 'list') {
                            $item['url_preView'] = "/index.php/content/category?siteId=" . $item['siteId'] . "&catId=" . $item['id'];
                        } elseif ($item['categoryType'] == 'page') {
                            $item['url_preView'] = "/index.php/content/category?siteId=" . $item['siteId'] . "&catId=" . $item['id'];
                        } else {
                            $item['url_preView'] = $item['url'];
                        }
                        $item['open'] = true;
                        $item['categoryTypeName'] = $typeList[$item['categoryType']];
                    }
                }

                $entityList[$key] = $item;
            }
        }
        $this->json($entityList);
    }

    public function ApiByListsOfContent()
    {
        $entityList = $this->_serviceOfCategory->GetList(
            [],
            ['id', 'parentId', 'categoryType', 'name', 'url', 'template', 'countContent']
        );
        !Arrays::Is($entityList) ?: $entityList = $this->_treeCategoryList($entityList, 0);
        $this->json($entityList);
    }

    public function ApiBySelect()
    {
        $params = [
            'siteId'=>request()->has('siteId','get') ? $this->getId('siteId','Site_idEmpty') : $this->loginUser()->SiteId
        ];
        $entityList = $this->_serviceOfCategory->GetList($params, ['id', 'parentId', 'name']);
        if (Arrays::Is($entityList)) {
            $entityList = $this->_treeCategoryList($entityList, 0);
        }
        $this->json($entityList);
    }

    public function ApiGetDetail()
    {
        if (request()->has('parent', 'get')) {
            $entity = $this->_serviceOfCategory->interface_getEntityById(
                request()->get('parent')
            );
            $result = [
                'id' => 0,
                'parentId' => $entity['id'],
                'categoryType' => $entity['categoryType'],
                'modelId' => $entity['modelId'],
                'template' => $entity['template'],
                'power' => $entity['power'],
            ];
        } else {
            $result = $this->_serviceOfCategory->interface_getEntityById(
                request()->get('id')
            );
        }
        $this->json($result);
    }
}
