<?php
/**
 * 栏目模型
 */

namespace PKApp\Content\Model;


use PKFrame\DataHandler\Arrays;

class CategoryModel
{

    public $id;
    public $siteId;
    public $modelId;
    public $parentId;
    public $parentIdList;
    public $childrenIdList;
    public $categoryType;
    public $name;
    public $info;
    public $image;
    public $seo;
    public $url;
    public $template;

    public function __construct($entity)
    {
        if (Arrays::Is($entity)) {
            foreach ($entity as $index => $item) {
                if (property_exists($this, $index)) {
                    $this->{$index} = $item;
                }
            }
        }
    }

}