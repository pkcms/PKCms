<?php
/**
 * 前台 内容 控制器
 */

namespace PKApp\Content;

use PKApp\Content\Classes\ContentDetailController;
use PKFrame\DataHandler\Arrays;

class Content extends ContentDetailController
{
    private $_categoryEntity;


    public function Main()
    {
        $this->getParentIdList();
        $this->getModelFieldByPower();
        $this->_getContent();
        $this->fetch($this->tpl_content);
    }

    private function _getContent()
    {
        $entity_content = $this->handlerContentData(
            $this->serviceContent()->interface_getEntityById(request()->get('contentId'))
        );
        $this->assign($entity_content);
        $this->getUpOrNext($entity_content);
    }

    public function ApiByHits()
    {
        // 检查 IP 是否正常访问
        $is_expire = $this->GuestIP_isExpire('hits', 60);
        $contentId = request()->get('contentId');
        $entity = $this->serviceContent()->interface_getEntityById($contentId);
        if ($is_expire) {
            $entity['hits'] += 1;
            $this->serviceContent()->UpdateById($contentId, ['hits' => $entity['hits']]);
        }
        $this->json(['hits' => $entity['hits']]);
    }

    public function ApiGetPowerByField()
    {
        $this->getModelFieldByPower();
        $entity = $this->serviceContent()->interface_getEntityById(
            request()->get('contentId')
        );
        $result = [];
        if (Arrays::Is($this->list_power_field) && Arrays::Is($entity)) {
            foreach ($this->list_power_field as $field => $power) {
                if (array_key_exists($field, $entity)) {
                    $result[$field] = $entity[$field];
                }
            }
        }
        $this->json($result);
    }


}