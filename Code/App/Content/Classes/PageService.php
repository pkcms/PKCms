<?php


namespace PKApp\Content\Classes;


use PKFrame\Service;

class PageService extends Service
{

    public function interface_getEntityById($id, $viewField = '*')
    {
        return $this->getEntityById($id, 'Category_idEmpty', 'Page_Empty', $viewField);
    }

    protected function db()
    {
        return new PageDataBase();
    }

    public function GetList($viewParams = [], $viewField = '*')
    {
        return $this->db()->Where($viewParams)->Select($viewField)->ToList();
    }

    public function GetEntity($viewParams = [], $viewField = '*')
    {
        return $this->db()->Where($viewParams)->Select($viewField)->First();
    }
}