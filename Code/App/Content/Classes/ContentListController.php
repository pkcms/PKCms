<?php


namespace PKApp\Content\Classes;

use PKApp\Model\Classes\ToOutData;
use PKApp\Site\Classes\SiteController;
use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\Numbers;

abstract class ContentListController extends SiteController
{

    use TraitContent;

    protected $list_lineNum, $modelId;
    protected $list_category, $count_content;

    protected $is_list_category;
    protected $is_filterContentMirror;

    protected function getListFieldByModel($key = 'isSearch'): array
    {
        static $data;
        if (empty($data)) {
            $data = cache()->Disk()->ReadByData('Model_' . $this->modelId);
        }
        return array_key_exists($key, $data) ? $data[$key] : [];
    }

    protected function getListCategory(array $viewParams = [], $viewField = '*')
    {
        return $this->serviceOfCategory(
            $this->getIdOfSite()
        )->GetList($viewParams, $viewField);
    }

    protected function getListCategoryByParentId(array $params = []): array
    {
        $catId = Arrays::GetKey('catId', $params, 'Category_idEmpty', 'content');
        $catId_arr = ($this->is_list_category = stristr($catId, ',')) ? explode(',', $catId) : [$catId];
        $field_category = ['id', 'siteId', 'modelId', 'categoryType', 'name', 'childrenIdList', 'template'];
        $siteId = array_key_exists('siteId',$params) ? Numbers::To($params['siteId']) : $this->getIdOfSite();
        $this->list_category = $this->serviceOfCategory($siteId)->GetTypeListById($catId_arr, $field_category);
        // 检查子级
        $catId_arr_children = [];
        foreach ($this->list_category as $item) {
            if (empty($this->modelId)) {
                $this->modelId = $item['modelId'];
            } elseif ($this->modelId != $item['modelId']) {
                $this->noticeByPage('Category_model_notEqual');
            }
            if (!empty($item['childrenIdList'])) {
                $catId_arr_children = array_merge($catId_arr_children, explode(',', $item['childrenIdList']));
            }
        }
        if (count($catId_arr_children) > 0) {
            // 取得差集，也是没有查询到的栏目
            $cat_arr_view = array_diff($catId_arr_children, $catId_arr);
            $catId_arr = array_unique(array_merge($catId_arr, $catId_arr_children));
            if (count($cat_arr_view) > 0) {
                $list_children = $this->serviceOfCategory($siteId)->GetListById($cat_arr_view, $field_category);
                !is_array($list_children) ?: $this->list_category = array_merge($this->list_category, $list_children);
            }
        }
        $this->list_category = Arrays::Column($this->list_category, 'id', null, 1);
        $this->list_lineNum = Numbers::To(Arrays::GetKey('num', $params));
        return [
            'catId' => $catId_arr,
        ];
    }

    protected function getContentList($viewParams, $orderBy): ?array
    {
        $service_content = new ContentService($this->modelId);
        if ((bool)$this->is_filterContentMirror) {
            $viewParams['mirroringId'] = 0;
        }
        $this->count_content = $service_content->GetCount($viewParams);
        if ($this->count_content == 0) {
            return [];
        }
        $out_cls = new ToOutData();
        $list_content = $service_content->GetList($viewParams, '*',
            max(Numbers::To(self::$pageIndex), 1), $this->list_lineNum, $orderBy);
        foreach ($list_content as $index => $item) {
            $item = $out_cls->Get($item, $item['modelId']);
            if (Numbers::IsId($item['mirroringId'])) {
                $list_content[$index] = $item;
            } elseif (array_key_exists($item['catId'], $this->list_category)) {
                $this->list_category[$item['catId']]['template'] = Arrays::Unserialize($this->list_category[$item['catId']]['template']);
                $item['catName'] = $this->list_category[$item['catId']]['name'];
                $item['url'] = self::UrlPathOfContent(
                    $this->list_category[$item['catId']], $this->list_category[$item['catId']]['template'], $item, $this->getSitePath()
                );
                $list_content[$index] = $item;
            } else {
                $this->noticeByPage('Search_Category_IdExists');
            }
        }
        return $list_content;
    }

}