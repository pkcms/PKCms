<?php


namespace PKApp\Content\Classes;

use PKFrame\DataHandler\Arrays;
use PKFrame\Lib\DataBase;

class CategoryDataBase extends DataBase
{

    protected $table = 'category';

    public function GetListByIds($siteId, $viewId = null, $noInId = null)
    {
        $option = array('siteId' => $siteId, 'isDeleted' => 0);
        if (!is_null($viewId) && is_array($viewId)) {
            $option['id'] = $viewId;
        } elseif (!is_null($viewId) && is_numeric($viewId)) {
            $option[''] = $viewId;
        }
        if (!is_null($noInId) && is_array($noInId)) {
            $option[] = 'id NOT IN(' . implode(',', $noInId) . ')';
        }
        return $this->_getList($option, array(
            'id', 'siteId', 'childrenIdList', 'parentId', 'categoryType', 'name', 'url',
            'template', 'total', 'listSort'
        ));
    }

    public function GetListById($siteId, $idList = [])
    {
        $option = array('siteId' => $siteId, 'id' => $idList, 'isDeleted' => 0);
        return $this->Where($option)
            ->OrderBy('id', 'ASC')
            ->Select(array('id', 'parentId', 'modelId', 'categoryType', 'name', 'url', 'template'))->ToList();
    }

    private function _getList(array $option, $field = '*', $lineNum = null, $index = 0)
    {
        array_key_exists('isDeleted', $option) ?: $option['isDeleted'] = 0;
        if ($lineNum > 0) {
            $this->Limit($lineNum, $index);
        }
        return $this->Where($option)
            ->OrderBy('listSort', 'ASC')
            ->OrderBy('id', 'ASC')
            ->Select($field)->ToList();
    }

    public function GetCategoryByMakeHtml($siteId, $lineIndex = 0)
    {
        return $this->_getByLimit(array('siteId' => $siteId), $lineIndex);
    }

    public function GetCategoryByNext($eachIndex = 0)
    {
        return $this->_getByLimit(array('isDeleted' => 0), $eachIndex, array('id', 'name', 'seo'));
    }

    public function GetCategoryDetail($id, $field = '*')
    {
        $option = array('id' => $id, 'isDeleted' => 0);
        $entity = $this->Where($option)->Select($field)->First();
        is_array($entity) ?: out()->noticeByJson(language('Category_Empty'));
        if (is_array($entity)) {
            if (array_key_exists('seo', $entity)) {
                $entity['seo'] = Arrays::Unserialize($entity['seo']);
            }
            if (array_key_exists('template', $entity)) {
                $entity['template'] = Arrays::Unserialize($entity['template']);
            }
            if (array_key_exists('setting', $entity)) {
                $entity['setting'] = Arrays::Unserialize($entity['setting']);
            }
        }
        return $entity;
    }

    private function _getByLimit(array $option, $lineIndex, $field = '*')
    {
        $option['isDeleted'] = 0;
        return $this->Where($option)
            ->OrderBy('id', 'ASC')
            ->Limit(1, $lineIndex)
            ->Select($field)->First();
    }

    public function SetCategoryDetailById($param, $id)
    {
        $this->Where(array('id' => $id))->Update($param)->Exec();
    }
}
