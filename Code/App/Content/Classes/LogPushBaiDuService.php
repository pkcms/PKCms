<?php


namespace PKApp\Content\Classes;


use PKFrame\Service;

class LogPushBaiDuService extends Service
{

    public function interface_getEntityById($id, $viewField = '*')
    {
    }

    protected function db(): LogPushBaiDuDataBase
    {
        static $cls;
        !empty($cls) ?: $cls = new LogPushBaiDuDataBase();
        return $cls;
    }

    public function GetList($viewParams = [], $viewField = '*')
    {
        return $this->db()->Where($viewParams)->Select($viewField)->ToList();
    }

    public function GetEntity($viewParams = [], $viewField = '*')
    {
    }
}