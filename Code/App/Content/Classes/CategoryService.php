<?php


namespace PKApp\Content\Classes;


use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\Numbers;
use PKFrame\Service;

class CategoryService extends Service
{

    protected $moduleName = 'content';
    protected $siteId;
    protected $listCatId_noMakeHtml = [], $list_url = [];

    public function __construct($site_id)
    {
        parent::__construct();
        if (!(request()->controller() == 'AdminImportSQLite'
            && request()->action() == 'ApiImportModuleByCategory')) {
            Numbers::IsId($site_id) ?: $this->noticeByJson('Site_idEmpty');
        }
        $this->siteId = $site_id;
    }

    public function interface_getEntityById($id, $viewField = '*')
    {
        return $this->getEntityById($id, 'Category_idEmpty', 'Category_Empty', $viewField);
    }

    protected function db(): CategoryDataBase
    {
        return new CategoryDataBase();
    }

    public function GetList($viewParams = [], $viewField = '*')
    {
        array_key_exists('siteId', $viewParams) ?: $viewParams['siteId'] = $this->siteId;
        array_key_exists('isDeleted', $viewParams) ?: $viewParams['isDeleted'] = 0;
        return $this->handerList(
            $this->db()
                ->Where($viewParams)
                ->OrderBy('listSort', 'ASC')
                ->OrderBy('id', 'ASC')
                ->Select($viewField)
                ->ToList()
        );
    }

    public function GetListByMakeHtml($index = 0)
    {
        $list_category = $this->handerList(
            $this->db()
                ->Where([
                    'siteId' => $this->siteId,
                    'isDeleted' => 0,
                ])
                ->OrderBy('id', 'ASC')
                ->Limit(1, $index)
                ->Select('*')
                ->ToList()
        );
        count($list_category) == 1 ?: $this->noticeByJson('Category_Empty');
        return $list_category[0];
    }

    protected function handerList($lists)
    {
        foreach ($lists as $index => $list) {
            if (array_key_exists('template', $list)) {
                $list['template'] = Arrays::Unserialize($list['template']);
            }
            if (array_key_exists('seo', $list)) {
                $list['seo'] = Arrays::Unserialize($list['seo']);
            }
            $lists[$index] = $list;
        }
        return $lists;
    }

    public function GetEntity($viewParams = [], $viewField = '*')
    {
        array_key_exists('siteId', $viewParams) ?: $viewParams['siteId'] = $this->siteId;
        array_key_exists('isDeleted', $viewParams) ?: $viewParams['isDeleted'] = 0;
        $entity = $this->db()->Where($viewParams)->Select($viewField)->First();
        if (is_array($entity)) {
            if (array_key_exists('seo', $entity)) {
                $entity['seo'] = Arrays::Unserialize($entity['seo']);
            }
            if (array_key_exists('template', $entity)) {
                $entity['template'] = Arrays::Unserialize($entity['template']);
            }
            if (array_key_exists('power', $entity)) {
                $entity['power'] = Arrays::Unserialize($entity['power']);
            }
        }
        return $entity;
    }

    public function GetListByModel($id)
    {
        return $this->GetList(['modelId' => $id], ['id', 'name']);
    }

    public function GetListByParent($id_lists = [], $viewField = '*')
    {
        return $this->GetList(['parentId' => $id_lists], $viewField);
    }

    public function GetListById($id_lists = [], $viewField = '*')
    {
        return $this->GetList(['id' => $id_lists], $viewField);
    }

    // 取得类型为列表的栏目的陈列信息
    public function GetTypeListById($id_lists = [], $viewField = '*')
    {
        return $this->GetList(['id' => $id_lists, 'categoryType' => 'list'], $viewField);
    }

}