<?php


namespace PKApp\Content\Classes;

use PKFrame\Lib\DataBase;

class PageDataBase extends DataBase
{

    protected $table = 'content_page';

}