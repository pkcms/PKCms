<?php


namespace PKApp\Content\Classes;


use PKFrame\Lib\DataBase;

class LogPushBaiDuDataBase extends DataBase
{

    public function __construct($linkIndex = 0)
    {
        parent::__construct($linkIndex);
        $this->table = 'log_baidu_push';
    }

}