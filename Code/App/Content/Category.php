<?php


namespace PKApp\Content;

use PKApp\Content\Classes\ContentDetailController;
use PKFrame\DataHandler\Numbers;

class Category extends ContentDetailController
{

    public function Main()
    {
        $this->getParentIdList();
        $tplFile = '';
        switch ($this->getCategoryEntityByNow()->categoryType) {
            case 'page':
                $this->getDetailByPage();
                $tplFile = $this->getCategoryEntityByNow()->template['pageTpl'];
                break;
            case 'list':
                $this->assign(['nowPage' => max(Numbers::To(request()->get('page')), 1)]);
                $tplFile = $this->getCategoryEntityByNow()->template['categoryTpl'];
                break;
        }
        if (!empty($tplFile)) {
            $this->fetch($this->getTplFileOfCustomize($tplFile));
        } else {
            $this->noticeByPage('Category_Empty');
        }
    }

}