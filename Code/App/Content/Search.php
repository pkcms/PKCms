<?php


namespace PKApp\Content;

use PKApp\Content\Classes\ContentListController;
use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\Numbers;
use PKFrame\DataHandler\Pages;

class Search extends ContentListController
{

    private $params_db;
    private $_isSearch_field = false;
    private $_result;

    public function Main()
    {
        $dataType = trim(request()->get('data'));
        $this->_result = ['query' => '', 'searchData' => [], 'pageNavList' => []];
        $this->_checkParams_Query();
        // 按模型搜索
        $this->_searchByModelId();
        if (strtolower($dataType) == 'json') {
            $this->json($this->_result, $this->count_content);
        } else {
            $this->assign($this->_result);
            $this->fetch('search');
        }
    }

    // 根据模型去搜索
    private function _searchByModelId()
    {
        $this->modelId = $this->getId('modelId', 'Model_IdEmpty');
        $this->_checkParams_SearchField();
        array_key_exists('query_like', $this->params_db) || $this->_isSearch_field
            ?: $this->noticeByPage('Search_ParamEmpty');
        $list_category = $this->getListCategory(['modelId' => $this->modelId], ['id', 'modelId', 'template']);
        if (Arrays::Is($list_category)) {
            $this->params_db = array_merge($this->params_db, [
                'catId' => Arrays::Column($list_category, 'id'),
                'modelId' => $this->modelId,
            ]);
            $this->list_category = [];
            foreach ($list_category as $val) {
                $this->list_category[$val['id']] = $val;
            }
            $order = Arrays::GetKey(0, dict('category_content_orderList'), 'Content_attribute_Error');
            $this->_result['searchData'] = $this->getContentList($this->params_db, $order['value']);
            $this->_result['pageNavList'] = Pages::PageNavs(
                $this->count_content, self::$pageSize, self::$pageIndex,
                self::UrlPathOfSearch()
            );
        }
    }

    private function _checkParams_Query()
    {
        $this->params_db = ['siteId' => $this->getSiteEntity()->Id,];
        $this->list_lineNum = Numbers::To($this->getSiteEntity()->Setting['searchItemSize']);
        if (request()->has('query', 'get')) {
            $query = urldecode(request()->get('query'));
            !empty($query) ?: out()->notice('SearchQuery_Empty', null, 'content');
            $this->params_db = array_merge($this->params_db, ['query_like' => $query]);
            $this->_result['query'] = $query;
        }
        // 翻页操作
        // 返回行数
        $this->GetPages();
        self::$pageSize = Numbers::To(Arrays::GetKey('searchItemSize', $this->getSiteEntity()->Setting));
        self::$pageSize > 0 ?: $this->noticeByPage('Search_LineSize_Empty');
    }

    private function _checkParams_SearchField()
    {
        // 字段搜索
        $list_field = $this->getListFieldByModel('fieldList');
        $list_field_search = $this->getListFieldByModel();
        if (Arrays::Is($list_field) || !Arrays::Is($list_field_search)) {
            $params = [];
            foreach ($list_field_search as $fieldName) {
                if (!request()->has($fieldName, 'get')) {
                    continue;
                }
                $value_get = request()->get($fieldName);
                if (empty($value_get)) {
                    continue;
                }
                // 该字段名的相关属性
                $entity_field = Arrays::GetKey($fieldName, $list_field, 'field name:' . $fieldName . ' not in model');
                if (Arrays::GetKey('formType', $entity_field) == 'option') {
                    $setting = Arrays::GetKey('setting', $entity_field);
                    $boxType = Arrays::GetKey('boxType', $setting);
                    if ($boxType == 'checkbox') {
                        $params[] = [$fieldName, 'find_in_set', $value_get];
                    } else {
                        $params[$fieldName] = $value_get;
                    }
                }
            }
            $this->_isSearch_field = count($params) > 0;
            $this->params_db = array_merge($this->params_db, $params);
        }
    }

}
