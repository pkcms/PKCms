<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>内容管理</title>
    <link rel="stylesheet" href="/statics/layui/css/layui.css">
    <link rel="stylesheet" href="/statics/pkcms/icons/iconfont.css" media="all">
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <fieldset class="layui-elem-field layui-field-title">
                <legend>模型内容列表</legend>
            </fieldset>
        </div>

        <div class="layui-card-body">
            <form class="layui-form" lay-filter="form-search">
                <div class="layui-form-item">
                    <label class="layui-form-label">第1步</label>
                    <div class="layui-input-inline">
                        <div class="pkcms_select" id="modelId" data-name="modelId"
                             data-api="Model/AdminGetModel" data-params="modelType=content"></div>
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                        选择搜索的内容模型
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">第2步</label>
                    <div class="layui-input-inline">
                        <input type="text" name="query" class="layui-input" placeholder="请输入标题的搜索词"/>
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">操作选项</label>
                    <div class="layui-input-inline" style="width: auto;">
                        <button class="layui-btn layui-btn-sm layui-btn-normal searchContent"
                                onclick="searchContent()"
                                lay-submit lay-filter="form-search" lay-tips="按模型搜索内容，关键词作模糊搜索">
                            <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>检索
                        </button>
                        <button id="btn_exportSearchContent" class="layui-btn layui-btn-sm layui-btn-normal layui-disabled exportSearchContent" disabled="disabled" onclick="exportSearchContent()"
                                lay-submit lay-filter="form-search" lay-tips="导出当前的搜索结果">
                            <i class="layui-icon layui-icon-export layuiadmin-button-btn"></i>导出
                        </button>
                        <button class="layui-btn layui-btn-sm layui-btn-danger"
                                onclick="recoveryContext();"
                                lay-submit lay-filter="form-search" lay-tips="按模型的回收站进行误删回收操作">
                            <i class="layui-icon layui-icon-delete"></i>回收
                        </button>
                    </div>
                    <div class="layui-form-mid layui-word-aux">
                    </div>
                </div>
            </form>
            <div id="view-table"></div>
            <div id="view_check_context"></div>
        </div>
    </div>
</div>

<script type="text/html" id="tpl-table">
    <table class="layui-hide PK-table" id="table_adminList" lay-filter="table_adminList"
           data-get="Content/AdminGetContent" data-set="Content/AdminSetContent"></table>
</script>

<script type="text/html" id="table_tool">
    {{# if (d.isDeleted){ }}
    <button class="layui-btn layui-btn-normal layui-btn-xs" lay-tips="修改内容"
            lay-event="open|修改并回收内容|Content/AdminSetContent?modelId={{d.modelId}}&catId={{d.catId}}&id={{d.id}}&mode=recycle">
        <i class="layui-icon layui-icon-edit"></i></button>
    {{# }else{ }}
    <button class="layui-btn layui-btn-normal layui-btn-xs" lay-tips="修改内容"
            lay-event="open|修改内容|Content/AdminSetContent?modelId={{d.modelId}}&catId={{d.catId}}&id={{d.id}}">
        <i class="layui-icon layui-icon-edit"></i></button>
    <button class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del" lay-tips="删除该内容">
        <i class="layui-icon layui-icon-delete"></i></button>
    <a class="layui-btn layui-btn-primary layui-btn-xs" lay-tips="预览内容"
       href="/index.php/content/content?siteId={{ d.siteId }}&catId={{ d.catId }}&contentId={{ d.id }}" target="_blank">
        <i class="iconfont pkcms-icon-view"></i></a>
    {{# } }}
</script>

<script type="text/html" id="isAttributeTpl">
    {{#  if(d != 'undefined'){ }}
    {{#  if(d.thumb){ }}
    <div id="tong_{{  d.id  }}" style="width: 670px; height: 320px;display: none;">
        <img src="{{  d.thumb  }}" style="max-width: 100%;">
    </div>
    <button type="button" class="layui-btn layui-btn-xs" lay-tips="有缩略图"
            onclick="PKLayer.ImageView('tong_{{  d.id  }}');">
        <i class="layui-icon layui-icon-picture"></i>
    </button>
    {{# }else{ }}
    <button class="layui-btn layui-btn-xs layui-btn-disabled" lay-tips="无缩略图">
        <i class="layui-icon layui-icon-picture-fine"></i>
    </button>
    {{#  } }}
    {{#  if(d.toTop == 1){ }}
    <button class="layui-btn layui-btn-xs" lay-tips="已置顶">
        <i class="iconfont pkcms-icon-toTop-white"></i>
    </button>
    {{# }else{ }}
    <button class="layui-btn layui-btn-xs layui-btn-disabled" lay-tips="未置顶">
        <i class="iconfont pkcms-icon-toTop-white"></i>
    </button>
    {{#  } }}
    {{#  if(d.toPush == 1){ }}
    <button class="layui-btn layui-btn-xs" lay-tips="已推荐">
        <i class="layui-icon layui-icon-heart-fill"></i>
    </button>
    {{# }else{ }}
    <button class="layui-btn layui-btn-xs layui-btn-disabled" lay-tips="未推荐">
        <i class="layui-icon layui-icon-heart"></i>
    </button>
    {{#  } }}
    {{#  if(d.isDeleted == 1){ }}
    <button class="layui-btn layui-btn-xs layui-btn-danger" lay-tips="已经被删除">
        <i class="layui-icon layui-icon-delete"></i>
    </button>
    {{#  } }}
    {{#  } }}
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>
<script type="text/javaScript">

    var data = {
        contextCount: 0,
        checkIndex: 0,
        init: {
            query: '',
            modelId: 0,
            recovery: 0
        }
    }, element_check = null;

    function recoveryContext() {
        PKLayer.useFormControlByParams(function (params) {
            if (params.modelId === '') {
                PKLayer.tipsMsg('请选择模型！');
                return false;
            }
            data.init = Object.assign(data.init, params, {recovery: 1});
            PKLayer.useTplCall(Object.create(null), 'tpl-table', 'view-table', function (tpl) {
                $('#view-table').show();
                $('#view_check_context').hide();
                PKAdmin.table({
                    cols: [[
                        {field:'id', title:'ID', width: 60},
                        {field:'title', title:'标题'},
                        {field:'thumb', title:'内容属性', width: 150, templet: '#isAttributeTpl'},
                        {
                            title: "操作",
                            align: "center",
                            width: 60,
                            fixed: "right",
                            toolbar: "#table_tool"
                        }
                    ]]
                }, true);
            });
            return false;
        });
    }

    function renderTable() {
        $('.searchContent').click();
    }

    function exportSearchContent() {
        PKLayer.useFormControlByParams(function (params) {
            if (params.modelId === '') {
                PKLayer.tipsMsg('请选择模型！');
            } else {
                var params_export = Object.assign(data.init, params, {recovery: 0});
                console.log(params_export);
                var url = '/index.php/Content/AdminGetContent/ApiByExport?'+PKCms.JSONToUrlQuery(params_export);
                window.open(url,'_blank');
            }
            return false;
        });
    }

    function searchContent() {
        PKLayer.useFormControlByParams(function (params) {
            if (params.modelId === '') {
                PKLayer.tipsMsg('请选择模型！');
            } else {
                data.init = Object.assign(data.init, params, {recovery: 0});
                PKLayer.useTplCall(Object.create(null), 'tpl-table', 'view-table', function (tpl) {
                    $('#view-table').show();
                    $('#view_check_context').hide();
                    $('#btn_exportSearchContent').removeClass('layui-disabled').removeAttr('disabled');
                    PKAdmin.table({
                        cols: [[
                            {field:'id', title:'ID', width: 60},
                            {field:'listSort', title:'排序', width: 60, edit: 'number'},
                            {field:'title', title:'标题'},
                            {field:'thumb', title:'内容属性', width: 150, templet: '#isAttributeTpl'},
                            {
                                title: "操作",
                                align: "center",
                                width: 200,
                                fixed: "right",
                                toolbar: "#table_tool"
                            }
                        ]]
                    }, true);
                });
            }
            return false;
        });
    }

    PKAdmin.ready(function () {
        PKLayer.useFormByControlAndSubmit('form-search', function (paramsByForm) {
        })
    });
</script>
</body>
</html>