<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
    <link href="/statics/ueditor/themes/default/css/ueditor.min.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" charset="utf-8" src="/statics/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="/statics/ueditor/ueditor.all.min.js"></script>
</head>

<body>

<div class="layui-fluid">

    <div class="layui-fluid">

        <div class="layui-card">
            <div class="layui-card-header layuiadmin-card-header-auto">
                <fieldset class="layui-elem-field layui-field-title">
                    <legend>单页面内容编辑</legend>
                </fieldset>
            </div>

            <div class="layui-card-body layui-card-body-mb">
                <form class="layui-form" lay-filter="form-page">
                    <div id="view-form"></div>

                    <div class="layui-form-footer-fixed">
                        <div class="layui-input-block">
                            <div class="layui-footer">
                                <button class="layui-btn" lay-submit lay-filter="form-page">立即提交</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script type="text/html" data-id="<{$id}>" data-api="Content/AdminPage" id="tpl-form">
    <input type="hidden" name="id" value="{{#  if(d.id){ }}{{  d.id  }}{{#  } }}"/>
    <div class="layui-form-item">
        <label class="layui-form-label">标题：</label>
        <div class="layui-input-block">
            <input type="text" name="title" class="layui-input" lay-verify="required" autocomplete="off"
                   value="{{#  if(d.title){ }}{{  d.title  }}{{#  } }}"/>
        </div>
    </div>
    {{#
    var power = PKCms.localStorage_get('powerByAction');
    }}
    {{#  if (power.indexOf('changeCategorySEOPower') >= 0){ }}
    <div class="layui-form-item">
        <label class="layui-form-label">SEO 标题：</label>
        <div class="layui-input-block">
            <input type="text" name="seoTitle" class="layui-input"
                   value="{{#  if(d.seoTitle){ }}{{  d.seoTitle  }}{{#  } }}"/>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">SEO 关键词：</label>
        <div class="layui-input-block">
            <textarea name="seoKeywords"
                      class="layui-textarea">{{#  if(d.seoKeywords){ }}{{  d.seoKeywords  }}{{#  } }}</textarea>
            <div class="layui-form-mid layui-word-aux">
                多词之间使用英文逗号（,）分隔
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">SEO 简述：</label>
        <div class="layui-input-block">
            <textarea name="seoDescription"
                      class="layui-textarea">{{#  if(d.seoDescription){ }}{{  d.seoDescription  }}{{#  } }}</textarea>
        </div>
    </div>
    {{#  } }}
    <div class="layui-form-item">
        <label class="layui-form-label">内容：</label>
        <div class="layui-input-block">
            <textarea class="pkcms_editor" id="field_content"
                      name="content">{{#  if(d.content){ }}{{  d.content  }}{{#  } }}</textarea>
        </div>
    </div>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">
    PKAdmin.ready(function () {
        PKAdmin.tplFormByTpl({}, function (paramsByForm) {
            PKAdmin.submitFormByContent(paramsByForm);
        });
    });
</script>
</body>
</html>

