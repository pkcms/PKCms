<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
</head>

<body>

<div class="layui-fluid">

    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <fieldset class="layui-elem-field layui-field-title">
                <legend>栏目编辑-列表形式</legend>
            </fieldset>
        </div>

        <div class="layui-card-body layui-card-body-mb">
            <form class="layui-form" lay-filter="form-frame">
                <div id="view-form"></div>

                <div class="layui-form-footer-fixed">
                    <div class="layui-input-block">
                        <div class="layui-footer">
                            <button class="layui-btn" lay-submit lay-filter="form-frame">立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/html" data-id="<{$id}>" data-parent="<{$parentId}>" data-api="Content/AdminGetCategory" id="tpl-form">
    {{#
    var power = PKCms.localStorage_get('powerByAction');
    }}
    {{#  if (power.indexOf('changeCategoryBaseSetPower') >= 0){ }}
    <input type="hidden" name="id" value="{{#  if(d.id){ }}{{  d.id  }}{{#  } }}"/>
    <input type="hidden" name="categoryType" value="{{#  if(d.categoryType){ }}{{  d.categoryType  }}{{#  }else{ }}<{$categoryType}>{{#  } }}"/>

    <div class="layui-card">
        <div class="layui-card-header layui-bg-gray">基本信息</div>
        <div class="layui-card-body">
            <div class="layui-form-item">
                <label class="layui-form-label">栏目名：</label>
                <div class="layui-input-block">
                    <input type="text" name="name" class="layui-input" lay-verify="required" autocomplete="off"
                           value="{{#  if(d.name){ }}{{  d.name  }}{{#  } }}"/>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">父级选择：</label>
                <div class="layui-input-block">
                    <input type="hidden" name="fromParentId"
                           value="{{#  if(d.parentId){ }}{{  d.parentId  }}{{#  } }}"/>
                    <div class="pkcms_selectTree" id="parentId" data-id="parentId"
                         data-name="parentId" data-value="{{#  if(d.parentId){ }}{{  d.parentId  }}{{#  } }}"
                         data-api="Content/AdminGetCategory"></div>

                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">选择模型：</label>
                <div class="layui-input-block">
                    <div class="pkcms_select" id="modelId" data-name="modelId"
                         data-value="{{#  if(d.modelId){ }}{{  d.modelId  }}{{#  } }}"
                         data-api="Model/AdminGetModel" data-params="modelType=content"></div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">栏目描述：</label>
                <div class="layui-input-block">
                    <textarea class="layui-textarea" name="info">{{#  if(d.info){ }}{{  d.info  }}{{#  } }}</textarea>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">栏目图片：</label>
                <div class="layui-input-block">
                    <input type="text" class="layui-input" style="width: 60%;float: left;"
                           id="image" name="image" value="{{#  if(d.image){ }}{{  d.image  }}{{#  } }}"/>
                    <button type="button" class="layui-btn pkcms_upload" id="uploadBtn_image" data-field="image">
                        <i class="layui-icon layui-icon-upload"></i>选择
                    </button>
                    <button type="button" class="layui-btn" onclick="PreViewImage('image')">预览
                    </button>
                </div>
            </div>
        </div>
    </div>
    {{#  } }}

    {{#  if (power.indexOf('changeCategorySEOPower') >= 0){ }}
    <div class="layui-card">
        <div class="layui-card-header layui-bg-gray">SEO设置</div>
        <div class="layui-card-body">
            <div class="layui-form-item">
                <label class="layui-form-label">SEO 标题：</label>
                <div class="layui-input-block">
                    <input type="text" name="seo[title]" class="layui-input"
                           value="{{#  if((d.seo != null) && d.seo.title){ }}{{  d.seo.title  }}{{#  } }}"/>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">SEO 关键词：</label>
                <div class="layui-input-block">
                    <textarea name="seo[keyword]"
                              class="layui-textarea">{{#  if((d.seo != null) && d.seo.keyword){ }}{{  d.seo.keyword  }}{{#  } }}</textarea>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">SEO 描述：</label>
                <div class="layui-input-block">
                    <textarea name="seo[description]"
                              class="layui-textarea">{{#  if((d.seo != null) && d.seo.description){ }}{{  d.seo.description  }}{{#  } }}</textarea>
                </div>
            </div>
        </div>
    </div>
    {{#  } }}

    {{#  if (power.indexOf('changeCategoryTplPower') >= 0){ }}
    <div class="layui-card">
        <div class="layui-card-header layui-bg-gray">模板设置</div>
        <div class="layui-card-body">
            <div class="layui-form-item">
                <label class="layui-form-label">栏目页模板：</label>
                <div class="layui-input-block">
                    <div class="pkcms_select" id="categoryTpl"
                         data-name="template[categoryTpl]"
                         data-value="{{#  if((d.template != null) && d.template.categoryTpl){ }}{{  d.template.categoryTpl  }}{{#  } }}"
                         data-api="Template/AdminGetTemplate"
                         data-params="folder=content&query=category"></div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">内容页模板：</label>
                <div class="layui-input-block">
                    <div class="pkcms_select" id="contentTpl"
                         data-name="template[contentTpl]"
                         data-value="{{#  if((d.template != null) && d.template.contentTpl){ }}{{  d.template.contentTpl  }}{{#  } }}"
                         data-api="Template/AdminGetTemplate"
                         data-params="folder=content&query=show"></div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">列表页显示条数：</label>
                <div class="layui-input-block">
                    <input type="number" name="template[listDataSize]" class="layui-input"
                           lay-verify="required" autocomplete="off"
                           value="{{#  if((d.template != null) && d.template.listDataSize){ }}{{  d.template.listDataSize  }}{{#  }else{ }}20{{#  } }}"/>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">列表页的排序方式：</label>
                <div class="layui-input-block">
                    <div class="pkcms_select" id="orderList" data-name="template[orderList]"
                         data-value="{{#  if((d.template != null)){ }}{{  d.template.orderList  }}{{#  } }}"
                         data-api="Content/AdminApi/GetSortModeList"></div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">是否显示在导航栏：</label>
                <div class="layui-input-block">
                    <input type="checkbox" lay-skin="switch" id="isShow" lay-filter="isShow" name="isShow"
                            {{#  if(d.isShow){ }} value="1" checked{{#  } }}/>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">是否启用伪静态：</label>
                <div class="layui-input-block">
                    <input type="checkbox" lay-skin="switch" id="isRewrite" lay-filter="isRewrite"
                           name="template[isRewrite]"
                            {{#  if((d.template != null) && d.template.isRewrite){ }} value="1"
                           checked{{#  } }}/>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">是否过滤内容的镜像：</label>
                <div class="layui-input-block">
                    <input type="checkbox" lay-skin="switch" id="isFilterContentMirror" lay-filter="isFilterContentMirror"
                           name="template[isFilterContentMirror]"
                            {{#  if((d.template != null) && d.template.isFilterContentMirror){ }} value="1"
                           checked{{#  } }}/>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">列表页是否关闭第1页：</label>
                <div class="layui-input-block">
                    <input type="checkbox" lay-skin="switch" id="isClosePageByFirst" lay-filter="isExistsPageByOne"
                           name="template[isClosePageByFirst]"
                            {{#  if((d.template != null) && d.template.isClosePageByFirst){ }} value="1"
                           checked{{#  } }}/>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">栏目页是否生成静态：</label>
                <div class="layui-input-block">
                    <input type="checkbox" lay-skin="switch" id="categoryHtml" lay-filter="categoryHtml"
                           name="template[categoryHtml]"
                            {{#  if((d.template != null) && d.template.categoryHtml){ }} value="1"
                           checked{{#  } }}/>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">内容页是否生成静态：</label>
                <div class="layui-input-block">
                    <input type="checkbox" lay-skin="switch" id="contentHtml" lay-filter="contentHtml"
                           name="template[contentHtml]"
                            {{#  if((d.template != null) && d.template.contentHtml){ }} value="1"
                           checked{{#  } }}/>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">栏目页URL规则：</label>
                <div class="layui-input-block">
                    <input type="text" name="template[categoryUrl]" class="layui-input"
                           lay-verify="required" autocomplete="off"
                           value="{{#  if((d.template != null) && d.template.categoryUrl){ }}{{  d.template.categoryUrl  }}{{#  }else{ }}category[id]-[page].htm{{#  } }}"/>
                    <div class="layui-form-mid layui-word-aux">
                        涉及到参数有（取双引号里面的，不含双引号）：“[id]”是当前栏目的ID，“[page]”是页码。
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">内容页URL规则：</label>
                <div class="layui-input-block">
                    <input type="text" name="template[contentUrl]" class="layui-input"
                           lay-verify="required" autocomplete="off"
                           value="{{#  if((d.template != null) && d.template.contentUrl){ }}{{  d.template.contentUrl  }}{{#  }else{ }}content[catId]-[id].htm{{#  } }}"/>
                    <div class="layui-form-mid layui-word-aux">
                        涉及到参数有（取双引号里面的，不含双引号）：“[id]”是当前内容的ID，“[catId]”是内容所属栏目的ID。
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{#  } }}

    {{#  if (power.indexOf('changeCategoryHeightSetPower') >= 0){ }}
    <div class="layui-card">
        <div class="layui-card-header layui-bg-gray">权限设置</div>
        <div class="layui-card-body">
            <div class="layui-form-item">
                <label class="layui-form-label">可编辑：</label>
                <div class="layui-input-block">
                    <div class="pkcms_select" id="PowerEditGroup" data-name="power[EditGroup]" data-type="checkbox"
                         data-value="{{#  if((d.power != null) && d.power.EditGroup){ }}{{  d.power.EditGroup  }}{{#  } }}"
                         data-api="Member/AdminGetGroup" data-params="modelId=1"></div>
                    <div class="layui-form-mid layui-word-aux">
                        设置后，只有勾选的角色级别才能在后台访问该栏目，并编辑所属的内容。
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">只看自己发布：</label>
                <div class="layui-input-block">
                    <div class="pkcms_select" id="PowerReadMeGroup" data-name="power[ReadMeGroup]"
                         data-type="checkbox"
                         data-value="{{#  if((d.power != null) && d.power.ReadMeGroup){ }}{{  d.power.ReadMeGroup  }}{{#  } }}"
                         data-api="Member/AdminGetGroup" data-params="modelId=1"></div>
                    <div class="layui-form-mid layui-word-aux">
                        设置后，只有勾选的角色级别下的用户只能查看、编辑自己的内容。<br/>
                        <font color="red"><b>【可编辑的角色】的权限优先</b></font>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label"></label>
                <div class="layui-input-block">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label"></label>
                <div class="layui-input-block">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label"></label>
                <div class="layui-input-block">
                </div>
            </div>
        </div>
    </div>
    {{#  } }}

</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>
<script type="text/javascript">
    PKAdmin.ready(function () {
        PKAdmin.tplFormBySubmit({});
    });
</script>
</body>
</html>
