<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/pkcms/icons/iconfont.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/steps.css" media="all">
</head>

<body>

<div class="layui-fluid">
    <fieldset class="layui-elem-field layui-field-title">
        <legend>批量替换内容</legend>
    </fieldset>
    <div class="layui-row">
        <div class="layui-col-md6 layui-col-sm6 layui-col-xs12">
            <div class="layui-card">
                <div class="layui-card-header layui-bg-black">选择数据模型</div>
                <div class="layui-card-body">
                    <div id="view-model"></div>
                </div>
            </div>
            <div class="layui-card">
                <div class="layui-card-header layui-bg-black">
                    选择替换的字段<span style="color: yellow"></span>
                </div>
                <div class="layui-card-body">
                    <div id="view-field"></div>
                </div>
            </div>
            <div class="layui-card">
                <div class="layui-card-header layui-bg-black">
                    搜索被替换的内容
                    <span style="color: yellow">（注意：备份原数据）</span>
                </div>
                <div class="layui-card-body">
                    <input type="text" class="layui-input" id="search"/>
                </div>
            </div>
            <div class="layui-card">
                <div class="layui-card-header layui-bg-black">
                    替换成的目标内容
                </div>
                <div class="layui-card-body">
                    <input type="text" class="layui-input" id="replace"/>
                </div>
            </div>
            <div class="layui-card">
                <div class="layui-card-header layui-bg-black">操作选项<span
                            style="color: yellow">（注意：执行前先备份源数据）</span></span>
                </div>
                <div class="layui-card-body">
                    <button type="button" class="layui-btn" id="btn-replace">执行替换</button>
                </div>
            </div>
        </div>
        <div class="layui-col-md6 layui-col-sm6 layui-col-xs12">
        </div>
    </div>
</div>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">

    var data = {
        params_list: {
            model: null,
            field: null
        },
        params: {
            modelId: 0,
            listField: null
        }
    };

    function getApiModel(fn) {
        if (typeof data.params_list.model == "object" && data.params_list.model != null) {
            fn(data.params_list.model);
        } else {
            PKLayer.apiGet('Model/AdminGetModel/ApiBySelect', {modelType:'content'}, function (res) {
                data.params_list.model = res;
                fn(res);
            });
        }
    }

    function getApiField(fn) {
        PKLayer.apiGet('Model/AdminGetField/ApiBySelect', {modelId:data.params.modelId}, function (res) {
            data.params_list.field = res;
            PKLayer.useFormBySelectFun('#view-field', res, null, false, false, fn);
        });
    }

    function initModelSelect() {
        getApiModel(function (res) {
            PKLayer.useFormBySelectFun('#view-model', res, null, false, true, function (rows) {
                data.params.modelId = rows.length === 1 ? rows[0].id : 0;
                getApiField(function (rows) {
                    data.params.listField = rows;
                });
            });
        });
    }

    PKAdmin.ready(function () {
        initModelSelect();
        $('#btn-replace').on('click', function () {
            var params = {
                search: $('#search').val(),
                replace: $('#replace').val()
            };
            PKLayer.apiPost('Content/AdminBatReplace/ApiByBatReplace', Object.assign(params, data.params),
                function (res) {
                    PKLayer.tipsMsg('操作成功');
                });
        });
    });

</script>
</body>
</html>
