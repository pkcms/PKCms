<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>内容管理</title>
    <link rel="stylesheet" href="/statics/layui/css/layui.css">
    <link rel="stylesheet" href="/statics/pkcms/icons/iconfont.css" media="all">
    <link rel="stylesheet" href="/statics/admin/dtree/dtree.css">
    <link rel="stylesheet" href="/statics/admin/dtree/font/dtreefont.css">
    <style>
        .layadmin-iframe {
            position: absolute;
            width: 100%;
            height: 100%;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0;
        }
    </style>
</head>
<body class="layui-layout-body">


<div class="layui-side">
    <div class="layui-side-scroll">
        <ul id="categoryTree" class="dtree" data-id="0"></ul>
    </div>
</div>

<div class="layui-body" id="LAY_app_body" style="padding: 0 10px;">
    <div class="layui-tab layui-tab-card auto-full-height" lay-allowclose="true" lay-filter="content-tab">
        <ul class="layui-tab-title" id="category-card">
            <li class="layui-this">
                综合
            </li>
        </ul>
        <div class="layui-tab-content layui-body" style="top:65px;left: 10px;right: 10px;">
            <div class="layui-tab-item layui-show">
                <iframe src="/index.php/Content/AdminContent/complex" id="complex_iframe" name="complex_iframe" frameborder="0" class="layadmin-iframe"></iframe>
            </div>
        </div>
    </div>
</div>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>
<script type="text/javaScript">

    var data = {};

    PKAdmin.ready(function () {
        PKLayer.useTree('categoryTree', 'Content/AdminGetCategory/ApiByListsOfContent',
            function (obj, element, dtree) {
                // 当前节点为最后一级节点时触发事件
                openTab(obj.param.context, obj.param.nodeId);// 这里并没有像示例1一样带参数过去，如果你想带参数的话，可以将整个param都带走，但是需要对由中文的参数做转码处理

                function openTab(context, nodeId) {
                    // 匹配选项卡是否存在
                    var matchTo = false,
                        tabs = $('#category-card>li');
                    tabs.each(function (index) {
                        var li = $(this),
                            layid = li.attr('lay-id');

                        if (layid === nodeId) {
                            matchTo = true;
                            return;
                        }
                    });

                    $('#tab_recovery').on('click', function () {
                        alert('b');
                    });

                    // 如果未在选项卡中匹配到，则追加选项卡
                    if (!matchTo) {
                        PKLayer.apiGet(
                            'Content/AdminGetCategory/ApiGetDetail',
                                {id:nodeId},
                            function (res) {
                                if (res.hasOwnProperty('categoryType')) {
                                    switch (res.categoryType) {
                                        case 'page':
                                            element.tabAdd("content-tab", {
                                                id: nodeId,
                                                title: context,
                                                content: "<iframe src='/index.php/Content/AdminPage?id=" + nodeId + "' id='" + nodeId + "_iframe' name='" + context + "_iframe' frameborder='0' class='layadmin-iframe'></iframe>"
                                            });

                                            // 定位当前tabs
                                            element.tabChange("content-tab", nodeId);
                                            // PKLayer.useModelByFrame('单页面编辑', '/index.php/Content/AdminPage?id=' + nodeId + '&iframe=1')
                                            break;
                                        case 'list':
                                            element.tabAdd("content-tab", {
                                                id: nodeId,
                                                title: context,
                                                content: "<iframe src='/index.php/Content/AdminGetContent?modelId=" + res.modelId + "&catId=" + nodeId + "' id='" + nodeId + "_iframe' name='" + context + "_iframe' frameborder='0' class='layadmin-iframe'></iframe>"
                                            });

                                            // 定位当前tabs
                                            element.tabChange("content-tab", nodeId);
                                            break;
                                    }
                                }
                            }
                        );
                    } else {
                        // 定位当前tabs
                        element.tabChange("content-tab", nodeId);
                    }

                }
            });
    });
</script>
</body>
</html>