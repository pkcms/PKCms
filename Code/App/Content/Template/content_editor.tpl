<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/steps.css" media="all">
    <link rel="stylesheet" href="/statics/admin/dtree/dtree.css">
    <link rel="stylesheet" href="/statics/admin/dtree/font/dtreefont.css">
    <link rel="stylesheet" href="/statics/pkcms/icons/iconfont.css" media="all">
    <link href="/statics/ueditor/themes/default/css/ueditor.min.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" charset="utf-8" src="/statics/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="/statics/ueditor/ueditor.all.min.js"></script>
</head>

<body>

<div class="layui-fluid">

    <div class="layui-fluid">

        <div class="layui-card">
            <div class="layui-card-header layuiadmin-card-header-auto">
                <fieldset class="layui-elem-field layui-field-title">
                    <legend>内容编辑</legend>
                </fieldset>
            </div>

            <div class="layui-card-body layui-card-body-mb">
                <form class="layui-form" lay-filter="form-content">
                    <div id="view-form"></div>
                    <div class="layui-form-footer-fixed layui-hide">
                        <div id="steps" style="display: none;"></div>
                        <div id="btnBar" class="layui-input-block">
                            <div class="layui-footer">
                                <button class="layui-btn" lay-submit lay-filter="form-content"
                                        data-action="editor_page">立即提交
                                </button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script id="tpl-form" type="text/html">
    {{#
    var power = PKCms.localStorage_get('powerByAction');
    }}
    {{#  if (power.indexOf('changeContentBaseSetPower') > 0){ }}
    <input type="hidden" name="id" value="{{  d.apiParams.id  }}"/>
    <input type="hidden" name="catId" value="{{  d.apiParams.catId  }}">
    <input type="hidden" name="modelId" value="{{  d.apiParams.modelId  }}">

    <div class="layui-card">
        <div class="layui-card-header layui-bg-gray">基本信息</div>
        <div class="layui-card-body">
            <div class="layui-form-item">
                <label class="layui-form-label">标题：</label>
                <div class="layui-input-block">
                    <input type="text" name="title" class="layui-input" lay-verify="required" autocomplete="off"
                           value="{{#  if(d.entity.title){ }}{{  d.entity.title  }}{{#  } }}"/>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">
                    栏目专题:
                </label>
                <div class="layui-input-block">
                    <table class="layui-table">
                        <thead class="layui-table-header">
                        <tr>
                            <th>所属栏目</th>
                            <th>
                                镜像的栏目
                                <button type="button" class="layui-btn layui-btn-xs layui-btn-primary"
                                        id="selectCategory_btn" lay-tips="创建镜像到其他的栏目">
                                    <i class="layui-icon layui-icon-add-1"></i>
                                </button>
                            </th>
                            <th>
                                专题及其的分类
                                <button type="button" class="layui-btn layui-btn-xs layui-btn-primary"
                                        id="selectTopic_btn" lay-tips="添加该内容到专题">
                                    <i class="layui-icon layui-icon-add-1"></i>
                                </button>
                            </th>
                        </tr>
                        </thead>
                        <tbody class="layui-table-body">
                        <tr>
                            <td>{{  d.category.name  }}</td>
                            <td>
                                <ul id="addTo">
                                    {{#  layui.each(d.entity.mirroring_category, function(index, item){ }}
                                    <li id="cat_{{ item.id }}" data-id="{{ item.id }}" data-model="{{ item.modelId }}">
                                        <button type="button" onclick="dropMirroring({{ item.id }},{{ item.modelId }})">
                                            <i class="layui-icon layui-icon-delete"></i>
                                            {{ item.name }}
                                        </button>
                                    </li>
                                    {{#  }); }}
                                </ul>
                            </td>
                            <td>
                                <ul id="view-topic-type">
                                </ul>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">SEO标题：</label>
                <div class="layui-input-block">
                    <input type="text" name="seoTitle" class="layui-input"
                           value="{{#  if(d.entity.seoTitle){ }}{{  d.entity.seoTitle  }}{{#  } }}"/>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">SEO关键词：</label>
                <div class="layui-input-block">
                        <textarea name="seoKeywords"
                                  class="layui-textarea">{{#  if(d.entity.seoKeywords){ }}{{  d.entity.seoKeywords  }}{{#  } }}</textarea>
                    <div class="layui-form-mid layui-word-aux">
                        多词之间使用英文逗号（,）分隔
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">SEO简述：</label>
                <div class="layui-input-block">
                        <textarea name="seoDescription"
                                  class="layui-textarea">{{#  if(d.entity.seoDescription){ }}{{  d.entity.seoDescription  }}{{#  } }}</textarea>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">
                    缩略图:
                </label>
                <div class="layui-input-block layui-upload">
                    <input type="text" class="layui-input" style="width: 60%;float: left;" id="thumb" name="thumb"
                           value="{{#  if(d.entity.thumb){ }}{{  d.entity.thumb  }}{{#  } }}"/>
                    <button type="button" class="layui-btn pkcms_upload"
                            id="uploadBtn_thumb" data-field="thumb">
                        <i class="layui-icon layui-icon-upload"></i>选择
                    </button>
                    <button type="button" class="layui-btn" onclick="PreViewImage('thumb')">预览
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="layui-card">
        <div class="layui-card-header layui-bg-gray">模型数据</div>
        <div id="view_fieldList" class="layui-card-body">
        </div>
    </div>
    {{#  } }}

    {{#  if (power.indexOf('changeContentHeightSetPower') > 0){ }}
    <div class="layui-card">
        <div class="layui-card-header layui-bg-gray">扩展属性</div>
        <div class="layui-card-body">
            <div class="layui-form-item">
                <label class="layui-form-label">
                    创建时间:
                </label>
                <div class="layui-input-block">
                    <input type="text" id="createTime" name="createTime" class="layui-input pkcms_date"
                           value="{{#  if(d.entity.createTime){ }}{{  dateFormat(d.entity.createTime) }}{{#  } }}"/>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">同步更新</label>
                <div class="layui-input-block">
                    <input type="checkbox" lay-skin="switch" id="syncUpdateTime" lay-filter="syncUpdateTime"
                           name="syncUpdateTime" value="1" /><br/>
                    <div class="layui-form-mid layui-word-aux">
                        开启后，将此时的更新时间同步到服务链
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">
                    定制模板:
                </label>
                <div class="layui-input-block">
                    <div class="pkcms_select" id="template" data-name="template"
                         data-value="{{#  if(d.entity.template){ }}{{  d.entity.template  }}{{#  } }}"
                         data-api="Template/AdminGetTemplate"
                         data-params="folder=content&query=show"></div>
                    <div class="layui-form-mid layui-word-aux">
                        留空，则按照栏目的配置来
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">
                    URL规则:
                </label>
                <div class="layui-input-block">
                    <input type="text" name="url" class="layui-input"
                           value="{{#  if(d.entity.url){ }}{{  d.entity.url  }}{{#  } }}"/>
                    <div class="layui-form-mid layui-word-aux">
                        留空，则按照栏目的配置来
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">
                    内容属性：
                </label>
                <div class="layui-input-block">
                    <input type="checkbox" name="toHead" title="头条"{{# if(d.entity.toHead) { }} checked{{# } }} />
                    <input type="checkbox" name="toPush" title="推荐"{{# if(d.entity.toPush) { }} checked{{# } }} />
                    <input type="checkbox" name="toTop" title="置顶"{{# if(d.entity.toTop) { }} checked{{# } }} />
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">
                    内容排序：
                </label>
                <div class="layui-input-block">
                    <input type="number" name="listSort" class="layui-input"
                           value="{{#  if(d.entity.listSort){ }}{{  d.entity.listSort  }}{{#  }else{ }}0{{#  } }}"/>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">
                </label>
                <div class="layui-input-block">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">
                </label>
                <div class="layui-input-block">
                </div>
            </div>
        </div>
    </div>
    {{#  } }}

</script>

<script id="tpl_fieldList" type="text/html">
    {{#  layui.each(d.fieldList, function(index, item){ }}
    <div class="layui-form-item">
        <label class="layui-form-label">{{ item.name }}：</label>
        <ul class="layui-input-block">
            {{#  if(item.formType === 'text'){ }}
            <!-- 类型：文本 -->
            <input type="text" name="{{ item.field }}" class="layui-input"
                   value="{{#  if(d.entity[item.field]){ }}{{  d.entity[item.field]  }}{{#  } }}"/>
            {{#  } else if(item.formType === 'textarea'){ }}
            <!-- 类型：多行文本 -->
            <textarea name="{{ item.field }}"
                      class="layui-textarea">{{#  if(d.entity[item.field]){ }}{{  d.entity[item.field]  }}{{#  } }}</textarea>
            {{#  } else if(item.formType === 'number'){ }}
            <!-- 类型：数字 -->
            <input type="number" name="{{ item.field }}" class="layui-input"
                   value="{{#  if(d.entity[item.field]){ }}{{  d.entity[item.field]  }}{{#  } }}"/>
            {{#  } else if(item.formType === 'upload_list'){ }}
            <!-- 类型：多文件上传 -->
            <div class="layui-upload">
                <button type="button" class="layui-btn pkcms_upload" id="uploadBtn_{{ item.field }}"
                        data-model="{{  d.category.modelId  }}"
                        data-exts="{{ item.setting.upload_allowExt }}"
                        data-field="{{ item.field }}">
                    <i class="layui-icon layui-icon-upload"></i>选择文件
                </button>
            </div>
            <ul id="view_{{ item.field }}" style="margin-top: 5px;">
            </ul>
            {{#  if(d.entity[item.field]){ }}
            {{#  layui.each(d.entity[item.field], function(index, item_image){ }}
            {{#
            item_image['index'] = index;
            item_image['list_domId'] = 'view_' + item.field;
            item_image['item_domId'] = 'fileItem_' + item.field + index;
            item_image['field_name'] = item.field;
            d.fileList.push(item_image);
            }}
            {{#  }); }}
            {{#  } }}
            {{#  } else if(item.formType === 'image_list'){ }}
            <!-- 类型：多图片上传 -->
            <div class="layui-upload">
                <button type="button" class="layui-btn pkcms_upload" id="uploadBtn_{{ item.field }}"
                        data-field="{{ item.field }}">
                    <i class="layui-icon layui-icon-upload"></i>选择图片
                </button>
            </div>
            <ul id="view_{{ item.field }}" style="margin-top: 5px;">
            </ul>
            {{#  if(d.entity[item.field]){ }}
            {{#  layui.each(d.entity[item.field], function(index, item_image){ }}
            {{#
            item_image['index'] = index;
            item_image['list_domId'] = 'view_' + item.field;
            item_image['item_domId'] = 'imageItem_' + item.field + index;
            item_image['field_name'] = item.field;
            d.imageList.push(item_image);
            }}
            {{#  }); }}
            {{#  } }}
            {{#  } else if(item.formType === 'editor'){ }}
            <!-- 类型：编辑器 -->
            <div>分页标识符：“&lt;hr/&gt;”（不包含双引号），将其放在&lt;/p&gt;之后</div>
            <textarea class="pkcms_editor" id="field_{{ item.field }}"
                      name="{{ item.field }}">{{#  if(d.entity[item.field]){ }}{{  d.entity[item.field]  }}{{#  } }}</textarea>
            {{#  } else if(item.formType === 'option'){ }}
            <!-- 类型：选项 -->
            <div class="pkcms_select" id="{{ item.field }}" data-name="{{ item.field }}"
                 data-value="{{#  if(d.entity[item.field]){ }}{{  d.entity[item.field]  }}{{#  } }}"
                 data-type="{{#  if(item.setting.boxType == 'checkbox'){ }}checkbox{{#  } }}"
                 data-api="Model/AdminGetOptions"
                 data-params="parentId={{  item.setting.option  }}"></div>
            {{#  } else if(item.formType === 'frontend_code'){ }}
            <!-- 类型：前端代码 -->
            <textarea name="{{ item.field }}" class="layui-textarea layui-code"
                      lay-skin="notepad">{{#  if(d.entity[item.field]){ }}{{  d.entity[item.field]  }}{{#  } }}</textarea>
            {{#  } else if(item.formType === 'baidu_map'){ }}
            <!-- 类型：百度定位 -->
            <div class="pkcms_BaiDuMaps" data-id="{{ item.field }}"
                 data-name="{{ item.field }}"
                 data-value="{{#  if(d.entity[item.field]){ }}{{  d.entity[item.field]  }}{{#  } }}"></div>
            {{#  } }}
    </div>
    </div>
    {{#  }); }}
</script>

<script id="tpl_imageList" type="text/html">
    <li id="{{ d.item_domId }}" class="layui-card {{ d.list_domId }}" style="border: #e2e2e2 1px solid">
        <div class="layui-card-header layui-row layui-bg-gray">
            <div class="layui-col-lg9 layui-col-md9 layui-col-sm9 layui-col-xs12">
                图片路径： {{ d.img_src }}
            </div>
            <div class="layui-col-lg3 layui-col-md3 layui-col-sm3 layui-col-xs12">
                <label type="button" class="layui-btn layui-btn-primary layui-btn-xs arrow_{{ d.list_domId }}"
                        onclick="permutation_uploadList('{{ d.list_domId }}')">
                    <i class="iconfont pkcms-icon-round-up"></i>
                </label>
                <label type="button" class="layui-btn layui-btn-primary layui-btn-xs arrow_{{ d.list_domId }}"
                        onclick="permutation_uploadList('{{ d.list_domId }}')">
                    <i class="iconfont pkcms-icon-round-down"></i>
                </label>
                <label type="button" class="layui-btn layui-btn-danger layui-btn-xs"
                        onclick="remove_id('{{ d.item_domId }}')">
                    <i class="iconfont pkcms-icon-delete"></i>
                </label>
            </div>
        </div>
        <div class="layui-card-body layui-row">
            <div class="layui-col-lg3 layui-col-md3 layui-col-sm3 layui-col-xs12">
                <img src="{{ d.img_src }}" style="max-width: 98%;max-height: 150px;"/>
            </div>
            <div class="layui-col-lg9 layui-col-md9 layui-col-sm9 layui-col-xs12">
                <div class="layui-form-item">
                    <label class="layui-form-label">
                        图片路径：
                    </label>
                    <div class="layui-input-block">
                        <input type="text" class="layui-input"
                               name="{{ d.field_name }}[{{ d.index }}][img_src]" value="{{ d.img_src }}"/>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">
                        图片描述：
                    </label>
                    <div class="layui-input-block">
                            <textarea class="layui-textarea"
                                      name="{{ d.field_name }}[{{ d.index }}][img_tips]">{{ d.img_tips }}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </li>
</script>

<script id="tpl-fileList" type="text/html">
    <div id="{{ d.item_domId }}" class="layui-card {{ d.list_domId }}" style="border: #e2e2e2 1px solid">
        <div class="layui-card-header layui-row layui-bg-gray">
            <div class="layui-col-lg9 layui-col-md9 layui-col-sm9 layui-col-xs12">
                文件路径：{{ d.file_path }}
                <input type="hidden" name="{{ d.field_name }}[{{ d.index }}][file_path]" value="{{ d.file_path }}"/>
            </div>
            <div class="layui-col-lg3 layui-col-md3 layui-col-sm3 layui-col-xs12">
                <label class="layui-btn layui-btn-primary layui-btn-xs arrow_{{ d.list_domId }}"
                        onclick="permutation_uploadList('{{ d.list_domId }}')">
                    <i class="iconfont pkcms-icon-round-up"></i>
                </label>
                <label class="layui-btn layui-btn-primary layui-btn-xs arrow_{{ d.list_domId }}"
                        onclick="permutation_uploadList('{{ d.list_domId }}')">
                    <i class="iconfont pkcms-icon-round-down"></i>
                </label>
                <label class="layui-btn layui-btn-danger layui-btn-xs"
                        onclick="remove_id('{{ d.item_domId }}')">
                    <i class="iconfont pkcms-icon-delete"></i>
                </label>
            </div>
        </div>
        <div class="layui-card-body">
            <div class="layui-form-item">
                <label class="layui-form-label">
                    文件描述：
                </label>
                <div class="layui-input-block">
                        <textarea class="layui-textarea"
                                  name="{{ d.field_name }}[{{ d.index }}][file_tips]">{{ d.file_tips }}</textarea>
                </div>
            </div>
        </div>
    </div>
</script>

<script id="tpl-sendToOtherCategory" type="text/html">
    <li id="cat_{{  d.id  }}" data-id="{{  d.id  }}" data-model="{{  d.modelId  }}">
        <div type="button" onclick="sendToCategoryDel({{  d.id  }})">
            <i class="layui-icon layui-icon-delete"></i>
            {{  d.name  }}
        </div>
    </li>
</script>

<script type="text/html" id="tpl-topic">
    <div class="layui-card">
        <div class="layui-card-header">请先选择专题</div>
        <div class="layui-card-body" id="list_topic"></div>
        <div class="layui-card-header">再选择专题分类</div>
        <div class="layui-card-body" id="list_topicType"></div>
    </div>
</script>

<script type="text/html" id="tpl-topic-type">
    {{#  layui.each(d.entity.topicList, function(index, item){ }}
    <li id="topic_{{ item.topicTypeId }}" data-topicId="{{ item.topicId }}" data-typeId="{{ item.topicTypeId }}">
        <div type="button" onclick="dropTopicType({{ index }},{{ item.topicTypeId }},{{ item.topicId }})">
            <i class="layui-icon layui-icon-delete"></i>
            {{ item.topicTitle }} -> {{ item.topicTypeName }}
        </div>
    </li>
    {{#  }); }}
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">
    var data = {
        apiParams: {
            id: '<{$id}>',
            catId: '<{$catId}>',
            modelId: '<{$modelId}>'
        },
        entity: {
            topicList: []
        },
        category:{},
        fieldList: [],
        imageList: [],
        fileList: []
    };

    function sendToCategoryDel(id) {
        $('#cat_' + id).remove();
    }

    function dropMirroring(id_cat, id_model) {
        var id_content = data.entity.id;
        if (id_content > 0) {
            PKLayer.tipsConfirm('是否确定在该栏目下的镜像', function () {
                PKLayer.apiPost('Content/AdminSetContent/ApiByDel',
                        {modelId:id_model,catId:id_cat,id:[id_content]},
                    function () {
                        $('#cat_' + id_cat).remove();
                        PKLayer.tipsMsg('删除成功！');
                    });
            });
        } else {
            $('#cat_' + id_cat).remove();
        }
    }

    function dropTopicType(index, id_type, id_topic) {
        var id_content = data.entity.id;
        if (id_content > 0) {
            PKLayer.tipsConfirm('是否确定在该栏目下的镜像', function () {
                PKLayer.apiPost('topic/AdminSetTopicContent/ApiByDel',
                        {topicId:id_topic,topicTypeId:id_type,contentId:id_content},
                    function () {
                        $('#topic_' + id_type).remove();
                        data.entity.topicList.splice(index, 1);
                        PKLayer.useTpl(data, 'tpl-topic-type', 'view-topic-type');
                        PKLayer.tipsMsg('删除成功！');
                    });
            });
        } else {
            data.entity.topicList.splice(index, 1);
            PKLayer.useTpl(data, 'tpl-topic-type', 'view-topic-type');
                    $('#topic_' + id_type).remove();
        }
    }

    // 模板 - 数据模型
    function getModel() {
        PKLayer.apiGet(
            'Model/AdminGetField/ApiByLists', data.apiParams,
            function (res) {
                data.fieldList = res;
                // console.log('useFormByControl');
                PKLayer.useTplCall(data, 'tpl_fieldList', 'view_fieldList', function (Tpl) {
                    // console.log('useFormByControl');
                    layui.$("#selectCategory_btn").on('click', function () {
                        // console.log('selectCategory_btn');
                        PKAdmin.selectCategoryType("同时发布到哪个栏目", function (param) {
                            getCategory(param.nodeId, function (res) {
                                PKLayer.useTplAppend(res, 'tpl-sendToOtherCategory', 'addTo');
                            });
                        })
                    });
                    layui.$("#selectTopic_btn").on('click', function () {
                        // console.log('selectTopic_btn');
                        // console.log(data);
                        var select_topic = Object.create(null);
                        PKLayer.useModelByTpl('选择添加到哪个专题及专题分类',{}, 'tpl-topic', function () {
                            PKLayer.apiGet(
                                'Topic/AdminTopic/ApiByLists', {},
                                function (res_topic) {
                                    var list_topic = [];
                                    if (typeof res_topic == "object" && res_topic.length > 0) {
                                        for (let re of res_topic) {
                                            list_topic.push({name:re.title,value:re.id});
                                        }
                                        PKLayer.useFormBySelectFun('#list_topic', list_topic, null, false, true, function (rows) {
                                            if (rows.length > 0) {
                                                select_topic = {
                                                    topicId: rows[0].value, topicTitle: rows[0].name
                                                };
                                                PKLayer.apiGet('Topic/AdminTopicType/ApiByLists', select_topic, function (res_type) {
                                                    var list_type = [];
                                                    if (typeof res_type == "object" && res_type.length > 0) {
                                                        for (var resTypeElement of res_type) {
                                                            list_type.push({name:resTypeElement.name,value:resTypeElement.id});
                                                        }
                                                        PKLayer.useFormBySelectFun('#list_topicType', list_type, null, false, true, function (rows_type) {
                                                            // console.log(rows_type);
                                                            if (rows_type.length > 0) {
                                                                select_topic = Object.assign(select_topic, {
                                                                    topicTypeId: rows_type[0].value,
                                                                    topicTypeName: rows_type[0].name
                                                                });
                                                                layer.closeAll();
                                                                data.entity.topicList.push(select_topic);
                                                                // console.log(data);
                                                                PKLayer.useTpl(data, 'tpl-topic-type', 'view-topic-type');
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                }
                            );
                        });
                    });
                    var domId_list = [];
                    if (data.imageList.length > 0) {
                        for (var imageListElement of data.imageList) {
                            if (domId_list.indexOf(imageListElement.list_domId) < 0) {
                                domId_list.push(imageListElement.list_domId);
                            }
                            PKLayer.useTplAppend(imageListElement, 'tpl_imageList', imageListElement.list_domId);
                        }
                    }
                    if (data.fileList.length > 0) {
                        for (var fileListElement of data.fileList) {
                            if (domId_list.indexOf(fileListElement.list_domId) < 0) {
                                domId_list.push(fileListElement.list_domId);
                            }
                            PKLayer.useTplAppend(fileListElement, 'tpl-fileList', fileListElement.list_domId);
                        }
                    }
                    if (domId_list.length > 0) {
                        for (let domIdListElement of domId_list) {
                            permutation_uploadList(domIdListElement);
                        }
                    }
                    PKLayer.useFormByControlAndSubmit('form-content', function (paramsByForm) {
                        PKAdmin.submitFormByContent(paramsByForm);
                    });
                });
            }
        );
    }

    function getCategory(cat_id, callback) {
        PKLayer.apiGet(
            'Content/AdminGetCategory/ApiGetDetail', {id: cat_id},
            function (res) {
                callback(res);
            }
        );
    }

    function getEntity() {
        if (parseInt(data.apiParams.id) > 0) {
            PKLayer.apiGet(
                'Content/AdminGetContent/ApiGetDetail', data.apiParams,
                function (res) {
                    data.entity = Object.assign(data.entity, res);
                    // console.log(data);
                    getCategory(data.apiParams.catId, function (res) {
                        data.category = res;
                        PKLayer.useTplCall(data, 'tpl-form', 'view-form', function (Tpl) {
                            getModel();
                            if (data.entity.topicList.length > 0) {
                                PKLayer.useTpl(data, 'tpl-topic-type', 'view-topic-type');
                            }
                            $('.layui-form-footer-fixed').removeClass('layui-hide');
                        })
                    });
                }
            );
        } else {
            getCategory(data.apiParams.catId, function (res) {
                data.category = res;
                PKLayer.useTplCall(data, 'tpl-form', 'view-form', function (Tpl) {
                    getModel();
                    $('.layui-form-footer-fixed').removeClass('layui-hide');
                })
            });
        }
    }

    PKAdmin.ready(function () {
        getEntity();
    });
</script>
</body>
</html>

