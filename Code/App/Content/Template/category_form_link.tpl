<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
</head>

<body>

<div class="layui-fluid">

    <div class="layui-fluid">

        <div class="layui-card">
            <div class="layui-card-header layuiadmin-card-header-auto">
                <fieldset class="layui-elem-field layui-field-title">
                    <legend>栏目编辑-URL形式</legend>
                </fieldset>
            </div>

            <div class="layui-card-body">
                <form class="layui-form" lay-filter="form-frame">
                    <div id="view-form"></div>

                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer">
                                <button class="layui-btn" lay-submit lay-filter="form-frame">立即提交</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/html" data-id="<{$id}>" data-api="Content/AdminGetCategory" id="tpl-form">
    {{#
    var power = PKCms.localStorage_get('powerByAction');
    }}
    {{#  if (power.indexOf('changeCategoryBaseSetPower') >= 0){ }}
    <input type="hidden" name="id" value="{{#  if(d.id){ }}{{  d.id  }}{{#  } }}"/>
    <input type="hidden" name="categoryType" value="{{#  if(d.categoryType){ }}{{  d.categoryType  }}{{#  }else{ }}<{$categoryType}>{{#  } }}"/>
    <div class="layui-form-item">
        <label class="layui-form-label">栏目名：</label>
        <div class="layui-input-block">
            <input type="text" name="name" class="layui-input" lay-verify="required" autocomplete="off"
                   value="{{#  if(d.name){ }}{{  d.name  }}{{#  } }}"/>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">父级选择：</label>
        <div class="layui-input-block">
            <input type="hidden" name="fromParentId"
                   value="{{#  if(d.parentId){ }}{{  d.parentId  }}{{#  } }}"/>
            <div class="pkcms_selectTree" id="parentId" data-id="parentId"
                 data-name="parentId" data-value="{{#  if(d.parentId){ }}{{  d.parentId  }}{{#  } }}"
                 data-api="Content/AdminGetCategory"></div>

        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">栏目描述：</label>
        <div class="layui-input-block">
            <textarea class="layui-textarea" name="info">{{#  if(d.info){ }}{{  d.info  }}{{#  } }}</textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">栏目图片：</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input" style="width: 60%;float: left;"
                   id="image" name="image" value="{{#  if(d.image){ }}{{  d.image  }}{{#  } }}"/>
            <button type="button" class="layui-btn pkcms_upload" id="uploadBtn_image" data-field="image">
                <i class="layui-icon layui-icon-upload"></i>选择
            </button>
            <button type="button" class="layui-btn" onclick="PreViewImage('image')">预览
            </button>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">链接地址：</label>
        <div class="layui-input-block">
            <input type="text" name="url" class="layui-input" lay-verify="required" autocomplete="off"
                   value="{{#  if(d.url){ }}{{  d.url  }}{{#  } }}"/>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">是否显示在导航栏：</label>
        <div class="layui-input-block">
            <input type="checkbox" lay-skin="switch" id="isShow" lay-filter="isShow" name="isShow"
                    {{#  if(d.isShow){ }} value="1" checked{{#  } }}/>
        </div>
    </div>
    {{#  } }}
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>
<script type="text/javaScript">
    PKAdmin.ready(function () {
        PKAdmin.tplFormBySubmit({});
    });
</script>

</body>
</html>
