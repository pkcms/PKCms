<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/pkcms/icons/iconfont.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
</head>

<body>

<div class="layui-fluid">

    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <fieldset class="layui-elem-field layui-field-title">
                <legend>栏目列表</legend>
            </fieldset>
        </div>

        <div class="layui-card-body">
            <table class="layui-hide PK-tableTree" id="table_adminList" lay-filter="table_adminList"
                   data-get="Content/AdminGetCategory"
                   data-set="Content/AdminSetCategory"></table>
        </div>
    </div>
</div>

<script type="text/html" id="urlTpl">
    <a href="{{  d.url  }}" target="_blank">{{  d.url  }}</a>
</script>

<script type="text/html" id="table_toolBar">
    <div class="layui-btn-container">
        <{loop $categoryTypeList $key $item}>
        <button class="layui-btn layui-btn-sm"
                lay-event="open|添加新的栏目|Content/AdminSetCategory?categoryType=<{$key}>">添加<{$item}>
        </button>
        <{/loop}>
        <button class="layui-btn layui-btn-danger layui-btn-sm" lay-event="batchDel">批量删除</button>
        <button class="layui-btn layui-btn-primary layui-btn-sm" lay-event="batchUpdateOfContext">更新上下文</button>
    </div>
</script>

<script type="text/html" id="table_tool">
    <a class="layui-btn layui-btn-normal layui-btn-xs" lay-tips="添加子级分类"
       lay-event="open|添加新的子分类|Content/AdminSetCategory?parentId={{d.id}}">
        <i class="layui-icon layui-icon-add-1"></i></a>
    <a class="layui-btn layui-btn-normal layui-btn-xs" lay-tips="修改该分类的信息"
       lay-event="open|修改栏目的信息|Content/AdminSetCategory?categoryType={{d.categoryType}}&id={{d.id}}">
        <i class="layui-icon layui-icon-edit"></i></a>
    {{# if (d.childrenIdList) { }}
    <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="api|Content/AdminSetCategory/ApiByTplConfig" lay-tips="应用相同模板配置到子分类">
        <i class="layui-icon layui-icon-set"></i></a>
    {{# } }}
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del" lay-tips="删除该分类">
        <i class="layui-icon layui-icon-delete"></i></a>
    <a class="layui-btn layui-btn-primary layui-btn-xs"
       href="{{ d.url_preView }}" target="_blank" lay-tips="预览该分类">
        <i class="iconfont pkcms-icon-view"></i></a>
</script>

<script type="text/html" id="tpl-update-context">
    <div class="layui-card">
        <div class="layui-card-header">
            更新量：{{ d.length }}
        </div>
        <div class="layui-card-body">
            <div class="layui-progress layui-progress-big" lay-filter="demo" lay-showpercent="true">
                <div class="layui-progress-bar layui-bg-blue" lay-percent="0%">
                    <span class="layui-progress-text"></span>
                </div>
            </div>
        </div>
    </div>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">
    var data = {
        speed_length: 0,
        speed_index: 0,
        speed_data: []
    };

    function batchUpdateOfContext(element_speed) {
        element_speed.progress('demo', PKCms.Percentage(data.speed_index, data.speed_length) + '%');
        if (data.speed_index < data.speed_length) {
            PKLayer.apiGet('/Content/AdminSetCategory/ApiByContext',
                    {id:data.speed_data[data.speed_index]}, function (res, count) {
                    if (data.speed_index < data.speed_length) {
                        setTimeout(function () {
                            data.speed_index += 1;
                            batchUpdateOfContext(element_speed);
                        }, 1000);
                    }
                });
        } else {
            PKLayer.tipsAlert('已经完成');
        }
    }

    function batchUpdate(api_params) {
        data.speed_data = api_params;
        data.speed_index = 0;
        data.speed_length = api_params.length
        PKLayer.useModelByTpl('更新栏目的上下文',
                {length: data.speed_length}, 'tpl-update-context',
            function (index) {
                layui.use(['element'], function () {
                    var element_speed = layui.element;
                    batchUpdateOfContext(element_speed);
                })
            });
    }

    function renderTable() {
        PKAdmin.tableTree('id', 'parentId', {
            cols: [[
                {type:'checkbox'},
                {field:'name', title:'栏目名'},
                {field:'id', title:'ID', width: 60},
                {field:'listSort', title:'排序', width: 60, edit: 'number'},
                {field:'url', title:'URL规则', templet: '#urlTpl'},
                {field:'categoryTypeName', title:'栏目类型', width: 80},
                {
                    title: "操作",
                    align: "center",
                    width: 250,
                    fixed: "right",
                    toolbar: "#table_tool"
                }
            ]],
            initSort: {
                field: 'listsort',
                type: 'asc'
            },
            even: true
        }, 1);
    }

    PKAdmin.ready(function () {
        renderTable();
    });
</script>
</body>
</html>
