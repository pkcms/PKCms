<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/pkcms/icons/iconfont.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/steps.css" media="all">
    <link rel="stylesheet" href="" media="all">
</head>

<body>

<div class="layui-fluid">

    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <fieldset class="layui-elem-field layui-field-title">
                <legend>发布内容的静态文件</legend>
            </fieldset>
        </div>

        <div class="layui-card-body">
            <div class="layui-row">
                <div class="layui-col-md6 layui-col-sm6 layui-col-xs12">
                    <table class="layui-hide PK-tableTree" id="table_categoryList" lay-filter="table_categoryList"
                           data-get="Content/AdminGetCategory"></table>
                </div>
                <div class="layui-col-md6 layui-col-sm6 layui-col-xs12">
                    <div class="layui-card" style="margin: 10px 0;">
                        <div class="layui-card-header layui-bg-gray">单项发布操作</div>
                        <div class="layui-card-body">
                            <form class="layui-form">
                                <table class="layui-table">
                                    <thead class="layui-table-header">
                                    <tr>
                                        <th>操作选项</th>
                                        <th>操作说明</th>
                                    </tr>
                                    </thead>
                                    <tbody class="layui-table-body">
                                    <tr>
                                        <td>
                                            <button class="layui-btn layui-btn-sm" lay-submit lay-filter="makeHome">
                                                &emsp;本站首页&emsp;
                                            </button>
                                        </td>
                                        <td>创建该站点的静态首页（index.htm）。</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <button class="layui-btn layui-btn-sm" lay-submit lay-filter="makeSitemap">
                                                &emsp;站点地图&emsp;
                                            </button>
                                        </td>
                                        <td>同时发布站点地图的HTML版和XML版，并筛选统计未向百度推送的（新）页面的URL。</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <button class="layui-btn layui-btn-sm" lay-submit lay-filter="pushBaiDu">
                                                &emsp;百度推新&emsp;
                                            </button>
                                        </td>
                                        <td>
                                            向百度的普通提交的接口提交新页面的URL，<b style="color: red">进行此操作前，先进行【站点地图】的操作</b>。
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <button class="layui-btn layui-btn-sm" lay-submit lay-filter="makeRewrite">
                                                &emsp;路由规则&emsp;
                                            </button>
                                        </td>
                                        <td>
                                            发布伪静态的路由规则，<b style="color: red">此操作是针对采取 伪静态的栏目</b>。
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                    <div class="layui-card">
                        <div class="layui-card-header layui-bg-gray">执行进度</div>
                        <div class="layui-card-body">
                            <div class="layui-progress layui-progress-big" lay-filter="demo" lay-showpercent="true">
                                <div class="layui-progress-bar layui-bg-blue" lay-percent="0%">
                                    <span class="layui-progress-text"></span>
                                </div>
                            </div>
                        </div>
                        <div class="layui-card-body">
                            <div class="layui-border-box" style="height: 500px;padding: 5px;overflow: auto;">
                                <ul id="view-log"></ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script type="text/html" id="table_toolBar">
    <div class="layui-btn-container">
        <button class="layui-btn" lay-event="makeCategory">
            &emsp;选中栏目（及内容）发布&emsp;
        </button>
    </div>
</script>


<script type="text/html" id="tpl-log">
    <li>
        完成进度：【
        {{# if(d.makeType=='category_list'){ }}
        列表栏目
        {{# }else if(d.makeType=='category_page'){ }}
        单页栏目
        {{# }else if(d.makeType=='category_link'){ }}
        链接栏目
        {{# }else if(d.makeType=='content'){ }}
        列表内容
        {{# }else if(d.makeType=='site_home'){ }}
        站点首页
        {{# }else if(d.makeType=='site_maps'){ }}
        站点地图
        {{# } }}
        】
        {{# if(d.name){ }}
        -&gt;【{{ d.name }}】
        {{# } }}
        {{# if(d.now_is_makeHtml){ }}
        {{# if(d.makeType=='category_list'){ }}
        第&nbsp;{{ d.now_category_page }}&nbsp;页
        {{# } }}
        {{# }else{ }}
        没有开启静态发布
        {{# } }}
    </li>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">

    var data = {
        params_api: {
            taskName: '',
            makeType: null,
            count_category: 0,
            count_total: 0
        },
    };

    function api(api, params_api, callback) {
        PKLayer.apiPost(
            'Content/AdminMakeHtml/' + api, params_api, function (params_res, count) {
                callback(params_res, count);
            }
        )
    }

    function makeHtml_count(callback) {
        api('ApiByCount', data.params_api, function (res_api, count) {
            data.params_api = Object.assign(data.params_api, res_api);
            callback();
        });
    }

    function renderTable(element_speed) {
        PKLayer.useTableTreeBySelect('id', 'parentId', {
            cols: [[
                {type:'checkbox'},
                {field:'name', title:'栏目名'},
                {field:'id', title:'ID', width: 60},
                {field:'categoryTypeName', title:'栏目类型', width: 80}
            ]],
            initSort: {
                field: 'listsort',
                type: 'asc'
            },
            even: true
        }, 1, function (event, res_params) {
            // console.log('event', event);
            // console.log('res_params', res_params);
            data.params_api.taskName = PKCms.nowDateTime();
            data.params_api.count_category = res_params.id.length;
            data.params_api = Object.assign(data.params_api, res_params);
            // console.log('params_api', data.params_api);
            makeHtml_count(function () {
                element_speed.progress('demo', PKCms.Percentage(0, data.params_api.count_total) + '%');
                makeHtml_create(element_speed);
            });
        });
    }

    function makeHtmlByOneStep(path_api, element_speed) {
        element_speed.progress('demo', PKCms.Percentage(0, 1) + '%');
        api(path_api, {}, function (res_params, count) {
            PKLayer.useTplPrepend(res_params, 'tpl-log', 'view-log');
            element_speed.progress('demo', PKCms.Percentage(1, 1) + '%');
        });
    }

    function makeHtml_create(element_speed) {
        api('ApiByCreate', data.params_api, function (res_params_api, count) {
            PKLayer.useTplPrepend(res_params_api, 'tpl-log', 'view-log');
            delete res_params_api.name;
            delete res_params_api.makeType;
            if (res_params_api.hasOwnProperty('count_total') && res_params_api.hasOwnProperty('finish_total')) {
                element_speed.progress('demo', PKCms.Percentage(res_params_api.finish_total, data.params_api.count_total) + '%');
                if (res_params_api.count_total > res_params_api.finish_total) {
                    data.params_api = Object.assign(data.params_api, res_params_api);
                    setTimeout(function () {
                        makeHtml_create(element_speed);
                    }, 1000);
                }
            } else {
                PKLayer.tipsAlert('没有接收到接口返回的总数和已完成量');
            }
        });
    }

    PKAdmin.ready(function () {

        layui.use(['form', 'element'], function () {
            var form = layui.form
                , element_speed = layui.element;

            renderTable(element_speed);

            form.on('submit(makeHome)', function (params_form) {
                makeHtmlByOneStep('ApiByCreateSite', element_speed);
                return false;
            });

            form.on('submit(makeSitemap)', function (params_form) {
                makeHtmlByOneStep('ApiByCreateSiteMaps', element_speed);
                return false;
            });

            form.on('submit(pushBaiDu)', function (params_form) {
                element_speed.progress('demo', PKCms.Percentage(0, 1) + '%');
                PKLayer.apiPost(
                    'Content/AdminBaiDuOfPushUrl', {}, function (params_res, count) {
                        element_speed.progress('demo', PKCms.Percentage(1, 1) + '%');
                        if (params_res.hasOwnProperty('success'), params_res.hasOwnProperty('remain')) {
                            PKLayer.tipsAlert('成功推送的url条数:' + params_res.success + '，当天剩余的可推送url条数：' + params_res.remain);
                        }
                    }
                )
                return false;
            });

            form.on('submit(makeRewrite)', function (params_form) {
                element_speed.progress('demo', PKCms.Percentage(0, 1) + '%');
                PKLayer.apiPost(
                    'Content/AdminMakeRewrite', {}, function (params_res, count) {
                        element_speed.progress('demo', PKCms.Percentage(1, 1) + '%');
                        if (params_res.hasOwnProperty('success'), params_res.hasOwnProperty('remain')) {
                            PKLayer.tipsAlert('成功推送的url条数:' + params_res.success + '，当天剩余的可推送url条数：' + params_res.remain);
                        }
                    }
                )
                return false;
            });
        })

    });

</script>
</body>
</html>
