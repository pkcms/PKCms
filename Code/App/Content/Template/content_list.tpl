<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/pkcms/icons/iconfont.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
    <link rel="stylesheet" href="/statics/admin/dtree/dtree.css">
    <link rel="stylesheet" href="/statics/admin/dtree/font/dtreefont.css">
</head>

<body>

<div class="layui-fluid">

    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <fieldset class="layui-elem-field layui-field-title">
                <legend>内容列表</legend>
            </fieldset>
            <div class="layui-card-header layuiadmin-card-header-auto">
                <div class="layui-form">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <select name="attribute" lay-filter="attribute">
                                <option value="">全部</option>
                                <{loop $attribute $key $item}>
                                <option value="<{$key}>"><{$item}></option>
                                <{/loop}>
                            </select>
                        </div>
                        <div class="layui-inline">
                            <input type="text" name="query" class="layui-input" placeholder="请输入标题的搜索词"/>
                        </div>
                        <div class="layui-inline">
                            <button class="layui-btn layuiadmin-btn-admin" lay-search lay-filter="form-table">
                                <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                            </button>
                        </div>
                        <div class="layui-inline">
                            <button id="btn_exportSearchContent" class="layui-btn layui-btn-normal exportSearchContent"
                                    lay-submit lay-filter="form-table" lay-tips="导出当前的结果">
                                <i class="layui-icon layui-icon-export layuiadmin-button-btn"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="layui-card-body">
            <table class="layui-table layui-hide PK-table" lay-even id="table_adminList" lay-filter="table_adminList"
                   data-get="Content/AdminGetContent"
                   data-set="Content/AdminSetContent"></table>

        </div>
    </div>
</div>

<script type="text/html" id="table_toolBar">
    <div class="layui-btn-container">
        <button class="layui-btn layui-btn-sm" lay-event="refresh" lay-tips="刷新列表">
            <i class="layui-icon layui-icon-refresh-3"></i>
        </button>
        <button class="layui-btn layui-btn-sm layui-btn-normal" lay-tips="添加新的内容"
                lay-event="open|添加新的内容">
            <i class="layui-icon layui-icon-add-circle"></i>
        </button>
        <button class="layui-btn layui-btn-sm layui-btn-danger" lay-tips="将选中的内容批量删除"
                lay-event="batchDel">
            <i class="layui-icon layui-icon-delete"></i>
        </button>
        <{loop $attribute $key $item}>
        <button class="layui-btn layui-btn-sm"
                lay-event="setParams|Attribute|<{$key}>" lay-tips="将选中的内容设为<{$item}>">
            <i class="iconfont pkcms-icon-<{$key}>"></i>
        </button>
        <{/loop}>
        <button class="layui-btn layui-btn-sm layui-btn-primary"
                lay-event="func|mobileToOtherCategory" lay-tips="将选中的内容移动到……">
            <i class="iconfont pkcms-icon-move"></i>
        </button>
        <button class="layui-btn layui-btn-sm layui-btn-primary"
                lay-event="func|copyToOtherCategory" lay-tips="将选中的内容复制到……">
            <i class="iconfont pkcms-icon-copy"></i>
        </button>
        <button class="layui-btn layui-btn-sm layui-btn-primary"
                lay-event="func|mirroringToOtherCategory" lay-tips="将选中的内容镜像到……">
            <i class="iconfont pkcms-icon-mirroring"></i>
        </button>
    </div>
</script>

<script type="text/html" id="table_tool">
    {{# if (!d.mirroringId) { }}
    <button class="layui-btn layui-btn-xs" lay-tips="修改内容"
            lay-event="open|编辑内容">
        <i class="layui-icon layui-icon-edit"></i>
    </button>
    <a class="layui-btn layui-btn-primary layui-btn-xs" lay-tips="预览内容"
       href="/index.php/content/content?siteId={{ d.siteId }}&catId={{ d.catId }}&contentId={{ d.id }}" target="_blank">
        <i class="iconfont pkcms-icon-view"></i></a>
    {{#  } else { }}
    镜像内容
    {{# } }}
</script>

<script type="text/html" id="urlTpl">
    <a href="{{  d.url  }}" target="_blank">{{  d.url  }}</a>
</script>

<script type="text/html" id="isAttributeTpl">
    {{#  if(d != 'undefined'){ }}
    {{#  if(d.thumb){ }}
    <button type="button" class="layui-btn layui-btn-xs" lay-tips="有缩略图">
        <i class="layui-icon layui-icon-picture"></i>
    </button>
    {{# }else{ }}
    <button class="layui-btn layui-btn-xs layui-btn-disabled" lay-tips="无缩略图">
        <i class="layui-icon layui-icon-picture-fine"></i>
    </button>
    {{#  } }}
    {{#  if(d.toHead == 1){ }}
    <button class="layui-btn layui-btn-xs" lay-tips="已头条">
        <i class="iconfont pkcms-icon-toHead"></i>
    </button>
    {{# }else{ }}
    <button class="layui-btn layui-btn-xs layui-btn-disabled" lay-tips="未头条">
        <i class="iconfont pkcms-icon-toHead"></i>
    </button>
    {{#  } }}
    {{#  if(d.toPush == 1){ }}
    <button class="layui-btn layui-btn-xs" lay-tips="已推荐">
        <i class="iconfont pkcms-icon-toPush"></i>
    </button>
    {{# }else{ }}
    <button class="layui-btn layui-btn-xs layui-btn-disabled" lay-tips="未推荐">
        <i class="iconfont pkcms-icon-toPush"></i>
    </button>
    {{#  } }}
    {{#  if(d.toTop == 1){ }}
    <button class="layui-btn layui-btn-xs" lay-tips="已置顶">
        <i class="iconfont pkcms-icon-toTop"></i>
    </button>
    {{# }else{ }}
    <button class="layui-btn layui-btn-xs layui-btn-disabled" lay-tips="未置顶">
        <i class="iconfont pkcms-icon-toTop"></i>
    </button>
    {{#  } }}
    {{#  } }}
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">
    var data = {
        init: {
            modelId: '<{$modelId}>',
            catId: '<{$catId}>'
        },
        table: {
            attribute: ''

        }
    }, openCategoryTree = function (post_url, post_params, str_msg) {
        var domId = 'openSelectCategoryTree';
        post_params = JSON.parse(post_params);
        PKLayer.dtree(function (dtree) {
            PKLayer.useModel('请选择栏目',
                '<ul id="' + domId + '" class="dtree" data-id="0"></ul>',
                function (index) {
                    dtree.render({
                        elem: "#" + domId,
                        url: "/index.php/Content/AdminGetCategory/ApiByListsOfContent",
                        cache: false,
                        dataStyle: "layuiStyle",
                        type: "all",
                        initLevel: 1,
                        response: {
                            statusCode: 0,
                            message: "msg",
                            treeId: "id",
                            parentId: "parentId",
                            title: "name",
                            childName: "children",
                        }
                    });
                }, function (index) {
                    var param = dtree.getNowParam(domId);
                    console.log('yes param id', param.nodeId);
                    if (param.leaf) {
                        post_params['toCatId'] = param.nodeId;
                        console.log(post_params);
                        PKLayer.apiPost(post_url, post_params,
                            function (apiResult, count) {
                                PKLayer.tipsMsg('转移成功');
                                renderTable();
                            }
                        );
                        layer.close(index);
                    } else {
                        PKLayer.tipsMsg('只能选择子级栏目');
                    }
                });
        });
    };

    function mobileToOtherCategory(api_params) {
        openCategoryTree('Content/AdminSetContent/ApiByMobileToOtherCategory', api_params)
    }

    function copyToOtherCategory(api_params) {
        openCategoryTree('Content/AdminSetContent/ApiByCopyToOtherCategory', api_params)
    }

    function mirroringToOtherCategory(api_params) {
        openCategoryTree('Content/AdminSetContent/ApiByBatMirroringToOtherCategory', api_params)
    }

    function renderTable() {
        PKAdmin.table({
            cols: [[
                {type:'checkbox'},
                {field:'id', title:'ID', width: 60},
                {field:'listSort', title:'排序', width: 60, edit: 'number'},
                {field:'title', title:'标题'},
                {field:'hits', title:'浏量', width: 60, edit: 'number'},
                {field:'url', title:'URL规则', edit: 'text', templet: '#urlTpl'},
                {field:'thumb', title:'内容属性', align: "center", width: 200, templet: '#isAttributeTpl'},
                {
                    title: "操作",
                    align: "center",
                    width: 120,
                    fixed: "right",
                    toolbar: "#table_tool"
                }
            ]]
        }, true);
    }

    function exportSearchContent() {
        console.log('11');
        PKLayer.useFormControlByParams(function (params) {
            console.log('params',params);
            var params_export = Object.assign(data.init, params, {recovery: 0});
            console.log(params_export);
            var url = '/index.php/Content/AdminGetContent/ApiByExport?'+PKCms.JSONToUrlQuery(params_export);
            window.open(url,'_blank');
            return false;
        });
    }

    PKAdmin.ready(function () {
        renderTable();
        layui.use('form', function () {
            var form = layui.form;
            form.on('select(attribute)', function (rows) {
                data.table.attribute = rows.value;
            });
        });
        PKLayer.useFormControlByParams(function (params) {
            console.log('params',params);
            var params_export = Object.assign(data.init, params, {recovery: 0});
            console.log(params_export);
            var url = '/index.php/Content/AdminGetContent/ApiByExport?'+PKCms.JSONToUrlQuery(params_export);
            window.open(url,'_blank');
            return false;
        }, 'lay-submit');
    });
</script>
</body>
</html>
