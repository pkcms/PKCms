<?php
function eachCategory($categoryList)
{
    if (is_array($categoryList)) {
        foreach ($categoryList as $index => $category) {
?>
            <a href="<?php echo $category['url']; ?>" title="<?php echo $category['name']; ?>">
                <?php echo $category['name']; ?></a><br/>
            <?php
            if (isset($category['child'])) {
            ?>

                <div class="layui-card">
                    <div class="layui-card-body">
                        <?php
                        eachCategory($category['child']);
                        ?>
                    </div>
                </div>
            <?php
            }
            ?>
<?php
        }
    }
}

?>
<html lang="zh-cn">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>站点地图</title>
    <link href="/statics/layui/css/layui.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-header layui-bg-blue">栏目：</div>
        <div class="layui-card-body">
            <?php eachCategory(isset($categoryList) ? $categoryList : []); ?>
        </div>
    </div>
    <?php
    if (isset($modelList) && is_array($modelList)) {
        foreach ($modelList as $model) {
            ?>
    <div class="layui-card">
        <div class="layui-card-header layui-bg-blue">
            <?php echo $model['name'] ?>：
        </div>
        <div class="layui-card-body">
            <table class="layui-table">
                <tbody>
                <?php
                if (isset($model['lists']) && is_array($model['lists'])) {
                    foreach ($model['lists'] as $content) {
                        ?>
                        <tr>
                            <td>
                                <a href="<?php echo $content['url']; ?>" title="<?php echo $content['title']; ?>">
                                    <?php echo $content['title']; ?></a>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php
        }
    }
    ?>
</div>

</body>

</html>