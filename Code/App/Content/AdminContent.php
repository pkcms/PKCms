<?php


namespace PKApp\Content;


use PKCommon\Controller\AdminController;

class AdminContent extends AdminController
{

    public function Main()
    {
        if (request()->action() == 'Complex') {
            $this->fetch('content_complex');
        } else {
            $this->fetch('content_init');
        }
    }
}