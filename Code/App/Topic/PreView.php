<?php


namespace PKApp\Topic;


use PKApp\Topic\Classes\TopicSController;

class PreView extends TopicSController
{

    public function Main()
    {
        $this->fetch($this->preView());
    }

}