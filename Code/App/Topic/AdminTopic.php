<?php


namespace PKApp\Topic;


use PKApp\Topic\Classes\TraitTopic;
use PKCommon\Controller\AdminController;
use PKFrame\DataHandler\Numbers;

class AdminTopic extends AdminController
{
    use TraitTopic;

    public function Main()
    {
        $this->fetch('topic');
    }

    public function ApiByLists()
    {
        $siteEntity = $this->serviceOfSite()->interface_getEntityById($this->loginUser()->SiteId, ['Site_Domain', 'Site_Path']);
        $this->getPages();
        $this->serviceOfTopic()->SetSitePath($siteEntity['Site_Path']);
        $this->json(
            $this->serviceOfTopic()->GetList(),
            $this->serviceOfTopic()->Count()
        );
    }

    public function ApiGetDetail()
    {
        $this->json(
            $this->serviceOfTopic()->interface_getEntityById(
                Numbers::To(request()->get('id'))
            )
        );
    }

}