<?php


namespace PKApp\Topic;


use PKApp\Topic\Classes\TraitTopic;
use PKCommon\Controller\AdminController;

class AdminTopicType extends AdminController
{
    use TraitTopic;

    public function Main()
    {
        $this->assign(['topicId' => $this->getId('topicId', 'Empty_TopicId')])
            ->fetch('topic_type');
    }

    public function ApiByLists()
    {
        $params = ['topicId' => $this->getId('topicId', 'Empty_TopicId'),
            'siteId' => $this->loginUser()->SiteId];
        $list = $this->serviceOfTopicType()->GetList($params);
        $this->json($list, count($list));
    }

    public function ApiGetDetail()
    {
        $params = ['id'=>$this->getId('id','Empty_TopicTypeId'),
            'siteId' => $this->loginUser()->SiteId];
        $this->json($this->serviceOfTopicType()->GetEntity($params));
    }

}