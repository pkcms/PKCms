<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
    <link rel="stylesheet" href="/statics/pkcms/icons/iconfont.css" media="all">
</head>

<body>

<div class="layui-fluid">

    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <fieldset class="layui-elem-field layui-field-title">
                <legend>专题分类管理</legend>
                <div class="layui-field-box">
                </div>
            </fieldset>
        </div>

        <div class="layui-card-body">
            <table class="layui-hide PK-table" id="table_adminList" lay-filter="table_adminList"
                   data-get="Topic/AdminTopic" data-set="Topic/adminSetTopic"></table>
        </div>
    </div>
</div>

<script type="text/html" id="table_toolBar">
    <div class="layui-btn-container">
        <button class="layui-btn layui-btn-sm layui-btn-normal" lay-event="add" lay-tips="添加新的广告位">
            <i class="layui-icon layui-icon-add-1"></i>
        </button>
    </div>
</script>

<script type="text/html" id="table_tool">
    <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="edit" lay-tips="修改该专题">
        <i class="layui-icon layui-icon-edit"></i></a>
    <a class="layui-btn layui-btn-primary layui-btn-xs"
       lay-href="/index.php/topic/AdminTopicType?topicId={{ d.id }}">
        <i class="layui-icon layui-icon-list" lay-tips="浏览该专题的分类">专题分类</i></a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del" lay-tips="删除该专题">
        <i class="layui-icon layui-icon-delete"></i></a>
    <a class="layui-btn layui-btn-primary layui-btn-xs"
       href="/index.php/Topic/PreView?siteId={{ d.siteId }}&topicId={{ d.id }}" target="_blank" lay-tips="预览该分类">
        <i class="iconfont pkcms-icon-view"></i></a>
    <a class="layui-btn layui-btn-normal layui-btn-xs" lay-tips="发布该专题"
       lay-event="api|Topic/AdminMakeHtml/apiByCreate">
        <i class="layui-icon layui-icon-release"></i></a>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">
    function renderTable() {
        PKAdmin.table({
            cols: [[
                {field:'id', title:'ID'},
                {field:'title', title:'专题名'},
                {field:'url', title:'静态文件名'},
                {field:'createTime', title:'创建时间',templet:"<div>{{dateFormat(d.createTime)}}</div>"},
                {
                    title: "操作",
                    align: "center",
                    width: 300,
                    fixed: "right",
                    toolbar: "#table_tool"
                }
            ]]
        });
    }

    PKAdmin.ready(function () {
        renderTable();
    });
</script>
</body>
</html>
