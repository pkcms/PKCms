<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
    <link href="/statics/ueditor/themes/default/css/ueditor.min.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" charset="utf-8" src="/statics/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="/statics/ueditor/ueditor.all.min.js"></script>
</head>

<body>

<div class="layui-fluid">

    <div class="layui-fluid">

        <div class="layui-card">
            <div class="layui-card-header layuiadmin-card-header-auto">
                <fieldset class="layui-elem-field layui-field-title">
                    <legend>专题信息</legend>
                </fieldset>
            </div>

            <div class="layui-card-body">
                <form class="layui-form" lay-filter="form-frame">
                    <div id="view-form"></div>

                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer">
                                <button class="layui-btn" lay-submit lay-filter="form-frame">立即提交</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/html" data-id="<{$id}>" data-api="Topic/AdminTopic" id="tpl-form">
    <input type="hidden" name="id" value="{{#  if(d.id){ }}{{  d.id  }}{{#  } }}"/>
    <div class="layui-form-item">
        <label class="layui-form-label">专题名：</label>
        <div class="layui-input-block">
            <input type="text" name="title" class="layui-input" lay-verify="required" autocomplete="off"
                   value="{{#  if(d.title){ }}{{  d.title  }}{{#  } }}"/>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">SEO 标题：</label>
        <div class="layui-input-block">
            <input type="text" name="seoTitle" class="layui-input"
                   value="{{#  if(d.seoTitle != null){ }}{{  d.seoTitle  }}{{#  } }}"/>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">SEO 关键词：</label>
        <div class="layui-input-block">
                    <textarea name="seoKeyword"
                              class="layui-textarea">{{#  if(d.seoKeyword != null){ }}{{  d.seoKeyword  }}{{#  } }}</textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">SEO 描述：</label>
        <div class="layui-input-block">
                    <textarea name="seoDescription"
                              class="layui-textarea">{{#  if(d.seoDescription){ }}{{  d.seoDescription  }}{{#  } }}</textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">专题 banner 图：</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input" style="width: 60%;float: left;"
                   id="banner" name="banner" value="{{#  if(d.banner){ }}{{  d.banner  }}{{#  } }}"/>
            <button type="button" class="layui-btn pkcms_upload" id="uploadBtn_banner" data-field="banner">
                <i class="layui-icon layui-icon-upload"></i>选择
            </button>
            <button type="button" class="layui-btn" onclick="PreViewImage('banner')">预览
            </button>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">专题列表图：</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input" style="width: 60%;float: left;"
                   id="image" name="image" value="{{#  if(d.image){ }}{{  d.image  }}{{#  } }}"/>
            <button type="button" class="layui-btn pkcms_upload" id="uploadBtn_image" data-field="image">
                <i class="layui-icon layui-icon-upload"></i>选择
            </button>
            <button type="button" class="layui-btn" onclick="PreViewImage('image')">预览
            </button>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">专题描述：</label>
        <div class="layui-input-block">
            <textarea class="pkcms_editor" id="content"
                      name="content">{{#  if(d.content){ }}{{  d.content  }}{{#  } }}</textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">专题模板：</label>
        <div class="layui-input-block">
            <div class="pkcms_select" id="fileTpl"
                 data-name="fileTpl"
                 data-value="{{#  if(d.fileTpl != null){ }}{{  d.fileTpl  }}{{#  } }}"
                 data-api="Template/AdminGetTemplate"
                 data-params="folder=content&query=topic"></div>
            <div class="layui-form-mid layui-word-aux">
                请要在模板主题中的 content 目录下手动创建以【topic_】为前缀的模板文件（tpl）
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">静态文件规则：</label>
        <div class="layui-input-block">
            <input type="text" name="fileHtml" class="layui-input" lay-verify="required"
                   autocomplete="off"
                   value="{{#  if(d.fileHtml != null){ }}{{  d.fileHtml  }}{{#  }else{ }}topic[id].htm{{#  } }}"/>
            <div class="layui-form-mid layui-word-aux">
                涉及到参数有（取双引号里面的，不含双引号）：“[id]”是当前专题的ID。
            </div>
        </div>
    </div>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">

    PKAdmin.ready(function () {
        PKAdmin.tplFormBySubmit({});
    });

</script>
</body>
</html>
