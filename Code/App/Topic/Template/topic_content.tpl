<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/pkcms/icons/iconfont.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
</head>

<body>

<div class="layui-fluid" id="view-type">
</div>

<script type="text/html" id="tpl-type">
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <fieldset class="layui-elem-field layui-field-title">
                <legend>{{ d.topic.title }} 专题 -> {{ d.topicType.name }} 分类 -> 内容列表</legend>
            </fieldset>

            <div class="layui-card-body">
                <form class="layui-form" lay-filter="form-search">
                    <div class="layui-form-item layui-row">
                        <div class="layui-col-md3">
                            <label class="layui-form-label-col">
                                选择内容模型
                            </label>
                            <div class="layui-form-select">
                                <div class="pkcms_select" id="modelId" data-name="modelId"
                                     data-api="Model/AdminGetModel" data-params="modelType=content"></div>
                            </div>
                        </div>
                        <div class="layui-col-md3">
                            <label class="layui-form-label-col">
                                检索要添加内容的关键字
                            </label>
                            <div CLASS="">
                                <input type="text" name="query" class="layui-input" placeholder="请输入标题的搜索词"/>
                            </div>
                        </div>
                        <div class="layui-col-md5 layui-col-md-offset1">
                            <label class="layui-form-label-col">
                            </label>
                            <div class="layui-inline">
                                <button class="layui-btn layui-btn-sm layui-btn-normal"
                                        lay-submit lay-filter="form-search">
                                    <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                                    开始检索
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
                <div id="view-table"></div>
                <div id="view_check_context"></div>
            </div>

        </div>

        <div class="layui-card-body">
            <table class="layui-hide PK-table" id="table_adminList" lay-filter="table_adminList"
                   data-get="topic/AdminTopicContent"
                   data-set="topic/AdminSetTopicContent"></table>
        </div>
    </div>
</script>

<script type="text/html" id="table_toolBar">
    <button class="layui-btn layui-btn-sm layui-btn-danger" lay-tips="将选中的内容批量删除"
            lay-event="batchDel">
        <i class="layui-icon layui-icon-delete"></i>
    </button>
    <button class="layui-btn layui-btn-sm layui-btn-syncUrl" lay-tips="同步原内容的URL原则"
            lay-event="syncContentUrl">
        <i class="iconfont pkcms-icon-sync"></i>
    </button>
</script>

<script type="text/html" id="tpl-search">
    <div class="layui-card">
        <div class="layui-card-body layui-row">
            <div class="layui-col-xs8" id="select_content">
            </div>
            <div class="layui-col-xs3">
                <div class="layui-btn layui-btn-normal" id="btn-create"> 确定添加</div>
            </div>
        </div>
    </div>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">
    var data = {
        init: {
            topicId: '<{$topicId}>',
            topicTypeId: '<{$topicTypeId}>'
        },
        topic: {
            id: '<{$topicId}>'
        },
        topicType: {
            id: '<{$topicTypeId}>'
        },
        list_content: [],
        list_select: []
    };

    function renderTable() {
        PKAdmin.table({
            cols: [[
                {type:'checkbox'},
                {field:'id', title:'ID', width: 60},
                {field:'listSort', title:'排序', width: 60, edit: 'number'},
                {field:'contentTitle', title:'内容标题'},
                {field:'contentCatName', title:'原栏目名', width: 180},
                {field:'contentUrl', title:'静态文件', width: 120},
                {field:'createTime', title:'创建时间', width: 180,templet:"<div>{{dateFormat(d.createTime)}}</div>"}
            ]]
        }, true);
    }

    function syncContentUrl() {
        PKLayer.tipsMsg('开始同步……');
        PKLayer.apiGet('Topic/AdminTopicContent/ApiSyncContentUrl',data.init,function () {
            PKLayer.tipsMsg('完成同步');
        });
    }

    PKAdmin.ready(function () {
        PKLayer.apiGet('Topic/AdminTopic/ApiGetDetail', data.topic, function (res_topic, count) {
            data.topic = Object.assign(data.topic, res_topic);
            PKLayer.apiGet('Topic/AdminTopicType/ApiGetDetail', data.topicType, function (res_type, count) {
                data.topicType = Object.assign(data.topicType, res_type);
                PKLayer.useTplCall(data, 'tpl-type', 'view-type', function () {
                    renderTable();
                    PKLayer.useFormByControlAndSubmit('form-search', function (paramsByForm) {
                        PKLayer.useModelByTpl('请选择添加到专题分类的内容', paramsByForm, 'tpl-search', function (index) {
                            PKLayer.apiGet('Content/AdminGetContent/ApiByLists', paramsByForm, function (res, count) {
                                data.list_content = res;
                                let list_select = [];
                                for (let resKey in res) {
                                    list_select.push({name:res[resKey].title,value:resKey});
                                }
                                PKLayer.useFormBySelectFun('#select_content', list_select, null, false, false, function (select_rows) {
                                    data.list_select = [];
                                    for (var selectRow of select_rows) {
                                        data.list_select.push(data.list_content[selectRow.value]);
                                    }
                                });
                            });
                            layui.$('#btn-create').on('click', function () {
                                PKLayer.apiPost('Topic/AdminSetTopicContent', data, function (res, count) {
                                    renderTable();
                                    layer.closeAll();
                                });
                            });
                        });
                    })
                });
            });
        });
    });
</script>
</body>
</html>

