<?php


namespace PKApp\Topic;


use PKApp\Topic\Classes\TraitTopic;
use PKCommon\Controller\AdminController;
use PKFrame\DataHandler\Numbers;

class AdminSetTopicType extends AdminController
{
    use TraitTopic;

    public function Main()
    {
        $this->assign([
            'topicId' => $this->getId('topicId', 'Empty_TopicId'),
            'id' => Numbers::To(request()->get('id')),
        ])->fetch('topic_type_set');
    }

    private function _modelOfTopic(): array
    {
        return [
            'name' => $this->checkPostFieldIsEmpty('name', 'Empty_topicTypeName'),
            'topicId' => $this->postId('topicId', 'Empty_topicId', request()->module()),
            'siteId' => $this->loginUser()->SiteId,
        ];
    }

    public function ApiByCreate()
    {
        $params = $this->_modelOfTopic();
        $params['createTime'] = TIMESTAMP;
        $this->serviceOfTopicType()->Add($params);
        $this->json();
    }

    public function ApiByChange()
    {
        $params = $this->_modelOfTopic();
        $id = $this->postId('id', 'Empty_TopicTypeId');
        $this->serviceOfTopicType()->UpdateById($id, $params);
        $this->serviceOfTopicContent()->Update(['topicTypeId' => $id], ['topicTypeName' => $params['name']]);
        $this->json();
    }

    public function ApiByDel()
    {
        $id = $this->postId('id', 'Empty_TopicTypeId');
        $this->serviceOfTopicType()->HideById($id);
        $this->json();
    }
}