<?php
return [
    'Empty_topicCreate' => array(
        'zh-cn' => '添加到专题的接收数据为空',
        'en' => 'The received data added to the topic is empty'
    ),
    'Empty_Topic' => [
        'zh-cn' => '专题的数据为空',
        'en' => 'The data for the topic is empty'
    ],
    'Empty_TopicId' => [
        'zh-cn' => '专题的ID不能为空',
        'en' => 'The ID of the topic cannot be empty'
    ],
    'Empty_topicTitle' => [
        'zh-cn' => '专题的名称不能为空',
        'en' => 'The name of the topic cannot be empty'
    ],
    'Empty_topicFileTpl' => [
        'zh-cn' => '专题的模板文件不能为空',
        'en' => 'The template file for the theme cannot be empty'
    ],
    'Empty_topicFileHtml' => [
        'zh-cn' => '专题的静态文件名不能为空',
        'en' => 'The static file name of the theme cannot be empty'
    ],
    'Empty_TopicType' => [
        'zh-cn' => '专题分类的数据为空',
        'en' => 'The data for thematic classification is empty'
    ],
    'Empty_TopicTypeId' => [
        'zh-cn' => '专题分类的ID不能为空',
        'en' => 'The ID of the topic classification cannot be empty'
    ],
    'Empty_topicTypeName' => [
        'zh-cn' => '专题分类的名不能为空',
        'en' => 'The name of the topic classification cannot be empty'
    ],
    'Empty_TopicContent' => [
        'zh-cn' => '专题内容的数据为空',
        'en' => 'The data for the topic content is empty'
    ],
    'Empty_TopicContentId' => [
        'zh-cn' => '专题内容的ID不能为空',
        'en' => 'The ID of the topic content cannot be empty'
    ],
];