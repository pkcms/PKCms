<?php


namespace PKApp\Topic;


use PKApp\Topic\Classes\MakeHtml;
use PKCommon\Controller\AdminController;
use PKFrame\DataHandler\Numbers;

class AdminMakeHtml extends AdminController
{

    public function Main()
    {
        $this->apiByCreate();
    }

    protected function apiByCreate()
    {
        request()->get([
            'siteId' => $this->loginUser()->SiteId,
            'topicId' => Numbers::To(request()->get('id'))
        ]);
        $make = new MakeHtml();
        $make->Main();
        $this->json();
    }
}