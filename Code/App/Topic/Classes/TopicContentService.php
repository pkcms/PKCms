<?php


namespace PKApp\Topic\Classes;


use PKFrame\Service;

class TopicContentService extends Service
{

    public function interface_getEntityById($id, $viewField = '*')
    {
        return $this->getEntityById($id, 'Empty_TopicId', 'Empty_Topic', $viewField);
    }

    protected function db(): TopicContentDB
    {
        static $cls;
        !empty($cls) ?: $cls = new TopicContentDB();
        return $cls;
    }

    public function Count($viewParams = []): int
    {
        array_key_exists('isDeleted', $viewParams) ?: $viewParams['isDeleted'] = 0;
        return $this->db()->Where($viewParams)->Count();
    }

    public function GetList($viewParams = [], $viewField = '*', int $rows = 0, int $index = 0)
    {
        $db = $this->db()->Where($viewParams)->OrderBy('listSort')->OrderBy('id');
        if ($rows > 0) {
            $db = $db->Limit($rows, $index);
        }
        return $db->Select($viewField)->ToList();
    }

    public function GetListByIn($viewParams = [], $viewField = '*', int $rows = 0, int $index = 0)
    {
        array_key_exists('isDeleted', $viewParams) ?: $viewParams['isDeleted'] = 0;
        return $this->GetList($viewParams, $viewField, $rows, $index);
    }

    public function GetEntity($viewParams = [], $viewField = '*')
    {
        return $this->db()->Where($viewParams)->Select($viewField)->First();
    }
}