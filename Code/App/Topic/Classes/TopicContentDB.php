<?php


namespace PKApp\Topic\Classes;


use PKFrame\Lib\DataBase;

class TopicContentDB extends DataBase
{

    public function __construct($linkIndex = 0)
    {
        parent::__construct($linkIndex);
        $this->table = 'topic_content';
    }

}