<?php


namespace PKApp\Topic\Classes;


use PKFrame\Service;

class TopicTypeService extends Service
{

    public function interface_getEntityById($id, $viewField = '*')
    {
        return $this->getEntityById($id, 'Empty_TopicId', 'Empty_Topic', $viewField);
    }

    protected function db(): TopicTypeDB
    {
        static $cls;
        !empty($cls) ?: $cls = new TopicTypeDB();
        return $cls;
    }

    public function GetList($viewParams = [], $viewField = '*')
    {
        array_key_exists('isDeleted', $viewParams) ?: $viewParams['isDeleted'] = 0;
        return $this->db()->Where($viewParams)->Select($viewField)->ToList();
    }

    public function GetEntity($viewParams = [], $viewField = '*')
    {
        array_key_exists('isDeleted', $viewParams) ?: $viewParams['isDeleted'] = 0;
        return $this->db()->Where($viewParams)->Select($viewField)->First();
    }
}