<?php


namespace PKApp\Topic\Classes;


use PKApp\Site\Classes\SiteController;
use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\Numbers;

abstract class TopicSController extends SiteController
{
    use TraitTopic;

    protected $params_query;
    protected $topic_file;

    protected function tplParamsOfCat()
    {
        $list_type = $this->serviceOfTopicType()->GetList(array_merge($this->params_query,
            ['topicId' => $this->getIdOfTopic(),]), ['id', 'name']);
        $this->assign(['list_topicType' => $list_type]);
    }

    protected function tplParamsOfTopic(array $entity_topic)
    {
        $tplParams_filter = ['siteId', 'fileHtml', 'fileTpl', 'createTime', 'isDeleted'];
        foreach ($entity_topic as $key => $value) {
            if (in_array($key, $tplParams_filter)) {
                unset($entity_topic[$key]);
            }
        }
        $this->assign($entity_topic);
    }

    protected function getIdOfTopic()
    {
        static $id_topic;
        if (!Numbers::IsId($id_topic)) {
            $id_topic = Numbers::To(request()->get('topicId'));
            Numbers::IsId($id_topic) ?: $this->noticeByJson('Empty_TopicId');
        }
        return $id_topic;
    }

    protected function preView()
    {
        $this->params_query = ['siteId' => $this->getSiteEntity()->Id,];
        $entity_topic = $this->serviceOfTopic()->GetEntity(array_merge($this->params_query,
            ['id' => $this->getIdOfTopic(),]));
        Arrays::Is($entity_topic) ?: $this->noticeByPage('Empty_Topic');
        $this->tplParamsOfTopic($entity_topic);
        $this->tplParamsOfCat();
        $this->view()->SetTplDir('content');
        $this->topic_file = str_replace('[id]', $entity_topic['id'], $entity_topic['fileHtml']);
        return $this->getTplFileOfCustomize($entity_topic['fileTpl']);
    }

}