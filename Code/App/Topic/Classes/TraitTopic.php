<?php


namespace PKApp\Topic\Classes;


trait TraitTopic
{

    protected function serviceOfTopic(): TopicService
    {
        static $cls;
        !empty($cls) ?: $cls = new TopicService();
        return $cls;
    }

    protected function serviceOfTopicType(): TopicTypeService
    {
        static $cls;
        !empty($cls) ?: $cls = new TopicTypeService();
        return $cls;
    }

    protected function serviceOfTopicContent(): TopicContentService
    {
        static $cls;
        !empty($cls) ?: $cls = new TopicContentService();
        return $cls;
    }

}