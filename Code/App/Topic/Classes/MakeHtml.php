<?php

namespace PKApp\Topic\Classes;

defined('PATH_ROOT') ?: die();

class MakeHtml extends TopicSController
{

    public function Main()
    {
        ob_start();
        echo $this->view()->TplDisplay($this->preView());
        $html = ob_get_contents();
        ob_end_clean();
        fileHelper()->PutContents(
            PATH_ROOT . (empty($this->getSiteEntity()->PathSite) ? '' : DS . $this->getSiteEntity()->PathSite),
            $this->topic_file, $html
        );
    }
}