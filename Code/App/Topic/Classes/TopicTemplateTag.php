<?php


namespace PKApp\Topic\Classes;


use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\Numbers;

class TopicTemplateTag extends TopicSController
{

    public function Main()
    {
    }

    public function ListTopic(): array
    {
        $this->serviceOfTopic()->SetSitePath($this->getSiteEntity()->PathSite);
        $list_topic = $this->serviceOfTopic()->GetList(['siteId' => $this->getSiteEntity()->Id],
            ['id', 'title', 'image', 'content', 'fileHtml', 'createTime']);
        return ['listOfTopic' => $list_topic];
    }

    public function ListType($params): array
    {
        $lang_app = 'topic';
        $id_topic = Arrays::GetKey('topicId', $params, 'Empty_TopicId', $lang_app);
        $this->serviceOfTopic()->SetSitePath($this->getSiteEntity()->PathSite);
        $list_topic = $this->serviceOfTopicType()->GetList(['siteId' => $this->getSiteEntity()->Id, 'topicId' => $id_topic]);
        return ['listOfTopicType' => $list_topic];
    }

    public function ListContent($params): array
    {
        $lang_app = 'topic';
        $id_topic = Arrays::GetKey('topicId', $params, 'Empty_TopicId', $lang_app);
        $id_topicType = Arrays::GetKey('topicTypeId', $params, 'Empty_TopicTypeId', $lang_app);
        $rows = Numbers::To(Arrays::GetKey('rows', $params));
        Numbers::IsId($rows) ?: $rows = 10;
        $list_content = $this->serviceOfTopicContent()->GetListByIn(
            ['siteId' => $this->getSiteEntity()->Id, 'topicId' => $id_topic, 'topicTypeId' => $id_topicType],
            ['contentId', 'contentTitle', 'contentImage', 'contentUrl', 'createTime'], $rows);
        return ['listOfTopicContent' => $list_content];
    }

}