<?php


namespace PKApp\Topic;


use PKApp\Content\Classes\TraitContent;
use PKApp\Topic\Classes\TraitTopic;
use PKCommon\Controller\AdminController;
use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\Numbers;

class AdminTopicContent extends AdminController
{
    use TraitTopic, TraitContent;

    public function Main()
    {
        $this->assign([
            'topicId' => $this->getId('topicId', 'Empty_TopicId'),
            'topicTypeId' => $this->getId('topicTypeId', 'Empty_TopicTypeId'),
        ])->fetch('topic_content');
    }

    private function _queryParmas(): array
    {
        return ['topicId' => $this->getId('topicId', 'Empty_TopicId'),
            'topicTypeId' => $this->getId('topicTypeId', 'Empty_TopicTypeId'),
            'siteId' => $this->loginUser()->SiteId];
    }

    public function ApiByLists()
    {
        $params = $this->_queryParmas();
        $count = $this->serviceOfTopicContent()->Count($params);
        $this->getPages();
        $list = $this->serviceOfTopicContent()->GetListByIn($params, '*', self::$pageSize, self::$pageIndex);
        $this->json($list, $count);
    }

    public function ApiSyncContentUrl()
    {
        $params = $this->_queryParmas();
        $list_topicC = $this->serviceOfTopicContent()->GetListByIn($params, '*');
        $list_cat = $this->serviceOfCategory(
            $this->loginUser()->SiteId
        )->GetListById(Arrays::Column($list_topicC, 'contentCatId'));
        $list_modelIdAndCatId = Arrays::Column($list_cat, 'modelId', 'id');
        $list_modelId = Arrays::Column($list_cat, 'modelId');
        $list_contentId = Arrays::Column($list_topicC, 'contentId');
        $list_content = [];
        foreach ($list_modelId as $item_modelId) {
            $list_content[$item_modelId] = $this->serviceOfContent($item_modelId)->GetList(['id' => $list_contentId],
                ['id', 'modelId', 'catId', 'url']);
        }
        if (Arrays::Is($list_topicC) && Arrays::Is($list_content)) {
            $siteEntity = $this->serviceOfSite()->interface_getEntityById($this->loginUser()->SiteId, ['Site_Path']);
            foreach ($list_topicC as $index => $item_topicC) {
                $tmp_content = array_filter($list_content[$item_topicC['contentModelId']],
                    function ($item_con) use ($item_topicC) {
                    return $item_con['id'] == $item_topicC['contentId'] ? $item_con : [];
                    });
                if (Arrays::Is($tmp_content)) {
                    $tmp_content = end($tmp_content);
                    if (!Numbers::IsId($tmp_content['mirroringId'])) {
                        $contentUrl = $this->getContentUrl($list_cat, $tmp_content, $siteEntity);
                    } else {
                        $contentUrl = $tmp_content['url'];
                    }
                    $this->serviceOfTopicContent()->UpdateById($item_topicC['id'],['contentUrl'=>$contentUrl]);
                }
            }
        }
        $this->json();
    }

}