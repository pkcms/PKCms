<?php


namespace PKApp\Topic;


use PKApp\Content\Classes\TraitContent;
use PKApp\Topic\Classes\TraitTopic;
use PKCommon\Controller\AdminController;
use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\Numbers;

class AdminSetTopicContent extends AdminController
{
    use TraitContent, TraitTopic;

    public function Main()
    {
        $topic = request()->post('topic');
        $topicType = request()->post('topicType');
        $list_content = request()->post('list_select');
        $params = ['topicId' => Arrays::GetKey('id', $topic, 'Empty_TopicId'),
            'topicTitle' => Arrays::GetKey('title', $topic, 'Empty_topicTitle'),
            'topicTypeId' => Arrays::GetKey('id', $topicType, 'Empty_TopicTypeId'),
            'topicTypeName' => Arrays::GetKey('name', $topicType, 'Empty_topicTypeName')];
        Numbers::IsId($params['topicId']) ?: $this->noticeByJson('Empty_TopicId');
        Numbers::IsId($params['topicTypeId']) ?: $this->noticeByJson('Empty_TopicTypeId');
        Arrays::Is($list_content) ?: $this->noticeByJson('Empty_TopicContent');
        $list_idContent = Arrays::Column($list_content, 'id');
        $list_idCat = Arrays::Column($list_content, 'catId');
        $list_exists = $this->serviceOfTopicContent()->GetList(
            ['contentId' => $list_idContent, 'topicId' => $params['topicId'], 'topicTypeId' => $params['topicTypeId'],
                'siteId' => $this->loginUser()->SiteId],
            ['topicId', 'id', 'topicTypeId', 'contentId']);
        $list_category = $this->serviceOfCategory(
            $this->loginUser()->SiteId
        )->GetList(['id' => array_unique($list_idCat)]);
        $siteEntity = $this->serviceOfSite()->interface_getEntityById($this->loginUser()->SiteId);
        $list_insert = [];
        foreach ($list_content as $contentEntity) {
            if (Arrays::Is($list_exists)) {
                $tmp_exists = array_filter($list_exists, function ($item_exists) use ($contentEntity) {
                    return $item_exists['contentId'] == $contentEntity['id'] ? $item_exists : [];
                });
                if (Arrays::Is($tmp_exists)) {
                    $tmp_exists = end($tmp_exists);
                    $this->serviceOfTopicContent()->UpdateById($tmp_exists['id'], ['isDeleted' => 0]);
                    continue;
                }
            }
            $categoryEntity = array_filter($list_category, function ($item_category) use ($contentEntity) {
                return $item_category['id'] == $contentEntity['catId'] ? $item_category : [];
            });
            if (Arrays::Is($categoryEntity)) {
                $categoryEntity = end($categoryEntity);
            }
            array_push($list_insert, array_merge($params, [
                'contentTitle' => $contentEntity['title'],
                'contentUrl' => self::UrlPathOfContent(
                    $categoryEntity, $categoryEntity['template'],
                    $contentEntity, $siteEntity['Site_Path']),
                'createTime' => TIMESTAMP,
                'contentId' => $contentEntity['id'],
                'contentImage' => $contentEntity['thumb'],
                'contentModelId' => $contentEntity['modelId'],
                'contentCatId' => $contentEntity['catId'],
                'contentCatName' => $categoryEntity['name'],
                'siteId' => $this->loginUser()->SiteId,
            ]));
        }
        $this->serviceOfTopicContent()->BatchInsert($list_insert);
        $this->json();
    }

    public function ApiByDel()
    {
        $params = ['topicId' => $this->postId('topicId', 'Empty_TopicId'),
            'topicTypeId' => $this->postId('topicTypeId', 'Empty_TopicTypeId')];
        $req_id = request()->post('id');
        $list_id = [];
        foreach ($req_id as $item) {
            !Numbers::IsId($item) ?: $list_id[] = $item;
        }
        $params = array_merge($params, ['id' => $list_id,]);
        $this->serviceOfTopicContent()->HideByParams($params);
        $this->json();
    }

    public function ApiBySortIndex()
    {
        $this->serviceOfTopicContent()->Update(
            ['id' => $this->postId('id', 'Empty_TopicContentId'),
                'topicId' => $this->postId('topicId', 'Empty_TopicId'),
                'topicTypeId' => $this->postId('topicTypeId', 'Empty_TopicTypeId')],
            ['listSort' => Numbers::To(request()->post('sortIndex'))]
        );
        $this->json();
    }
}