<?php

namespace PKApp\Attachment;

use PKApp\Attachment\Classes\AttachmentCtrl;
use PKApp\Attachment\Classes\TraitAttachment;

class AdminUpload extends AttachmentCtrl
{
    use TraitAttachment;

    private $_setting, $_ext_list;
    private $_nowExt;

    public function Main()
    {
        $this->accept = $this->checkPostFieldIsEmpty('accept', 'upload_fileType_empty');
        $this->id_model = request()->post('modelId');
        $this->id_field = request()->post('field');
        if ($this->id_model > 0 && $this->id_field > 0) {
            $this->paramsOfModelField();
        }
        $this->requestFile();
        $params = $this->upload();
        fileHelper()->Uploaded($this->uploadFile_nameTmp, $this->dirPathOfDisk(), $this->save_fileName);
        $this->serviceOfUpload()->Add($params);
        $this->json($params);
    }

}