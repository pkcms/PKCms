<?php

namespace PKApp\Attachment;

use PKApp\Attachment\Classes\AttachmentCtrl;
use PKApp\Attachment\Classes\TraitAttachment;
use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\Convert;
use PKFrame\DataHandler\MatchHelper;

class AdminSaveRemote extends AttachmentCtrl
{

    use TraitAttachment;

    public function Main()
    {
        $this->url_img = trim(request()->post('url_img'));
        $this->checkReqOfPicUrl();
        $info = ['fileName' => $this->getImageNameOfUrl(), 'fileType' => 'images',
            'createTime' => TIMESTAMP, 'updateTime' => TIMESTAMP,];
        $params = array_merge($info, $this->saveRemoteImg());
        $this->serviceOfUpload()->Add($params);
        $this->json($params);
    }

}