<?php


namespace PKApp\Attachment\Classes;


trait TraitAttachment
{

    protected function serviceOfUpload(): UploadService
    {
        static $cls;
        !empty($cls) ?: $cls = new UploadService();
        return $cls;
    }

    protected function allowFilesOfImg()
    {
        static $allow;
        if (empty($allow)) {
            $upload_allowExt = dict('upload_allowExt', 'Attachment');
            $allow = $upload_allowExt['images'];
        }
        return $allow;
    }

}