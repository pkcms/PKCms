<?php


namespace PKApp\Attachment\Classes;


use PKFrame\Lib\DataBase;

class UploadDB extends DataBase
{

    public function __construct()
    {
        parent::__construct();
        $this->table = 'upload';
    }

}