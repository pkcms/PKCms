<?php


namespace PKApp\Attachment\Classes;

use PKFrame\Service;

class UploadService extends Service
{

    public function interface_getEntityById($id, $viewField = '*')
    {
        return $this->getEntityById($id, 'Empty_uploadId', 'Empty_upload', $viewField);
    }

    protected function db(): UploadDB
    {
        static $cls;
        !empty($cls) ?: $cls = new UploadDB();
        return $cls;
    }

    public function Count($viewParams = []): int
    {
        return $this->db()->Where($viewParams)->Count();
    }

    public function GetList($viewParams = [], $viewField = '*', int $rows = 0, int $index = 0)
    {
        return $this->db()->Where($viewParams)->OrderBy('id')
            ->Limit($rows, $index)->Select($viewField)->ToList();
    }

    public function GetEntity($viewParams = [], $viewField = '*')
    {
        return $this->db()->Where($viewParams)->Select($viewField)->First();
    }

}