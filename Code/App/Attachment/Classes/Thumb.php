<?php

namespace PKApp\Attachment\Classes;

use Grafika\Grafika;

class Thumb
{

    private $fromImgFile, $newFile;

    /**
     * 生成缩略图函数
     * @param string $image 图片路径
     * @param int $width 缩略图宽度
     * @param int $height 缩略图高度
     * @param int $narrow 是否等比例
     * @param null $exp 保存的扩展名
     * @param string $smallpic 无图片是默认图片路径
     * @return mixed
     */
    public function Main($image, $width = 135, $height = 135, $narrow = 0, $exp = NULL, $smallpic = 'nopic.gif')
    {
        if (empty($image)) {
            return '/statics/images/nopic.gif';
        }
        if (strpos($image, 'ttp:') > 0) {
            return $image;
        }
        $fileArr = explode('.', $image);
        if (strtolower($fileArr[count($fileArr) - 1]) == 'gif') {
            return $image;
        }
        $imagePath = PATH_ROOT . ltrim(str_replace('/', DS, $image), DS);
        if (!file_exists($imagePath)) {
            return $image;
        }
        $width = \PKFrame\DataHandler\Numbers::Is($width);
        $height = \PKFrame\DataHandler\Numbers::Is($height);

        $arr = explode('/', $image);
        $fileName = explode('.', $arr[count($arr) - 1]);
        unset($arr[count($arr) - 1]);
        $thumbImgFileName = $fileName[0] . '_' . $width . '-' . $height . '.' . (empty($exp) ? strtolower($fileName[1]) : $exp);
        $thumbPath = PATH_ROOT . 'thumb' . DS . $arr[count($arr) - 1] . DS . $thumbImgFileName;
        if (!file_exists($thumbPath)) {
            $this->_make($image, $thumbPath, $width, $height, $narrow);
        }
        return str_replace(DS, '/', str_replace(PATH_ROOT, '/', $thumbPath));
    }

    // 开始制作
    private function _make($fromImgFile, $newFile, $newWidth, $newHeight, $narrow)
    {
        $fromImgPath = PATH_ROOT . str_replace('/', DS, ltrim($fromImgFile, '/'));
        if (file_exists($fromImgPath)) {
            $this->fromImgFile = $fromImgPath;
            list($width, $height) = getimagesize($fromImgPath);
            $this->newFile = $newFile;
            if (($narrow == 1) || (($width == $height) && ($newWidth == $newHeight))) {
                $this->narrow_thumb($newWidth, $newHeight);
            } else {
                if ($width < $height) {
                    $this->maxheight_thumb($newHeight);
                } else {
                    $this->maxwidth_thumb($newWidth);
                }
            }
        }
    }

    /**
     * 等比例缩小性截图
     * @param $new_width
     * @param $new_height
     */
    public function narrow_thumb($new_width, $new_height)
    {
        try {
            $editor = Grafika::createEditor();
            $editor->open($image, $this->fromImgFile);
            $editor->resizeFit($image, $new_width, $new_height);
            $editor->save($image, $this->newFile);
        } catch (\Exception $exception) {
            handlerException($exception);
        }
    }

    /**
     * 按最大高度的区域性截图
     * @param $new_height
     */
    public function maxheight_thumb($new_height)
    {
        try {
            $editor = Grafika::createEditor();
            $editor->open($image, $this->fromImgFile);
            $editor->resizeExactHeight($image, $new_height);
            $editor->save($image, $this->newFile);
        } catch (\Exception $exception) {
            handlerException($exception);
        }
    }

    /**
     * 按最大宽度的区域性截图
     * @param $new_width
     */
    public function maxwidth_thumb($new_width)
    {
        try {
            $editor = Grafika::createEditor();
            $editor->open($image, $this->fromImgFile);
            $editor->resizeExactWidth($image, $new_width);
            $editor->save($image, $this->newFile);
        } catch (\Exception $exception) {
            handlerException($exception);
        }
    }

}
