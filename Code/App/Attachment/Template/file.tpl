<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
    <link href="/statics/ueditor/themes/default/css/ueditor.min.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" charset="utf-8" src="/statics/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="/statics/ueditor/ueditor.all.min.js"></script>
    <style>
        div.cell-pic {
            width: calc(23% - 4px);
            display: inline-block;
            position: relative;
            padding: 8px;
        }

        div.cell-pic:hover {
            box-shadow: 1px 0px 5px 0px #858585;
        }

        .cell-pic .pic-check, .cell-pic .pic-action {
            display: inline-block;
            position: absolute;
            top: 5px;
            z-index: 1;
            color: #eeeeee;
            font-size: 12px;
            line-height: 12px;
            padding: 2px;
            background-color: #2F4056;
        }

        .cell-pic .pic-check {
            left: 4px;
        }

        .cell-pic .pic-action {
            right: 5px;
            top: 3px;
            padding: 4px;
            z-index: 2;
            cursor: pointer;
            background-color: black;
            color: white;
            display: none;
        }

        div.cell-pic:hover .pic-action {
            display: inline-block;
        }

        div.active {
            box-shadow: inset 0 0 0 3px #fff, inset 0 0 0 7px #2271b1;
        }

        div.cell-pic > a {
            display: inline-block;
            text-align: center;
        }

        div.cell-pic .showimg {
            max-width: 100%;
        }
    </style>
</head>
<body>

<div class="layui-fluid">

    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <div class="layui-form">
                <div class="layui-inline">
                    <button type="button" id="PKBtn-Upload" class="layui-btn">
                        上传新的
                    </button>
                </div>
                <div class="layui-inline">
                    <input type="text" id="query" name="query" class="layui-input" placeholder="请输入图片的搜索词"/>
                </div>
                <div class="layui-inline">
                    <button type="button" id="PKBtn-Search" class="layui-btn layui-btn-primary" lay-search>
                        搜索附件
                    </button>
                </div>
                <div class="layui-inline" style="float: right;">
                    <button type="button" id="PKBtn-Sure" class="layui-btn">
                        确定选择
                    </button>
                </div>
            </div>
        </div>

        <div class="layui-card-body layui-card-body-mb">
            <table class="layui-table layui-hide PK-table" lay-even id="table_adminList" lay-filter="table_adminList"
                   data-get="Attachment/AdminFiles"
                   data-set="Attachment/AdminFiles"></table>
        </div>
    </div>
</div>

<script type="text/html" id="table_tool">
    <a class="layui-btn layui-btn-normal layui-btn-xs" id="btnSelect_{{ d.id }}"
       lay-event="func|selectUrl" lay-tips="选择文件">
        <i class="layui-icon layui-icon-circle"></i></a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del" lay-tips="删除该文件">
        <i class="layui-icon layui-icon-delete"></i></a>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">
    var data = {select:{}},
        DISABLED = 'layui-btn-primary layui-disabled',
        field_name = '',
        ScanningImgOfGet = function () {
            $('#piclist').empty();
            setTimeout(function () {
                PKLayer.useTplFlow('Attachment/AdminScanningImg', 20, 'tpl-box', 'piclist');
            }, 500);
        },
        selectUrl = function (params) {
            params = JSON.parse(params);
            let id = params.id;
            let dom_btn = $('#btnSelect_' + id + ' i');
            if (data.select.hasOwnProperty(id)) {
                delete data.select[id];
                dom_btn.removeClass('layui-icon-radio');
                dom_btn.addClass('layui-icon-circle');
            } else {
                data.select[id] = {
                    path: params.url,
                    name: params.fileName,
                    id: id
                };
                dom_btn.addClass('layui-icon-radio');
                dom_btn.removeClass('layui-icon-circle');
            }
        };

    function child(data) {
        console.log('data', typeof data, data);
        var json = JSON.parse(data);
        field_name = json.field_name;
        PKLayer.useUpload(json.field_name, json.modelId, json.field_ext,
            function (is_multiple, accept, res) {
                renderTable();
            });
    }

    function renderTable() {
        PKAdmin.table({
            cols: [[
                {field:'id', title:'ID', width: 60},
                {field:'fileName', title:'文件名'},
                {field:'size', title:'大小'},
                {field:'url', title:'路径', templet: '#urlTpl'},
                {field:'updateTime', title:'上传时间',templet:"<div>{{dateFormat(d.updateTime)}}</div>"},
                {
                    title: "操作",
                    align: "center",
                    width: 120,
                    fixed: "right",
                    toolbar: "#table_tool"
                }
            ]]
        }, true);
    }

    PKAdmin.ready(function () {
        renderTable();
        $('#PKBtn-Sure').on('click', function () {
            // upload list file or image
            var tpl_domId = 'tpl-fileList', className = 'view_' + field_name;
            for (var selectKey in data.select) {
                var selectElement = data.select[selectKey];
                parent.PKLayer.useTplAppend({
                    list_domId: className,
                    field_name: field_name,
                    index: selectElement.id,
                    item_domId: 'fileItem_' + field_name + selectElement.id,
                    file_path: selectElement.path,
                    file_tips: selectElement.name
                }, tpl_domId, 'view_' + field_name);
            }
            setTimeout(function () {
                parent.permutation_uploadList(className);
                parent.layer.closeAll();
            }, 1000);
        });
    });
</script>
</body>
</html>