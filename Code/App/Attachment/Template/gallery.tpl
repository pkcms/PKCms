<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
    <link href="/statics/ueditor/themes/default/css/ueditor.min.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" charset="utf-8" src="/statics/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="/statics/ueditor/ueditor.all.min.js"></script>
    <style>
        li.item-pic {
            padding: 3px;
            background-color: #fff;
        }

        div.cell-pic {
            width: calc(100% - 14px);
            height: 175px;
            line-height: 175px;
            padding: 7px;
            overflow: hidden;
            background-color: #e9edef;
            display: inline-block;
            text-align: center;
            cursor: pointer;
        }

        div.cell-pic:hover {
            box-shadow: inset 0 0 5px 0 #858585;
        }

        .cell-pic .pic-check, .cell-pic .pic-action {
            display: inline-block;
            position: absolute;
            top: 5px;
            z-index: 1;
            color: #eeeeee;
            font-size: 12px;
            line-height: 12px;
            padding: 2px;
            background-color: #2F4056;
        }

        .cell-pic .pic-check {
            left: 4px;
        }

        .item-pic .pic-action {
            position: absolute;
            right: 5px;
            top: 3px;
            padding: 4px;
            z-index: 2;
            cursor: pointer;
            background-color: black;
            color: white;
            display: none;
        }

        li.item-pic:hover .pic-action {
            display: inline-block;
        }

        div.active {
            box-shadow: inset 0 0 0 7px #2271b1;
        }

        div.pic-alt {
            padding: 0 10px;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }

        div.cell-pic img {
            max-width: 100%;
            max-height: 100%;
        }

        .header-action {
            position: fixed;
            top: 0;
            z-index: 9;
            background-color: white;
            width: 95%;
            box-shadow: 1px 1px 8px 1px;
        }
    </style>
</head>
<body>

<div class="layui-fluid">

    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto header-action">
            <div class="layui-form">
                <div class="layui-inline">
                    <button type="button" id="PKBtn-Upload" class="layui-btn" lay-tips="上传新的">
                        上传
                    </button>
                    <button type="button" id="PKBtn-Extract" class="layui-btn" lay-tips="提取远程图片">
                        提取
                    </button>
                    <button type="button" id="PKBtn-S" class="layui-btn" lay-tips="扫描站点的本地图片">
                        扫描
                    </button>
                </div>
                <div class="layui-inline">
                    <input type="text" id="query" name="query" class="layui-input" placeholder="请输入图片的搜索词"/>
                </div>
                <div class="layui-inline">
                    <button type="button" id="PKBtn-Search" class="layui-btn layui-btn-primary">
                        搜索
                    </button>
                </div>
                <button type="button" id="PKBtn-Sure" class="layui-btn" style="float: right;">
                    确定
                </button>
            </div>
        </div>

        <div class="layui-card-body layui-card-body-mb" style="margin-top: 73px">
            <ul id="piclist" class="layui-row"></ul>
        </div>
    </div>
</div>

<script type="text/html" id="tpl-box">
    {{#  layui.each(d.list, function(index, item){ }}
    <li class="layui-col-md3 layui-col-sm4 layui-col-xs6 item-pic">
        <div class="pic-action">
            <div class="pic-action-preview" lay-tips="查看（放大）原图" onclick="previewImg('{{ item.url }}');">
                <i class="layui-icon">&#xe615;</i>
            </div>
            <div class="pic-action-preview" lay-tips="修改备注"
                 onclick="changeImg({{ item.id }}, '{{ item.fileName }}', '{{ item.url }}');">
                <i class="layui-icon">&#xe642;</i>
            </div>
            <div class="pic-action-del" lay-tips="删除图片"
                 onclick="dropImg({{ item.id }}, '{{ item.fileName }}');">
                <i class="layui-icon layui-icon-delete"></i>
            </div>
        </div>
        <div class="cell-pic" id="cellPic{{ item.id }}"
             onclick="selectUrl({{ item.id }}, '{{ item.fileName }}', '{{ item.url }}')">
            <img src="{{ item.url }}">
        </div>
        <div class="pic-alt">
            {{ item.fileName }}
        </div>
    </li>
    {{#  }) }}
</script>

<script type="text/html" id="tpl-thumb">
    {{#  layui.each(d.list, function(index, item){ }}
    <li class="layui-col-md3 layui-col-sm4 layui-col-xs6 item-pic">
        <div class="pic-action">
            <div class="pic-action-del" lay-tips="删除图片"
                 onclick="dropThumb('{{ item.file_name }}','{{ item.file_path }}');">
                <i class="layui-icon layui-icon-delete"></i>
            </div>
        </div>
        <div class="cell-pic">
            <img src="{{ item.url }}">
        </div>
        <div class="pic-alt">
            {{ item.file_name }}
        </div>
    </li>
    {{#  }) }}
</script>

<script type="text/html" id="tpl-change">
    <div class="layui-card">
        <div class="layui-card-body">
            <div class="layui-row">
                <div class="layui-col-sm4 item-pic">
                    <div class="cell-pic">
                        <img src="{{ d.url }}">
                    </div>
                </div>
                <div class="layui-col-sm8">
                    <div class="layui-card">
                        <div class="layui-card-body">
                            <div class="layui-form">
                                <div class="layui-form-item">
                                    <label class="layui-form-label-col">图片描述信息：</label>
                                    <input type="text" class="layui-input" id="input-tips" value="{{ d.tips }}"
                                           placeholder="请输入图片的备注"/>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label-col">操作选项：</label>
                                    <div class="layui-inline">
                                        <button type="button" class="layui-btn" id="btn-saveImg">保存修改</button>
                                        <button type="button" class="layui-btn layui-btn-primary"
                                                onclick="previewImg('{{ d.url }}');">查看（放大）原图
                                        </button>
                                        <button type="button" class="layui-btn layui-btn-danger"
                                                onclick="dropImg({{ d.id }}, '{{ d.tips }}');">删除
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="layui-card">
        <div class="layui-card-header layui-bg-black">相关的缩略图</div>
        <div class="layui-card-body">
            <ul id="list-thumb" class="layui-row"></ul>
        </div>
    </div>
</script>

<script type="text/html" id="tpl-extract">
    <div class="layui-card">
        <div class="layui-card-body">
            <div class="layui-form">
                <div class="layui-form-item">
                    <input type="text" class="layui-input" id="input-extract" placeholder="请输入图片的远程地址"/>
                </div>
            </div>
        </div>
    </div>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">
    var data = {flowParams:{query:''}, img_id:0 },
        DISABLED = 'layui-btn-primary layui-disabled',
        field_name = '',model_parentIdx = 0,
        select,
        is_multiple = function () {
            return parent.$('#view_' + field_name).length === 1;
        },
        dropImg = function (id, file_name) {
            PKLayer.tipsConfirm('是否要删除该图片' + file_name, function () {
                apiScanningImg('dropImg', {id: id}, function (res) {
                    ScanningImgOfGet();
                    layer.closeAll();
                });
            });
        },
        dropThumb = function (file_name, path) {
            PKLayer.tipsConfirmByTwoBtn('是否要删除缩略图：' + file_name,
                    {icon: 3,time: 30000, offset: 't',title:'提示',btn:['确定','取消']},
                function (index) {
                    apiScanningImg('dropThumb', {path: path}, function (res) {
                        scanningThumbOfGet();
                        layer.close(index);
                    });
                }, function () {
                });
        },
        changeImg = function (id, img_tips, img_url) {
            data.img_id = id;
            PKLayer.useModelByTpl('修改图片的描述信息', {
                id: id, tips: img_tips, url: img_url
            }, 'tpl-change', function (m_idx) {
                scanningThumbOfGet();
                $('#btn-saveImg').die().on('click', function () {
                    PKLayer.apiPost('Attachment/AdminScanningImg/SetImg',
                            {id:id,tips:$('#input-tips').val()},
                        function () {
                            ScanningImgOfGet();
                            layer.close(m_idx);
                        });
                });
            });
        },
        previewImg = function (url_img) {
            window.open(url_img, '_blank');
        },
        scanningImg = function (list_dir, count, index) {
            $('#PKBtn-S').text('扫描进度：' + index + ' / ' + count + ' 个');
            apiScanningImg('scanningFile', {dir: list_dir[index]}, function (res) {
                index += 1;
                if (index < count) {
                    scanningImg(list_dir, count, index);
                } else {
                    $('#PKBtn-S').removeClass(DISABLED).text('重新扫描');
                    ScanningImgOfGet();
                    PKLayer.tipsMsg('扫描完毕');
                }
            });
        },
        ScanningImgOfDir = function () {
            PKLayer.tipsMsg('开始扫描……');
            $('#PKBtn-S').addClass(DISABLED).text('准备扫描中。。。');
            apiScanningImg('scanningDir', {}, function (res, count) {
                if (count > 0 && res.hasOwnProperty('list_dir')) {
                    scanningImg(res.list_dir, count, 0);
                } else {
                    PKLayer.tipsMsg('没有发现新的资源');
                    $('#PKBtn-S').removeClass(DISABLED).text('重新扫描');
                }
            });
        },
        ScanningImgOfGet = function () {
            $('#piclist').empty();
            setTimeout(function () {
                PKLayer.useTplFlow('Attachment/AdminScanningImg', 20, 'tpl-box', 'piclist');
            }, 500);
        },
        scanningThumbOfGet = function () {
            PKLayer.apiPost('Attachment/AdminScanningImg/ScanningThumb',{id: data.img_id}, function (res) {
                PKLayer.useTpl({list:res}, 'tpl-thumb', 'list-thumb');
            });
        },
        selectUrl = function (id, name, url) {
            if (is_multiple()) {
                if (typeof select != "object") {
                    select = {};
                }
                if (select.hasOwnProperty(id)) {
                    delete select[id];
                    $('#cellPic' + id).removeClass('active');
                } else {
                    select[id] = {
                        path: url,
                        name: name,
                        id: id
                    };
                    $('#cellPic' + id).addClass('active');
                }
            } else {
                // 单选
                $('.cell-pic').removeClass('active');
                $('#cellPic' + id).addClass('active');
                select = select === url ? '' : url;
            }
        },
        apiScanningImg = function (action, params, fn) {
            PKLayer.apiPost('Attachment/AdminScanningImg/' + action, params, fn);
        };

    function child(data) {
        console.log('data', typeof data, data);
        var json = JSON.parse(data);
        field_name = json.field_name;
        model_parentIdx = json.model_idx;
        PKLayer.useUpload(json.field_name, json.modelId, null,
            function (is_multiple, accept, res) {
                ScanningImgOfGet();
            });
    }

    PKAdmin.ready(function () {
        ScanningImgOfGet();
        $('#PKBtn-Extract').die().on('click', function () {
            PKLayer.useModelByTplBtn('提取远程图片', 'tpl-extract', function (m_index) {
                //
            }, function (m_index) {
                var params = {
                    url_img: $('#input-extract').val()
                };
                PKLayer.apiPost('/Attachment/AdminSaveRemote', params, function (res) {
                    ScanningImgOfGet();
                    layer.close(m_index);
                });
            });
        });
        $('#PKBtn-S').die().on('click', function () {
            PKLayer.tipsConfirm('请确认被扫描的图片文件已经放置在站点根目录的 upload 的子文件夹内了', function () {
                ScanningImgOfDir();
            });
        });
        $('#PKBtn-Search').on('click', function () {
            data.flowParams.query = $('#query').val();
            ScanningImgOfGet();
        });
        $('#PKBtn-Sure').on('click', function () {
            if (is_multiple()) {
                // upload list file or image
                var tpl_domId = 'tpl_imageList', className = 'view_' + field_name;
                for (var selectKey in select) {
                    var selectElement = select[selectKey];
                    parent.PKLayer.useTplAppend({
                        list_domId: className,
                        field_name: field_name,
                        index: selectElement.id,
                        item_domId: 'imageItem_' + field_name + selectElement.id,
                        img_src: selectElement.path,
                        img_tips: selectElement.name
                    }, tpl_domId, 'view_' + field_name);
                }
                setTimeout(function () {
                    parent.permutation_uploadList(className);
                    parent.layer.close(model_parentIdx);
                }, 1000);
            } else {
                // upload once pic
                parent.$('#' + field_name).val(select);
                setTimeout(function () {
                    console.log('model_parentIdx',model_parentIdx);
                    parent.layer.close(model_parentIdx);
                }, 500);
            }
        });
    });
</script>
</body>
</html>