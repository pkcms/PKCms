<?php


namespace PKApp\Attachment;


use PKApp\Attachment\Classes\AttachmentCtrl;
use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\Str;

class AdminScanningImg extends AttachmentCtrl
{

    public function Main()
    {
        switch (request()->action()) {
            case 'ScanningDir':
                $this->scanningChildrenDir();
                break;
            case 'ScanningFile':
                $this->scanningFile();
                break;
            case 'ScanningThumb':
                $this->scanningThumb();
                break;
            case 'SetImg':
                $this->setImg();
                break;
            case 'DropImg':
                $this->dropFile();
                break;
            case 'DropThumb':
                $this->dropThumb();
                break;
            default:
                $this->getList('images');
                break;
        }
    }

    protected function dropThumb()
    {
        $path = trim(request()->post('path'));
        !empty($path) ? $path = urldecode($path) : $this->noticeByJson('Empty_url');
        fileHelper()->UnLink($path);
        $this->inputSuccess();
    }

    protected function setImg()
    {
        $id = $this->postId('id', 'Empty_id');
        $this->serviceOfUpload()->interface_getEntityById($id);
        $tips = trim(request()->post('tips'));
        !empty($tips) ?: $this->noticeByJson('Empty_imgTips');
        $this->serviceOfUpload()->UpdateById($id, ['fileName' => $tips]);
        $this->inputSuccess();
    }

    protected function scanningThumb()
    {
        $id = $this->postId('id', 'Empty_id');
        $entity = $this->serviceOfUpload()->interface_getEntityById($id);
        $url_arr = explode('/', ltrim($entity['url'], '/'));
        $url_arr_count = count($url_arr);
        $name_file = $url_arr[$url_arr_count - 1];
        $search_fileName = substr($name_file, 0, strpos($name_file, '.'));
        $list_file = fileHelper()->OpenFolder(
            PATH_ROOT . 'thumb' . DS . $url_arr[$url_arr_count - 2],
            null, $search_fileName);
        if (Arrays::Is($list_file)) {
            foreach ($list_file as $index => $item) {
                $list_file[$index]['url'] = '/' . str_replace(
                        str_replace(DS, '/', PATH_ROOT), '', urldecode($item['file_path']));
            }
        }
        $this->json($list_file);
    }

    protected function scanningFile()
    {
        $dir_name = str_replace('/', DS, $this->checkPostFieldIsEmpty('dir', 'Empty_dir'));
        $list_file = fileHelper()->OpenFolder($this->dirPathOfDisk(true) . $dir_name, ['jpg', 'gif', 'png']);
        $list_view = $list_push = [];
        if (Arrays::Is($list_file)) {
            foreach ($list_file as $item) {
                $path_disk = str_replace('/', DS, urldecode($item['file_path']));
                if (Str::IsExistsInChinese($item['file_name']) || stristr($item['file_name'], ' ')) {
                    $path_disk = fileHelper()->Rename($path_disk, $this->renameFileOfRand());
                }
                $md5_file = md5_file($path_disk);
                $disk_path = str_replace(PATH_ROOT, '', $path_disk);
                $url_path = '/' . str_replace(DS, '/', $disk_path);
                $list_view[] = $url_path;
                if (array_key_exists($md5_file, $list_push)) {
                    $list_push[$md5_file]['fileName'] = '/' . $item['file_name'];
                } else {
                    $list_push[$md5_file] = ['md5' => $md5_file, 'url' => $url_path, 'fileType' => 'images',
                        'createTime' => $item['c_time'], 'updateTime' => $item['m_time'],
                        'fileName' => $item['file_name'], 'size' => $item['size']];
                }
            }
        }
        // 查询存在性
        if (Arrays::Is($list_push)) {
            $list_exists = $this->serviceOfUpload()->GetList(['url' => $list_view], ['md5'], count($list_push));
            if (Arrays::Is($list_exists)) {
                foreach ($list_exists as $item_exists) {
                    unset($list_push[$item_exists['md5']]);
                }
            }
        }
        if (Arrays::Is($list_push)) {
            $push = [];
            foreach ($list_push as $key => $arr) {
                array_push($push, $arr);
            }
            $this->serviceOfUpload()->Add($push);
        }
        $this->json(['dir' => $dir_name, 'listFile' => $list_file]);
    }

    protected function scanningChildrenDir()
    {
        $path_upload = $this->dirPathOfDisk(true);
        $list_dir = fileHelper()->OpenFolder($path_upload);
        $index = array_search('image', $list_dir);
        if (Arrays::Is($list_dir)) {
            foreach ($list_dir as $index => $item) {
                if (Str::IsExistsInChinese($item)) {
                    $list_dir[$index] = $this->renameDirOfRand();
                    fileHelper()->Rename($path_upload . $item, $list_dir[$index]);
                }
            }
        }
        if (is_numeric($index)) {
            $list_dir_uEditor = fileHelper()->OpenFolder($path_upload . 'image');
            if (Arrays::Is($list_dir_uEditor)) {
                foreach ($list_dir_uEditor as $index_uEditor => $item) {
                    if ($index_uEditor == 0) {
                        $list_dir[$index] = 'image/' . $item;
                    } else {
                        array_push($list_dir, 'image/' . $item);
                    }
                }
            }
        }
        $this->serviceOfUpload()->Truncate();
        $this->json(['list_dir' => $list_dir], count($list_dir));
    }

}