<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return [
    'Empty_uploadId' => [
        'zh-cn' => '上传数据的id不能为空',
        'en' => 'The ID of the uploaded data cannot be empty'
    ],
    'Empty_upload' => [
        'zh-cn' => '上传的数据为空，或者查找不到',
        'en' => 'The uploaded data is empty or cannot be found'
    ],
    'Empty_dir' => [
        'zh-cn' => '上传的目录不能为空',
        'en' => 'The uploaded directory cannot be empty'
    ],
    'Empty_act' => [
        'zh-cn' => '操作方式不能为空',
        'en' => 'Operation method cannot be empty'
    ],
    'Empty_id' => [
        'zh-cn' => '资源的ID不能为空',
        'en' => 'The ID of the resource cannot be empty'
    ],
    'Empty_url' => [
        'zh-cn' => '请输入图片地址！',
        'en' => ''
    ],
    'Empty_imgTips' => [
        'zh-cn' => '图片的描述信息不能为空',
        'en' => 'Please enter the image address'
    ],
    'tmp_nameEmpty' => [
        'zh-cn' => '找不致临时缓存文件',
        'en' => 'Temporary cache file not found'
    ],
    'tmp_extensionError' => [
        'zh-cn' => '请检查上传文件的类型',
        'en' => 'Please check the type of uploaded file'
    ],
    'Error_MIME' => [
        'zh-cn' => '上传文件的MIME类型不合法，为安全起见请上传合法的文件',
        'en' => 'The MIME type of the uploaded file is illegal. For security reasons, please upload a legal file'
    ],
    'upload_fileType_empty' => [
        'zh-cn' => '找不到允许上传文件的类型列表',
        'en' => 'Unable to find the list of types allowed to upload files'
    ],
    'upload_fileErr_empty' => [
        'zh-cn' => '没有找到安全的文件扩展名',
        'en' => 'No secure file extension found'
    ],
    'ERROR_FILE_NOT_FOUND' => [
        'zh-cn' => '找不到上传文件',
        'en' => 'Unable to find the uploaded file'
    ],
    'ERROR_SIZE_EXCEED' => [
        'zh-cn' => '文件大小超出网站限制',
        'en' => 'File size exceeds website limit'
    ],
    'ERROR_UNKNOWN' => [
        'zh-cn' => '未知错误',
        'en' => 'unknown error'
    ],
    'ERROR_TMP_FILE_NOT_FOUND' => [
        'zh-cn' => '找不到临时文件',
        'en' => 'Unable to find temporary file'
    ],
    'ERROR_TYPE_NOT_ALLOWED' => [
        'zh-cn' => '文件类型不允许',
        'en' => 'File type not allowed'
    ],
    'ERROR_urlOfImg' => [
        'zh-cn' => '请输入有效的图片地址！',
        'en' => 'Please enter a valid image address!'
    ],
    'ERROR_urlOfState' => [
        'zh-cn' => '链接不可用',
        'en' => 'link is unavailable'
    ],
    'ERROR_urlOfContentType' => [
        'zh-cn' => '链接contentType不正确',
        'en' => 'Incorrect link contentType'
    ],
    'ERROR_urlOfWeChat' => [
        'zh-cn' => '微信（公众号）图片无权限提取',
        'en' => 'No permission to extract WeChat (official account) pictures'
    ],
    'uploadFail_formSize' => [
        'zh-cn' => '上传文件的大小不能超过服务器要求的大小',
        'en' => 'The size of the uploaded file cannot exceed the size required by the server'
    ],
    'uploadFail_partial' => [
        'zh-cn' => '文件上传不完整（可能因为请求时间过长被终止）',
        'en' => 'Incomplete file upload (possibly terminated due to request taking too long)'
    ],
    'uploadFail_noFile' => [
        'zh-cn' => '没有获取到上传的文件流',
        'en' => ''
    ],
    'uploadFail_noTmpDir' => [
        'zh-cn' => '在php.ini中没有指定临时文件夹',
        'en' => 'Failed to obtain the uploaded file stream'
    ],
    'saveFail' => [
        'zh-cn' => '写入文件内容错误',
        'en' => 'Error writing file content'
    ],
];