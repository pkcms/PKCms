<?php
return [
    'upload_allowExt' => [
        'images' => ['jpg', 'gif', 'png', 'jpeg'],
        'file' => [
            'zip', 'doc', 'xls', 'ppt', 'docx', 'xlsx', 'pptx', 'pdf',
        ],
    ],
    'File_EXT_MIME' => [
        'gif' => ['image/gif'],
        'jpeg' => ['image/jpeg'],
        'jpg' => ['image/jpeg', 'image/pjpeg'],
        'png' => ['image/png', 'image/x-png'],
        'doc' => ['application/msword'],
        'docx' => ['application/vnd.openxmlformats-officedocument.wordprocessingml.document'],
        'pdf' => ['application/pdf'],
        'ppt' => ['application/vnd.ms-powerpoint'],
        'pptx' => ['application/vnd.openxmlformats-officedocument.presentationml.presentation'],
        'xls' => ['application/vnd.ms-excel'],
        'xlsx' => ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
        'zip' => ['application/x-zip-compressed', 'application/zip'],
    ],
];