<?php

namespace PKApp\Attachment;


use PKApp\Attachment\Classes\AttachmentCtrl;

class AdminFiles extends AttachmentCtrl
{

    public function Main()
    {
        $this->fetch('file');
    }

    public function ApiByLists()
    {
        $this->getList('file');
    }

    public function ApiByDel()
    {
        $this->dropFile();
    }
}