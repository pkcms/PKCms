<?php


namespace PKApp\Attachment;


use PKCommon\Controller\AdminController;

class AdminGallery extends AdminController
{

    public function Main()
    {
        $this->fetch('gallery');
    }
}