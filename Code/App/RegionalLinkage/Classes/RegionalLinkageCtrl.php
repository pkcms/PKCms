<?php


namespace PKApp\RegionalLinkage\Classes;


use PKFrame\Controller;

abstract class RegionalLinkageCtrl extends Controller
{

    use TraitRegionalLinkage;
}