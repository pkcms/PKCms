<?php


namespace PKApp\RegionalLinkage\Classes;


use PKFrame\Lib\DataBase;

class AreaDataBase extends DataBase
{

    public function __construct()
    {
        parent::__construct();
        $this->table = 'area';
    }


}