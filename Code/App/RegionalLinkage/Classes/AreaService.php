<?php


namespace PKApp\RegionalLinkage\Classes;


use PKFrame\Service;

class AreaService extends Service
{

    public function interface_getEntityById($id, $viewField = '*')
    {
    }

    protected function db(): AreaDataBase
    {
        static $cls;
        !empty($cls) ?: $cls = new AreaDataBase();
        return $cls;
    }

    public function GetList($viewParams = [], $viewField = '*')
    {
        return $this->db()->Where($viewParams)->Select($viewField)->ToList();
    }

    public function GetEntity($viewParams = [], $viewField = '*')
    {
    }
}