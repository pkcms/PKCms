<?php


namespace PKApp\RegionalLinkage\Classes;


trait TraitRegionalLinkage
{

    protected function serviceOfArea(): AreaService
    {
        static $cls;
        !empty($cls) ?: $cls = new AreaService();
        return $cls;
    }

}