<?php

namespace PKApp\RegionalLinkage;

use PKApp\RegionalLinkage\Classes\RegionalLinkageCtrl;

class ApiGetAreaListByParent extends RegionalLinkageCtrl
{

    public function Main()
    {
        $parentCode = $this->getId('parentCode');
        $list = $this->serviceOfArea()->GetList(['parentCode' => $parentCode]);
        $this->json($list, count($list));
    }
}