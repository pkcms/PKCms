<?php


namespace PKApp\RegionalLinkage;


use PKApp\RegionalLinkage\Classes\TraitRegionalLinkage;
use PKCommon\Controller\AdminController;

class AdminGetAreaList extends AdminController
{
    use TraitRegionalLinkage;

    public function Main()
    {
        $parentCode = $this->getId('parentCode');
        $list = $this->serviceOfArea()->GetList(['parentCode' => $parentCode]);
        $this->json($list, count($list));
    }
}