<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return array(
    'DownLoad_FileError' => array(
        'zh-cn' => '您要下载的文件已经找不到，或者是已不存在服务器上',
        'en' => 'The file you are trying to download cannot be found or no longer exists on the server'
    ),
    'DownLoad_FileNoExists' => array(
        'zh-cn' => '请提供下载文件的路径，不能为空',
        'en' => 'Please provide the path to download the file, which cannot be empty'
    ),
);