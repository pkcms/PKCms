<?php


namespace PKApp\Tools\Classes;


use PKCore\Request;
use PKCore\Tpl;

class ToolsTemplateTag
{

    public function CaptchaCode()
    {
        \request()->session('authCodeType', 'form');
        $tpl = new Tpl();
        return $tpl->SetTplDir('tools')->PhpDisplay('CaptchaCode');
    }

}