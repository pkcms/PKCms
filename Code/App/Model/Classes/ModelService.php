<?php


namespace PKApp\Model\Classes;

use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\MatchHelper;
use PKFrame\Service;

class ModelService extends Service
{

    protected $moduleName = 'Model';
    protected static $modelEntity_List = [];
    protected static $uniqueTitle_list = [];

    public function GetEntityByTableName($name)
    {
        return $this->GetEntity(['tableName' => $name]);
    }

    public function GetUniqueTitle(int $model_id)
    {
        return self::$uniqueTitle_list[$model_id];
    }

    public function GetTableNameByModelId($modelId): string
    {
        $modelEntity = $this->interface_getEntityById($modelId);
        if (array_key_exists('setting', $modelEntity)) {
            self::$uniqueTitle_list[$modelId] = array_key_exists('uniqueTitle', $modelEntity['setting']);
        }
        if (
            array_key_exists('modelType', $modelEntity)
            && array_key_exists('tableName', $modelEntity)
        ) {
            return $modelEntity['modelType'] . '_' . $modelEntity['tableName'];
        }
        return '';
    }

    public function CheckTableNameAndCreate($typeEnName, $tableName)
    {
        // 合法性字符
        MatchHelper::MatchFormat('letter', $tableName, 'Model_NameError_english');
        $modelName_prohibit = dict('EnName_prohibit');
        !in_array($tableName, $modelName_prohibit) ?: $this->noticeByJson('NameError_prohibit');
        // 查模型表
        $entity = $this->GetEntityByTableName($tableName);
        !Arrays::Is($entity) ?: $this->noticeByJson('Model_TableNameExists');
        // 提取模型类型
        $list_type = dict('modelTypeLists', 'model');
        array_key_exists($typeEnName, $list_type) ?: $this->noticeByJson('Model_TypeEmpty');
        $tableName_detail = $typeEnName . '_' . $tableName;
        // 从实体中查询
        !$this->db()->CheckTableNameExists($tableName_detail) ?: $this->noticeByJson('Model_TableNameExists');
        // 创建实体
        $this->db()->doSQL($typeEnName, $tableName_detail);
    }

    protected function db(): ModelDatabase
    {
        return new ModelDatabase();
    }

    public function GetEntity($viewParams = [], $viewField = '*')
    {
        $entity = $this->db()->Where($viewParams)->Select($viewField)->First();
        if (!in_array(request()->action(), ['ApiImportModelByCheckField', 'ApiByCreate'])) {
            Arrays::Is($entity) ?: $this->noticeByJson('Model_NoExists');
            if (array_key_exists('setting', $entity)) {
                $entity['setting'] = Arrays::Unserialize($entity['setting']);
            }
        }
        return $entity;
    }

    public function GetCount($type_model = 'content'): int
    {
        return $this->db()->Where(['modelType' => $type_model])->Count();
    }

    public function GetList($viewParams = [], $viewField = '*')
    {
        return $this->db()->Where($viewParams)->Select($viewField)->ToList();
    }

    public function GetListByType($type, $viewField = '*')
    {
        return $this->GetList(['modelType' => $type], $viewField);
    }

    public function interface_getEntityById($id, $viewField = '*')
    {
        // 检查模型是否已经存在
        if (array_key_exists($id, self::$modelEntity_List)) {
            return self::$modelEntity_List[$id];
        }
        self::$modelEntity_List[$id] = $this->getEntityById(
            $id, 'Model_IdEmpty', 'Model_NoExists', $viewField
        );
        return self::$modelEntity_List[$id];
    }

    public function isContentOfModel(int $id_model)
    {
        $entity_model = $this->interface_getEntityById($id_model);
        $entity_model['modelType'] == 'content' ?:
            $this->noticeByJson('modelError_content', 'content', ', id:' . $id_model);
    }

}