<?php


namespace PKApp\Model\Classes;

use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\Auth;
use PKFrame\DataHandler\MatchHelper;
use PKFrame\DataHandler\Numbers;

class ToOutData
{

    protected $service;
    protected $service_options;
    public $pageSize = 0;
    public $list_translate = [];
    public $list_translate_field = [];

    public function __construct()
    {
        $this->service = new FieldService();
        $this->service_options = new OptionsService();
        $this->list_translate = [];
        $this->list_translate_field = [];
    }

    public function Get($dataList, $modelId, $filterField = NULL): array
    {
        $this->pageSize = 0;
        $fieldLists = $this->service->GetList(['modelId' => $modelId]);
        if (!Arrays::Is($fieldLists)) {
            return [];
        }
        $filterField_array = !empty($filterField) ? explode(',', $filterField) : [];
        foreach ($fieldLists as $item) {
            $field = $item['field'];
            if (count($filterField_array) && in_array($field, $filterField_array)) {
                continue;
            }
            if (array_key_exists($field, $dataList) && !boolval($item['isShow'])) {
                unset($dataList[$field]);
                continue;
            }
            $func = $item['formType'];
            $value = (is_array($dataList) && array_key_exists($field, $dataList))
                ? $dataList[$field] : '';
            $dataList[$field] = method_exists($this, $func) ? $this->{$func}($item, $value) : '';
        }
        return $dataList;
    }

    protected function editor($fieldEntity, $value)
    {
        $page_break_tag = '<hr/>';
        $value = Auth::HtmlspecialcharsDeCode($value);
        // 内容分页
        if (stristr($value, $page_break_tag)) {
            $value_arr = explode($page_break_tag, $value);
            $this->pageSize = count($value_arr);
        }
        if (in_array(request()->controller(), ['Content', 'AdminMakeHtml']) && isset($value_arr)) {
            $page = max(Numbers::To(request()->get('page')) - 1, 0);
            if (0 <= $page && $page < $this->pageSize) {
                return $value_arr[$page];
            }
        }
        // 整理要被翻译的
        if (in_array(request()->controller(), ['AdminImport','AdminImportOfSite'])) {
            $this->list_translate_field[] = $fieldEntity['field'];
            if (isset($value_arr)) {
                $this->list_translate[$fieldEntity['field']] = $value_arr;
            } else {
                $this->list_translate[$fieldEntity['field']] = $value;
            }
        }
        return $value;
    }

    protected function textarea($fieldEntity, $value)
    {
        // value 回车处理
        $replace = array("\r\n", " ");
        $search = array("<br/>", '&nbsp;');
        return str_replace($search, $replace, $value);
    }

    protected function frontend_code($fieldEntity, $value): string
    {
        return htmlspecialchars_decode($value, ENT_QUOTES);
    }

    protected function text($fieldEntity, $value)
    {
        return $value;
    }

    protected function number($fieldEntity, $value)
    {
        return Numbers::To($value);
    }

    protected function upload_list($fieldEntity, $value): array
    {
        return Arrays::Unserialize($value);
    }

    protected function image_list($fieldEntity, $value): array
    {
        $value = Arrays::Unserialize($value);
        $i = 0;
        $result = [];
        if (Arrays::Is($value)) {
            foreach ($value as $index => $item) {
                $item['i'] = $i;
                $result[] = $item;
                $i += 1;
            }
        }
        return $result;
    }

    protected function baidu_map($fieldEntity, $value)
    {
        return $value;
    }

    protected function option($fieldEntity, $value): string
    {
        $result = '';
        $setting = Arrays::Unserialize($fieldEntity['setting']);
        if (is_array($setting) && array_key_exists('boxType', $setting)) {
            //选项类型
            switch ($setting['boxType']) {
                case 'radio':
                    if (request()->controller() == 'AdminGetContent') {
                        $result = $value;
                    } elseif (Numbers::IsId($value)) {
                        $optionEntity = $this->service_options->interface_getEntityById($value, ['name']);
                        $result = $optionEntity['name'];
                    }
                    break;
                case 'checkbox':
                    if (request()->controller() == 'AdminGetContent') {
                        $result = $value;
                    } elseif (MatchHelper::checkCheckBox($value)) {
                        $valueList = explode(',', $value);
                        $optionEntity = $this->service_options->GetListByIdList($valueList, ['name']);
                        if (is_array($optionEntity)) {
                            $optionEntity = Arrays::GetArrayByKey($optionEntity, 'name');
                            $result = implode(',', $optionEntity);
                        }
                    }
                    break;
            }
        }
        return $result;
    }
}
