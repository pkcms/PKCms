<?php


namespace PKApp\Model\Classes;


use PKFrame\Service;

class OptionsService extends Service
{

    protected function db()
    {
        return new OptionsDataBase();
    }

    public function GetList($viewParams = [], $viewField = ['id', 'parentId', 'name'])
    {
        return $this->db()
            ->Where($viewParams)
            ->Select($viewField)
            ->ToList();
    }

    public function GetListByIdList($idList, $viewField = '*')
    {
        return $this->GetList(['id' => $idList], $viewField);
    }

    public function GetEntity($viewParams = [], $viewField = '*')
    {
        return $this->db()->Where($viewParams)->Select($viewField)->First();
    }

    public function interface_getEntityById($id, $viewField = '*')
    {
        return $this->getEntityById($id, 'Options_IdEmpty', 'Options_Empty', $viewField);
    }
}