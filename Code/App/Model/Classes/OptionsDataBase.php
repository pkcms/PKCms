<?php


namespace PKApp\Model\Classes;

use PKFrame\DataHandler\Arrays;
use PKFrame\Lib\DataBase;

class OptionsDataBase extends DataBase
{
    protected $table = 'model_options';

}