<?php

namespace PKApp\Model\Classes;

use PKFrame\Lib\DataBase;

class ModelDatabase extends DataBase
{
    protected $table = 'model';

    public function doSQL($modelType, $replaceTableName)
    {
        $nowConfig = $this->GetNowConfig();
        if (is_array($nowConfig) && array_key_exists('type', $nowConfig)) {
            $path = PATH_APP . 'Install' . DS . 'SQL' . DS . 'Model' . DS;
            switch ($nowConfig['type']) {
                case 'sqlite':
                    $path .= 'sqlite.model.' . $modelType . '.sql';
                    break;
                case 'mysql':
                    $path .= 'mysql.model.' . $modelType . '.sql';
                    break;
            }
            $this->sql = [str_replace(
                '[tableName]', $this->getTableName($replaceTableName),
                fileHelper()->GetContentsByDisk($path, 'SQLFile_Exists'))];
            $this->Exec();
        }
    }
}
