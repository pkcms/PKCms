<?php


namespace PKApp\Model\Classes;

use PKFrame\Controller;
use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\Numbers;

class ModelTemplateTag extends Controller
{
    use TraitModel;

    public function options($params_tpl): array
    {
        $id_options = Arrays::GetKey('id', $params_tpl, 'Options_IdEmpty', 'model');
        Numbers::IsId($id_options) ?: $this->noticeByPage('Options_IdEmpty', 'model');
        $lists = $this->serviceOfOptions()->GetList(['parentId' => $id_options], ['id', 'name']);
        return [
            'optionList' => $lists,
        ];
    }

    public function model(): array
    {
        $lists = $this->serviceOfModel()->GetListByType('content', ['id', 'name']);
        return [
            'modelList' => (Arrays::Column($lists, 'name', 'id')),
        ];
    }

    public function Main()
    {
    }
}
