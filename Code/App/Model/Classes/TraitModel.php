<?php


namespace PKApp\Model\Classes;


trait TraitModel
{

    protected function serviceOfModel(): ModelService
    {
        static $cls;
        !empty($cls) ?: $cls = new ModelService();
        return $cls;
    }

    protected function serviceOfField(): FieldService
    {
        static $cls;
        !empty($cls) ?: $cls = new FieldService();
        return $cls;
    }

    protected function serviceOfOptions(): OptionsService
    {
        static $cls;
        !empty($cls) ?: $cls = new OptionsService();
        return $cls;
    }

    protected function modelOfToInsertData(): ToInsertData
    {
        static $cls;
        !empty($cls) ?: $cls = new ToInsertData();
        return $cls;
    }

    public function modelOfToOutData(): ToOutData
    {
        static $cls;
        !empty($cls) ?: $cls = new ToOutData();
        return $cls;
    }

}