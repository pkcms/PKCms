<?php


namespace PKApp\Model\Classes;

use PKFrame\DataHandler\Arrays;
use PKFrame\Lib\DataBase;

class FieldDateBase extends DataBase
{

    protected $table = 'model_field';
    protected $config;

    public function __construct()
    {
        parent::__construct();
        $this->config = $this->GetNowConfig();
    }

    public function CheckFieldExists($tableName, $fieldName): bool
    {
        $is_exists = false;
        $tableName = $this->getTableName($tableName);
        switch (strtolower(Arrays::GetKey('type', $this->config, 'db_type_error'))) {
            case 'mysql':
                $config = $this->GetNowConfig();
                $db_name = $config['name'];
                $this->sql[] = <<<sql
SELECT column_name FROM information_schema.columns WHERE table_schema="{$db_name}" AND table_name="{$tableName}" AND column_name="{$fieldName}"
sql;
                $entity = $this->First();
                $is_exists = is_array($entity);
                break;
            case 'sqlite':
                $this->sql[] = <<<sql
select sql from sqlite_master where name="{$tableName}" and type='table';
sql;
                $entity = $this->First();
                if (is_array($entity) && array_key_exists('sql', $entity)) {
                    $is_exists = stristr($entity['sql'], '[' . $fieldName . ']') ? TRUE : FALSE;
                }
                break;
            default:
                out()->noticeByLJson('db_type_error');
                break;
        }
        return $is_exists;
    }

    public function AddFieldChange($tableName, $fieldParam)
    {
        $tableName = $this->GetTableName($tableName);
        switch (strtolower(Arrays::GetKey('type', $this->config, 'db_type_error'))) {
            case 'mysql':
                $type = $this->resultMySQLFieldType($fieldParam);
                $this->sql[] = <<<sql
ALTER TABLE `{$tableName}` ADD COLUMN `{$fieldParam['field']}` {$type} NULL;
sql;
                break;
            case 'sqlite':
                $type = $this->resultSqlite3FieldType($fieldParam);
                $this->sql[] = <<<sql
ALTER TABLE {$tableName} ADD COLUMN [{$fieldParam['field']}] {$type} NULL;
sql;
                break;
            default:
                out()->noticeByLJson('db_type_error');
                break;
        }
        $this->Exec();
    }

    public function DropFieldChange($name_table, $name_field)
    {
        switch (strtolower(Arrays::GetKey('type', $this->config, 'db_type_error'))) {
            case 'mysql':
                $tableName = $this->GetTableName($name_table);
                $this->sql[] = <<<sql
ALTER TABLE `{$tableName}` DROP COLUMN `{$name_field}`;
sql;
                $this->Exec();
                break;
            case 'sqlite':
                out()->noticeByLJson('SQLFile_Field_dropError');
                break;
            default:
                out()->noticeByLJson('db_type_error');
                break;
        }
    }

    protected function resultMySQLFieldType(array $fieldParam): string
    {
        $result = '';
        if (array_key_exists('formType', $fieldParam)) {
            switch ($fieldParam['formType']) {
                case 'editor':
                    $result = 'longtext';
                    break;
                case 'text':
                    $result = 'varchar(' . $fieldParam['setting']['len'] . ')';
                    break;
                case 'option':
                case 'baidumap';
                    $result = 'varchar(160)';
                    break;
                case 'textarea':
                case 'upload_list':
                case 'image_list':
                case 'frontend_code':
                    $result = 'text';
                    break;
                case 'number':
                    $result = 'int DEFAULT 0';
                    break;
                default:
                    out()->noticeByLJson('ModelField_FormType_ListNoExists');
                    break;
            }
        } else {
            out()->noticeByLJson('ModelField_FormTypeEmpty');
        }
        return $result;
    }

    protected function resultSqlite3FieldType(array $fieldParam): string
    {
        $result = '';
        if (array_key_exists('formType', $fieldParam)) {
            switch ($fieldParam['formType']) {
                case 'editor':
                case 'text':
                case 'option':
                case 'textarea':
                case 'upload_list':
                case 'image_list':
                case 'baidumap';
                case 'frontend_code':
                    $result = 'TEXT';
                    break;
                default:
                    out()->noticeByLJson('ModelField_FormType_ListNoExists');
                    break;
            }
        } else {
            out()->noticeByLJson('ModelField_FormTypeEmpty');
        }
        return $result;
    }

}
