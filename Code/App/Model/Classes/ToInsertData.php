<?php

namespace PKApp\Model\Classes;

use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\Auth;
use PKFrame\DataHandler\MatchHelper;
use PKFrame\DataHandler\Numbers;
use PKFrame\DataHandler\Pages;

class ToInsertData
{

    protected $service_option;
    public $listField_option_data;
    public $listField_base = [], $listField_data = [], $listField_name = [];
    public $thumb;

    public function __construct()
    {
        $this->service_option = new OptionsService();
        $this->listField_option_data = [];
    }

    protected function initVar()
    {
        $this->listField_base = [];
        $this->listField_data = [];
        $this->listField_name = [];
    }

    public function Get($dataList, $modelId)
    {
        static $list_field;
        if (!Arrays::Is($list_field)) {
            $service_field = new FieldService();
            $list_field = $service_field->GetList(['modelId' => $modelId, 'isShow' => 1]);
            Arrays::Is($list_field) ?: out()->noticeByLJson('ModelField_NoExists', 'model');
        }
        $this->initVar();;
        foreach ($list_field as $fieldEntity) {
            $field = $fieldEntity['field'];
            $this->listField_name[$field] = $fieldEntity['name'];
            $value = array_key_exists($field, $dataList) ? $dataList[$field] : '';
            $func = $fieldEntity['formType'];
            if ($fieldEntity['isEmpty'] && empty($value)) {
                out()->noticeByJson(str_replace('[name]', $fieldEntity['name'], language('field_postEmpty', 'model')));
                break;
            }
            if ($regular = Arrays::GetKey('fieldRegular', $fieldEntity)) {
                MatchHelper::MatchFormat($regular, $value, 'field_formatError', 'model',
                    [
                        ['[name]',],
                        [$fieldEntity['name'],]
                    ]);
            }
            if (method_exists($this, $func)) {
                $value = $this->{$func}($fieldEntity, $value);
                if (!$fieldEntity['isList']) {
                    $this->listField_data[$field] = $value;
                } else {
                    $this->listField_base[$field] = $value;
                }
            }
        }
    }

    protected function text($field, $value)
    {
        return $value;
    }

    protected function textarea($field, $value)
    {
        return $value;
    }

    protected function number($field, $value)
    {
        return Numbers::To($value);
    }

    protected function frontend_code($field, $value)
    {
        return $value;
    }

    protected function editor($field, $value)
    {
        $setting = $field['setting'];
        $imgList = Pages::GetImgsInArticle(htmlspecialchars_decode($value));
        $thumb = \request()->post('thumb');
        if (Arrays::Is($imgList)) {
            foreach ($imgList as $index => $item_img) {
                $img_arr = parse_url($item_img);
                if (count($img_arr) == 1) {
                    $img_now = $item_img;
                } else {
                    $img_now = '/' . ltrim($img_arr['path'], '/');
                }
                $value = str_replace($item_img, $img_now, $value);
                if ($index == 0 && empty($thumb)) {
                    $this->thumb = $img_now;
                }
            }
        }
        return $value;
    }

    protected function option($field, $value)
    {
        !empty($value) ?: out()->noticeByJson(str_replace('[name]', $field['name'], language('field_postEmpty', 'model')));
        $setting = Arrays::Unserialize($field['setting']);
        if (array_key_exists($field['field'], $this->listField_option_data) == false) {
            $list_option = $this->service_option->GetList([
                'parentId' => Arrays::GetKey('option', $setting, 'Options_ParentError', 'model'),
            ], ['id', 'name']);
            Arrays::Is($list_option)
                ? $list_option = Arrays::Column($list_option, 'name', 'id')
                : out()->noticeByLJson('ModelField_option_Empty', 'model');
            $this->listField_option_data[$field['field']] = $list_option;
        } else {
            $list_option = $this->listField_option_data[$field['field']];
        }
        $value_arr = explode(',', $value);
        foreach ($value_arr as $item) {
            array_key_exists($item, $list_option) ?: out()->noticeByLJson('Options_Empty', 'model');
        }
        return $value;
    }

    protected function upload_list($field, $value): string
    {
        $setting = $field['setting'];
        if (Arrays::Is($value)) {
            if ($setting['isPass']) {
                foreach ($value as $key => $v) {
                    $value[$key]['password'] = Numbers::Random(6);
                }
            }
        } elseif (!empty($value)) {
            $value = str_replace('&quot;', '"', $value);
            return Arrays::IsSerialize($value) ? $value : serialize([]);
        } else {
            $value = [];
        }
        return serialize($value);
    }

    protected function image_list($field, $value): string
    {
        if (Arrays::Is($value)) {
            foreach ($value as $item) {
                $this->thumb = $item['img_src'];
                break;
            }
        } elseif (!empty($value)) {
            $value = str_replace('&quot;', '"', $value);
            return Arrays::IsSerialize($value) ? $value : serialize([]);
        } else {
            $value = [];
        }
        return serialize($value);
    }

    protected function baidu_map($field, $value)
    {
        return $value;
    }
}
