<?php


namespace PKApp\Model;

use PKApp\Model\Classes\TraitModel;
use PKCommon\Controller\AdminController;
use PKFrame\DataHandler\Numbers;

class AdminGetModel extends AdminController
{
    use TraitModel;

    private $_type;

    public function Main()
    {
        $this->assign([
            'modelType' => request()->get('type'),
        ]);
        $this->fetch('model_lists');
    }

    public function ApiByLists()
    {
        $this->_checkType();
        $lists = $this->serviceOfModel()->GetListByType($this->_type);
        $typeList = dict('modelTypeLists');
        foreach ($lists as $index => $item) {
            $item['typeName'] = $typeList[$item['modelType']];
            $lists[$index] = $item;
        }
        $this->json($lists);
    }

    public function ApiByCount()
    {
        $this->json([], $this->serviceOfModel()->GetCount());
    }

    public function ApiBySelect()
    {
        $this->_checkType();
        $lists = $this->serviceOfModel()->GetListByType($this->_type, ['id', 'name']);
        if ($this->_type == 'member') {
            foreach ($lists as $index => $item) {
                $item['status'] = $item['id'] == 1 ? 0 : 1;
                $lists[$index] = $item;
            }
        }
        $this->json($lists);
    }

    private function _checkType()
    {
        $this->_type = request()->get('modelType');
        $modelTypeLists = dict('modelTypeLists','model');
        array_key_exists($this->_type, $modelTypeLists) ?: $this->noticeByJson('Model_TypeError');
    }

    public function ApiGetDetail()
    {
        $id = request()->get('id');
        Numbers::IsId($id) ?: $this->noticeByJson('Model_IdEmpty');
        $entity = $this->serviceOfModel()->interface_getEntityById($id);
        $this->json($entity);
    }
}