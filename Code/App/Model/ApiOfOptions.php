<?php


namespace PKApp\Model;


use PKApp\Model\Classes\OptionsService;
use PKFrame\Controller;
use PKFrame\DataHandler\Numbers;

class ApiOfOptions extends Controller
{

    public function Main()
    {
        $id = Numbers::To(request()->get('id'));
        Numbers::IsId($id) ?: $this->noticeByJson('Options_IdEmpty');
        $service = new OptionsService();
        $this->json(
            $service->GetList(['parentId'=>$id])
        );
    }
}