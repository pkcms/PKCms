<?php


namespace PKApp\Model;


use PKCommon\Controller\AdminController;
use PKFrame\DataHandler\Arrays;

class AdminApi extends AdminController
{

    public function Main()
    {
    }

    public function GetFieldTypeList()
    {
        $type_model = Arrays::GetKey('modelType',request()->get(),'Model_TypeEmpty');
        array_key_exists($type_model,dict('modelTypeLists', request()->module())) ?: $this->noticeByJson('Model_TypeError');
        $result = [];
        switch ($type_model) {
            case 'content':
                foreach (dict('fieldTypeList_base') as $key => $item) {
                    $result[] = ['id' => $key, 'name' => $item, 'state' => 1];
                }
                foreach (dict('fieldTypeList_high') as $key => $item) {
                    $result[] = ['id' => $key, 'name' => $item, 'state' => 1];
                }
                break;
            case 'form':
                foreach (dict('fieldTypeList_base') as $key => $item) {
                    $result[] = ['id' => $key, 'name' => $item, 'state' => 1];
                }
                break;
        }
        $this->json($result);
    }

    public function GetFieldRegularList()
    {
        $this->json($this->getDictToArray('fieldRegularList'));
    }

    public function GetFieldByOptionsType()
    {
        $this->json($this->getDictToArray('options_type'));
    }

}