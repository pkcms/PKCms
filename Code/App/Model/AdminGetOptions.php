<?php

namespace PKApp\Model;

use PKApp\Model\Classes\TraitModel;
use PKCommon\Controller\AdminController;
use PKFrame\DataHandler\Numbers;

class AdminGetOptions extends AdminController
{
    use TraitModel;

    public function Main()
    {
        $this->fetch('options_list');
    }

    public function ApiByLists()
    {
        $this->json($this->serviceOfOptions()->GetList());
    }

    public function ApiBySelect()
    {
        if (request()->has('parentId','get')) {
            $parentId = request()->get('parentId');
            Numbers::IsId($parentId) ?: $this->noticeByJson('Options_ParentError');
        } else {
            $parentId = 0;
        }
        $this->json($this->serviceOfOptions()->GetList(['parentId' => $parentId]));
    }

    public function ApiGetDetail()
    {
        $this->json(
            $this->serviceOfOptions()->interface_getEntityById(
                request()->get('id')
            )
        );
    }
}