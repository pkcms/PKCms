<?php


namespace PKApp\Model;

use PKApp\Model\Classes\TraitModel;
use PKCommon\Controller\AdminController;
use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\Numbers;

class AdminGetField extends AdminController
{
    use TraitModel;

    public function Main()
    {
        $this->assign([
            'modelId' => request()->get('modelId'),
        ])->fetch('field_lists');
    }

    public function ApiByLists()
    {
        $modelId = request()->get('modelId');
        Numbers::IsId($modelId) ?: $this->noticeByJson('Model_IdEmpty');
        $lists = $this->serviceOfField()->GetListByModelId($modelId);
        foreach ($lists as $index => $list) {
            if (is_array($list) && array_key_exists('setting', $list)) {
                $list['setting'] = Arrays::Unserialize($list['setting']);
            }
            $lists[$index] = $list;
        }
        $this->json($lists);
    }

    public function ApiBySelect()
    {
        $modelId = request()->get('modelId');
        Numbers::IsId($modelId) ?: $this->noticeByJson('Model_IdEmpty');
        $list_data = $this->serviceOfField()->GetListByModelId($modelId, ['field', 'name']);
        $result = [];
        foreach (dict('fieldList_IsSystem') as $key => $name) {
            $result[] = ['value' => $key, 'name' => $name];
        }
        if (Arrays::Is($list_data)) {
            foreach ($list_data as $list_datum) {
                $result[] = ['value' => $list_datum['field'], 'name' => $list_datum['name']];
            }
        }
        $this->json($result);
    }

    public function ApiGetDetail()
    {
        $this->json(
            $this->serviceOfField()->interface_getEntityById(
                request()->get('id')
            )
        );
    }
}