<?php


namespace PKApp\Model;

use PKApp\Model\Classes\TraitModel;
use PKCommon\Controller\AdminController;
use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\MatchHelper;
use PKFrame\DataHandler\Numbers;

class AdminSetField extends AdminController
{

    use TraitModel;

    protected $service_field;

    public function __construct()
    {
        parent::__construct();
        $this->service_field = $this->serviceOfField();
    }

    public function Main()
    {
        $modelId = request()->get('modelId');
        Numbers::IsId($modelId) ?: $this->noticeByPage('Model_IdEmpty');
        $params = ['modelId' => $modelId,];
        if (request()->has('id', 'get')) {
            $id = request()->get('id');
            Numbers::IsId($id) ?: $this->noticeByPage('ModelField_IdEmpty');
            $params['id'] = $id;
        }
        $this->assign($params)->fetch('field_form');
    }

    public function ApiByCreate()
    {
        $post = $this->model();
        $post['field'] = strtolower($this->checkPostFieldIsEmpty('field', 'ModelField_NameEmpty'));
        $post['name'] = $this->checkPostFieldIsEmpty('name', 'ModelField_CnNameEmpty');
        $post['modelId'] = $this->checkPostFieldIsEmpty('modelId', 'Model_IdEmpty');
        $post['formType'] = $this->checkPostFieldIsEmpty('formType', 'ModelField_FormTypeEmpty');
        // 查询模型的实体
        $this->service_field->CheckExistsAndCreate($post);
        // 信息入库
        $post['setting'] = serialize(is_array($post['setting']) ? $post['setting'] : []);
        $this->service_field->Add($post);
        $this->service_field->saveCache($post['modelId']);
        $this->inputSuccess();
    }

    public function ApiByChange()
    {
        $post = $this->model();
        $fieldId = $this->checkPostFieldIsEmpty('id', 'ModelField_IdEmpty');
        $post['name'] = $this->checkPostFieldIsEmpty('name', 'ModelField_CnNameEmpty');
        $post['setting'] = serialize(is_array($post['setting']) ? $post['setting'] : []);
        $this->service_field->UpdateById($fieldId, $post);
        $this->service_field->saveCache($post['modelId']);
        $this->json('Data_Input_Success');
    }

    public function ApiByDel()
    {
        $id_field = request()->post('id');
        $this->service_field->DropField(request()->post('modelId'), $id_field);
        $this->service_field->DeleteById($id_field);
        $this->inputSuccess();
    }

    public function ApiBySortIndex()
    {
        $fieldId = $this->checkPostFieldIsEmpty('id', 'ModelField_IdEmpty');
        $listSort = Numbers::To(request()->post('sortIndex'));
        // 查询字段的存在
        $entity = $this->service_field->interface_getEntityById($fieldId);
        Arrays::Is($entity) ?: $this->noticeByJson('ModelField_NoExists');
        $this->service_field->UpdateById($fieldId, ['listSort' => $listSort]);
        $this->service_field->saveCache($entity['modelId']);
        $this->inputSuccess();
    }

    protected function model(): array
    {
        $bool_field = ['isSearch', 'isEmpty', 'isShow', 'isOrder'];
        if (request()->action() == 'ApiByCreate') {
            $post = $this->checkModel(array_merge($bool_field, [
                'modelId', 'formType', 'field', 'name', 'setting', 'fieldRegular', 'power', 'isList',
            ]));
        } else {
            $post = $this->checkModel(array_merge($bool_field, [
                'id', 'modelId', 'name', 'formType', 'setting', 'fieldRegular', 'power',
            ]));
        }
        foreach ($bool_field as $item) {
            $post[$item] = array_key_exists($item, $post) ? 1 : 0;
        }
        if (!empty($post['fieldRegular'])) {
            array_key_exists($post['fieldRegular'], dict('fieldRegularList')) ?: $this->noticeByJson('ModelField_fieldRegular_NoExists');
        }
        $post['power'] = serialize(is_array($post['power']) ? $post['power'] : []);
        $setting = $post['setting'];
        switch ($post['formType']) {
            case 'upload_list':
                if (is_array($setting)) {
                    array_key_exists('upload_allowExt', $setting) && !empty($setting['upload_allowExt']) ?: $this->noticeByJson('setting_upload_allowExt_empty');
                    array_key_exists('savePath', $setting) ?: $this->noticeByJson('setting_savePath_noExists');
                    $setting['savePath'] = trim($setting['savePath']);
                    if (!empty($setting['savePath'])) {
                        MatchHelper::MatchFormat('letter', $setting['savePath'], 'setting_savePath_error');
                    }
                } else {
                    $this->noticeByJson('setting_empty');
                }
                break;
            case 'image_list':
                if (is_array($setting)) {
                    array_key_exists('savePath', $setting) ?: $this->noticeByJson('setting_savePath_noExists');
                    $setting['savePath'] = trim($setting['savePath']);
                    if (!empty($setting['savePath'])) {
                        MatchHelper::MatchFormat('letter', $setting['savePath'], 'setting_savePath_error');
                    }
                } else {
                    $this->noticeByJson('setting_empty');
                }
                break;
            case 'option':
                if (is_array($setting)) {
                    array_key_exists('boxType', $setting) && !empty($setting['boxType']) ?: $this->noticeByJson('setting_boxType_empty');
                    $options_type = dict('options_type');
                    array_key_exists($setting['boxType'], $options_type) ?: $this->noticeByJson('setting_boxType_error');
                    if (!array_key_exists('option', $setting) || empty($setting['option'])) {
                        $this->noticeByJson('setting_option_empty');
                    }
                } else {
                    $this->noticeByJson('setting_empty');
                }
                break;
        }
        return $post;
    }

}
