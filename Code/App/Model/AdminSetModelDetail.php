<?php


namespace PKApp\Model;

use PKApp\Model\Classes\TraitModel;
use PKCommon\Controller\AdminController;
use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\Numbers;

class AdminSetModelDetail extends AdminController
{

    use TraitModel;

    protected $service_model;

    public function __construct()
    {
        parent::__construct();
        $this->service_model = $this->serviceOfModel();
    }

    public function Main()
    {
        $params = [
            'id' => Numbers::To(request()->get('id')),
        ];
        if (request()->has('modelType', 'get')) {
            $typeEnName = request()->get('modelType');
            $typeList = dict('modelTypeLists');
            array_key_exists($typeEnName, $typeList) ?: $this->noticeByJson('Model_TypeEmpty');
            $params = array_merge($params, ['modelType' => $typeEnName]);
        }
        $this->assign($params)->fetch('model_form');
    }

    public function ApiByCreate()
    {
        $post = $this->model();
        // 并检查数据表名的存在
        $this->setting($post);
        unset($post['id']);
        $post['isSystem'] = 0;
        $this->service_model->Add($post);
        $this->inputSuccess();
    }

    public function ApiByChange()
    {
        $post = $this->model();
        $id = $this->checkPostFieldIsEmpty('id', 'Model_IdEmpty');
        if (array_key_exists('tableName', $post)) {
            // 模型在修改的情况下不修改数据表名
            unset($post['tableName']);
        }
        if (array_key_exists('setting', $post) && is_array($post['setting'])) {
            $this->setting($post);
        }
        $this->service_model->UpdateById(Numbers::Is($id), $post);
        $this->inputSuccess();
    }

    protected function setting(array &$post)
    {
        $setting = $post['setting'];
        if (request()->action() == 'ApiByCreate') {
            $this->service_model->CheckTableNameAndCreate($post['modelType'],
                $this->checkPostFieldIsEmpty('tableName', 'Model_TableNameEmpty'));
        }
        // 表单模型 只提交一次
        if (array_key_exists('singleSubmit', $setting)) {
            $restrictField = Arrays::GetKey('restrictField', $setting);
            switch (request()->action()) {
                case 'ApiByCreate':
                    $this->noticeByJson('Err_restrictField');
                    break;
                case 'ApiByChange':
                    if (!empty($restrictField)) {
                        $field_isExists = $this->serviceOfField()->GetEntity(['field' => $restrictField, 'modelId' => $post['id']]);
                        Arrays::Is($field_isExists) ?: $this->noticeByJson('NoExists_field');
                    }
                    break;
            }
        }
        $post['setting'] = serialize($setting);
    }

    protected function model(): array
    {
        $result = $this->checkModel(array('id', 'modelType', 'name', 'tableName', 'setting'));
        $result['name'] = $this->checkPostFieldIsEmpty('name', 'Model_NameEmpty');
        return $result;
    }

}