<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
</head>

<body>

<div class="layui-fluid">

    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <fieldset class="layui-elem-field layui-field-title">
                <legend>模型选项</legend>
            </fieldset>
        </div>

        <div class="layui-card-body">
            <table class="layui-hide PK-tableTree" id="table_adminList" lay-filter="table_adminList"
                   data-get="Model/AdminGetOptions"
                   data-set="Model/AdminSetOptions"></table>

            <script type="text/html" id="table_toolBar">
                <div class="layui-btn-container">
                    <button class="layui-btn layui-btn-sm"
                            lay-event="add">添加
                    </button>
                </div>
            </script>

            <script type="text/html" id="table_tool">
                <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="edit">
                    <i class="layui-icon layui-icon-edit"></i>修改</a>
                <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">
                    <i class="layui-icon layui-icon-delete"></i>删除</a>
            </script>

        </div>
    </div>
</div>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">
    function renderTable() {
        PKAdmin.tableTree('id', 'parentId', {
            cols: [[
                {field:'name', title:'选项名'},
                {field:'id', title:'ID'},
                {field:'parentId', title:'父级ID'},
                {
                    title: "操作",
                    align: "center",
                    width: 250,
                    fixed: "right",
                    toolbar: "#table_tool"
                }
            ]]
        });
    }

    PKAdmin.ready(function () {
        renderTable();
    });
</script>
</body>
</html>
