<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
</head>

<body>

<div class="layui-fluid">

    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <fieldset class="layui-elem-field layui-field-title">
                <legend>模型管理</legend>
            </fieldset>
        </div>

        <div class="layui-card-body">
            <table class="layui-hide PK-table" id="table_adminList" lay-filter="table_adminList"
                   data-get="Model/AdminGetModel" data-set="Model/AdminSetModelDetail"
                   data-params="modelType=<{$modelType}>"></table>

            <script type="text/html" id="table_toolBar">
                <div class="layui-btn-container">
                    <button class="layui-btn layui-btn-sm" lay-event="add">添加
                    </button>
                </div>
            </script>

            <script type="text/html" id="isSystemTpl">
                {{#  if(d.isSystem == 1){ }}
                <button class="layui-btn layui-btn-xs">是</button>
                {{#  } else { }}
                <button class="layui-btn layui-btn-primary layui-btn-xs">否</button>
                {{#  } }}
            </script>

            <script type="text/html" id="table_tool">
                <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="edit">
                    <i class="layui-icon layui-icon-edit"></i>修改</a>
                <a class="layui-btn layui-btn-primary layui-btn-xs"
                   lay-href="/index.php/Model/AdminGetField?modelId={{ d.id }}">
                    <i class="layui-icon layui-icon-list"></i>字段列表</a>
                {{#  if(d.modelType == 'form'){ }}
                <a class="layui-btn layui-btn-primary layui-btn-xs"
                   lay-href="/index.php/Form/AdminGetData?modelId={{ d.id }}">
                    <i class="layui-icon layui-icon-list"></i>数据列表</a>
                <a class="layui-btn layui-btn-warm layui-btn-xs"
                   lay-event="api|Power/AdminSetPower/ApiByAddModelForm">
                    <i class="layui-icon layui-icon-add-1"></i>添加到导航</a>
                {{#  } }}
            </script>

        </div>
    </div>
</div>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">
    function renderTable() {
        PKAdmin.table({
            cols: [[
                {field:'id', title:'ID'},
                {field:'name', title:'模型名'},
                {field:'tableName', title:'数据表'},
                {field:'typeName', title:'类别'},
                {field:'isSystem', title:'是否系统内置', templet: '#isSystemTpl'},
                {
                    title: "操作",
                    align: "center",
                    width: 380,
                    fixed: "right",
                    toolbar: "#table_tool"
                }
            ]]
        });
    }

    PKAdmin.ready(function () {
        renderTable();
    });
</script>
</body>
</html>
