<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
</head>

<body>

<div class="layui-fluid" lay-filter="tpl-element">
    <div id="view-table"></div>
</div>

<script type="text/html" data-id="<{$modelId}>" data-api="Model/AdminGetModel" id="tplDetail-table">
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <fieldset class="layui-elem-field layui-field-title">
                <legend>模型字段管理&nbsp;-&nbsp;{{  d.name  }}</legend>
            </fieldset>
        </div>
        <div class="layui-card-body">
            <table class="layui-hide PK-table" id="table_adminList" lay-filter="table_adminList"
                   data-get="Model/AdminGetField"
                   data-set="Model/AdminSetField"
                   data-params="modelId={{  d.id  }}"></table>
        </div>
    </div>
</script>

<script type="text/html" id="table_toolBar">
    <div class="layui-btn-container">
        <button class="layui-btn layui-btn-sm" lay-event="add">添加</button>
    </div>
</script>

<script type="text/html" id="isAttributeTpl">
    <label class="layui-btn layui-btn-xs{{#  if(d.isList == 0){ }} layui-btn-primary{{#  } }}">
        列表字段
    </label>
    <label class="layui-btn layui-btn-xs{{#  if(d.isSearch == 0){ }} layui-btn-primary{{#  } }}">
        搜索条件
    </label>
    <label class="layui-btn layui-btn-xs{{#  if(d.isEmpty == 0){ }} layui-btn-primary{{#  } }}">
        必填
    </label>
    <label class="layui-btn layui-btn-xs{{#  if(d.isShow == 0){ }} layui-btn-primary{{#  } }}">
        显示
    </label>
</script>

<script type="text/html" id="table_tool">
    <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="edit">
        <i class="layui-icon layui-icon-edit"></i>修改</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">
        <i class="layui-icon layui-icon-delete"></i>删除</a>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">
    function renderTable() {
        PKAdmin.table({
            cols: [[
                {field:'id', title:'ID'},
                {field:'listSort', title:'排序', edit:true},
                {field:'name', title:'字段名'},
                {field:'field', title:'英文名'},
                {field:'formType', title:'类型'},
                {field:'isList', title:'字段属性是否启用',align: "center",width: 320, templet: '#isAttributeTpl'},
                {
                    title: "操作",
                    align: "center",
                    width: 160,
                    fixed: "right",
                    toolbar: "#table_tool"
                }
            ]],
            initSort: {
                field: 'listSort',
                type: 'asc'
            }
        });
    }

    PKAdmin.ready(function () {
    });
</script>
</body>
</html>
