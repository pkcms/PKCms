<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
</head>

<body>

<div class="layui-fluid">

    <div class="layui-fluid">

        <div class="layui-card">
            <div class="layui-card-header layuiadmin-card-header-auto">
                <fieldset class="layui-elem-field layui-field-title">
                    <legend>选项数据</legend>
                </fieldset>
            </div>

            <div class="layui-card-body">
                <form class="layui-form" lay-filter="form-frame">
                    <div id="view-form"></div>

                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer">
                                <button class="layui-btn" lay-submit lay-filter="form-frame">立即提交</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/html" data-id="<{$id}>" data-api="Model/AdminGetOptions" id="tpl-form">
    <input type="hidden" name="id" value="{{#  if(d.id){ }}{{  d.id  }}{{#  } }}"/>
    <div class="layui-form-item">
        <label class="layui-form-label">选项名：</label>
        <div class="layui-input-block">
            <input type="text" name="name" class="layui-input" lay-verify="required" autocomplete="off"
                   value="{{#  if(d.name){ }}{{  d.name  }}{{#  } }}"/>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">呈现图片：</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input" style="width: 60%;float: left;"
                   id="image" name="image" value="{{#  if(d.image){ }}{{  d.image  }}{{#  } }}"/>
            <button type="button" id="uploadBtn_image" class="layui-btn pkcms_upload" data-field="image">
                <i class="layui-icon layui-icon-upload"></i>选择
            </button>
            <button type="button" class="layui-btn" onclick="PreViewImage('image')">预览
            </button>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">选择顶级：</label>
        <div class="layui-input-block">
            <div class="pkcms_select" id="parentId"
                 data-name="parentId" data-value="{{#  if(d.parentId){ }}{{  d.parentId  }}{{#  } }}"
                 data-api="Model/AdminGetOptions"></div>
        </div>
    </div>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">
    PKAdmin.ready(function () {
        PKAdmin.tplFormBySubmit({});
    });
</script>
</body>
</html>
