<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
</head>

<body>

<div class="layui-fluid">

    <div class="layui-fluid">

        <div class="layui-card">
            <div class="layui-card-header layuiadmin-card-header-auto">
                <fieldset class="layui-elem-field layui-field-title">
                    <legend>数据模型</legend>
                </fieldset>
            </div>

            <div class="layui-card-body">
                <form class="layui-form" lay-filter="form-frame">
                    <div id="view-form"></div>

                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer">
                                <button class="layui-btn" lay-submit lay-filter="form-frame">立即提交</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/html" data-id="<{$id}>" data-api="Model/AdminGetModel" id="tpl-form">
    <input type="hidden" name="id" value="{{#  if(d.id){ }}{{  d.id  }}{{#  } }}"/>
    <input type="hidden" name="modelType" value="{{#  if(d.modelType){ }}{{  d.modelType  }}{{#  } }}"/>
    <div class="layui-form-item">
        <label class="layui-form-label">中文名：</label>
        <div class="layui-input-block">
            <input type="text" name="name" class="layui-input" lay-verify="required" autocomplete="off"
                   value="{{#  if(d.name){ }}{{  d.name  }}{{#  } }}"/>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">英文名：</label>
        <div class="layui-input-block">
            <input type="text" id="tableName" name="tableName" class="layui-input"
                   value="{{#  if(d.tableName){ }}{{  d.tableName  }}{{#  } }}"
                   {{#  if(d.tableName){ }}disabled="disabled" {{#  } }}/>
        </div>
    </div>
    {{#  if(d.modelType == 'content'){ }}
    <div class="layui-form-item">
        <label class="layui-form-label">标题的唯一性：</label>
        <div class="layui-input-block">
            <input type="checkbox" lay-skin="switch" id="uniqueTitle" lay-filter="uniqueTitle" name="setting[uniqueTitle]"
                   {{#  if(d.setting != null && d.setting.uniqueTitle){ }}value="1" checked{{#  } }}/>
        </div>
    </div>
    {{#  } }}
    {{#  if(d.modelType == 'form'){ }}
    <div class="layui-form-item">
        <label class="layui-form-label">是否保存到数据库：</label>
        <div class="layui-input-block">
            <input type="checkbox" lay-skin="switch" id="database" lay-filter="database" name="setting[database]"
                   {{#  if(d.setting != null && d.setting.database){ }}value="1" checked{{#  } }}/>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">是否限制单次提交：</label>
        <div class="layui-input-block">
            <input type="checkbox" lay-skin="switch" id="single" lay-filter="database" name="setting[singleSubmit]"
                   {{#  if(d.setting != null && d.setting.singleSubmit){ }}value="1" checked{{#  } }}/>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">限制条件查询字段：</label>
        <div class="layui-input-block">
            <input type="text" name="setting[restrictField]" class="layui-input"
                   value="{{#  if(d.setting != null && d.setting.restrictField != null){ }}{{  d.setting.restrictField }}{{#  } }}"/>
            <div class="layui-form-mid layui-word-aux">
                只能对某一个字段名起作用，不含多个。
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">接收提交的邮箱地址：</label>
        <div class="layui-input-block">
            <input type="text" name="setting[email_address]" class="layui-input"
                   value="{{#  if(d.setting != null){ }}{{  d.setting.email_address }}{{#  } }}"/>
        </div>
    </div>
    {{#  } }}
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">
    PKAdmin.ready(function () {
        PKAdmin.tplFormBySubmit({
            modelType: '<{$modelType}>'
        });
    });
</script>
</body>
</html>
