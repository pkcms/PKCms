<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
</head>

<body>

<div class="layui-fluid">

    <div class="layui-fluid">

        <div class="layui-card">
            <div class="layui-card-header layuiadmin-card-header-auto">
                <fieldset class="layui-elem-field layui-field-title">
                    <legend>创建新的字段</legend>
                </fieldset>
            </div>

            <div class="layui-card-body layui-card-body-mb">
                <form class="layui-form" id="form-element" lay-filter="form-frame">
                    <input type="hidden" name="id" value="<{$id}>"/>
                    <input type="hidden" name="modelId" value="<{$modelId}>"/>
                    <div id="view-form"></div>

                    <div class="layui-form-footer-fixed">
                        <div class="layui-input-block">
                            <div class="layui-footer">
                                <button class="layui-btn" lay-submit lay-filter="form-frame">立即提交</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script id="tpl-form" type="text/html">
        <div class="layui-form-item">
            <label class="layui-form-label">中文名：</label>
            <div class="layui-input-block">
                <input type="text" name="name" class="layui-input"
                       value="{{#  if(d.entity != null){ }}{{ d.entity.name }}{{#  } }}"
                       lay-verify="required" autocomplete="off"/>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">英文名：</label>
            <div class="layui-input-block">
                <input type="text" name="field" class="layui-input{{#  if(d.entity != null){ }} layui-disabled{{#  } }}"
                       value="{{#  if(d.entity != null){ }}{{ d.entity.field }}{{#  } }}"
                       lay-verify="required" autocomplete="off"/>
                <input type="hidden" name="formType"
                       value="{{#  if(d.entity != null){ }}{{ d.entity.formType }}{{#  } }}"/>
                <div class="layui-form-mid layui-word-aux">
                    可使用以英文字母（a~z）为开头，中间加下划线（_）
                </div>
            </div>
        </div>
        {{#  if(d.entity == null){ }}
        <div class="layui-form-item">
            <label class="layui-form-label">字段类型：</label>
            <div class="layui-input-block">
                <div id="field_type"></div>
                <div class="layui-form-mid layui-word-aux">
                    此类型的选择只能选择一次，请谨慎操作。
                </div>
            </div>
        </div>
        {{#  } }}
        {{#  if(d.modelEntity.modelType == 'content' && d.entity == null){ }}
        <div class="layui-form-item">
            <label class="layui-form-label">作为列表字段：</label>
            <div class="layui-input-block">
                <input type="checkbox" lay-skin="switch" id="isList" lay-filter="isList" name="isList" value="1"
                        {{#  if((d.entity != null) && (d.entity.isList == 1)){ }} checked {{#  } }}/>
            </div>
            <div class="layui-form-mid layui-word-aux">
                在显示内容列表时需要显示此字段的请选择此项，如无此需求的建议不要选择（为效率方面的考虑）。
            </div>
        </div>
        {{#  } }}
        <div class="layui-form-item">
            <label class="layui-form-label">作为搜索条件：</label>
            <div class="layui-input-block">
                <input type="checkbox" lay-skin="switch" id="isSearch" lay-filter="isSearch" name="isSearch" value="1"
                        {{#  if((d.entity != null) && (d.entity.isSearch == 1)){ }} checked {{#  } }}/>
            </div>
            <div class="layui-form-mid layui-word-aux">
                与列表字段搭配，作为前台的筛选条件
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">相关配置：</label>
            <div class="layui-input-block layui-form" id="view_fieldType" lay-filter="view_fieldType">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">数据检验：</label>
            <div class="layui-input-block">
                <div class="pkcms_select" id="fieldRegular"
                     data-name="fieldRegular"
                     data-value=""
                     data-api="Model/AdminApi/GetFieldRegularList"></div>
                <div class="layui-form-mid layui-word-aux">
                    系统将通过此正则检验表单提交的数据合法性，如果不想检验数据请留空
                </div>
            </div>
        </div>
        {{#  if(d.modelEntity.modelType == 'content'){ }}
        <div class="layui-form-item">
            <label class="layui-form-label">指定会员角色访问：</label>
            <div class="layui-input-block">
                <div class="pkcms_select" id="PowerReadGroup" data-name="power[ReadGroup]" data-type="checkbox"
                     data-value="{{#  if((d.entity != null) && (d.entity.power != null) && (d.entity.power.ReadGroup != null)){ }}{{ d.entity.power.ReadGroup }}{{#  } }}"
                     data-api="Member/AdminGetGroup" data-params="modelId=2"></div>
                <div class="layui-form-mid layui-word-aux">
                    设置后，前台模板的常规返回将不带上此字段。<br/>
                    需要通过 webApi （俗称的 AJAX） 来获取数据，这个过程有前台会员是否在线的判断操作然后再返回。
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">商品规格字段：</label>
            <div class="layui-input-block">
                <input type="checkbox" lay-skin="switch" id="isOrder" lay-filter="isOrder" name="isOrder" value="1"
                        {{#  if((d.entity != null) && (d.entity.isOrder == 1)){ }} checked {{#  } }}/>
            </div>
        </div>
        {{#  } }}
        <div class="layui-form-item">
            <label class="layui-form-label">是否必填：</label>
            <div class="layui-input-block">
                <input type="checkbox" lay-skin="switch" id="isEmpty" lay-filter="isEmpty" name="isEmpty" value="1"
                        {{#  if((d.entity != null) && (d.entity.isEmpty == 1)){ }} checked {{#  } }}/>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">是否显示：</label>
            <div class="layui-input-block">
                <input type="checkbox" lay-skin="switch" id="isShow" lay-filter="isShow" name="isShow" value="1"
                        {{#  if((d.entity != null) && (d.entity.isShow == 1)){ }} checked {{#  } }}/>
            </div>
        </div>
    </script>

    <script id="tpl_fieldType" type="text/html">
        {{#  if(d.selectFieldType === 'text'){ }}
        <!-- 类型：文本 -->
        <div class="layui-form-item">
            <label class="layui-form-label">字符长度：</label>
            <div class="layui-input-block">
                <input type="number" name="setting[len]" class="layui-input"
                       value="{{#  if(d.entity == null){ }}80{{#  } else { }}{{  d.entity.setting.len }}{{#  } }}"/>
            </div>
        </div>
        {{#  } else if(d.selectFieldType === 'image_list'){ }}
        <!-- 类型：多图片上传 -->
        <div class="layui-form-item">
            <label class="layui-form-label">保存目录：</label>
            <div class="layui-input-block">
                <input type="text" name="setting[savePath]" class="layui-input"
                       value="{{#  if((d.entity == null) || (d.entity.setting == '')){ }}{{#  } else { }}{{  d.entity.setting.savePath }}{{#  } }}"/>
                <div class="layui-form-mid layui-word-aux">
                    留空，则以系统默认（upload）的目录来保存。<br/>
                    设置后，则以输入的目录名（如：youName）来保存，<b style="color: red">但只可使用英文，中间不带空格</b>。
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">留原名：</label>
            <div class="layui-input-block">
                <div>
                    <input type="checkbox" lay-skin="switch" id="isFromName" lay-filter="isFromName"
                           name="setting[isFromName]" value="1"
                            {{#  if((d.entity != null) && (d.entity.setting != null) && (d.entity.setting.isFromName == 1)){ }}
                           checked {{#  } }}/>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    关闭，则以系统默认的来保存。<br/>
                    开启，则尽量以原名来保存，<b style="color: red">但特殊字符（包括中文）会因文件名可读性的条件下被转码</b>。
                </div>
            </div>
        </div>
        {{#  } else if(d.selectFieldType === 'upload_list'){ }}
        <!-- 类型：多文件上传 -->
        <div class="layui-form-item">
            <label class="layui-form-label">文件类型：</label>
            <div class="layui-input-block">
                <input type="text" name="setting[upload_allowExt]" class="layui-input"
                       lay-verify="required" autocomplete="off"
                       value="{{#  if((d.entity == null) || (d.entity.setting == '')){ }}zip{{#  } else { }}{{  d.entity.setting.upload_allowExt }}{{#  } }}"/>
                <div class="layui-form-mid layui-word-aux">
                    请允许上传文件的文件扩展名（不带点“.”），多个中间以坚扛“|”来分隔。<br>
                    <b style="color: red">安全起见（不易感染病毒木马），建议使用打包类文件（如：zip、rar等）</b>
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">保存目录：</label>
            <div class="layui-input-block">
                <input type="text" name="setting[savePath]" class="layui-input"
                       value="{{#  if((d.entity == null) || (d.entity.setting == '')){ }}{{#  } else { }}{{  d.entity.setting.savePath }}{{#  } }}"/>
                <div class="layui-form-mid layui-word-aux">
                    留空，则以系统默认（upload）的目录来保存。<br/>
                    设置后，则以输入的目录名（如：youName）来保存，<b style="color: red">但只可使用英文，中间不带空格</b>。
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">留原名：</label>
            <div class="layui-input-block">
                <div>
                    <input type="checkbox" lay-skin="switch" id="isFromName" lay-filter="isFromName"
                           name="setting[isFromName]"
                            {{#  if((d.entity != null) && (d.entity.setting != null) && (d.entity.setting.isFromName == 1)){ }}
                           value="1" checked {{#  } }}/>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    关闭，则以系统默认的来保存。<br/>
                    开启，则尽量以原名来保存，<b style="color: red">但特殊字符（包括中文）会因文件名可读性而被转码</b>。
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">密码下载：</label>
            <div class="layui-input-block">
                <div>
                    <input type="checkbox" lay-skin="switch" id="isPass" lay-filter="isPass" name="setting[isPass]"
                           value="1"
                            {{#  if((d.entity != null) && (d.entity.setting != null) && (d.entity.setting.isPass == 1)){ }}
                           checked {{#  } }}/>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    关闭，则以普通的下载形式。<br/>
                    开启，则在下载前要输入系统给的随机密码后方可下载。
                </div>
            </div>
        </div>
        {{#  } else if(d.selectFieldType === 'option'){ }}
        <!-- 类型：选项 -->
        <div class="layui-form-item">
            <label class="layui-form-label">选项类型：</label>
            <div class="layui-input-block">
                <div class="pkcms_select" id="option_type"
                     data-name="setting[boxType]"
                     data-value="{{#  if((d.entity != null) && (d.entity.setting != null)){ }}{{  d.entity.setting.boxType }}{{#  } }}"
                     data-api="Model/AdminApi/GetFieldByOptionsType"></div>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">选项列表：</label>
            <div class="layui-input-block">
                <div class="pkcms_select" id="option_menu"
                     data-name="setting[option]"
                     data-value="{{#  if((d.entity != null) && (d.entity.setting != null)){ }}{{  d.entity.setting.option }}{{#  } }}"
                     data-api="Model/AdminGetOptions/ApiBySelect"></div>
            </div>
        </div>

        {{#  } }}
    </script>

    <script type="text/javaScript" src="/statics/jquery/jquery-1.8.3.min.js"></script>
    <script type="text/javaScript" src="/statics/layui/layui.js"></script>
    <script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

    <script type="text/javascript">
        var data = {
            id: $('input[name="id"]').val(),
            modelId: $('input[name="modelId"]').val(),
            entity: null,
            modelEntity: null,
            selectFieldType: '',
            optionsType: ''
        };

        function getModelDetail() {
            PKLayer.apiGet(
                'Model/AdminGetModel/ApiGetDetail',
                    {id:data.modelId},
                function (apiResult, count) {
                    data.modelEntity = apiResult;
                    if (data.id !== '') {
                        getDetail();
                    } else {
                        getForm();
                    }
                }
            );
        }

        function getDetail() {
            PKLayer.apiGet(
                'Model/AdminGetField/ApiGetDetail',
                    {id:data.id},
                function (apiResult, count) {
                    data.entity = apiResult;
                    data.selectFieldType = apiResult.formType;
                    if (data.selectFieldType === 'option') {
                        data.optionsType = data.entity.setting.boxType;
                    }
                    getForm();
                }
            );
        }

        function getForm() {
            console.log('getForm');
            PKAdmin.tplFormSubmitByFn(data, function () {
                if ($('#field_type').length === 1) {
                    getFieldTypeList();
                } else {
                    getFieldTypeTpl();
                }
            });
        }

        function getFieldTypeList() {
            PKLayer.apiGet(
                'Model/AdminApi/GetFieldTypeList',
                    {modelType:data.modelEntity.modelType},
                function (apiResult) {
                    PKLayer.useFormBySelectFun('#field_type', apiResult, '', false, true, function (res) {
                        let input_formType = $('input[name="formType"]');
                        console.log(res);
                        if (res.length > 0) {
                            data.selectFieldType = res[0].id;
                            input_formType.val(data.selectFieldType);
                            getFieldTypeTpl();
                        } else {
                            input_formType.val('');
                            document.getElementById('view_fieldType').innerHTML = '';
                        }
                    });
                }
            );
        }

        function getFieldTypeTpl() {
            PKLayer.useFormByTpl(data, 'tpl_fieldType', 'view_fieldType');
        }

        PKAdmin.ready(function () {
            getModelDetail();
        });
    </script>
</body>
</html>
