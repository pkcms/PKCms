<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return [
    'Model_TypeEmpty' => [
        'zh-cn' => '模型的类型不能为空',
        'en' => 'The type of the model cannot be empty'
    ],
    'Model_TypeError' => [
        'zh-cn' => '模型的类型不在允许的范围内',
        'en' => 'The type of the model is not allowed'
    ],
    'Model_NameEmpty' => [
        'zh-cn' => '模型名称不能为空',
        'en' => 'Model name cannot be empty'
    ],
    'Model_NameError_english' => [
        'zh-cn' => '模型名称只能为英文',
        'en' => 'The model name can only be in English'
    ],
    'NameError_prohibit' => [
        'zh-cn' => '您输入的英文为敏感词，请换个',
        'en' => 'The English you input is a sensitive word, please change it'
    ],
    'Model_TableNameEmpty' => [
        'zh-cn' => '数据表名不能为空',
        'en' => 'Datasheet name cannot be empty'
    ],
    'Model_TableNameExists' => [
        'zh-cn' => '数据表名已经存在',
        'en' => 'The data table name already exists'
    ],
    'Model_IdEmpty' => [
        'zh-cn' => '数据表模型ID不能为空',
        'en' => 'Datasheet model ID cannot be empty'
    ],
    'Model_NoExists' => [
        'zh-cn' => '数据表模型不存在',
        'en' => 'Datasheet model does not exist'
    ],
    'model_setting_empty' => [
        'zh-cn' => '模型的相关配置数据为空',
        'en' => 'The relevant configuration data of the model is empty'
    ],
    'SQLFile_Exists' => [
        'zh-cn' => 'SQL文件不存在',
        'en' => 'The SQL file does not exist'
    ],
    'SQLFile_Field_dropError' => [
        'zh-cn' => '系统不支持 SQLite 实体表删除',
        'en' => 'SQLite entity table deletion is not supported'
    ],
    'ModelField_NoExists' => [
        'zh-cn' => '字段不存在',
        'en' => 'Field does not exist'
    ],
    'ModelField_IdEmpty' => [
        'zh-cn' => '字段Id不能为空',
        'en' => 'Field ID cannot be empty'
    ],
    'ModelField_NameEmpty' => [
        'zh-cn' => '字段英文名称不能为空',
        'en' => 'Field English name cannot be empty'
    ],
    'ModelField_NameError' => [
        'zh-cn' => '字段英文名称只能为英文，或加下划线',
        'en' => 'Field English names can only be in English or underlined'
    ],
    'ModelField_NameExists' => [
        'zh-cn' => '字段英文名称在数据表中已经存在，字段名是：',
        'en' => 'The English name of the field already exists in the data table, and the field name is:'
    ],
    'ModelField_CnNameEmpty' => [
        'zh-cn' => '字段中文名称不能为空',
        'en' => 'The Chinese name of the field cannot be empty'
    ],
    'ModelField_FormTypeEmpty' => [
        'zh-cn' => '字段的类型不能为空',
        'en' => 'The type of the field cannot be empty'
    ],
    'ModelField_FormType_ListNoExists' => [
        'zh-cn' => '字段的类型在允许的列表中不存在',
        'en' => 'The type of field does not exist in the allowed list'
    ],
    'ModelField_fieldRegular_NoExists' => [
        'zh-cn' => '字段的正则检验的类型不存在系统中',
        'en' => 'The type of regularization test for fields does not exist in the system'
    ],
    'ModelField_option_Empty' => [
        'zh-cn' => '字段的选项列表的数据为空',
        'en' => "The data in the field's option list is empty"
    ],
    'field_postEmpty' => [
        'zh-cn' => '【[name]】的信息不能为空，请填写后再提交',
        'en' => 'Information of 【[name]】 cannot be blank. Please fill in and submit again'
    ],
    'field_formatError' => [
        'zh-cn' => '【[name]】的内容格式不对，请重新填写后再提交',
        'en' => 'The content format of 【[name]】 is not correct. Please fill in again and submit again'
    ],
    'Options_Empty' => [
        'zh-cn' => '该选项信息找不到，或不存在',
        'en' => 'The option information cannot be found or does not exist'
    ],
    'Options_IdEmpty' => [
        'zh-cn' => '选项数据的ID不能为空',
        'en' => 'The ID of the option data cannot be empty'
    ],
    'Options_ParentError' => [
        'zh-cn' => '请检查父级的选择',
        'en' => 'Please check the parent selection'
    ],
    'Options_NameEmpty' => [
        'zh-cn' => '选项名不能为空',
        'en' => 'Option name cannot be empty'
    ],
    'Options_DelError' => [
        'zh-cn' => '该选项信息下有子信息的存在，不可删除！',
        'en' => 'There is sub information under this option information. It cannot be deleted!'
    ],
    'setting_empty' => [
        'zh-cn' => '字段的相关配置数据为空',
        'en' => 'The relevant configuration data of the field is empty'
    ],
    'setting_upload_allowExt_empty' => [
        'zh-cn' => '允许上传文件的文件扩展名不能为空',
        'en' => 'The Filename extension allowed to upload files cannot be empty'
    ],
    'setting_savePath_noExists' => [
        'zh-cn' => '保存目录的参数不存在',
        'en' => 'The parameter for saving the directory does not exist'
    ],
    'setting_savePath_error' => [
        'zh-cn' => '保存目录名影响可读性',
        'en' => 'Saving directory names affects readability'
    ],
    'setting_boxType_empty' => [
        'zh-cn' => '选项类型为空',
        'en' => 'Option type is empty'
    ],
    'setting_boxType_error' => [
        'zh-cn' => '选项类型不正确',
        'en' => 'Incorrect option type'
    ],
    'setting_option_empty' => [
        'zh-cn' => '选项菜单选择不能为空',
        'en' => 'Option menu selection cannot be empty'
    ],
    'db_type_error' => [
        'zh-cn' => '数据库配置信息里面的类型为空，或者不在系统支持的范围，请在 Config/Database 文件中检查配置参数',
        'en' => 'The type in the database configuration information is empty or not within the range supported by the system. Please check the configuration parameters in the Config/Database file'
    ],
    'Err_restrictField' => [
        'zh-cn' => '请不要在创建模型时设置，在创建完模型和字段后再进行设置',
        'en' => 'Please do not set it when creating the model, only after creating the model and fields'
    ],
    'NoExists_field' => [
        'zh-cn' => '设置的【限制条件查询字段】必须是存在于该模型中的字段',
        'en' => 'The constraint query field set must be a field that exists in the model'
    ],
];