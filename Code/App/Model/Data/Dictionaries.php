<?php
return [
    'modelTypeLists' => [
        'content' => '内容模块',
        'special' => '专题',
        'member' => '会员',
        'form' => '表单',
    ],
    'EnName_prohibit' => [
        'action', 'select', 'from', 'count', 'where', 'and', 'or', 'input', 'update', 'del',
        'info', 'like', 'asc', 'desc', 'group', 'sqlite_master'
    ],
    'fieldList_IsSystem' => [
        'title' => '标题',
        'thumb' => '缩略图',
        'seoTitle' => 'SEO-标题',
        'seoKeywords' => 'SEO-关键词',
        'seoDescription' => 'SEO-描述',
        'template' => '自定义-模板',
        'url' => '自定义-URL',
        'hits'=>'浏览量',
        'createTime'=>'创建时间',
    ],
    'fieldTypeList_high' => [
        'editor' => '编辑器', 'upload_list' => '多文件上传', 'image_list' => '多图片上传',
        'frontend_code' => '前端代码', 'baidu_map' => '百度定位'
    ],
    'fieldTypeList_base' => [
        'text' => '单行文本', 'textarea' => '多行文本', 'number' => '数字', 'option' => '选项',
    ],
    'fieldRegularList' => [
        'num' => '数字、点、负符号', 'int' => '数字、负符号',
        'letter' => '字母', 'letterNum' => '数字、字母',
        'letter_num' => '数字、字母、下划线', 'letter_' => '字母、下划线',
        'email' => '电子邮箱', 'qq' => 'QQ号码', 'url' => '网址（含HTTP协议）',
        'mobile' => '手机号码', 'tel' => '固定号码', 'zipCode' => '邮政编码',
        'date' => '日期', 'datetime' => '日期、时间', 'color' => '颜色代码',
    ],
    'options_type' => [
        'radio' => '单选',
        'checkbox' => '多选',
    ],
];