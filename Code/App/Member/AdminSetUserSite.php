<?php


namespace PKApp\Member;

use PKApp\Member\Classes\TraitMember;
use PKCommon\Controller\AdminSetController;
use PKFrame\DataHandler\Numbers;

class AdminSetUserSite extends AdminSetController
{

    use TraitMember;

    public function Main()
    {
        $userId = request()->get('id');
        Numbers::IsId($userId) ?: $this->noticeByPage('Empty_userId');
        $this->assign([
            'id' => $userId
        ])->fetch('set_user_site');
    }

    public function ApiByCreate()
    {
    }

    public function ApiByChange()
    {
        $post = $this->model();
        $id = request()->post('id');
        $entity = $this->serviceByUser()->interface_getEntityById($id);
        $entity['groupId'] > 1 && $entity['modelId'] == 1 ?: $this->noticeByJson('Site_EditError');
        $this->serviceByUser()->UpdateById($id, $post);
        $this->inputSuccess();
    }

    public function ApiByDel()
    {
    }

    protected function model(): array
    {
        $post = $this->checkModel(['id', 'modelId', 'groupId', 'siteIdByAdmin']);
        Numbers::IsId($post['id']) ?: $this->noticeByJson('Empty_userId');
        Numbers::IsId($post['groupId']) ?: $this->noticeByJson('Group_IdEmpty');
        Numbers::IsId($post['modelId']) ?: $this->noticeByJson('Model_IdEmpty', 'Model');
        $post['siteIdByAdmin'] = Numbers::To($post['siteIdByAdmin']);
        return $post;
    }
}