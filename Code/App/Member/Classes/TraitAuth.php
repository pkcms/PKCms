<?php


namespace PKApp\Member\Classes;


trait TraitAuth
{

    protected function isLowLevelOfPassword(string $pwd): bool
    {
        $list = ['12345678', '12345', '123456789', '123456', 'admin', 'abc123', '123123', 'iloveyou',
            'password', 'football', 'qwerty'];
        $is = in_array($pwd, $list);
        $is ?: $is = is_numeric($pwd);
        return $is;
    }

    /**
     * 判断是否在正常的工作时间内
     * @return bool
     */
    protected function checkFunctionTime(): bool
    {
        $time_now = time();
        $time_begin = strtotime('08:30');
        $time_end = strtotime('21:30');
        return $time_now >= $time_begin && $time_now <= $time_end;
    }

}