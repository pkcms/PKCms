<?php


namespace PKApp\Member\Classes;

use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\Str;
use PKFrame\Service;

class MemberService extends Service
{

    protected function db(): UserDataBase
    {
        return new UserDataBase();
    }

    public function GetEntity($viewParams = [], $viewField = '*')
    {
        return $this->db()->CheckAndGetUserInfo($viewParams, $viewField);
    }

    public function CheckLogin($login, $password, $loginType = 'userName', bool $isAdmin = false)
    {
        $params = [];
        switch ($loginType) {
            case 'phone':
                $params = ['mobile' => $login];
                break;
            case 'email':
                $params = ['email' => $login];
                break;
            case 'userName':
                $params = ['userName' => $login];
                break;
            default:
                return 'Empty_loginType';
        }
        !$isAdmin ?: $params['modelId'] = 1;
        $dbUserInfo = $this->GetEntity($params);
        if (empty($dbUserInfo) || !Arrays::Is($dbUserInfo)) {
            return 'Empty_User';
        }
        if (is_array($dbUserInfo) && array_key_exists('password', $dbUserInfo)
            && array_key_exists('encrypt', $dbUserInfo)) {
            $password = $this->getInputPwdMD5($password, $dbUserInfo['encrypt']);
            if ($password == $dbUserInfo['password']) {
                $this->UpdateById($dbUserInfo['id'], [
                    'lastTime' => time(),
                    'lastIp' => request()->GuestIP(),
                ]);
            } else {
                return 'Error_login';
            }
        }
        unset($dbUserInfo['password']);
        return $dbUserInfo;
    }

    public function SetPass($id, $password)
    {
        $this->UpdateById($id, $this->createNewPassWord($password));
    }

    /**
     * 产生新的密码
     * @param string $str_pwd
     * @return array
     */
    public function createNewPassWord(string $str_pwd): array
    {
        $new_encrypt = Str::Random();
        return [
            'password' => $this->getInputPwdMD5($str_pwd, $new_encrypt),
            'encrypt' => $new_encrypt,
        ];
    }

    /**
     * 对输入的用户密码进行加密
     * @param string $str_pwd
     * @param string $encrypt
     * @return string
     */
    protected function getInputPwdMD5(string $str_pwd, string $encrypt): string
    {
        return md5(md5(trim($str_pwd)) . $encrypt);
    }

    public function GetCount($options = []): int
    {
        return $this->db()->Where($options)->Count();
    }

    public function GetList($viewParams = [], $viewField = '*', $lineNum = 0, $index = 0)
    {
        return $this->db()->GetList($viewParams, $lineNum = 0, $index = 0);
    }

    public function CheckAndGetUserInfo($option = [], $field = '*')
    {
        return $this->db()->CheckAndGetUserInfo($option, $field);
    }

    public function interface_getEntityById($id, $viewField = '*')
    {
        return $this->getEntityById($id, 'Empty_userId', 'Empty_User', $viewField);
    }
}