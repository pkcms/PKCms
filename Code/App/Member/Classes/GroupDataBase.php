<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/5/27
 * Time: 10:37
 */

namespace PKApp\Member\Classes;

use PKFrame\Lib\DataBase;

class GroupDataBase extends DataBase
{
    protected $table = 'member_group';

}
