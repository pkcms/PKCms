<?php


namespace PKApp\Member\Classes;


use PKFrame\DataHandler\Arrays;
use PKFrame\Lib\DataBase;

class UserDataBase extends DataBase
{
    protected $table = 'member_user';
    protected $table_g = 'member_group';

    public function CheckAndGetUserInfo($option = [], $field = '*')
    {
        return $this->Where($option)->Select($field)->First();
    }

    public function GetList($option = [], $lineNum = 0, $index = 0)
    {
        if ($lineNum > 0) {
            $this->Limit($lineNum, $index);
        }
        $this->join($this->table_g, 'groupId');
        if (Arrays::Is($option)) {
            foreach ($option as $key => $value) {
                $this->WhereJoin($key, $value);
            }
        }
        return $this->Select([
            'id', 'userName', 'createTime', 'lastTime', 'lastIp', 'groupId', 'siteIdByAdmin',
            'groupName' => [$this->table_g, 'name'],
        ])->ToList();
    }

}
