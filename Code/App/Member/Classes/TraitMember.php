<?php


namespace PKApp\Member\Classes;


use PKApp\Member\Model\LoginUserInfo;
use PKApp\Power\Classes\PowerService;
use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\Auth;

trait TraitMember
{

    protected function getOnlineOfUser()
    {
        return $this->checkPostFieldIsEmpty('userKey', 'Empty_userKey');
    }

    protected function online(): LoginUserInfo
    {
        static $m;
        if (empty($m)) {
            $key_user = $this->getOnlineOfUser();
            $sign = $this->checkPostFieldIsEmpty('userSign', 'Empty_userSign');
            $sign_tmp = fileHelper()->GetContentsByDisk($this->pathOfUserLogin() . $key_user . '.log');
            $sign == $sign_tmp ?: $this->noticeByJson('Err_userSign');
            // 用户信息
            $tmp_userInfo = Auth::Base64Encode($sign, false, $key_user, USER_ExpTime);
            $m = new LoginUserInfo();
            Arrays::ToModel(json_decode($tmp_userInfo, true), $m);
        }
        return $m;
    }

    protected function serviceByUser(): MemberService
    {
        static $cls;
        !empty($cls) ?: $cls = new MemberService();
        return $cls;
    }

    protected function serviceByGroup(): GroupService
    {
        static $cls;
        !empty($cls) ?: $cls = new GroupService();
        return $cls;
    }

    protected function serviceByPower(): PowerService
    {
        static $cls;
        !empty($cls) ?: $cls = new PowerService();
        return $cls;
    }

    protected function pathOfUserLogin(): string
    {
        static $is_exists;
        $path = PATH_TMP . 'UserLogin' . DS;
        if (!$is_exists) {
            $is_exists = fileHelper()->MKDir($path);
        }
        return $path;
    }

}