<?php


namespace PKApp\Member\Classes;

use PKFrame\Service;

class GroupService extends Service
{

    protected $moduleName = 'Member';

    protected function db(): GroupDataBase
    {
        static $db;
        !empty($db) ?: $db = new GroupDataBase();
        return $db;
    }

    public function GetEntity($viewParams = [], $viewField = '*')
    {
        static $entity;
        if (empty($entity)) {
            $entity = $this->db()->Where($viewParams)->Select($viewField)->First();
            !empty($entity) ?: $this->noticeByJson('Group_Empty');
        }
        return $entity;
    }

    public function GetList($viewParams = [], $viewField = '*')
    {
        return $this->db()->Where($viewParams)->Select($viewField)->ToList();
    }

    public function interface_getEntityById($id, $viewField = '*')
    {
        return $this->getEntityById($id,'Group_IdEmpty','Group_Empty', $viewField);
    }
}