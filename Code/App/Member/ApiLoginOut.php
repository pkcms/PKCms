<?php


namespace PKApp\Member;


use PKApp\Member\Classes\TraitMember;
use PKApp\Member\Classes\UserCtrl;

class ApiLoginOut extends UserCtrl
{
    use TraitMember;

    public function Main()
    {
        $this->online();
        fileHelper()->UnLink($this->pathOfUserLogin() . $this->getOnlineOfUser() . '.log');
        $this->json();
    }
}