<?php


namespace PKApp\Member;

use PKApp\Member\Classes\TraitMember;
use PKCommon\Controller\AdminGetDetailController;
use PKFrame\DataHandler\Numbers;

class AdminGetUser extends AdminGetDetailController
{
    use TraitMember;

    public function Main()
    {
        switch (request()->get('type')) {
            case 'admin':
                $this->loginUser()->GroupId == 1 ?: $this->noticeByJson('User_EditPower_Error');
                $modelId = 1;
                break;
            default:
                $modelId = 2;
                break;
        }
        $this->assign(array(
            'modelId' => $modelId,
        ));
        $this->fetch('admin_lists');
    }

    public function ApiByLists()
    {
        $modelId = request()->get('modelId');
        Numbers::IsId($modelId) ?: $this->noticeByJson('Model_IdEmpty','model');
        $this->GetPages();
        $options = ['modelId' => $modelId];
        $userCount = $this->serviceByUser()->GetCount($options);
        $userList = $this->serviceByUser()->GetList($options, [], self::$pageSize, self::$pageIndex);
        $this->json($userList, $userCount);
    }

    public function ApiByCount()
    {
        $modelId = request()->get('modelId');
        Numbers::IsId($modelId) ?: $this->noticeByJson('Model_IdEmpty','model');
        $this->json([], $this->serviceByUser()->GetCount(['modelId' => $modelId]));
    }

    public function ApiBySelect()
    {
        // TODO: Implement ApiBySelect() method.
    }

    public function ApiGetDetail()
    {
        $this->json(
            $this->serviceByUser()->interface_getEntityById(
                request()->get('id')
            )
        );
    }
}