<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
</head>

<body>

<div class="layui-fluid">

    <div class="layui-fluid">

        <div class="layui-card">
            <div class="layui-card-header layuiadmin-card-header-auto">
                <fieldset class="layui-elem-field layui-field-title">
                    <legend>修改密码</legend>
                </fieldset>
            </div>

            <div class="layui-card-body">
                <form class="layui-form" lay-filter="form-frame">
                    <div id="view-form"></div>

                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer">
                                <button class="layui-btn" lay-submit lay-filter="form-frame">立即提交</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                                <button type="button" class="layui-btn layui-btn-normal" onclick="confirm_is();">使用随机密码
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/html" data-id="<{$id}>" data-api="Member/AdminGetUser" id="tpl-form">
        <input type="hidden" name="id" value="{{#  if(d.id){ }}{{  d.id  }}{{#  } }}"/>
        <div class="layui-form-item">
            <label class="layui-form-label">会员名：</label>
            <div class="layui-input-block">
                <input type="text" name="userName" class="layui-input" disabled="disabled"
                       value="{{#  if(d.userName){ }}{{  d.userName  }}{{#  } }}" />
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">登录密码：</label>
            <div class="layui-input-block">
                <input type="password" name="password" class="layui-input"
                       lay-verify="required" autocomplete="off"/>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">确认密码：</label>
            <div class="layui-input-block">
                <input type="password" name="pwdconfirm" class="layui-input"
                       lay-verify="required" autocomplete="off"/>
            </div>
        </div>
        </script>

    <script type="text/javaScript" src="/statics/layui/layui.js"></script>
    <script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

    <script type="text/javascript">
        var pasArr = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

        //pasLen是你想要的密码的长度
        function passwords() {
            var pasLen = 12;
            var password = '';
            var pasArrLen = pasArr.length;
            for (var i = 0; i < pasLen; i++) {
                var x = Math.floor(Math.random() * pasArrLen);
                password += pasArr[x];
            }
            $('input[type="password"]').val(password);
            PKLayer.tipsAlert('请记下新密码：' + password,4);
        }

        function confirm_is() {
            PKLayer.tipsConfirmByTwoBtn(
                '是否使用高强度密码（包含特殊字符）？',
                    {icon: 3, title:'提示',btn:['是','否']},
                function (index) {
                    pasArr = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '_', '-', '$', '%', '&', '@', '+', '!'];
                    passwords();
                }, function () {
                    passwords();
                });
        }

        PKAdmin.ready(function () {
            PKAdmin.tplFormBySubmit({});
        });
    </script>
</body>
</html>
