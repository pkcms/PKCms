<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=7"/>
    <title>后台登陆</title>
    <link href="/statics/layui/css/layui.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/login.css" media="all">
    <link rel="stylesheet" href="/statics/pkcms/pkcms_login.css" media="all">
    <style>
        .layadmin-user-login {
            min-height: 50%;
        }
    </style>
</head>

<body class="body_login text-middle">
<div class="layadmin-user-login layadmin-user-display-show">

    <div class="layadmin-user-login-main login-box">
        <div class="layadmin-user-login-box layadmin-user-login-header">
            <h1>PKCMS后台管理系统</h1>
        </div>
        <div class="layadmin-user-login-box layadmin-user-login-body">
            <form class="layui-form" lay-filter="form-element">
                <input type="hidden" name="code" value="<{$code}>" />
                <div class="layui-form-item">
                    <label class="layadmin-user-login-icon layui-icon layui-icon-username"
                           for="LAY-user-login-userName"></label>
                    <input type="text" name="login" id="LAY-user-login-userName" lay-verify="required" placeholder="用户名"
                           class="layui-input" autocomplete="off">
                </div>
                <div class="layui-form-item">
                    <label class="layadmin-user-login-icon layui-icon layui-icon-password"
                           for="LAY-user-login-password"></label>
                    <input type="password" name="password" id="LAY-user-login-password" lay-verify="required"
                           placeholder="密码" class="layui-input" autocomplete="off">
                </div>
                <div class="layui-form-item">
                    <button class="layui-btn layui-btn-normal layui-btn-fluid layui-disabled" disabled="disabled" lay-submit lay-filter="form-element">
                        登 入
                    </button>
                </div>
                <div class="layui-form-item">
                    &copy; <a href="https://pkcms.cn" target="_blank">pkcms.cn</a> 版权所有
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javaScript" src="/statics/jquery/jquery-1.8.3.min.js"></script>
<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>
<script>
    PKAdmin.ready(function () {
        // $('.layui-btn').on('click', function () {
        //     return false;
        // });
        document.onkeydown = function (event) {
            var e = event || window.event || arguments.callee.caller.arguments[0];
            if (e && e.keyCode == 13) { // enter 键
                $('.layui-btn').click();
            }
        };
    });
</script>
</body>
</html>