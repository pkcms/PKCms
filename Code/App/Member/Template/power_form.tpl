<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
    <link rel="stylesheet" href="/statics/admin/dtree/dtree.css">
    <link rel="stylesheet" href="/statics/admin/dtree/font/dtreefont.css">
</head>

<body>

<div class="layui-fluid">

    <div class="layui-fluid">

        <div class="layui-card">
            <div class="layui-card-header layuiadmin-card-header-auto">
                <fieldset class="layui-elem-field layui-field-title">
                    <legend>用户角色 - 权限菜单</legend>
                </fieldset>
            </div>

            <div class="layui-card-body">
                <form class="layui-form" lay-filter="form-frame">
                    <div id="view-form"></div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer">
                                <button class="layui-btn layui-btn-lg" lay-submit lay-filter="form-frame">
                                    立即提交
                                </button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/html" data-id="<{$id}>" data-api="Member/AdminGetGroup" id="tpl-form">
    <input type="hidden" name="id" value="{{#  if(d.id){ }}{{  d.id  }}{{#  } }}"/>
    <div class="layui-form-item">
        <label class="layui-form-label">角色名：</label>
        <div class="layui-input-block">
            <input type="text" name="name" class="layui-input" disabled="disabled"
                   value="{{#  if(d.name){ }}{{  d.name  }}{{#  } }}"/>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">角色权限：</label>
        <div class="layui-input-block">
            <div id="menuTree" class="PK-Tree"></div>
        </div>
    </div>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">

    PKAdmin.ready(function () {
        PKAdmin.tplFormSubmitInFn({}, function (apiResult) {
            PKLayer.dtree(function (dtree) {
                // 初始化树
                var powerNode = dtree.render({
                    elem: "#menuTree",
                    url: "/index.php/Power/AdminGetPower/GetPowerMenu/?groupId=<{$id}>",
                    cache: false,
                    dataStyle: "layuiStyle",
                    width: '100%',
                    type: "all",
                    initLevel: 1,
                    checkbar: true,
                    checkBarType: "no-all",
                    response: {
                        statusCode: 0,
                        message: "msg",
                        treeId: "id",
                        parentId: "parentId",
                        title: "name",
                        childName: "children",
                    },
                    done: function (res, $ul, first) {
                        if (first && apiResult.hasOwnProperty('powerById')) {
                            dtree.chooseDataInit("menuTree", apiResult.powerById);
                        }
                        PKLayer.useFormByControlAndSubmit('form-frame', function (paramsByForm) {
                            PKLayer.apiPost('Member/AdminSetPowerMenuByUser/ApiByChange', paramsByForm,
                                function (res, count) {
                                    PKLayer.tipsMsg('保存成功');
                                });
                        });
                    }
                });
            });
        });
    });

</script>
</body>
</html>
