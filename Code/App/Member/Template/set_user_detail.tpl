<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
</head>

<body>

<div class="layui-fluid">

    <div class="layui-fluid">

        <div class="layui-card">
            <div class="layui-card-header layuiadmin-card-header-auto">
                <fieldset class="layui-elem-field layui-field-title">
                    <legend>用户信息</legend>
                </fieldset>
            </div>

            <div class="layui-card-body">
                <form class="layui-form" lay-filter="form-frame">
                    <div id="view-form"></div>

                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer">
                                <button class="layui-btn" lay-submit lay-filter="form-frame">立即提交</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/html" data-id="<{$id}>" data-api="Member/AdminGetUser" id="tpl-form">
        <input type="hidden" name="id" value="{{#  if(d.id){ }}{{  d.id  }}{{#  } }}"/>
        <input type="hidden" name="modelId" value="{{#  if(d.modelId){ }}{{  d.modelId  }}{{# }else{ }}<{$modelId}>{{#  } }}"/>
        <div class="layui-form-item">
            <label class="layui-form-label">会员名：</label>
            <div class="layui-input-block">
                <input type="text" name="userName" class="layui-input" lay-verify="required" autocomplete="off"
                       value="{{#  if(d.userName){ }}{{  d.userName  }}{{#  } }}"/>
            </div>
        </div>
        {{#  if(d.id!=1){ }}
            <div class="layui-form-item">
                <label class="layui-form-label">角色选择：</label>
                <div class="layui-input-block">
                    <div class="pkcms_select" id="groupId"
                         data-name="groupId" data-value="{{#  if(d.groupId){ }}{{  d.groupId  }}{{#  } }}"
                         data-api="Member/AdminGetGroup" data-params="modelId={{#  if(d.modelId){ }}{{  d.modelId  }}{{#  }else{ }}<{$modelId}>{{#  } }}"></div>
                </div>
            </div>
        {{#  } }}
        <div class="layui-form-item">
            <label class="layui-form-label">邮箱：</label>
            <div class="layui-input-block">
                <input type="text" name="email" class="layui-input"
                       value="{{#  if(d.email){ }}{{  d.email  }}{{#  } }}"/>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">手机：</label>
            <div class="layui-input-block">
                <input type="text" name="mobile" class="layui-input"
                       value="{{#  if(d.mobile){ }}{{  d.mobile  }}{{#  } }}"/>
            </div>
        </div>
    </script>

    <script type="text/javaScript" src="/statics/layui/layui.js"></script>
    <script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

    <script type="text/javascript">
        PKAdmin.ready(function () {
            PKAdmin.tplFormBySubmit({});
        });
    </script>
</body>
</html>
