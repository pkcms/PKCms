<?php


namespace PKApp\Member;


use PKApp\Member\Classes\UserCtrl;

class ApiGetInfoOfUser extends UserCtrl
{

    public function Main()
    {
        $this->json(['userName' => $this->online()->userName,
            'phone' => $this->online()->mobile, 'phoneActivation' => $this->online()->phoneActivation,
            'email' => $this->online()->email, 'emailActivation' => $this->online()->emailActivation,
            'createTime' => $this->online()->createTime]);
    }
}