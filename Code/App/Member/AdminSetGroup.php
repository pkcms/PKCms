<?php

namespace PKApp\Member;

use PKApp\Member\Classes\TraitMember;
use PKCommon\Controller\AdminSetController;

class AdminSetGroup extends AdminSetController
{
    use TraitMember;

    public function Main()
    {
        $this->assign([
            'id' => request()->has('id', 'get') ? request()->get('id') : 0
        ])->fetch('set_group');
    }

    public function ApiByCreate()
    {
        $post = $this->model();
        $this->serviceByGroup()->Add($post);
        $this->_inputSuccess($post['modelId']);
    }

    public function ApiByChange()
    {
        $post = $this->model();
        $id = $this->checkPostFieldIsEmpty('id','Group_IdEmpty');
        $this->serviceByGroup()->UpdateById($id, $post);
        $this->_inputSuccess($post['modelId']);
    }

    private function _inputSuccess($modelId)
    {
        $cache_key = 'Group_' . $modelId;
        cache()->Disk()->DelByData($cache_key);
    }

    public function ApiByDel()
    {
        // TODO: Implement ApiByDel() method.
    }

    protected function model(): array
    {
        return $this->checkModel(['name','modelId']);
    }
}