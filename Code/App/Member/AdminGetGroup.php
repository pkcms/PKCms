<?php


namespace PKApp\Member;

use PKApp\Member\Classes\TraitMember;
use PKCommon\Controller\AdminGetDetailController;

class AdminGetGroup extends AdminGetDetailController
{

    use TraitMember;

    public function Main()
    {
        $this->fetch('group_lists');
    }

    public function ApiByLists()
    {
        $groupList = $this->serviceByGroup()->GetList([], ['id', 'name', 'isDisabled', 'isSystem']);
        $this->json($groupList);
    }

    public function ApiBySelect()
    {
        $modelId = $this->getId('modelId', 'Model_IdEmpty', 'Model');
        $cache_key = 'Group_' . $modelId;
        $groupList = cache()->Disk()->ReadByData($cache_key);
        if (empty($groupList)) {
            $groupList = $this->serviceByGroup()->GetList(
                ['modelId' => $modelId],
                ['id', 'name']
            );
            if ($modelId == 1) {
                foreach ($groupList as $index => $item) {
                    $item['status'] = $item['id'] == 1 ? 0 : 1;
                    $groupList[$index] = $item;
                }
            }
            cache()->Disk()->WriteByData($cache_key, $groupList);
        }
        $this->json($groupList);
    }

    public function ApiGetDetail()
    {
        $this->json(
            $this->serviceByGroup()->interface_getEntityById(
                request()->get('id')
            )
        );
    }
}