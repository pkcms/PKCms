<?php


namespace PKApp\Member\Model;


class LoginUserInfo
{
    public $id;
    public $modelId;
    public $groupId;
    public $userName;
    public $mobile;
    public $phoneActivation;
    public $email;
    public $emailActivation;
    public $createTime;
}