<?php


namespace PKApp\Member;

use PKApp\Member\Classes\TraitAuth;
use PKApp\Member\Classes\TraitMember;
use PKCommon\Controller\AdminSetController;
use PKFrame\DataHandler\Numbers;

class AdminSetUserPass extends AdminSetController
{
    use TraitMember, TraitAuth;

    public function Main()
    {
        $userId = \request()->get('id');
        Numbers::IsId($userId) ?: $this->noticeByPage('Empty_userId');
        $this->assign([
            'id' => $userId
        ])->fetch('set_user_pass');
    }

    public function ApiByCreate()
    {
    }

    public function ApiByChange()
    {
        $this->model();
        $id = request()->post('id');
        $pwd = request()->post('password');
        if ($this->isLowLevelOfPassword($pwd)) {
            $this->noticeByJson('FailPwd_LowLevel');
        }
        $this->serviceByUser()->SetPass($id, $pwd);
        $this->inputSuccess();
    }

    public function ApiByDel()
    {
    }

    protected function model()
    {
        $post = $this->checkModel(['id', 'password', 'pwdconfirm']);
        Numbers::IsId($post['id']) ?: $this->noticeByJson('Empty_userId');
        $post['password'] = $this->checkPostFieldIsEmpty('password', 'Empty_userPwd');
        if (strlen($post['password']) < 6) {
            $this->noticeByJson('Empty_userPwd');
        } elseif ($post['password'] != $post['pwdconfirm']) {
            $this->noticeByJson('Err_confirmPwd');
        }
    }
}