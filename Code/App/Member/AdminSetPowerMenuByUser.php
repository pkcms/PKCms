<?php


namespace PKApp\Member;

use PKApp\Member\Classes\TraitMember;
use PKCommon\Controller\AdminSetController;
use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\Numbers;

class AdminSetPowerMenuByUser extends AdminSetController
{

    use TraitMember;

    public function Main()
    {
        $this->assign([
            'id' => request()->has('id', 'get') ? request()->get('id') : 0
        ])->fetch('power_form');
    }

    public function ApiByCreate()
    {
    }

    public function ApiByChange()
    {
        $params = $this->model();
        $this->serviceByGroup()->UpdateById($params['id'], $params);
        $this->inputSuccess();
    }

    public function ApiByDel()
    {
    }

    protected function model(): array
    {
        $post = $this->checkModel(['id', 'menuTree']);
        Numbers::IsId($post['id']) ?: $this->noticeByJson('Empty_userId');
        $result = ['id' => $post['id'], 'powerByAction' => '', 'powerById' => ''];
        if (Arrays::Is($post['menuTree'])) {
            $powerById = [];
            foreach ($post['menuTree'] as $item) {
                $powerById[] = Numbers::To($item);
            }
            $powerById = array_unique($powerById);
            $result['powerById'] = implode(',', $powerById);
            $db_power_action = $this->serviceByPower()->GetAdminActionList($powerById);
            $result['powerByAction'] = implode(',', Arrays::GetArrayByKey($db_power_action, 'action'));
        }
        return $result;
    }

}
