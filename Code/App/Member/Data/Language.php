<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return array(
    'GuestIP_isExpire' => array(
        'zh-cn' => '你的操作太频繁了，请喝怀茶休息一下！',
        'en' => 'Your operation is too frequent, please drink Huaicha and rest!',
    ),
    'Group_Empty' => array(
        'zh-cn' => '角色信息找不到或不存在',
        'en' => 'The role information cannot be found or does not exist',
    ),
    'Group_IdEmpty' => array(
        'zh-cn' => '角色信息找不到或不存在',
        'en' => 'The role information cannot be found or does not exist',
    ),
    'User_EditPower_Error' => array(
        'zh-cn' => '您当前没有用户编辑的权限操作',
        'en' => 'You currently do not have permission for user editing operations',
    ),
    'FailLogin_LowLevelOfPassword' => array(
        'zh-cn' => '疑似猜测登陆密码。',
        'en' => 'Suspected of guessing the login password.',
    ),
    'Fail_loginSizeByMax' => array(
        'zh-cn' => '验证次数过多，已经被封锁',
        'en' => 'Too many verifications, blocked',
    ),
    'FailPwd_LowLevel' => array(
        'zh-cn' => '您修改的密码过于简单，建议使用强度在四星级及以上的密码',
        'en' => 'The password you modified is too simple. It is recommended to use a password with a strength of four stars or above',
    ),
    'TipsLogin_time' => array(
        'zh-cn' => '登陆的时间为非正常工作时间',
        'en' => 'The login time is not normal working hours',
    ),
    'Error_onOnline' => array(
        'zh-cn' => '用户不在线，请重新登陆',
        'en' => 'User is not online, please log in again',
    ),
    'Error_login' => array(
        'zh-cn' => '用户登陆验证失败',
        'en' => 'User login verification failed',
    ),
    'Empty_code' => array(
        'zh-cn' => '登陆码不能为空',
        'en' => 'Login code cannot be empty',
    ),
    'Empty_loginType' => array(
        'zh-cn' => '用户的登陆类型不支持',
        'en' => "The user's login type is not supported",
    ),
    'Empty_User' => array(
        'zh-cn' => '用户信息找不到或不存在',
        'en' => 'User information cannot be found or does not exist',
    ),
    'Empty_userId' => array(
        'zh-cn' => '用户ID不能为空',
        'en' => '用户ID不能为空',
    ),
    'Empty_userKey' => array(
        'zh-cn' => '用户在线会话不能为空',
        'en' => "The user's online session cannot be empty",
    ),
    'Empty_userName' => array(
        'zh-cn' => '用户名不能为空',
        'en' => 'User name cannot be empty',
    ),
    'Empty_userPwd' => array(
        'zh-cn' => '用户密码长度不能小于6位',
        'en' => 'The user password length cannot be less than 6 digits',
    ),
    'Empty_userSign' => array(
        'zh-cn' => '用户的签名不能为空',
        'en' => "The user's signature cannot be empty",
    ),
    'Empty_userEmail' => array(
        'zh-cn' => '用户的邮箱不能为空',
        'en' => "The user's email cannot be empty",
    ),
    'Empty_confirmPwd' => array(
        'zh-cn' => '用户的确认邮箱不能为空',
        'en' => "The user's confirmation email cannot be empty",
    ),
    'Empty_authCode' => array(
        'zh-cn' => '验证码不能为空',
        'en' => "The verification code cannot be empty",
    ),
    'Exists_userName' => array(
        'zh-cn' => '该用户名已经存在',
        'en' => "The username already exists",
    ),
    'Exists_email' => array(
        'zh-cn' => '该电子邮箱已经存在',
        'en' => "The email already exists",
    ),
    'noExists_email' => array(
        'zh-cn' => '该电子邮箱不存在',
        'en' => 'The email does not exist',
    ),
    'Err_confirmPwd' => array(
        'zh-cn' => '两次输入的密码不一致',
        'en' => 'Entered passwords differ!',
    ),
    'Err_email' => array(
        'zh-cn' => '用户邮箱的格式错误。',
        'en' => "The format of the user's email is incorrect.",
    ),
    'Err_pwd' => array(
        'zh-cn' => '用户密码至少8-16个字符，至少1个大写字母，1个小写字母和1个数字，其他可以是任意字符。',
        'en' => 'The user password should be at least 8-16 characters, at least 1 uppercase letter, 1 Minuscule and 1 number, and the others can be any character.',
    ),
    'Err_userName' => array(
        'zh-cn' => '用户名的格式错误，必须以字母（a~z）、数字（0~9）或下划线（_）的组合。',
        'en' => 'The format of the username is incorrect and must be a combination of letters (a-z), numbers (0-9), or underscores (_).',
    ),
    'Err_userSign' => array(
        'zh-cn' => '用户的签名验证不通过',
        'en' => "User's signature verification failed",
    ),
    'Err_code' => array(
        'zh-cn' => '验证码错误，或者已经失效，请重新验证。',
        'en' => 'The verification code is incorrect or has expired. Please verify again.',
    ),
    'User_Group_AdministratorExists' => array(
        'zh-cn' => '超级管理员是只能一个',
        'en' => 'Super administrators can only have one',
    ),
    'PublicLogin_Error' => array(
        'zh-cn' => '该用户不是前台用户，请重新输入',
        'en' => 'The user is not a front-end user, please re-enter',
    ),
    'Site_EditError' => array(
        'zh-cn' => '该用户不能分配站点',
        'en' => 'This user cannot assign a site',
    ),
    'Verify_email' => array(
        'zh-cn' => '在浏览器中打开右边的验证链接：',
        'en' => 'Open the verification link on the right in the browser:',
    ),
    'Activation_Done' => array(
        'zh-cn' => '恭喜你，激活成功！',
        'en' => 'Congratulations, activation successful!',
    ),
);