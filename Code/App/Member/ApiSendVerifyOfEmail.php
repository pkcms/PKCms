<?php


namespace PKApp\Member;


use PKApp\Member\Classes\TraitMember;
use PKApp\Member\Classes\UserCtrl;
use PKCommon\MailTrait;
use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\Auth;
use PKFrame\DataHandler\JSON;
use PKFrame\DataHandler\MatchHelper;

class ApiSendVerifyOfEmail extends UserCtrl
{

    use TraitMember, MailTrait;

    public function Main()
    {
        $email = $this->checkPostFieldIsEmpty('email', 'Empty_userEmail');
        MatchHelper::MatchFormat('email', $email, 'Err_email', request()->module());
        $is_exists = $this->serviceByUser()->GetEntity(['email' => $email], ['id', 'encrypt', 'emailActivation']);
        Arrays::Is($is_exists) ?: $this->noticeByJson('noExists_email');
        $params = ['sendTime' => TIMESTAMP, 'expTime' => 600, 'encrypt' => $is_exists['encrypt']];
        $code = Auth::Base64Encode(JSON::EnCode($params), true, md5($email), 600);
        $url = request()->domain() . 'index.php/Member/ActivationOfEmail?e=' . $email . '&c=' . $code;
        $html = language('Verify_email') . "\r\n" . $url;
        $this->sendMail($email, $html);
        $this->json();
    }
}