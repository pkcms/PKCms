<?php


namespace PKApp\Member;


use PKApp\Member\Classes\TraitMember;
use PKApp\Member\Classes\UserCtrl;
use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\Auth;

class ActivationOfEmail extends UserCtrl
{

    use TraitMember;

    public function Main()
    {
        $email = trim(request()->get('e'));
        $code = trim(request()->get('c'));
        !empty($email) ?: $this->noticeByPage('Empty_userEmail');
        !empty($code) ?: $this->noticeByPage('Empty_authCode');
        $is_exists = $this->serviceByUser()->GetEntity(['email' => $email], ['id', 'encrypt']);
        Arrays::Is($is_exists) ?: $this->noticeByPage('noExists_email');
        $params = Auth::Base64Encode($code, false, md5($email), 600);
        if (!empty($params)) {
            $params = json_decode($params,true);
            if ($params['encrypt'] != $is_exists['encrypt']) {
                $this->noticeByPage('Err_code');
            }
            $this->serviceByUser()->Update(['email' => $email], ['emailActivation' => 1]);
            $this->noticeByPage('Activation_Done');
        } else {
            $this->noticeByPage('Err_code');
        }
    }
}