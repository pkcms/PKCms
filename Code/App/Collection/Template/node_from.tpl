<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
</head>

<body>

<div class="layui-fluid">

    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <fieldset class="layui-elem-field layui-field-title">
                <legend>采集配置</legend>
            </fieldset>
        </div>

        <div class="layui-card-body layui-card-body-mb">
            <form class="layui-form" lay-filter="form-config">
                <div id="view-step">
                    <div id="view-form"></div>


                    <div class="layui-form-footer-fixed">
                        <div class="layui-input-block">
                            <div class="layui-footer">
                                <button class="layui-btn" lay-submit lay-filter="form-config">采集</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/html" id="tpl-form">
    <div class="layui-card">
        <div class="layui-card-header layui-bg-gray">内容网址采集</div>
        <div class="layui-card-body">
            <div class="layui-form-item">
                <label class="layui-form-label">
                    采集页面编码:
                </label>
                <div class="layui-input-block">
                    <div class="pkcms_select" id="sourceCharsetid" data-name="sourceCharset"
                         data-value="{{#  if(d.sourceCharset){ }}{{  d.sourceCharset  }}{{#  } }}"
                         data-api="Collection/AdminAPI/GetCharsetList"></div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">
                    列表网址:
                </label>
                <div class="layui-input-block">
                    <input type="text" name="listConfig[urlPage]" class="layui-input"
                           lay-verify="required" autocomplete="off"
                           value="{{#  if((d.listConfig != null) && d.listConfig.urlPage){ }}{{  d.listConfig.urlPage  }}{{#  } }}"/>
                    <div class="layui-form-mid layui-word-aux">
                        如：http://……/help_(*).htm,页码使用(*)做为通配符。
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label">
                        页码范围:
                    </label>
                    <div class="layui-form-mid">从（第1页）:</div>
                    <div class="layui-input-inline">
                        <input type="number" name="listConfig[pageSize_start]" class="layui-input"
                               lay-verify="required" autocomplete="off"
                               value="{{#  if((d.listConfig != null) && d.listConfig.pageSize_start){ }}{{  d.listConfig.pageSize_start  }}{{#  } }}"/>
                    </div>
                    <div class="layui-form-mid">到（第n页）:</div>
                    <div class="layui-input-inline">
                        <input type="number" name="listConfig[pageSize_end]" class="layui-input"
                               lay-verify="required" autocomplete="off"
                               value="{{#  if((d.listConfig != null) && d.listConfig.pageSize_end){ }}{{  d.listConfig.pageSize_end  }}{{#  } }}"/>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label">
                        列表起始代码:
                    </label>
                    <div class="layui-input-inline">
                        <textarea class="layui-textarea"
                                  name="listConfig[code_start]">{{#  if((d.listConfig != null) && d.listConfig.code_start){ }}{{  d.listConfig.code_start  }}{{#  } }}</textarea>
                    </div>
                    <div class="layui-form-mid">列表结束代码:</div>
                    <div class="layui-input-inline">
                        <textarea class="layui-textarea"
                                  name="listConfig[code_end]">{{#  if((d.listConfig != null) && d.listConfig.code_end){ }}{{  d.listConfig.code_end  }}{{#  } }}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="layui-card">
        <div class="layui-card-header layui-bg-gray">正文内容采集配置</div>
        <div class="layui-card-body">
            <div class="layui-form-item">
                <label class="layui-form-label">
                    标题匹配:
                </label>
                <div class="layui-input-block">
                        <textarea class="layui-textarea"
                                  name="rule[title][rule_code]">{{#  if((d.rule != null) && (d.rule.title != null) && d.rule.title.rule_code){ }}{{  d.rule.title.rule_code  }}{{#  } }}</textarea>
                    <div class="layui-form-mid layui-word-aux">
                        使用"^"作为通配符（代表的是要采集的内容），其的两边则是确定其在源代码中位置的起始代码和结束代码。
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">
                    内容匹配:
                </label>
                <div class="layui-input-block">
                        <textarea class="layui-textarea"
                                  name="rule[content][rule_code]">{{#  if((d.rule != null) && (d.rule.content != null) && d.rule.content.rule_code){ }}{{  d.rule.content.rule_code  }}{{#  } }}</textarea>
                    <div class="layui-form-mid layui-word-aux">
                        使用"^"作为通配符（代表的是要采集的内容），其的两边则是确定其在源代码中位置的起始代码和结束代码。
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>

<script type="text/html" id="tpl-test">
    <div class="layui-row">
        <div class="layui-col-md5">
            <div class="layui-card" id="step_test_url">
                <div class="layui-card-header layui-bg-cyan">
                    内容页网址列表
                </div>
                <div class="layui-card-body">
                    <ul>
                        {{#  layui.each(d.list, function(index, item){ }}
                        <li onclick="checkUrl('{{ item }}');">{{ item }}</li>
                        {{#  }); }}
                    </ul>
                    <button type="button" id="nextImport" class="layui-btn layui-disabled"
                            onclick="stopImportCategory();">
                        测试<b id="test_number">3</b>次，选择导入栏目设置
                    </button>
                </div>
            </div>
            <div class="layui-card layui-hide" id="step-select-category">
                <div class="layui-card-header layui-bg-cyan">选择要导入的栏目</div>
                <div class="layui-card-body">
                    <div class="layui-form">
                        <div class="layui-form-item" style="color: red;font-weight: bold;">
                            请保持当前浏览器的标签为打开状态，否则会影响采集进度和视觉效果！
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">选择栏目</label>
                            <div class="layui-input-block">
                                <div id="select-category"></div>
                                <div id="show-category" class="layui-hide"></div>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">导入操作</label>
                            <div class="layui-input-block">
                                <button class="layui-btn layui-btn-danger layui-disabled"
                                        id="btn-import" data-action="start" onclick="btnImport($(this));">
                                    <i class="layui-icon">&#xe652;</i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="layui-col-md7">
            <div class="layui-card" id="step-test-content">
                <div class="layui-card-header layui-bg-cyan">
                    点击左边的内容页地址检验内容的正确性<b style="color: yellow">（至少测试三条）</b>
                </div>
                <div class="layui-card-body" id="view-preview"></div>
            </div>
            <div class="layui-card layui-hide" id="step-import">
                <div class="layui-card-header layui-bg-cyan">
                    采集及导入栏目的进度
                </div>
                <div class="layui-card-body">
                    <ul id="view-import"></ul>
                </div>
            </div>
        </div>
    </div>
</script>

<script type="text/html" id="tpl-preview">
    <div class="layui-card">
        <div class="layui-card-body">
            <div class="layui-form">
                <div class="layui-form-item">
                    <label class="layui-form-label">
                        标题:
                    </label>
                    <div class="layui-input-block">{{  d.title  }}</div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">
                        访问地址:
                    </label>
                    <div class="layui-input-block">
                        <a href="{{  d.url  }}" target="_blank">{{  d.url  }}</a>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">
                        内容:
                    </label>
                    <div class="layui-input-block">{{  d.content  }}</div>
                </div>
            </div>
        </div>
    </div>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">

    var data = {
        api_uri: '/Collection/AdminGetNode/',
        apiUri_collection: '/Collection/AdminCollection/',
        check_number_test: 3,
        state: '',
        list_category: [],
        import_params_init: {
            category: 0
        },
        import_params: {}
    };

    function checkUrl(url) {
        PKLayer.apiPost(data.api_uri + 'TestContentUrl', {url:url}, function (res) {
            PKLayer.useTpl(res, 'tpl-preview', 'view-preview');
            data.check_number_test = Math.max(data.check_number_test -= 1, 0);
            document.getElementById('test_number').innerText = data.check_number_test;
            if (data.check_number_test <= 0) {
                data.check_number_test = 0;
                $('#nextImport').removeClass('layui-disabled');
            }
        });
    }

    function btnImport(btn) {
        var action = btn.data('action');
        data.state = action;
        console.log(action);
        switch (action) {
            case 'start':
                changeBtnImportState('&#xe651;', 'stop');
                $('#select-category').addClass('layui-hide');
                $('#show-category').removeClass('layui-hide');
                startCollection();
                break;
            case 'stop':
                changeBtnImportState('&#xe652;', 'start');
                $('#select-category').removeClass('layui-hide');
                $('#show-category').addClass('layui-hide');
                break;
        }
    }

    function changeBtnImportState(icon, action) {
        $('#btn-import i').html(icon);
        $('#btn-import').data('action', action);
    }

    function startCollection() {
        PKLayer.apiGet(data.apiUri_collection + 'CheckCategory',
                {category_id:data.import_params_init.category}, function (res) {
                data.import_params = Object.assign(Object.create(null), data.import_params_init, res);
                collectionOfContentUrl();
            });
    }

    function collectionOfContentUrl() {
        PKLayer.apiPost(data.apiUri_collection + 'CollectionContentUrl', data.import_params, function (res) {
            if (res.hasOwnProperty('page_now') && res.hasOwnProperty('page_start')
                && res.hasOwnProperty('page_end')) {
                if (res.page_now >= res.page_start && res.page_now < res.page_end) {
                    // next
                    $('#view-import').prepend('<li>已经采集列表，第' + res.page_now + '页</li>');
                    res.page_now += 1;
                    data.import_params = Object.assign(data.import_params, res);
                    collectionOfContentUrl();
                } else {
                    // done
                    $('#view-import').prepend('<li>采集列表，已经完成</li>');
                    collectionOfContentDetail();
                }
            }
        });
    }

    function collectionOfContentDetail() {
        PKLayer.apiPost(data.apiUri_collection + 'collectionContentDetail', data.import_params, function (res) {
            if (res.hasOwnProperty('collection_index') && res.hasOwnProperty('collection_count')
                && res.hasOwnProperty('page_now') && res.hasOwnProperty('page_start')
                && res.hasOwnProperty('page_end')) {
                if (res.page_now >= res.page_start && res.page_now < res.page_end) {
                    // next
                    $('#view-import').prepend('<li>已经采集的内容，第' + res.page_now + '页，第' + res.collection_index + '条</li>');
                    if (res.collection_index <= res.collection_count) {
                        // next
                        res.collection_index += 1;
                    } else {
                        // done
                        res.page_now += 1;
                        res.collection_index = 0;
                    }
                    data.import_params = Object.assign(data.import_params, res);
                    collectionOfContentDetail();
                } else {
                    // done
                    $('#view-import').prepend('<li>采集流程，已经完成</li>');
                }
            }
        });
    }

    function stopImportCategory() {
        $('#step_test_url,#step-test-content').addClass('layui-hide');
        $('#step-select-category,#step-import').removeClass('layui-hide');
        PKLayer.useFormBySelectFun('#select-category', data.list_category, null, true, true, function (rows) {
            if (rows.length === 1) {
                data.import_params_init.category = rows[0].value;
                $('#show-category').text(rows[0].name);
                $('#btn-import').removeClass('layui-disabled');
            } else {
                data.import_params_init.category = 0;
                $('#show-category').text();
                $('#btn-import').addClass('layui-disabled');
            }
        });
    }

    function openModel(params) {
        PKLayer.useTplResult(params, 'tpl-test', function (html) {
            layui.use(['layer'], function () {
                var layer = layui.layer;
                layer.open({
                    type: 1,
                    title: '验证配置规则是否正确',
                    area: ["80%", "80%"],
                    content: html,
                    offset: 't',
                    closeBtn: 0,
                    btn: ['关闭'],
                    yes: function (index, layero) {
                        if (data.state === 'start') {
                            PKLayer.tipsMsg('采集已经启动不能关闭！请等待任务的执行完毕。');
                        } else {
                            data.check_number_test = 3;
                            layer.close(index);
                        }
                    }
                });
            });
        });
    }

    PKAdmin.ready(function () {
        PKLayer.apiGet('Content/AdminGetCategory/ApiBySelect', null, function (res) {
            data.list_category = res;
        });
        PKLayer.apiGet(data.api_uri + 'GetConfig', null, function (res) {
            PKLayer.useTplCall(res, 'tpl-form', 'view-form', function () {
                // 渲染
                PKLayer.useFormByControlAndSubmit('form-config', function (paramsByForm) {
                    PKLayer.apiPost(data.api_uri + 'SaveConfig', paramsByForm, function () {
                        PKLayer.apiGet(data.api_uri + 'TestListUrl', null, function (res_test) {
                            openModel({list:res_test});
                        });
                    });
                });
            });
        });
    });
</script>
</body>
</html>
