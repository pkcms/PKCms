<?php


namespace PKApp\Collection;

use PKApp\Collection\Classes\TraitCollection;
use PKCommon\Controller\AdminController;
use PKFrame\DataHandler\Arrays;

class AdminGetNode extends AdminController
{
    use TraitCollection;

    public function Main()
    {
        $this->fetch('node_from');
    }

    public function SaveConfig()
    {
        $this->writeConfig(request()->post());
        $this->json();
    }

    public function GetConfig()
    {
        $this->json($this->readConfig());
    }

    public function TestListUrl()
    {
        $config = $this->readConfig();
        $listConfig = Arrays::GetKey('listConfig', $config, 'Empty_configOfList');
        $page = Arrays::GetKey('pageSize_start', $listConfig, 'Empty_configOfUrlPage');
        $this->json($this->getListsUrlOfContent($page));
    }

    public function TestContentUrl()
    {
        $this->json($this->getDetailOfContent(
            trim(request()->post('url'))
        ));
    }

}