<?php

namespace PKApp\Collection;

use PKApp\Collection\Classes\TraitCollection;
use PKApp\Content\Classes\TraitContent;
use PKApp\Model\Classes\TraitModel;
use PKCommon\Controller\AdminController;
use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\JSON;
use PKFrame\DataHandler\Numbers;

class AdminCollection extends AdminController
{

    use TraitCollection, TraitContent, TraitModel;

    public function Main()
    {
        switch (request()->action()) {
            case 'CheckCategory':
                $this->checkCategory();
                break;
            case 'CollectionContentUrl':
                $this->collectionContentUrl();
                break;
        }
    }

    protected function checkCategory()
    {
        $id_cat = $this->getId('category_id');
        $category = $this->serviceOfCategory(
            $this->loginUser()->SiteId
        )->interface_getEntityById($id_cat);
        $category['categoryType'] == 'list' ?: $this->noticeByJson('Error_categoryType');
        // 检查数据模型
        $entity_field = $this->serviceOfField()->GetEntity(['modelId' => $category['modelId'], 'field' => 'content']);
        Arrays::Is($entity_field) ?: $this->noticeByJson('Error_fieldOfContent');
        // 保存检验数据
        $config = $this->readConfig();
        $config['import_category'] = $id_cat;
        $this->writeConfig($config);
        $this->json(['import_category' => $id_cat]);
    }

    protected function collectionContentUrl()
    {
        $post = request()->post();
        [$page_start, $page_now, $page_end] = $this->_checkPage();
        $list_content = $this->getListsUrlOfContent($page_now);
        fileHelper()->PutContents($this->tmpPath_collection, 'collection_' . $page_now . '.json', JSON::EnCode($list_content));
        $result = array_merge($post, ['page_start' => $page_start, 'page_end' => $page_end, 'page_now' => $page_now]);
        $this->json($result);
    }

    protected function collectionContentDetail()
    {
        $post = request()->post();
        $collection_index = max(Numbers::To($post['collection_index']), 1);
        [$page_start, $page_now, $page_end] = $this->_checkPage();
        $tmp_listUrl = json_decode(
            fileHelper()->GetContentsByDisk($this->tmpPath_collection . DS . 'collection_' . $page_now . '.json'),
            true);
        $res_detail = $this->getDetailOfContent($tmp_listUrl[$collection_index]);
        $result = array_merge($post, $res_detail, ['collection_index' => $collection_index,
            'collection_count' => count($tmp_listUrl),
            'page_start' => $page_start, 'page_end' => $page_end, 'page_now' => $page_now]);
        $this->json($result);
    }

    private function _checkPage(): array
    {
        $config = $this->readConfig();
        $listConfig = Arrays::GetKey('listConfig', $config, 'Node_listConfigEmpty');
        $page_start = $listConfig['pageSize_start'];
        $page_end = $listConfig['pageSize_end'];
        $page_end - $page_start <= 10 ?: $this->noticeByJson('Error_MaxPage');
        $page_now = max(Numbers::To(request()->post('page_now')), $page_start);
        $page_now >= $page_start && $page_now <= $page_end ?: $this->noticeByJson('Error_page');
        return [$page_start, $page_now, $page_end];
    }

}
