<?php


namespace PKApp\Collection\Classes;


use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\Auth;
use PKFrame\DataHandler\JSON;
use PKFrame\DataHandler\MatchHelper;
use PKFrame\DataHandler\Pages;

trait TraitCollection
{

    protected $file_config = 'CollectionConfig.json';
    protected $tmpPath_collection = PATH_TMP . 'collection';

    protected function getSourceCode(string $url = null, string $sourceCharset = null): string
    {
        !empty($url) ?: $this->noticeByJson('Node_UrlPageEmpty');
        $html = fileHelper()->GetContentsByHTTP(Auth::HtmlspecialcharsDeCode($url), 180);
        if (CHARSET != $sourceCharset) {
            $html = iconv($sourceCharset, CHARSET . '//TRANSLIT//IGNORE', $html);
        }
        !empty($html) ?: $this->noticeByJson('Empty_sourceCode', request()->module(), $url);
        $search = array("'<script[^>]*?>.*?</script>'si", "'<script*?>.*?</script>'si",
            "'<style[^>]*?>.*?</style>'si");
        return Pages::zip(preg_replace($search, "", $html));
    }

    protected function getListsUrlOfContent(int $pageIndex): array
    {
        $config = $this->readConfig();
        $sourceCharset = Arrays::GetKey('sourceCharset', $config, 'Empty_sourceCharset');
        $listConfig = Arrays::GetKey('listConfig', $config, 'Empty_configOfList');
        $url = str_replace('(*)', $pageIndex,
            Arrays::GetKey('urlPage', $listConfig, 'Empty_configOfUrlPage'));
        $sourceCode = $this->getSourceCode($url, $sourceCharset);
        $start = Pages::zip(Auth::HtmlspecialcharsDeCode(Arrays::GetKey('code_start', $listConfig)));
        $end = Pages::zip(Auth::HtmlspecialcharsDeCode(Arrays::GetKey('code_end', $listConfig)));
        $cut_html = $this->_cutHtmlCode($sourceCode, $start, $end);
        return $this->_contentUrl($url, $cut_html);
    }

    protected function getDetailOfContent(string $url_content): array
    {
        $config = $this->readConfig();
        $ruleConfig = Arrays::GetKey('rule', $config, 'Empty_configOfRule');
        MatchHelper::checkUrlOfScheme($url_content) ?: $this->noticeByJson('Error_contextUrl');
        $sourceCharset = Arrays::GetKey('sourceCharset', $config, 'Empty_sourceCharset');
        $sourceCode = $this->getSourceCode($url_content, $sourceCharset);
        return ['content' => $this->getHtmlCodeOfMaster($sourceCode, $ruleConfig, 'content'), 'createTime' => TIMESTAMP, 'isDeleted' => 0,
            'url' => $url_content, 'urlMD5' => md5($url_content),
            'title' => $this->getHtmlCodeOfMaster($sourceCode, $ruleConfig, 'title')];
    }

    protected function getHtmlCodeOfMaster(string $html, array $config, string $rule_field): string
    {
        $rule = explode('^', Auth::HtmlspecialcharsDeCode($config[$rule_field]['rule_code']));
        if (is_array($rule)) {
            foreach ($rule as $k => $v) {
                $rule[$k] = str_replace(array("\r", "\n"), '', trim($v));
            }
        }
        return strip_tags($this->_cutHtmlCode($html, $rule[0], $rule[1]),
            ['<table>', '<thead>', '<tbody>', '<tr>', '<th>', '<td>', '<p>']);
    }

    protected function readConfig(): array
    {
        $path = PATH_TMP . $this->file_config;
        if (file_exists($path)) {
            $params_config = fileHelper()->GetContentsByDisk($path);
            return json_decode($params_config, true);
        } else {
            return [];
        }
    }

    protected function writeConfig(array $params)
    {
        fileHelper()->PutContents(PATH_TMP, $this->file_config, JSON::EnCode($params));
    }

    private function _cutHtmlCode(string $htmlCode, string $start, string $end): string
    {
        $htmlCode = explode(trim($start), $htmlCode);
        if (is_array($htmlCode)) $htmlCode = explode(trim($end), $htmlCode[1]);
        return trim($htmlCode[0]);
    }

    private function _contentUrl(string $url_controller, string $html): array
    {
        $html = str_replace(array("\r", "\n"), '', $html);
        $html = str_replace(array("</a>", "</A>"), "</a>\n", $html);
        preg_match_all('/<a ([^>]*)>([^\/a>].*)<\/a>/i', $html, $out);
        //$out[1] = array_unique($out[1]);
        //$out[2] = array_unique($out[2]);
        $data = [];
        foreach ($out[1] as $k => $v) {
            if (preg_match('/href=[\'"]?([^\'" ]*)[\'"]?/i', $v, $match_out)) {
                $url_content = $match_out[1];
                $this->_checkUrl($url_content, $url_controller);
                in_array($url_content, $data) ?: $data[] = $url_content;
            }
        }
        return $data;
    }

    private function _checkUrl(string &$url_content, string $url_controller)
    {
        $urlInfo = parse_url($url_controller);
        $url_path = $urlInfo['scheme'] . '://' . $urlInfo['host'] .
            (substr($urlInfo['path'], -1, 1) === '/' ? substr($urlInfo['path'], 0, -1) : str_replace('\\', '/', dirname($urlInfo['path']))) . '/';
        if (strpos($url_content, '://') === false) {
            if ($url_content[0] == '/') {
                $url_content = $urlInfo['scheme'] . '://' . $urlInfo['host'] . $url_content;
            } else {
                $url_content = $url_path . $url_content;
            }
        }
    }

}