<?php


namespace PKApp\Collection;


use PKCommon\Controller\AdminController;
use PKFrame\DataHandler\Arrays;

class AdminAPI extends AdminController
{

    public function Main()
    {
    }

    public function GetCharsetList()
    {
        $this->json(
            $this->arrayToIdAndName(dict('sourceCharset'))
        );
    }

    public function GetHtmlTagList()
    {
        $tag_list = dict('html_tag');
        $result = [];
        $i = 0;
        if (Arrays::Is($tag_list)) {
            foreach ($tag_list as $key => $item) {
                $result[] = ['id' => $i, 'name' => $key,];
                $i += 1;
            }
        }
        $this->json($result);
    }

    public function GetContentStatus()
    {
        $this->json(
            $this->getDictToArray('CollectionContentStatus')
        );
    }

}