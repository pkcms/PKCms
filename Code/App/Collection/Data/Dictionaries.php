<?php
return [
    'sourceCharset' => ['GBK', 'UTF-8', 'BIG5'],
    'CollectionContentStatus' => [
        'collection' => '已采集',
        'await' => '未采集',
        'import' => '已导入',
    ],
    // HTML标签
    'html_tag' => [
        'p' => "<p([^>]*)>(.*)</p>[|]",
        'a' => "<a([^>]*)>(.*)</a>[|]",
        'script' => "<script([^>]*)>(.*)</script>[|]",
        'iframe' => "<iframe([^>]*)>(.*)</iframe>[|]",
        'table' => "<table([^>]*)>(.*)</table>[|]",
        'span' => "<span([^>]*)>(.*)</span>[|]",
        'b' => "<b([^>]*)>(.*)</b>[|]",
        'img' => "<img([^>]*)>[|]",
        'object' => "<object([^>]*)>(.*)</object>[|]",
        'embed' => "<embed([^>]*)>(.*)</embed>[|]",
        'param' => "<param([^>]*)>(.*)</param>[|]",
        'div' => '<div([^>]*)>[|]',
        '/div' => '</div>[|]',
        '!--' => '<!--([^>]*)-->[|]',
    ],
];