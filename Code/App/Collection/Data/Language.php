<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return array(
    'App_Name' => array(
        'zh-cn' => '采集器',
        'en' => 'collector'
    ),
    'fp_error' => array(
        'zh-cn' => '连接失败',
        'en' => 'connection failed'
    ),
    'del_idList_empty' => array(
        'zh-cn' => '请选择要删除的内容',
        'en' => 'Please select the content to delete'
    ),
    'import_idList_empty' => array(
        'zh-cn' => '请选择要导入的内容',
        'en' => 'Please select the content to import'
    ),
    'import_categoryId_empty' => array(
        'zh-cn' => '请选择要导入到哪个栏目',
        'en' => 'Please select which column to import'
    ),
    'import_categoryId_notChildren' => array(
        'zh-cn' => '请选择要导入的栏目不是子栏目',
        'en' => 'Please select the column to be imported is not a sub column'
    ),
    'import_nodeId_empty' => array(
        'zh-cn' => '要执行导入的节点 ID 不能为空',
        'en' => 'The node ID to perform the import cannot be empty'
    ),
    'import_status_await' => array(
        'zh-cn' => '执行导入的内容中有存在未采集的内容',
        'en' => 'There are uncollected contents in the imported contents'
    ),
    'import_status_import' => array(
        'zh-cn' => '执行导入的内容中有存在已导入的内容',
        'en' => 'The imported content exists in the imported content'
    ),
    'import_field_content' => array(
        'zh-cn' => '执行导入到栏目的数据模型中没有存在 content 字段名',
        'en' => 'There is no content field name in the data model imported into the column'
    ),
    'Content_Empty' => array(
        'zh-cn' => '采集的内容正文的信息找不到，或者不存在',
        'en' => 'The information of the collected content body cannot be found or does not exist'
    ),
    'Content_IdEmpty' => array(
        'zh-cn' => '采集的内容的Id找不到，或者不存在',
        'en' => 'The ID of the collected content cannot be found or does not exist'
    ),
    'Empty_configOfList' => array(
        'zh-cn' => '没有配置好列表页的规则',
        'en' => 'The rules for the list page are not configured properly',
    ),
    'Empty_configOfUrlPage' => array(
        'zh-cn' => '没有配置好列表页的地址规则',
        'en' => 'Address rules for list pages not configured properly',
    ),
    'Empty_sourceCharset' => array(
        'zh-cn' => '没有配置被采集网站的网页编码',
        'en' => 'The webpage encoding of the collected website is not configured',
    ),
    'Empty_sourceCode' => array(
        'zh-cn' => '被采集的网页源代码为空，页面地址：',
        'en' => 'The source code of the collected webpage is empty, and the page address is:',
    ),
    'Empty_urlOfList' => array(
        'zh-cn' => '被采集的列表页网址为空',
        'en' => 'The website of the collected list page is empty',
    ),
    'Empty_urlOfContent' => array(
        'zh-cn' => '被采集的内容 URL 不能为空',
        'en' => 'The collected content URL cannot be empty'
    ),
    'Error_categoryType' => array(
        'zh-cn' => '导入栏目的类型必须是列表类型',
        'en' => 'The type of imported column must be a list type',
    ),
    'Error_fieldOfContent' => array(
        'zh-cn' => '导入栏目的数据模型中不存在【content】字段',
        'en' => "The 'content' field does not exist in the data model of the imported column",
    ),
    'Error_MaxPage' => array(
        'zh-cn' => '建议采集的页数等于小于10，不然一天之内难以采集完',
        'en' => 'Suggest collecting pages equal to or less than 10, otherwise it will be difficult to complete the collection within a day',
    ),
    'Error_page' => array(
        'zh-cn' => '采集的页码不在采集页的范围之内！',
        'en' => 'The collected page number is not within the range of the collected page!',
    ),
);