<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return array(
    'Site_LanguageEmpty' => array(
        'zh-cn' => '站点语言包不能为空',
        'en' => 'Site language pack cannot be empty'
    ),
    'Site_ThemesEmpty' => array(
        'zh-cn' => '站点模板主题不能为空',
        'en' => 'Site template theme cannot be empty'
    ),
    'Module_App_UpdateOK' => array(
        'zh-cn' => '系统的扩展模块更新完毕',
        'en' => 'The expansion module of the system has been updated'
    ),
    'Upgrade_version_empty' => array(
        'zh-cn' => '没有检测到新的版本',
        'en' => 'No new version detected'
    ),
);