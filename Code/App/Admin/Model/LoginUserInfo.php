<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/5/29
 * Time: 14:05
 */

namespace PKApp\Admin\Model;


class LoginUserInfo
{
    public $SiteId;
    public $Id;
    public $UserName;
    public $GroupId;
    public $PowerByAction;
}