<?php


namespace PKApp\Admin;


use PKApp\Site\Classes\SiteService;
use PKCommon\Controller\AdminController;
use PKFrame\DataHandler\Numbers;

class Admin extends AdminController
{

    public function Main()
    {
        $this->getHost();
        $this->fetchByPHP('main');
    }

    protected function getHost()
    {
        $id_site = Numbers::IsId($this->loginUser()->SiteId) ? $this->loginUser()->SiteId : 1;
        $service_site = new SiteService();
        $urls = parse_url($service_site->GetDomain($id_site));
        $this->assign(['host' => $urls['host']]);
    }

}