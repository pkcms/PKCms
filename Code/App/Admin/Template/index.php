<?php $groupId = isset($groupId) ? $groupId : 0 ?>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>PKCMS后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
    <link rel="stylesheet" href="/statics/pkcms/icons/iconfont.css" media="all">
    <style>
        .layui-side-bottom {
            position: fixed;
            left: 0;
            bottom: 0;
            z-index: 1002;
            color: rgba(255,255,255,.7);
            width: 220px;
            height: 49px;
            padding: 0 15px;
            box-sizing: border-box;
            overflow: hidden;
            font-weight: 300;
            text-align: center;
        }
        .layui-side-bottom a {
            color: rgba(255,255,255,.7);
        }
    </style>
</head>
<body class="layui-layout-body" layadmin-themealias="classic-black-header">

<div id="LAY_app">
    <div class="layui-layout layui-layout-admin">
        <div class="layui-header">
            <!-- 头部区域 -->
            <ul class="layui-nav layui-layout-left">
                <li class="layui-nav-item layadmin-flexible" lay-unselect>
                    <a href="javascript:;" layadmin-event="flexible" title="侧边伸缩">
                        <i class="layui-icon layui-icon-shrink-right" id="LAY_app_flexible"></i>
                    </a>
                </li>
                <li class="layui-nav-item layadmin-flexible" lay-unselect>
                    <a href="https://www.kancloud.cn/pkcms_cn/pkcms_mysql" target="_blank"
                       title="程序使用文档">
                        <i class="iconfont pkcms-icon-book-solid" id="LAY_app_flexible"></i>
                    </a>
                </li>
                <li class="layui-nav-item" lay-unselect>
                    <a href="javascript:;" layadmin-event="refresh" title="刷新">
                        <i class="layui-icon layui-icon-refresh-3"></i>
                    </a>
                </li>
                <?php if (isset($home_url)) { ?>
                    <li class="layui-nav-item layui-hide-xs" lay-unselect>
                        <a href="<?php echo $home_url; ?>&perView=1" target="_blank" title="预览前台首页">
                            <i class="iconfont pkcms-icon-view"></i>
                        </a>
                    </li>
                    <li class="layui-nav-item layui-hide-xs" lay-unselect>
                        <a lay-href="/index.php/content/AdminContent">
                            内容管理
                        </a>
                    </li>
                    <li class="layui-nav-item layui-hide-xs" lay-unselect>
                        <a lay-href="/index.php/content/AdminGetCategory?parentId=0">
                            栏目管理
                        </a>
                    </li>
                    <li class="layui-nav-item layui-hide-xs" lay-unselect>
                        <a lay-href="/index.php/content/AdminMakeHtml?type=all">
                            内容发布
                        </a>
                    </li>
                <?php } ?>
            </ul>
            <ul class="layui-nav layui-layout-right" lay-filter="layadmin-layout-right">
                <!--                <li class="layui-nav-item layui-hide-xs" lay-unselect>-->
                <!--                    <a href="javascript:;" layadmin-event="theme">-->
                <!--                        <i class="layui-icon layui-icon-theme"></i>-->
                <!--                    </a>-->
                <!--                </li>-->
                <li class="layui-nav-item layui-hide-xs" lay-unselect>
                    <a href="javascript:;" layadmin-event="fullscreen">
                        <i class="layui-icon layui-icon-screen-full"></i>
                    </a>
                </li>
                <li class="layui-nav-item" lay-unselect>
                    <a href="javascript:;">
                        <cite><?php echo isset($userName) ? $userName : '' ?></cite>
                    </a>
                    <dl class="layui-nav-child">
                        <dd style="text-align: center;"><a href="javascript:PKAdmin.userLoginOut();">退出</a></dd>
                    </dl>
                </li>

                <li class="layui-nav-item layui-hide-xs" lay-unselect>
                    <a href="javascript:;" layadmin-event="about">
                        <i class="layui-icon layui-icon-more-vertical"></i></a>
                </li>
                <li class="layui-nav-item layui-show-xs-inline-block layui-hide-sm" lay-unselect>
                    <a href="javascript:;" layadmin-event="more">
                        <i class="layui-icon layui-icon-more-vertical"></i></a>
                </li>
            </ul>
        </div>

        <!-- 侧边菜单 -->
        <div class="layui-side layui-side-menu">
            <div class="layui-side-scroll">
                <div class="layui-logo">
                    PKCMS后台管理
                </div>

                <div id="pkcms-side-menu"></div>
                <div class="layui-side-bottom">
                    &copy; <a href="https://pkcms.cn" target="_blank">pkcms.cn</a> 版权所有
                </div>
            </div>
        </div>


        <!-- 页面标签 -->
        <div class="layadmin-pagetabs" id="LAY_app_tabs">
            <div class="layui-icon layadmin-tabs-control layui-icon-prev" layadmin-event="leftPage"></div>
            <div class="layui-icon layadmin-tabs-control layui-icon-next" layadmin-event="rightPage"></div>
            <div class="layui-icon layadmin-tabs-control layui-icon-down">
                <ul class="layui-nav layadmin-tabs-select" lay-filter="layadmin-pagetabs-nav">
                    <li class="layui-nav-item" lay-unselect>
                        <a href="javascript:;"></a>
                        <dl class="layui-nav-child layui-anim-fadein">
                            <dd layadmin-event="closeThisTabs"><a href="javascript:;">关闭当前标签页</a></dd>
                            <dd layadmin-event="closeOtherTabs"><a href="javascript:;">关闭其它标签页</a></dd>
                            <dd layadmin-event="closeAllTabs"><a href="javascript:;">关闭全部标签页</a></dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <div class="layui-tab" lay-unauto lay-allowClose="true" lay-filter="layadmin-layout-tabs">
                <ul class="layui-tab-title" id="LAY_app_tabsheader">
                    <li lay-id="{:url('index/home')}" lay-attr="{:url('index/home')}" class="layui-this"><i
                                class="layui-icon layui-icon-home"></i></li>
                </ul>
            </div>
        </div>


        <!-- 主体内容 -->
        <div class="layui-body" id="LAY_app_body">
            <div class="layadmin-tabsbody-item layui-show">
                <iframe src="/index.php/admin/admin" frameborder="0" class="layadmin-iframe"></iframe>
            </div>
        </div>

        <!-- 辅助元素，一般用于移动设备下遮罩 -->
        <div class="layadmin-body-shade" layadmin-event="shade"></div>
    </div>
</div>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javaScript">
    PKAdmin.ready(function () {
        PKLayer.useSideNav('pkcms-side-menu','Power/AdminGetPower/ApiByMenuInUser');
    });
</script>

</body>
</html>