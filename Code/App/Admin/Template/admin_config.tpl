<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
</head>
<body>

<div class="layui-fluid">

    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <fieldset class="layui-elem-field layui-field-title">
                <legend>站点基本功能模块的配置</legend>
            </fieldset>
        </div>

        <div class="layui-card-body layui-card-body-mb">
            <form class="layui-form" lay-filter="form-page">
                <div id="view-form"></div>

                <div class="layui-form-footer-fixed">
                    <div class="layui-input-block">
                        <div class="layui-footer">
                            <button class="layui-btn" lay-submit lay-filter="form-page">立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/html" data-id="<{$id}>" data-api="Site/AdminConfigOfFunc" id="tpl-form">
    <input type="hidden" name="id" value="{{#  if(d.id){ }}{{  d.id  }}{{#  } }}"/>

    <div class="layui-card">

        <div class="layui-card-header layui-bg-gray">百度-普通提交新页</div>
        <div class="layui-card-body">
            <div class="layui-form-item">
                <label class="layui-form-label">接口的Token码：</label>
                <div class="layui-input-block">
                    <input type="text" name="BaiDu[TokenOfGeneralCollection]" class="layui-input"
                           value="{{#  if(d.BaiDu && d.BaiDu.TokenOfGeneralCollection){ }}{{  d.BaiDu.TokenOfGeneralCollection  }}{{#  } }}">
                    <div class="layui-form-mid layui-word-aux">
                        请前往：<a href="https://ziyuan.baidu.com/" target="_blank"><b style="color: #00a0e9">百度搜索资源平台</b></a>，申请与本站域名相关的 token，<b style="color: red">请注意申请到的 token 与域名是相关联的！</b>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">是否根据IP自动切换中英文站点：</label>
                <div class="layui-input-block">
                    <input type="checkbox" lay-skin="switch" id="isAutoSwitchAccorToIP" lay-filter="isAutoSwitchAccorToIP" name="Config[isAutoSwitchAccorToIP]" {{#  if(d.Config && d.Config.isAutoSwitchAccorToIP){ }} value="1" checked{{#  } }}/>
                </div>
                <div class="layui-form-mid layui-word-aux">
                    需要将 <b style="color: #00a0e9">index.php</b> 设置为<b style="color: red">默认文档</b>
                </div>
            </div>
        </div>

    </div>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">
    PKAdmin.ready(function () {
        PKAdmin.tplFormBySubmit({});
    });
</script>
</body>
</html>