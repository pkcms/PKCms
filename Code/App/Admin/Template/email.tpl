<html lang="zh-cn">
    <head>
        <meta http-equiv="Content-Language" content="en">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <font color="red"><b>【不要直接回复】注意：这是程序系统代留言提交者都发送的邮件，所以请不要直接回复，不然提交者是接收不到邮件的</b></font><br/>
        <{$content}>
    </body>
</html>