<?php

if ($_GET['act'] == "phpinfo") {
    phpinfo();
    exit();
}

//检测PHP设置参数
function show($varName): string
{
    return get_cfg_var($varName);
}

?>
<html lang="zh-cn">

<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
    <style>
        .red-center {
            color: red;
            font-weight: bold;
            align-content: center;
        }
    </style>
</head>

<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md3 layui-col-sm3 layui-col-xs12">
            <div class="layui-card">
                <div class="layui-card-header layui-bg-cyan">
                    基础统计
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    站点总数：<b id="count_size"></b> 个 <br>
                    内容模型：<b id="count_model_content"></b> 个
                </div>
            </div>
        </div>
        <div class="layui-col-md3 layui-col-sm3 layui-col-xs12">
            <div class="layui-card">
                <div class="layui-card-header layui-bg-cyan">
                    用户统计
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    后台管员：<b id="count_member_admin"></b> 位 <br>
                    前台会员：<b id="count_member_user"></b> 位
                </div>
            </div>
        </div>
        <div class="layui-col-md6 layui-col-sm6 layui-col-xs12">
            <div class="layui-card">
                <div class="layui-card-header layui-bg-cyan">
                    软件开发
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    开发作者：<a href="https://pkcms.cn" target="_blank">pkcms.cn</a> <br>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-header layui-bg-cyan">
            系统信息
        </div>
        <div class="layui-card-body">
            <table class="layui-table">
                <tbody>
                <tr>
                    <th>PHP运行库</th>
                    <td>
                        版本：<?php echo PHP_VERSION; ?> ，
                        运行方式：<?php echo strtoupper(php_sapi_name()); ?> ，
                        信息：<?php
                        $phpSelf = $_SERVER['PHP_SELF'] ? $_SERVER['PHP_SELF'] : $_SERVER['SCRIPT_NAME'];
                        $disFuns = get_cfg_var("disable_functions");
                        echo (false !== preg_match("phpinfo", $disFuns)) ? '<font color="red">×</font>' : "<a href='$phpSelf?act=phpinfo' target='_blank'>PHPINFO</a>";
                        ?> 。
                    </td>
                </tr>
                <tr>
                    <th>资源限制信息</th>
                    <td>
                        脚本占用最大内存：<?php echo show("memory_limit"); ?> ，
                        POST方法提交最大限制：<?php echo show("post_max_size"); ?> ，
                        上传文件最大限制：<?php echo show("upload_max_filesize"); ?> 。
                    </td>
                </tr>
                <tr>
                    <th>网站根路径</th>
                    <td>
                        <?php
                        echo $_SERVER['DOCUMENT_ROOT'] ?
                            str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT']) :
                            str_replace('\\', '/', dirname(__FILE__));
                        ?>
                    </td>
                </tr>
                <tr>
                    <th>域名/IP地址</th>
                    <td>
                        <?php echo @get_current_user(); ?>
                        &nbsp;-&nbsp;<?php echo $_SERVER['SERVER_NAME']; ?>
                        (<?php if ('/' == DIRECTORY_SEPARATOR) {
                            echo $_SERVER['SERVER_ADDR'];
                        } else {
                            echo @gethostbyname($_SERVER['SERVER_NAME']);
                        } ?>)，&nbsp;
                        端口号：<?php echo $_SERVER['SERVER_PORT']; ?>，&nbsp;&nbsp;你的IP地址是：<?php echo @$_SERVER['REMOTE_ADDR']; ?>
                    </td>
                </tr>
                <tr>
                    <th>服务器标识</th>
                    <td><?php echo @php_uname();; ?></td>
                </tr>
                <tr>
                    <th>操作系统</th>
                    <td>
                        <?php $os = explode(" ", php_uname());
                        echo $os[0]; ?> &nbsp;内核版本：<?php if ('/' == DIRECTORY_SEPARATOR) {
                            echo $os[2];
                        } else {
                            echo $os[1];
                        } ?>
                    </td>
                </tr>
                <tr>
                    <th>解译引擎</th>
                    <td><?php echo $_SERVER['SERVER_SOFTWARE']; ?></td>
                </tr>
                <tr>
                    <th>服务器语言</th>
                    <td><?php echo getenv("HTTP_ACCEPT_LANGUAGE"); ?></td>
                </tr>
                <tr>
                    <th>服务器主机名</th>
                    <td>
                        <?php if ('/' == DS) {
                            echo $os[1];
                        } else {
                            echo $os[2];
                        } ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>

<script type="text/javascript" src="/statics/echarts/echarts.min.js"></script>
<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">

    function countMember(id_model, domId) {
        PKLayer.apiGet('Member/AdminGetUser/ApiByCount', {modelId: id_model}, function (res, count) {
            document.getElementById(domId).innerText = count;
        });
    }

    PKAdmin.ready(function () {

        PKLayer.apiGet('Site/AdminGetSite/ApiByCount', {}, function (res, count) {
            document.getElementById('count_size').innerText = count;
        });

        PKLayer.apiGet('Model/AdminGetModel/ApiByCount', {}, function (res, count) {
            document.getElementById('count_model_content').innerText = count;
        });

        countMember(1, 'count_member_admin');
        countMember(2, 'count_member_user');

    });

</script>
</body>

</html>