<?php

namespace PKApp\Admin;

use PKApp\Site\Classes\SiteDataBase;
use PKCommon\Controller\AdminController;

class Index extends AdminController
{

    public function Main()
    {
        $siteId = $this->loginUser()->SiteId;
        if ($siteId > 0) {
            $db = new SiteDataBase();
            $site = $db->GetSite(['id' => $siteId], ['Site_Name']);
            $this->view()->SetTplParamList([
                'home_url' => self::ToMeUrl('site/index?siteId=' . $siteId)
            ]);
        }
        $this->assign([
            'userName' => $this->loginUser()->UserName,
            'groupId' => $this->loginUser()->GroupId,
        ]);
        $this->fetchByPHP('index');
    }

    protected function db()
    {
    }
}
