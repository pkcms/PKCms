<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return array(
    'App_Name' => array(
        'zh-cn' => '表单',
        'en' => 'form'
    ),
    'ModelForm_TypeError' => [
        'zh-cn' => '该数据模型的类型不是表单类型',
        'en' => 'The type of the data model is not a form type'
    ],
    'fieldList_Empty' => array(
        'zh-cn' => '表单模型里面的字段找不到，或者不存在',
        'en' => 'The field in the form model cannot be found or does not exist'
    ),
    'Guest' => array(
        'zh-cn' => '访客',
        'en' => 'Guest'
    ),
    'AuthCode_Expire' => array(
        'zh-cn' => '表单验证码已经过期，请重新刷新验证码后再提交。',
        'en' => 'The form verification code has expired. Please refresh the verification code b'
    ),
    'AuthCode_Error' => array(
        'zh-cn' => '您提交的验证码验证失败。',
        'en' => 'The verification code you submitted failed to verify.'
    ),
    'Err_postHigh' => array(
        'zh-cn' => '您当前的提交频率太过勤快，请休息一下！',
        'en' => 'Your current submission frequency is too diligent, please have a rest!'
    ),
    'PostOk' => array(
        'zh-cn' => '提交成功',
        'en' => 'Submitted successfully'
    ),
    'field_postEmpty' => array(
        'zh-cn' => '您提交的信息中有存在空的情况，请检查信息：',
        'en' => 'The information you submitted is empty. Please check the information:'
    ),
    'Data_idEmpty' => array(
        'zh-cn' => '数据的ID编号不能为空，请检查信息',
        'en' => 'The ID number of the data cannot be empty, please check the information'
    ),
    'Data_statusEmpty' => array(
        'zh-cn' => '数据的操作状态不能为空',
        'en' => 'The operation status of data cannot be empty'
    ),
    'Empty_restrictField' => array(
        'zh-cn' => '找不到限制的字段',
        'en' => 'Can\'t find restricted field'
    ),
    'Empty_restrictFieldVal' => array(
        'zh-cn' => '限制字段的查询值不能为空',
        'en' => 'The query value of the restricted field cannot be empty'
    ),
    'Empty_uuid' => array(
        'zh-cn' => '没有获取到访客信息',
        'en' => 'No guest information is obtained'
    ),
    'Exists_data'=>array(
        'zh-cn' => '您已经提交，无需再次提交',
        'en' => 'You have already submitted, no need to submit again'
    ),
);