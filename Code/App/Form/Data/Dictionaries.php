<?php
return [
    // 表单数据的状态
    'form_dataStatusList' => [
        'await' => '等待处理',
        'reply' => '已经回复',
        'finish' => '成功解决',
    ],
];