<?php


namespace PKApp\Form;


use PKApp\Form\Classes\TraitFrom;
use PKApp\Site\Classes\SiteController;
use PKFrame\DataHandler\Arrays;

class ApiGetStarGrade extends SiteController
{
    use TraitFrom;

    protected $dir = 'StarRrade';

    public function Main()
    {
        $field = request()->get('name');
        $file_name = date('Ymd') . '_' . $field . $this->id_form;
        $params = [];
        if (!empty($this->serviceForm()->restrictField)) {
            $restrictField_value = Arrays::GetKey('restrictField_value', request()->get(), 'Empty_restrictFieldVal');
            $file_name .= '_' . $restrictField_value;
        }
        $cache_data = cache()->Tmp()->ReadByJSON($file_name, $this->dir);
        if (!empty($cache_data)) {
            $this->json($cache_data);
        }
        if (isset($restrictField_value)) {
            $params = $this->getFieldByRestrict($restrictField_value);
        }
        $result = $this->serviceForm()->GetStarGrade($field, $params);
        cache()->Tmp()->WriteByJSON($file_name, $result, $this->dir);
        $this->json($result);
    }
}