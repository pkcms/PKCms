<?php


namespace PKApp\Form;


use PKApp\Form\Classes\TraitFrom;
use PKApp\Site\Classes\SiteController;

class ApiGetListData extends SiteController
{

    use TraitFrom;

    public function __construct()
    {
        $this->getFieldByOption();
    }

    public function Main()
    {
        $this->GetPages();
        $params_db = [
            'siteId' => $this->getSiteEntity()->Id,
        ];
        $count = $this->serviceForm()->GetCount($params_db);
        $list = $this->serviceForm()->GetList($params_db, '*', self::$pageIndex, self::$pageSize);
        foreach ($list as $index => $item_data) {
            foreach ($this->listId_option_field as $item_field) {
                $item_data[$item_field] = $this->listField_option[$item_field][$item_data[$item_field]];
            }
            $list[$index] = $item_data;
        }
        $this->json($list, $count);
    }
}