<?php


namespace PKApp\Form\Classes;

use PKApp\Model\Classes\ModelExtend;
use PKFrame\DataHandler\Arrays;
use PKFrame\Lib\DataBase;

class FormDataBase extends DataBase
{

    public function __construct($tableName)
    {
        parent::__construct();
        $this->table = $tableName;
    }

    // 评级字段的统计（星级）
    public function GetGradeCount(string $fieldName, array $where_arr = null)
    {
        $from = $this->getFrom();
        $where_str = '';
        if (!is_null($where_arr)) {
            $this->Where($where_arr);
            $where_str = $this->GetWhere();
        }
        $this->sql[] = <<<SQL
SELECT {$fieldName}, count(id) AS count {$from} {$where_str} GROUP BY {$fieldName}
SQL;
        return $this->ToList();
    }

    public function Sum(string $fieldName, array $where_arr = null)
    {
        $from = $this->getFrom();
        $where_str = '';
        if (!is_null($where_arr)) {
            $this->Where($where_arr);
            $where_str = $this->GetWhere();
        }
        // SELECT COUNT(`id`) AS `count_action`, SUM(`grade`) AS `sum_fraction` FROM `pk_form_grade`
        $this->sql[] = <<<SQL
SELECT COUNT(`id`) AS `count_action`, SUM(`{$fieldName}`) AS `sum_fraction` {$from}{$where_str}
SQL;
        return $this->First();
    }

}
