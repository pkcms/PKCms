<?php


namespace PKApp\Form\Classes;


use PKApp\Model\Classes\ModelService;
use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\Numbers;
use PKFrame\Service;

class FormService extends Service
{

    protected $table_name;
    public $is_insertDB, $is_sendMail, $is_singleSubmit, $restrictField, $to_mailAddress;

    public function __construct($modelId)
    {
        parent::__construct();
        $service_model = new ModelService();
        $this->table_name = $service_model->GetTableNameByModelId($modelId);
        $entity_model = $service_model->interface_getEntityById($modelId);
        $setting = Arrays::GetKey('setting', $entity_model, 'model_setting_empty', 'model');
        $this->is_insertDB = array_key_exists('database', $setting);
        $this->is_singleSubmit = array_key_exists('singleSubmit', $setting);
        $this->restrictField = Arrays::GetKey('restrictField', $setting);
        $this->to_mailAddress = Arrays::GetKey('email_address', $setting);
        $this->is_sendMail = !empty($this->to_mailAddress);
    }

    public function interface_getEntityById($id, $viewField = '*')
    {
        return $this->getEntityById($id, 'Data_idEmpty', 'fieldList_Empty', $viewField);
    }

    protected function db(): FormDataBase
    {
        return new FormDataBase($this->table_name);
    }

    public function GetList($viewParams = [], $viewField = '*', $index = 0, $lineNum = 0)
    {
        array_key_exists('isDelete', $viewParams) ?: $viewParams['isDelete'] = 0;
        return $this->db()->Where($viewParams)
            ->OrderBy('id', 'DESC')
            ->Limit($lineNum, $index)
            ->Select($viewField)
            ->ToList();
    }

    public function GetCount($viewParams = []): int
    {
        array_key_exists('isDelete', $viewParams) ?: $viewParams['isDelete'] = 0;
        return $this->db()->Where($viewParams)->Count();
    }

    public function GetGradeCount($name_field = '',array $where_str = null)
    {
        return $this->db()->GetGradeCount($name_field, $where_str);
    }

    public function GetStarGrade($name_field = '',array $where_str = null)
    {
        $sum = $this->db()->Sum($name_field, $where_str);
        $sum['sum_fraction'] = $sum_fraction = Numbers::To(Arrays::GetKey('sum_fraction',$sum));
        $count_action = Numbers::To(Arrays::GetKey('count_action',$sum));
        if ($sum_fraction > 0 && $count_action > 0) {
            $sum['ratio'] = round($sum_fraction / $count_action, 2);
        } else {
            $sum['ratio'] = 0;
        }
        return $sum;
    }

    public function GetEntity($viewParams = [], $viewField = '*')
    {
        array_key_exists('isDelete', $viewParams) ?: $viewParams['isDelete'] = 0;
        return $this->db()->Where($viewParams)->Select($viewField)->First();
    }

    public function InsertDBByUser($params_insert)
    {
        $this->Add($params_insert);
    }

    public function isExistsData(string $user_name, string $ip, array $where_other): int
    {
        $where_un = ['createUserName' => $user_name];
        $where_ip = ['createIP' => $ip];
        $where = array_merge($where_un, $where_other);
        $data = $this->db()->Where($where)->Select()->First();
        $is_exists = Arrays::Is($data);
        if ($is_exists) {
            $where = array_merge($where_ip, $where_other);
            $data = $this->db()->Where($where)->Select()->First();
            $is_exists = Arrays::Is($data);
        }
        return $is_exists;
    }
}