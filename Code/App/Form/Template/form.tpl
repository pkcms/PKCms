<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
</head>

<body>

<div class="layui-fluid">

    <div class="layui-fluid">

        <div class="layui-card">
            <div class="layui-card-header layuiadmin-card-header-auto">
                <fieldset class="layui-elem-field layui-field-title">
                    <legend>表单数据详情 - <{$formName}></legend>
                </fieldset>
            </div>

            <div class="layui-card-body">
                <form class="layui-form" lay-filter="form-frame">
                    <input type="hidden" name="id" value="<{$id}>"/>
                    <input type="hidden" name="modelId" value="<{$modelId}>"/>
                    <div id="view-form"></div>

                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-footer">
                                <button class="layui-btn" lay-submit lay-filter="form-frame">立即提交</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/html" id="tpl-form">
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">提问人：</label>
            <div class="layui-input-block">
                {{#  if(d.data.createUserName){ }}{{  d.data.createUserName  }}{{#  } }}
                ({{#  if(d.data.createIP){ }}{{  d.data.createIP  }}{{#  } }})
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">录入时间：</label>
            <div class="layui-input-block">
                {{#  if(d.data.createTime){ }}{{  dateFormat(d.data.createTime)  }}{{#  } }}
            </div>
        </div>
    </div>
    <div class="layui-form-item">
    </div>
    {{#  layui.each(d.fieldLists, function(index, item){ }}
    <div class="layui-form-item">
        <label class="layui-form-label">{{ item }}：</label>
        <div class="layui-input-block">
            {{ d.data[index] }}
        </div>
    </div>
    {{#  }); }}
    <div class="layui-form-item">
        <label class="layui-form-label">状态：</label>
        <div class="layui-input-block">
            <div class="pkcms_select" id="status" data-name="status"
                 data-api="Form/AdminApi/GetFormDataStatusList"></div>
        </div>
    </div>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">
    var data = {
        id: '<{$id}>',
        modelId: '<{$modelId}>',
    };

    PKAdmin.ready(function () {
        PKLayer.apiGet(
            'Form/AdminGetData/ApiGetDetail', data,
            function (apiResult) {
                PKAdmin.tplFormBySubmit(apiResult);
            }
        );
    });
</script>
</body>
</html>
