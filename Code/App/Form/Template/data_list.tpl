<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
</head>

<body>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <fieldset class="layui-elem-field layui-field-title" id="view-model">
            </fieldset>
            <div class="layui-form layui-card-header layuiadmin-card-header-auto">
                <div class="layui-form-item">
                    <label class="layui-form-label">
                        筛选
                    </label>
                    <div class="layui-input-block">
                        <div id="status"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="layui-card-body">
            <table class="layui-hide PK-table" id="table_adminList" lay-filter="table_adminList"
                   data-get="Form/AdminGetData" data-set="form/AdminSetData"
                   data-params="modelId=<{$modelId}>"></table>

        </div>
    </div>
</div>

<script type="text/html" id="tpl-model">
    <legend>表单数据列表&nbsp;-&nbsp;{{  d.name  }}</legend>
</script>

<script type="text/html" id="table_tool">
    <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="edit">
        <i class="layui-icon layui-icon-list"></i>查看</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">
        <i class="layui-icon layui-icon-delete"></i>删除</a>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">
    var data = {
        modelId: '<{$modelId}>',
        table: {
            status: ''
        }
    };

    function renderTable() {
        PKAdmin.table({
            cols: [[
                {field:'id', title:'ID'},
                {field:'createUserName', title:'提交者称呼'},
                {field:'createIP', title:'提交者IP'},
                {field:'createTime', title:'提交时间',templet:"<div>{{dateFormat(d.createTime)}}</div>"},
                {field:'status', title:'状态'},
                {
                    title: "操作",
                    align: "center",
                    width: 200,
                    fixed: "right",
                    toolbar: "#table_tool"
                }
            ]]
        });
    }

    PKAdmin.ready(function () {
        renderTable();
        PKLayer.apiGet('Model/AdminGetModel/ApiGetDetail',{id:data.modelId},function (res) {
            PKLayer.useTpl(res,'tpl-model','view-model');
        });
        PKLayer.apiGet('Form/AdminApi/GetFormDataStatusList',{},function (res) {
            PKLayer.useFormBySelectFun('#status',res,null,false,true,function (res) {
                console.log(res);
                if (res.length > 0) {
                    data.table.status = res[0].id;
                } else {
                    data.table.status = '';
                }
                renderTable();
            });
        });
    });
</script>
</body>
</html>
