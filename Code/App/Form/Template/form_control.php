<?php
if (isset($formType) && isset($setting)) {
    $field = isset($field) ? $field : '';
    $name = isset($name) ? $name : '';
    switch ($formType) {
        case 'text':
            $str_len = \PKFrame\DataHandler\Arrays::GetKey('len', $setting);
            ?>
            <input type="text" id="form-text-<?php echo $field ?>" name="<?php echo $field; ?>" placeholder="请输入<?php echo $name; ?>" class="form-control"/>
            <?php
            break;
        case 'textarea':
            ?>
            <textarea id="form-textarea-<?php echo $field ?>" name="<?php echo $field; ?>" placeholder="请输入<?php echo $name; ?>" class="form-control"></textarea>
            <?php
            break;
        case 'number':
            ?>
            <input type="number" id="form-text-<?php echo $field ?>" name="<?php echo $field; ?>" placeholder="请输入<?php echo $name; ?>" class="form-control"/>
            <?php
            break;
        case 'option':
            $type_box = \PKFrame\DataHandler\Arrays::GetKey('type_box', $setting);
            $list_option = \PKFrame\DataHandler\Arrays::GetKey('list_option', $setting);
            if (\PKFrame\DataHandler\Arrays::Is($list_option)) {
                foreach ($list_option as $id => $name) {
                    $input_name = $type_box == 'radio' ? $field : $field . '[]';
                    ?>
                    <label for="form-<?php echo $type_box; ?>-<?php echo $field ?>-<?php echo $id; ?>">
                        <input type="<?php echo $type_box; ?>" id="form-<?php echo $type_box; ?>-<?php echo $field ?>-<?php echo $id; ?>"
                               name="<?php echo $input_name; ?>" value="<?php echo $id; ?>"/>
                        <?php echo $name; ?>
                    </label>
                    <?php
                }
            }
            break;
    }
}