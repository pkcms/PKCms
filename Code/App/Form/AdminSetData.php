<?php


namespace PKApp\Form;

use PKApp\Form\Classes\FormService;
use PKCommon\Controller\AdminController;
use PKFrame\DataHandler\Numbers;

class AdminSetData extends AdminController
{

    public function Main()
    {
        $this->assign([
            'modelId' => Numbers::To(request()->get('modelId')),
            'id' => Numbers::To(request()->get('id')),
        ])->fetch('form');
    }

    public function ApiByChange()
    {
        $model = $this->model();
        $service = new FormService(
            $this->checkPostFieldIsEmpty('modelId', 'Model_IdEmpty')
        );
        $service->UpdateById($model['id'], ['status' => $model['status']]);
        $this->inputSuccess();
    }

    public function ApiByDel()
    {
        $model = $this->model();
        $service = new FormService(
            $this->checkPostFieldIsEmpty('modelId', 'Model_IdEmpty')
        );
        Numbers::IsId($model['id']) ?: $this->noticeByJson('Data_idEmpty');
        $service->UpdateById($model['id'], ['isDelete' => 1]);
        $this->inputSuccess();
    }

    protected function model(): array
    {
        $post = $this->checkModel(['id', 'modelId', 'status']);
        $post['id'] = $this->checkPostFieldIsEmpty('id', 'Data_idEmpty');
        if (request()->action() == 'ApiByChange') {
            $post['status'] = $this->checkPostFieldIsEmpty('status', 'Data_statusEmpty');
        }
        return $post;
    }
}