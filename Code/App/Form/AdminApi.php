<?php


namespace PKApp\Form;


use PKCommon\Controller\AdminController;

class AdminApi extends AdminController
{

    public function GetFormDataStatusList()
    {
        $this->json($this->getDictToArray('form_dataStatusList'));
    }

    public function Main()
    {
    }
}