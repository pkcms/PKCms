<?php


namespace PKApp\Form;

use PKApp\Form\Classes\FormService;
use PKApp\Model\Classes\TraitModel;
use PKCommon\Controller\AdminController;
use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\Numbers;

class AdminGetData extends AdminController
{
    use TraitModel;

    public function Main()
    {
        $this->assign([
            'modelId' => Numbers::To(request()->get('modelId')),
        ])->fetch('data_list');
    }

    public function ApiByLists()
    {
        $this->service = new FormService(
            request()->get('modelId')
        );
        $options = [];
        if (request()->has('status', 'get')) {
            $status = request()->get('status');
            $status == '' ?: $options['status'] = $status;
        }
        $this->GetPages();
        $list = $this->service->GetList($options, '*', self::$pageIndex, self::$pageSize);
        $status = dict('form_dataStatusList');
        foreach ($list as $index => $item) {
            $item['status'] = $status[$item['status']];
            $list[$index] = $item;
        }
        $this->json($list, $this->service->GetCount($options));
    }

    public function ApiGetDetail()
    {
        $model_id = request()->get('modelId');
        $this->service = new FormService($model_id);
        $fieldListEntity = $this->serviceOfField()->GetList(['modelId' => $model_id], ['field', 'name']);
        Arrays::Is($fieldListEntity) ?: $this->noticeByPage('fieldList_Empty');
        $this->json([
            'fieldLists' => Arrays::Column($fieldListEntity, 'name', 'field'),
            'modelId' => $model_id,
            'data' => $this->service->interface_getEntityById(
                request()->get('id')
            ),
        ]);
    }
}
