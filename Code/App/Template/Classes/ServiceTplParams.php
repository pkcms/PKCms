<?php

namespace PKApp\Template\Classes;

use PKFrame\Service;

class ServiceTplParams extends Service
{

    public function interface_getEntityById($id, $viewField = '*')
    {
    }

    protected function db(): DBTplParams
    {
        static $cls;
        !empty($cls) ?: $cls = new DBTplParams();
        return $cls;
    }

    public function GetList($viewParams = [], $viewField = '*')
    {
        return $this->db()->Where($viewParams)->OrderBy('id')
            ->Select($viewField)->ToList();
    }

    public function GetEntity($viewParams = [], $viewField = '*')
    {
        return $this->db()->Where($viewParams)->Select($viewField)->First();
    }
}