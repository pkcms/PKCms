<?php

namespace PKApp\Template;

use PKApp\Site\Classes\SiteService;
use PKCommon\Controller\AdminSetController;
use PKFrame\DataHandler\Auth;

class AdminSetTpl extends AdminSetController
{
    protected $service_site;

    public function __construct()
    {
        parent::__construct();
        $this->service_site = new SiteService();
    }

    public function Main()
    {
        $path = request()->get('path');
        $code = fileHelper()->GetContentsByDisk(str_replace('/', DS, urldecode($path)));
        $this->assign([
            'code' => Auth::Htmlspecialchars($code),
            'path' => $path,
        ])->fetch('code');
    }

    public function ApiByChange()
    {
        $path = $this->model();
        $code = $this->checkPostFieldIsEmpty('code', 'template_codeEmpty');
        $folder = dirname($path);
        fileHelper()->PutContents($folder, str_replace($folder . DS, '', $path), Auth::HtmlspecialcharsDeCode($code));
        $this->inputSuccess();
    }

    protected function model()
    {
        $path = $this->checkPostFieldIsEmpty('path', 'template_PathNotExists');
        $path = str_replace('/', DS, urldecode($path));
        file_exists($path) ?: $this->noticeByJson('template_PathNotExists');
        return $path;
    }

    public function ApiByCreate()
    {
        // TODO: Implement ApiByCreate() method.
    }

    public function ApiByDel()
    {
        unlink($this->model());
        $this->inputSuccess();
    }
}