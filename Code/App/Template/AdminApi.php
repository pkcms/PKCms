<?php

namespace PKApp\Template;

use PKCommon\Controller\AdminController;

class AdminApi extends AdminController
{

    public function Main()
    {
        $this->callOfSellFunc();
    }

    protected function getTplParamsType()
    {
        $this->json($this->getDictToArray('params_type'));
    }
    
}