<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
    <link rel="stylesheet" href="/statics/pkcms/icons/iconfont.css" media="all">
</head>

<body>

<div class="layui-fluid">

    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <fieldset class="layui-elem-field layui-field-title">
                <legend>模板文件</legend>
            </fieldset>
        </div>

        <div class="layui-card-body">
            <div id="view_collapse"></div>
        </div>
    </div>
</div>

<script type="text/html" id="tpl_collapse">
    <div class="layui-collapse" lay-filter="test" lay-accordion>
        {{#  layui.each(d.list, function(index, item_folder){ }}
        <div class="layui-colla-item">
            <h2 class="layui-colla-title">
                <i class="iconfont pkcms-icon-folder"></i>
                {{  item_folder.folder  }}
            </h2>
            <div class="layui-colla-content layui-show layui-row">
                {{#  layui.each(item_folder.children, function(index, item_file){ }}
                <div class="layui-col-md3 layui-col-sm6 layui-col-xs6 layui-row">
                    <i class="layui-icon layui-icon-delete layui-bg-red" style="cursor: hand;"
                       onclick="del('{{  item_file.file_path  }}','{{  item_file.file_name  }}')"></i>
                    <a href="javascript: PKLayer.useModelByFrame('编辑文件 {{  item_file.file_name  }}','/index.php/Template/AdminSetTpl?path={{  item_file.file_path  }}');"
                       lay-tips="修改 {{  item_file.file_name  }} 代码">
                        {{#  if(item_file.file_ext=='tpl'){  }}
                        <i class="iconfont pkcms-icon-tpl"></i>
                        {{#  }else if(item_file.file_ext=='js'){ }}
                        <i class="iconfont pkcms-icon-js"></i>
                        {{#  }else if(item_file.file_ext=='css'){ }}
                        <i class="iconfont pkcms-icon-css"></i>
                        {{#  } }}
                        {{  item_file.file_name  }}
                    </a>
                </div>
                {{#  }); }}
            </div>
        </div>
        {{#  }); }}
    </div>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">
    function loading() {
        PKLayer.apiGet('Template/AdminGetTemplate/ApiByLists', {}, function (apiResult, count) {
            PKLayer.useTpl({list: apiResult}, 'tpl_collapse', 'view_collapse');
        });
    }

    function del(file_path, file_name) {
        PKLayer.tipsConfirmByTwoBtn(
            '真的要删除 ' + file_name + ' 么',
                {icon: 3,time: 30000, offset: 't',title:'提示',btn:['确定','取消']},
            function (index) {
                PKLayer.apiPost(
                    'Template/AdminSetTpl/ApiByDel',
                        {path:file_path},
                    function (apiResult, count) {
                        layer.close(index);
                        loading();
                    }
                );
            }, function () {
                layer.closeAll();
            });
    }

    PKAdmin.ready(function () {
        loading();
    });
</script>
</body>
</html>