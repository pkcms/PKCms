<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
    <link rel="stylesheet" href="/statics/pkcms/icons/iconfont.css" media="all">
</head>

<body>

<div class="layui-fluid">

    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <fieldset class="layui-elem-field layui-field-title">
                <legend>自定义信息</legend>
            </fieldset>
            <div class="layui-card layui-box layui-border-box">
                <div class="layui-card-header"><h3>说明:</h3></div>
                <div class="layui-card-body">
                    <ul>
                        <li>在这里可以设置自定义模板变量，满足对不同场景的需求！</li>
                        <li style="color: blue;font-weight: bold;">在模板中可通过 &lt;{$params[xxx]}&gt; 来调用，xxx 就是参数名（必须是英文）</li>
                        <li style="color: red;font-weight: bold;">
                            建议将不用、或者空值的变量名删除，减少对资源的占用。<br/>
                            不支持模板语言，所以不要将模板标签代码写在里面，只会当成普通字符输出到页面。
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="layui-card-body">
            <table class="layui-hide PK-table" id="table_adminList" lay-filter="table_adminList"
                   data-get="Template/AdminParams/getParamsList"
                   data-set="Template/AdminParams"></table>

        </div>
    </div>
</div>

<script type="text/html" id="table_toolBar">
    <div class="layui-btn-container">
        <button class="layui-btn layui-btn-normal layui-btn-sm" lay-tips="添加新的参数"
                lay-event="add|tpl-create">
            <i class="layui-icon layui-icon-add-circle"></i>
            添加新的参数
        </button>
    </div>
</script>

<script type="text/html" id="table_tool">
    <a class="layui-btn layui-btn-normal layui-btn-xs"
       lay-event="edit|tpl-change">
        <i class="layui-icon layui-icon-edit"></i>配值</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs"
       lay-event="del">
        <i class="layui-icon layui-icon-delete"></i>删除</a>
</script>

<script type="text/html" id="tpl-create" data-title="添加新的参数">
    <div class="layui-card">
        <div class="layui-card-body">
            <form class="layui-form" lay-filter="form-frame">
                <div class="layui-form-item">
                    <label class="layui-form-label">参数名：</label>
                    <div class="layui-input-block">
                        <input type="text" name="name" class="layui-input" lay-verify="required" autocomplete="off" />
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">参数备注：</label>
                    <div class="layui-input-block">
                        <input type="text" name="remark" class="layui-input" lay-verify="required" autocomplete="off" maxlength="100" />
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <div class="layui-footer">
                            <button class="layui-btn" lay-submit lay-filter="form-frame">立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</script>

<script type="text/html" id="tpl-change" data-title="编辑参数的内容">
    <div class="layui-card">
        <div class="layui-card-body">
            <form class="layui-form" lay-filter="form-frame">
                <div class="layui-form-item">
                    <label class="layui-form-label">参数名：</label>
                    <div class="layui-input-block">
                        <input type="text" name="name" class="layui-input layui-disabled" lay-verify="required" autocomplete="off" disabled="disabled" value="{{ d.name }}" />
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">参数备注：</label>
                    <div class="layui-input-block">
                        <input type="text" name="remark" class="layui-input layui-disabled" value="{{ d.remark }}" lay-verify="required" autocomplete="off" maxlength="100" disabled="disabled" />
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">参数值：</label>
                    <div class="layui-input-block">
                        <textarea class="layui-textarea" name="value">{{ d.value }}</textarea>
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <div class="layui-footer">
                            <input type="hidden" name="id" value="{{ d.id }}" />
                            <button class="layui-btn" lay-submit lay-filter="form-frame">立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">
    function renderTable() {
        PKAdmin.table({
            cols: [[
                {field:'name', title:'参数名',width:260},
                {field:'remark', title:'参数描述'},
                {field:'value', title:'参数值'},
                {
                    title: "操作",
                    align: "center",
                    width: 150,
                    fixed: "right",
                    toolbar: "#table_tool"
                }
            ]]
        });
    }

    PKAdmin.ready(function () {
        renderTable();
    });
</script>
</body>
</html>

