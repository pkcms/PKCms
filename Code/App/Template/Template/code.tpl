<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
    <link rel="stylesheet" href="/statics/pkcms/icons/iconfont.css" media="all">
    <!-- Create a simple CodeMirror instance -->
    <link rel="stylesheet" href="/statics/codemirror/lib/codemirror.css">
    <script src="/statics/codemirror/lib/codemirror.js"></script>
    <style>
        .CodeMirror {
            border: 1px solid #eee;
            height: auto;
        }

        .CodeMirror-scroll {
            height: auto;
            overflow-y: hidden;
            overflow-x: auto;
        }
    </style>
</head>

<body>

<div class="layui-fluid">

    <div class="layui-card">
        <div class="layui-card-body layui-card-body-mb">
            <textarea id="editor_code" name="editor_code"><{$code}></textarea>
        </div>

        <div class="layui-form-footer-fixed">
            <div class="layui-input-block">
                <div class="layui-footer">
                    <button type="button" class="layui-btn" id="submit">立即提交</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">
    var params = {
        path: '<{$path}>',
        code: ''
    };
    PKAdmin.ready(function () {
        var editor = CodeMirror.fromTextArea(
            document.getElementById('editor_code'),
            {
                lineNumbers: true,
                matchBrackets: true
            }
        );

        $('#submit').on('click', function () {
            params.code = editor.getValue();
            PKLayer.apiPost('Template/AdminSetTpl/ApiByChange', params, function (apiResult, count) {
                PKLayer.tipsMsg('保存成功')
            });
        });
    });
</script>
</body>
</html>

