<?php


namespace PKApp\Template;

use PKCommon\Controller\AdminGetController;

class AdminGetThemes extends AdminGetController
{

    public function ApiByLists()
    {
    }

    public function ApiBySelect()
    {
        $themesPath = PATH_ROOT . 'Templates';
        $themesDirList = fileHelper()->OpenFolder($themesPath);
        $result = [];
        foreach ($themesDirList as $item) {
            $result[] = ['id'=>$item,'name'=>$item];
        }
        $this->json($result);
    }

    public function Main()
    {
    }
}