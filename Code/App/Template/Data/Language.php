<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return array(
    'template_PathNotExists' => array(
        'zh-cn' => '模板路径不存在',
        'en' => 'Template path does not exist'
    ),
    'template_codeEmpty' => array(
        'zh-cn' => '文件的代码不能为空',
        'en' => 'The code of the file cannot be empty'
    ),
    'template_typeEmpty' => array(
        'zh-cn' => '文件的类型不能为空',
        'en' => 'The type of the file cannot be empty'
    ),
    'naterial_PathNotExists' => array(
        'zh-cn' => '素材路径不存在，路径：',
        'en' => 'The material path does not exist, path:'
    ),
    'naterial_Delete_Success' => array(
        'zh-cn' => '素材删除成功，请关闭此窗口后刷新列表',
        'en' => 'The material was successfully deleted. Please close this window and refresh the list'
    ),
    'template_file_delError' => array(
        'zh-cn' => '你要删除的文件已经不存在，或找不到',
        'en' => 'The file you want to delete no longer exists or cannot be found'
    ),
    'tpl_param_empty' => array(
        'zh-cn' => '模板的参数名、参数备注不能为空',
        'en' => 'The parameter name and parameter comment of the template cannot be empty'
    ),
    'Empty_paramsId' => array(
        'zh-cn' => '模板自定义变量的ID不能为空',
        'en' => 'The ID of a template custom variable cannot be empty',
    ),
    'Exists_tplParams' => array(
        'zh-cn' => '该模板自定义变量有同名存在',
        'en' => 'The template custom variable already exists with the same name'
    ),
);