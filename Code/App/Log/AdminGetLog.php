<?php


namespace PKApp\Log;


use PKApp\Log\Classes\LogService;
use PKCommon\Controller\AdminGetController;
use PKFrame\DataHandler\Arrays;

class AdminGetLog extends AdminGetController
{

    public function __construct()
    {
        parent::__construct();
        $this->service = new LogService();
    }

    public function ApiByLists()
    {
        $params = [
            'logType' => $this->service->GetAndIsExistsLogType(
                Arrays::GetKey('type', request()->get(), 'type_notEmpty')
            ),
        ];
        $this->getPages();
        $this->json(
            $this->service->GetList($params, '*', self::$pageIndex, self::$pageSize),
            $this->service->GetCount($params)
        );
    }

    public function ApiBySelect()
    {
    }

    public function Main()
    {
        $type = $this->service->GetAndIsExistsLogType(
            Arrays::GetKey('type', request()->get(), 'type_notEmpty')
        );
        $this->assign([
            'type' => $type,
        ])->fetch('list_' . $type);
    }
}