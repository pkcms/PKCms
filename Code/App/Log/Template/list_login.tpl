<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
    <link rel="stylesheet" href="/statics/pkcms/icons/iconfont.css" media="all">
</head>

<body>

<div class="layui-fluid">

    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <fieldset class="layui-elem-field layui-field-title">
                <legend>登陆日志</legend>
            </fieldset>
        </div>

        <div class="layui-card-body">
            <table class="layui-hide PK-table" id="table_adminList" lay-filter="table_adminList"
                   data-get="Log/AdminGetLog" data-params="type=<{$type}>"></table>

            <script type="text/html" id="table_toolBar">
                <div class="layui-btn-container">
                </div>
            </script>

            <script type="text/html" id="table_tool">
                <a class="layui-btn layui-btn-normal layui-btn-xs"
                   lay-event="edit" lay-tips="查看详细日志">
                    <i class="iconfont pkcms-icon-view"></i></a>
            </script>

        </div>
    </div>
</div>

<script type="text/html" id="stateTpl">
    {{#
    var class_name = function(){
    if (d.logStateEn == 'login_success'){
    return 'layui-btn-primary';
    } else {
    return 'layui-btn-danger';
    }
    };
    }}
    <button class="layui-btn {{ class_name() }} layui-btn-xs">{{ d.logStateCn }}</button>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">

    function renderTable() {
        PKAdmin.table({
            cols: [[
                {field:'id', title:'ID',width:80},
                {field:'userName', title:'用户名'},
                {field:'userIP', title:'登陆IP'},
                {field:'userIPArea', title:'定位地区'},
                {field:'userDevice', title:'登录设备头'},
                {field:'logStateEn', title:'登陆状态',width:100, templet: '#stateTpl'},
                {field:'createTime', title:'登陆时间',templet:"<div>{{dateFormat(d.createTime)}}</div>"},
                {field:'logContent', title:'备注'}
            ]]
        }, true);
    }

    PKAdmin.ready(function () {
        renderTable();
    });

</script>
</body>
</html>
