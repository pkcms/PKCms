<?php

namespace PKApp\Log\Classes;

use PKFrame\Lib\DataBase;

class LogTranslateDB extends DataBase
{
    protected $table = 'log_translate';
}