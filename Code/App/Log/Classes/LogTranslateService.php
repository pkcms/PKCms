<?php

namespace PKApp\Log\Classes;

use PKFrame\Service;

class LogTranslateService extends Service
{

    public function interface_getEntityById($id, $viewField = '*')
    {
    }

    protected function db()
    {
        static $cls;
        !empty($cls) ?: $cls = new LogTranslateDB();
        return $cls;
    }

    public function GetList($viewParams = [], $viewField = '*')
    {
        return $this->db()->Where($viewParams)->Select($viewField)->ToList();
    }

    public function GetEntity($viewParams = [], $viewField = '*')
    {
        return $this->db()->Where($viewParams)->Select($viewField)->First();
    }
}