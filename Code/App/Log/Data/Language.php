<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return [
    'state_en_noExists' => [
        'zh-cn' => '日志的状态（英文）在程序的支持范围中不存在',
        'en' => 'The status of the log (in English) does not exist within the supported scope of the program'
    ],
    'type_notEmpty' => [
        'zh-cn' => '日志的类型不能为空',
        'en' => 'The type of the log cannot be empty'
    ],
    'type_noExists' => [
        'zh-cn' => '日志的类型在程序的支持范围中不存在',
        'en' => 'The type of log does not exist within the supported scope of the program'
    ],
];