<?php
return [
    'log_type' => [
        'login' => '用户登陆',
    ],
    'log_state' => [
        'login_success' => '登陆成功',
        'login_fail' => '登陆失败',
    ],
];
