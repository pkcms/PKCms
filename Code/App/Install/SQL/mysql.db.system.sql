USE
`[db_name]`;

CREATE TABLE `pk_area`
(
    `code`       int(10) NOT NULL COMMENT '行政区划代码',
    `name`       varchar(20) NOT NULL COMMENT '名字',
    `phoneCode`  mediumint(9) NOT NULL DEFAULT '0' COMMENT '地区号',
    `zipCode`    mediumint(9) NOT NULL DEFAULT '0' COMMENT '邮政编码',
    `level`      varchar(8)  NOT NULL DEFAULT '' COMMENT 'country:国家、province:省份（直辖市会在province和city显示）、city:市（直辖市会在province和city显示）、district:区县',
    `parentCode` int(10) DEFAULT NULL COMMENT '所属行政区划代码',
    `parentName` varchar(20)          DEFAULT '' COMMENT '所属行政区划名字',
    `fullName`   varchar(90)          DEFAULT NULL COMMENT '行政区划完整名字',
    `pinyin`     varchar(50)          DEFAULT NULL COMMENT '全拼',
    `longitude`  double(10, 7
) DEFAULT NULL COMMENT '经度',
  `latitude` double(10,7) DEFAULT NULL COMMENT '纬度',
  PRIMARY KEY (`code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='行政区划表';

CREATE TABLE `pk_category`
(
    `id`             int(11) NOT NULL AUTO_INCREMENT,
    `siteId`         tinyint(4) NOT NULL DEFAULT '0' COMMENT '站点ID',
    `modelId`        tinyint(4) NOT NULL DEFAULT '0' COMMENT '模型ID',
    `parentId`       int(11) NOT NULL DEFAULT '0' COMMENT '父级ID',
    `parentIdList`   tinytext COMMENT '父级ID列',
    `childrenIdList` tinytext COMMENT '子级ID列',
    `categoryType`   enum('link','page','list') NOT NULL COMMENT '栏目类型',
    `name`           varchar(80) NOT NULL COMMENT '名称',
    `info`           text COMMENT '描述',
    `image`          tinytext COMMENT '图片',
    `seo`            text COMMENT 'SEO',
    `url`            tinytext COMMENT '链接（链接形式栏目）',
    `template`       text COMMENT '模板配置',
    `power`          text COMMENT '权限配置',
    `listSort`       tinyint(4) NOT NULL DEFAULT '0' COMMENT '排序',
    `countContent`   int(11) NOT NULL DEFAULT '0' COMMENT '内容量',
    `isShow`         tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否显示',
    `isDeleted`      tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除',
    PRIMARY KEY (`id`),
    KEY              `idx` (`siteId`,`isDeleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='栏目表';

CREATE TABLE `pk_collection_content`
(
    `id`         int(11) NOT NULL AUTO_INCREMENT,
    `siteId`     tinyint(4) NOT NULL DEFAULT '0' COMMENT '站点ID',
    `nodeId`     tinyint(4) NOT NULL DEFAULT '0' COMMENT '采集节点ID',
    `status`     enum('await','collection','import') NOT NULL COMMENT '内容状态',
    `url`        tinytext NOT NULL COMMENT '内容URL',
    `title`      tinytext COMMENT '内容标题',
    `data`       text COMMENT '采集来序列化数据',
    `createTime` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
    PRIMARY KEY (`id`),
    KEY          `idx` (`siteId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='采集器-内容表';

CREATE TABLE `pk_collection_node`
(
    `id`            tinyint(4) NOT NULL AUTO_INCREMENT,
    `siteId`        tinyint(4) NOT NULL DEFAULT '0' COMMENT '站点ID',
    `name`          varchar(200) NOT NULL COMMENT '名称',
    `sourceCharset` enum('GBK','UTF-8','BIG5') NOT NULL COMMENT '页面编码',
    `listConfig`    text         NOT NULL COMMENT '列表配置',
    `rule`          text         NOT NULL COMMENT '正文内容匹配规则',
    `createTime`    int(11) NOT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`),
    KEY             `idx` (`siteId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='采集器-节点表';

CREATE TABLE `pk_content_article`
(
    `id`             int(11) NOT NULL AUTO_INCREMENT,
    `siteId`         tinyint(4) DEFAULT '0' COMMENT '站点ID',
    `modelId`        tinyint(4) DEFAULT '0' COMMENT '模型ID',
    `catId`          int(11) DEFAULT '0' COMMENT '栏目ID',
    `title`          tinytext COMMENT '标题',
    `thumb`          varchar(200) DEFAULT NULL COMMENT '缩略图',
    `seoTitle`       tinytext COMMENT 'SEO标题',
    `seoKeywords`    text COMMENT 'SEO关键词',
    `seoDescription` text COMMENT 'SEO描述',
    `userId`         int(11) DEFAULT '0' COMMENT '用户ID',
    `createTime`     BIGINT DEFAULT '0' COMMENT '创建时间',
    `updateTime`     BIGINT DEFAULT '0' COMMENT '更新时间',
    `hits`           int(11) DEFAULT '0' COMMENT '点击率',
    `listSort`       smallint DEFAULT '0' COMMENT '排序值',
    `template`       varchar(50)  DEFAULT NULL COMMENT '自定义模板',
    `url`            varchar(225)  DEFAULT NULL COMMENT '自定义URL',
    `toHead`         tinyint(1) DEFAULT '0' COMMENT '头条',
    `toPush`         tinyint(1) DEFAULT '0' COMMENT '推荐',
    `toTop`          tinyint(1) DEFAULT '0' COMMENT '置顶',
    `isDeleted`      tinyint(1) DEFAULT '0' COMMENT '是否删除',
    `mirroringId`    int(11) DEFAULT '0' COMMENT '镜像ID',
    PRIMARY KEY (`id`),
    KEY              `idx_site` (`siteId`,`isDeleted`),
    KEY              `idx_cat` (`catId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='内容-文章基表';

CREATE TABLE `pk_content_article_data`
(
    `id`      int(11) NOT NULL,
    `content` longtext COMMENT '正文内容',
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='内容-文章附表';

CREATE TABLE `pk_content_page`
(
    `id`             int(11) NOT NULL AUTO_INCREMENT,
    `siteId`         tinyint(4) NOT NULL DEFAULT '0' COMMENT '站点ID',
    `title`          tinytext NOT NULL COMMENT '标题',
    `seoTitle`       tinytext COMMENT 'SEO标题',
    `seoKeywords`    tinytext COMMENT 'SEO关键词',
    `seoDescription` text COMMENT 'SEO描述',
    `content`        longtext COMMENT '正文内容',
    PRIMARY KEY (`id`),
    KEY              `idx` (`siteId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='单页表';

CREATE TABLE `pk_content_product`
(
    `id`             int(11) NOT NULL AUTO_INCREMENT,
    `siteId`         tinyint(4) DEFAULT '0' COMMENT '站点ID',
    `modelId`        tinyint(4) DEFAULT '0' COMMENT '模型ID',
    `catId`          int(11) DEFAULT '0' COMMENT '栏目ID',
    `title`          tinytext COMMENT '标题',
    `thumb`          varchar(200) DEFAULT NULL COMMENT '缩略图',
    `seoTitle`       tinytext COMMENT 'SEO标题',
    `seoKeywords`    text COMMENT 'SEO关键词',
    `seoDescription` text COMMENT 'SEO描述',
    `userId`         int(11) DEFAULT '0' COMMENT '用户ID',
    `createTime`     BIGINT DEFAULT '0' COMMENT '创建时间',
    `updateTime`     BIGINT DEFAULT '0' COMMENT '更新时间',
    `hits`           int(11) DEFAULT '0' COMMENT '点击率',
    `listSort`       smallint DEFAULT '0' COMMENT '排序值',
    `template`       varchar(50)  DEFAULT NULL COMMENT '自定义模板',
    `url`            varchar(255)  DEFAULT NULL COMMENT '自定义URL',
    `toHead`         tinyint(1) DEFAULT '0' COMMENT '头条',
    `toPush`         tinyint(1) DEFAULT '0' COMMENT '推荐',
    `toTop`          tinyint(1) DEFAULT '0' COMMENT '置顶',
    `isDeleted`      tinyint(1) DEFAULT '0' COMMENT '是否删除',
    `mirroringId`    int(11) DEFAULT '0' COMMENT '镜像ID',
    PRIMARY KEY (`id`),
    KEY              `idx_site` (`siteId`,`isDeleted`),
    KEY              `idx_cat` (`catId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

CREATE TABLE `pk_content_product_data`
(
    `id`      int(11) NOT NULL,
    `content` longtext COMMENT '正文内容',
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

CREATE TABLE `pk_lian_chat`
(
    `id`             int(11) NOT NULL AUTO_INCREMENT,
    `createUserName` varchar(80)  DEFAULT NULL COMMENT '提交者的称呼',
    `createPhone`    varchar(20)  DEFAULT NULL COMMENT '提交者的电话',
    `createIP`       varchar(64)  DEFAULT NULL COMMENT '提交者的IP',
    `createIPArea`   tinytext COMMENT '提交者的IP所属地区',
    `content`        text COMMENT '内容',
    `pageTitle`      varchar(200) DEFAULT NULL COMMENT '访问的页面标题',
    `pageUrl`        varchar(255) DEFAULT NULL COMMENT '访问的页面地址',
    `createTime`     BIGINT DEFAULT '0' COMMENT '创建时间',
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

CREATE TABLE `pk_links_category`
(
    `id`        tinyint(4) NOT NULL AUTO_INCREMENT,
    `siteId`    tinyint(4) NOT NULL DEFAULT '0' COMMENT '站点ID',
    `name`      varchar(200) NOT NULL COMMENT '分类名',
    `isDeleted` tinyint(1) DEFAULT '0' COMMENT '是否删除',
    PRIMARY KEY (`id`),
    KEY         `idx` (`siteId`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='链接-站点表';

CREATE TABLE `pk_links_site`
(
    `id`         tinyint(4) NOT NULL AUTO_INCREMENT,
    `siteId`     tinyint(4) NOT NULL DEFAULT '0' COMMENT '站点ID',
    `categoryId` tinyint(4) NOT NULL DEFAULT '0' COMMENT '父级ID',
    `siteName`   varchar(200) NOT NULL COMMENT '站点名',
    `siteLogo`   tinytext COMMENT '站点LOGO',
    `siteUrl`    tinytext     NOT NULL COMMENT '站点URL',
    `siteInfo`   text COMMENT '站点描述',
    `listSort`   tinyint(4) NOT NULL DEFAULT '0' COMMENT '排序',
    `isIndex`    tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否显示在首页',
    `createTime` BIGINT NOT NULL DEFAULT 0 COMMENT '创建时间',
    PRIMARY KEY (`id`),
    KEY          `idx` (`siteId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='链接-站点表';

CREATE TABLE `pk_log`
(
    `id`           int(11) NOT NULL AUTO_INCREMENT,
    `logType`      varchar(50)  NOT NULL COMMENT '日志类型',
    `userName`     varchar(200) NOT NULL COMMENT '用户名',
    `userIP`       varchar(64)  NOT NULL COMMENT '用户IP',
    `userIPAreaId` int(11) NOT NULL COMMENT 'IP所在地区ID',
    `userIPArea`   varchar(255) NOT NULL COMMENT 'IP所在地区',
    `userDevice`   text         NOT NULL COMMENT '用户设备',
    `logContent`   text COMMENT '日志内容',
    `logStateEn`   varchar(50)  NOT NULL COMMENT '日志状态-英文',
    `logStateCn`   varchar(100) NOT NULL COMMENT '日志状态-中文',
    `createTime`   BIGINT NOT NULL DEFAULT 0 COMMENT '创建时间',
    PRIMARY KEY (`id`),
    KEY            `idx_type` (`logType`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

CREATE TABLE `pk_member_group`
(
    `id`            tinyint(4) NOT NULL AUTO_INCREMENT,
    `modelId`       tinyint(4) NOT NULL DEFAULT '0' COMMENT '数据模型ID',
    `name`          varchar(200) NOT NULL COMMENT '角色名',
    `powerById`     text COMMENT '权限ID',
    `powerByAction` text COMMENT '操作权限',
    `isDisabled`    tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否禁用',
    `isSystem`      tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否系统内置',
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='成员-角色表';

CREATE TABLE `pk_member_user`
(
    `id`              tinyint(4) NOT NULL AUTO_INCREMENT,
    `siteIdByAdmin`   tinyint(4) NOT NULL DEFAULT '0' COMMENT '站点管理员的站点ID',
    `modelId`         tinyint(4) NOT NULL DEFAULT '0' COMMENT '数据模型ID',
    `groupId`         tinyint(4) NOT NULL DEFAULT '0' COMMENT '角色ID',
    `userName`        varchar(200) NOT NULL COMMENT '用户名',
    `password`        varchar(32) COMMENT '密码',
    `encrypt`         varchar(15) COMMENT '身份码',
    `email`           varchar(255),
    `emailActivation` tinyint(4) NOT NULL DEFAULT '0' COMMENT '邮箱激活',
    `mobile`          int(11) DEFAULT '0',
    `phoneActivation` tinyint(4) NOT NULL DEFAULT '0' COMMENT '手机激活',
    `createTime`      BIGINT NOT NULL DEFAULT '0' COMMENT '创建时间',
    `lastTime`        BIGINT DEFAULT '0' COMMENT '持续时间',
    `lastIp`          varchar(64) DEFAULT '0' COMMENT '持续IP',
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='成员-会员表';

CREATE TABLE `pk_model`
(
    `id`        tinyint(4) NOT NULL AUTO_INCREMENT,
    `modelType` enum('content','form','member') NOT NULL COMMENT '数据模型类型',
    `name`      varchar(200) NOT NULL COMMENT '模型名',
    `tableName` varchar(80) DEFAULT NULL COMMENT '数据表名',
    `setting`   text COMMENT '模型配置',
    `isSystem`  tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否内置',
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='模型表';

CREATE TABLE `pk_model_field`
(
    `id`           tinyint(4) NOT NULL AUTO_INCREMENT,
    `modelId`      tinyint(4) NOT NULL DEFAULT '0' COMMENT '数据模型ID',
    `formType`     enum('text','textarea','number','editor','upload_list','image_list','option','frontend_code','baidu_map') NOT NULL COMMENT '字段类型',
    `field`        varchar(200) NOT NULL COMMENT '字段',
    `name`         varchar(200) NOT NULL COMMENT '字段名',
    `setting`      text COMMENT '字段类型相关配置',
    `fieldRegular` enum('','num','int','letter','letterNum','letter_num','letter_','email','qq','url','mobile','tel','zipCode','date','datetime') DEFAULT NULL COMMENT '匹配规则',
    `power`        varchar(100) DEFAULT NULL COMMENT '权限',
    `isList`       tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否为列表',
    `isSearch`     tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否为搜索',
    `isEmpty`      tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否不为空',
    `isShow`       tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否显示',
    `isOrder`      tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否跟随订单',
    `listSort`     tinyint(4) NOT NULL DEFAULT '0' COMMENT '排序',
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='模型-字段表';

CREATE TABLE `pk_model_options`
(
    `id`       tinyint(4) NOT NULL AUTO_INCREMENT,
    `parentId` tinyint(4) NOT NULL DEFAULT '0' COMMENT '父级ID',
    `name`     varchar(200) NOT NULL COMMENT '名称',
    `Image`    varchar(200) DEFAULT NULL COMMENT '图片',
    `listSort` tinyint(4) NOT NULL DEFAULT '0' COMMENT '排序',
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='模型-选项表';

CREATE TABLE `pk_poster`
(
    `id`         tinyint(4) NOT NULL AUTO_INCREMENT,
    `name`       varchar(200) NOT NULL COMMENT '名称',
    `posterType` enum('text','image') NOT NULL COMMENT '广告位类型',
    `createTime` BIGINT NOT NULL DEFAULT '0' COMMENT '创建时间',
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='广告位表';

CREATE TABLE `pk_poster_image`
(
    `id`         tinyint(4) NOT NULL AUTO_INCREMENT,
    `siteId`     tinyint(4) NOT NULL DEFAULT '0' COMMENT '站点ID',
    `posterId`   tinyint(4) NOT NULL DEFAULT '0' COMMENT '广告位ID',
    `image_alt`  varchar(200) NOT NULL COMMENT '图片描述',
    `image_src`  varchar(200) NOT NULL COMMENT '图片地址',
    `image_url`  varchar(200) NOT NULL COMMENT '链接地址',
    `startTime`  BIGINT NOT NULL DEFAULT '0' COMMENT '开始时间',
    `stopTime`   BIGINT       NOT NULL DEFAULT '0' COMMENT '结束时间',
    `createTime` BIGINT NOT NULL DEFAULT '0' COMMENT '创建时间',
    `listSort`   tinyint(4) NOT NULL DEFAULT '0' COMMENT '排序',
    PRIMARY KEY (`id`),
    KEY          `idx` (`siteId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='广告位-图片表';

CREATE TABLE `pk_poster_text`
(
    `id`         tinyint(4) NOT NULL AUTO_INCREMENT,
    `siteId`     tinyint(4) NOT NULL DEFAULT '0' COMMENT '站点ID',
    `posterId`   tinyint(4) NOT NULL DEFAULT '0' COMMENT '广告位ID',
    `text`       tinytext     NOT NULL COMMENT '文字',
    `fontColor`  varchar(7) DEFAULT NULL COMMENT '文字颜色',
    `url`        varchar(200) NOT NULL COMMENT '链接地址',
    `startTime`  BIGINT NOT NULL DEFAULT '0' COMMENT '开始时间',
    `stopTime`   BIGINT NOT NULL DEFAULT '0' COMMENT '结束时间',
    `createTime` BIGINT NOT NULL DEFAULT '0' COMMENT '创建时间',
    `listSort`   tinyint(4) NOT NULL DEFAULT '0' COMMENT '排序',
    PRIMARY KEY (`id`),
    KEY          `idx` (`siteId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='广告位-文字表';

CREATE TABLE `pk_power`
(
    `id`        smallint(6) NOT NULL AUTO_INCREMENT,
    `scene`     enum('system','admin','user') NOT NULL COMMENT '场景',
    `powerType` enum('menu','action') NOT NULL COMMENT '类型',
    `parentId`  smallint(6) NOT NULL DEFAULT '0' COMMENT '父级ID',
    `name`      varchar(200) NOT NULL COMMENT '名称',
    `app`       varchar(20) DEFAULT NULL COMMENT '模块',
    `c`         varchar(50) DEFAULT NULL COMMENT '控制器',
    `action`    varchar(30) DEFAULT NULL COMMENT '操作',
    `data`      varchar(50) DEFAULT NULL COMMENT '附加参数',
    `listSort`  tinyint(4) NOT NULL DEFAULT '0' COMMENT '排序',
    PRIMARY KEY (`id`),
    KEY         `idx` (`parentId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='权限表';

CREATE TABLE `pk_site`
(
    `id`               tinyint(4) NOT NULL AUTO_INCREMENT,
    `Site_Name`        varchar(200) DEFAULT NULL COMMENT '名称',
    `Site_Domain`      varchar(200) DEFAULT NULL COMMENT '域名',
    `Site_Path`        varchar(50)  DEFAULT NULL COMMENT '路径PC',
    `Site_Tcp`         varchar(50)  DEFAULT NULL COMMENT '备案号',
    `Site_logo`        varchar(200) DEFAULT NULL COMMENT 'LOGO',
    `Site_Title`       tinytext COMMENT '标题',
    `Site_Keyword`     text COMMENT '关键词',
    `Site_Description` text COMMENT '描述',
    `Site_Contact`     text COMMENT '联系方式',
    `setting`          text COMMENT '设置',
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='站点表';

CREATE TABLE `pk_tpl_params`
(
    `id`         INT          NOT NULL AUTO_INCREMENT,
    `name`       VARCHAR(200) NOT NULL COMMENT '参数名',
    `remark`     VARCHAR(200) COMMENT '参数备注',
    `value`      text COMMENT '参数值',
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='专题';

CREATE TABLE `pk_topic`
(
    `id`             INT          NOT NULL AUTO_INCREMENT,
    `siteId`         INT          NOT NULL DEFAULT 0 COMMENT '站点ID',
    `title`          VARCHAR(200) NOT NULL COMMENT '专题名',
    `seoTitle`       VARCHAR(200) COMMENT 'seo标题',
    `seoKeyword`     VARCHAR(255) COMMENT 'seo关键词',
    `seoDescription` text COMMENT 'seo描述',
    `image`          VARCHAR(255) NOT NULL COMMENT '专题图片',
    `banner`         VARCHAR(255),
    `content`        TEXT COMMENT '专题描述',
    `fileHtml`       VARCHAR(255) NOT NULL COMMENT '静态文件名',
    `fileTpl`        VARCHAR(255) NOT NULL COMMENT '模板文件名',
    `createTime`     BIGINT          NOT NULL DEFAULT 0,
    `isDeleted`      TINYINT      NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    KEY              `idx` (`siteId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='专题';

CREATE TABLE `pk_topic_type`
(
    `id`         INT          NOT NULL AUTO_INCREMENT,
    `siteId`     INT          NOT NULL DEFAULT 0 COMMENT '站点ID',
    `topicId`    INT          NOT NULL DEFAULT 0 COMMENT '专题ID',
    `name`       VARCHAR(200) NOT NULL COMMENT '分类名',
    `createTime` BIGINT          NOT NULL DEFAULT 0 COMMENT '创建时间',
    `isDeleted`  TINYINT      NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    KEY          `idx` (`isDeleted`,`siteId`,`topicId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='专题的分类';

CREATE TABLE `pk_topic_content`
(
    `id`             int(11) NOT NULL AUTO_INCREMENT,
    `siteId`         int(11) NOT NULL DEFAULT '0' COMMENT '站点ID',
    `topicId`        int(11) NOT NULL DEFAULT '0' COMMENT '专题ID',
    `topicTitle`     varchar(255) NOT NULL,
    `topicTypeId`    int(11) NOT NULL DEFAULT '0' COMMENT '分类ID',
    `topicTypeName`  varchar(255) NOT NULL,
    `contentModelId` int(11) NOT NULL DEFAULT '0' COMMENT '内容的数据模型ID',
    `contentCatId`   int(11) NOT NULL DEFAULT '0' COMMENT '内容的分类ID',
    `contentCatName` varchar(255) NOT NULL COMMENT '内容的分类名称',
    `contentId`      int(11) NOT NULL DEFAULT '0' COMMENT '内容的正文ID',
    `contentTitle`   varchar(255) NOT NULL COMMENT '内容标题',
    `contentImage`   varchar(255) NOT NULL COMMENT '内容缩略图',
    `contentUrl`     varchar(255) NOT NULL COMMENT '内容的URL',
    `listSort`       int(11) NOT NULL DEFAULT '0' COMMENT '内容排序',
    `createTime`     BIGINT NOT NULL DEFAULT '0' COMMENT '创建时间',
    `isDeleted`      tinyint(4) NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`),
    KEY              `idx_site` (`isDeleted`,`siteId`),
    KEY              `idx_topicType` (`topicId`,`topicTypeId`),
    KEY              `idx_model` (`contentModelId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='专题的内容';

CREATE TABLE `pk_upload`
(
    `id`         INT          NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `md5`        VARCHAR(35)  NOT NULL COMMENT '文件码',
    `fileType`   VARCHAR(20)  NOT NULL COMMENT '文件类型',
    `fileName`   VARCHAR(80)  NOT NULL COMMENT '文件名称',
    `size`       VARCHAR(20)  NOT NULL COMMENT '文件大小',
    `url`        VARCHAR(255) NOT NULL COMMENT 'URL',
    `createTime` BIGINT       NOT NULL DEFAULT 0 COMMENT '创建时间',
    `updateTime` BIGINT       NOT NULL DEFAULT 0 COMMENT '更新时间',
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='上传的资源';