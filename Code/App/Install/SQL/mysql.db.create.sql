drop
database if exists `[db_name]`;

CREATE
DATABASE `[db_name]` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;