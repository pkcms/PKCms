CREATE TABLE `[tableName]` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `siteId` tinyint(4) DEFAULT '0' COMMENT '站点ID',
  `createIP` varchar(16) NOT NULL DEFAULT '0' COMMENT '留言者的IP',
  `createUserId` int(8) DEFAULT '0' COMMENT '留言者的会员ID',
  `createUserName` varchar(70) NOT NULL COMMENT '留言者的称呼',
  `createTime` BIGINT DEFAULT '0' COMMENT '留言者提交时间',
  `lastIP` varchar(16) DEFAULT '0' COMMENT '持续者的IP',
  `lastUserId` int(8) DEFAULT '0' COMMENT '持续者的会员ID',
  `lastUserName` varchar(70) COMMENT '持续者的称呼',
  `lastTime` BIGINT DEFAULT '0' COMMENT '持续者提交时间',
  `status` enum('await','reply','finish') NOT NULL COMMENT '状态',
  `isDelete` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

CREATE TABLE `[tableName]_last` (
  `lastId` int(10) DEFAULT NULL,
  `lastUserId` int(10) DEFAULT '0' COMMENT '持续者的会员ID',
  `lastUserName` varchar(70) NOT NULL COMMENT '持续者的称呼',
  `lastTime` BIGINT NOT NULL DEFAULT '0' COMMENT '持续者提交时间',
  `lastIp` varchar(16) NOT NULL COMMENT '持续者的IP',
  `replyContent` text NOT NULL COMMENT '持续者的回复',
  `isDelete` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`lastTime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;