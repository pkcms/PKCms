CREATE TABLE `[tableName]`
(
    `id`             int(11) NOT NULL AUTO_INCREMENT,
    `siteId`         tinyint(4) DEFAULT '0' COMMENT '站点ID',
    `modelId`        tinyint(4) DEFAULT '0' COMMENT '模型ID',
    `catId`          int(11) DEFAULT '0' COMMENT '栏目ID',
    `title`          tinytext COMMENT '标题',
    `thumb`          varchar(200) DEFAULT NULL COMMENT '缩略图',
    `seoTitle`       tinytext COMMENT 'SEO标题',
    `seoKeywords`    text COMMENT 'SEO关键词',
    `seoDescription` text COMMENT 'SEO描述',
    `userId`         int(11) DEFAULT '0' COMMENT '用户ID',
    `createTime`     BIGINT DEFAULT '0' COMMENT '创建时间',
    `updateTime`     BIGINT DEFAULT '0' COMMENT '更新时间',
    `hits`           int(11) DEFAULT '0' COMMENT '点击率',
    `listSort`       smallint DEFAULT '0' COMMENT '排序值',
    `template`       varchar(50)  DEFAULT NULL COMMENT '自定义模板',
    `url`            varchar(255)  DEFAULT NULL COMMENT '自定义URL',
    `toHead`         tinyint(1) DEFAULT '0' COMMENT '头条',
    `toPush`         tinyint(1) DEFAULT '0' COMMENT '推荐',
    `toTop`          tinyint(1) DEFAULT '0' COMMENT '置顶',
    `isDeleted`      tinyint(1) DEFAULT '0' COMMENT '是否删除',
    `mirroringId`    int(11) DEFAULT '0' COMMENT '镜像ID',
    PRIMARY KEY (`id`),
    KEY              `idx_site` (`siteId`,`isDeleted`),
    KEY              `idx_cat` (`catId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

CREATE TABLE `[tableName]_data`
(
    `id` int(11) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;