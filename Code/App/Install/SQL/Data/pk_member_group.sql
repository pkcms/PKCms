insert into `pk_member_group`(`id`, `modelId`, `name`, `powerById`, `powerByAction`, `isDisabled`, `isSystem`)
values (1, 1, '超级管理员', '1,2,25,6,8,10,5,37,4,3,7,17,23,18,19,27,20,28,29,30,31',
        'changeSiteBaseSetPower,changeSiteSEO,changeSiteContact,changeSiteHeightSetting', 0, 1),
       (2, 1, '开发管理员', '1,2,25,6,8,10,37,40,21,34,22,36,24,14,11,12,35,38,33,26,28,29,32,30,31,13,9,15,16',
        'changeCategoryBaseSetPower,changeContentBaseSetPower,changeSiteBaseSetPower,changeSiteSEO,changeCategorySEOPower,changeContentHeightSetPower,changeSiteContact,changeCategoryTplPower,changeSiteHeightSetting,changeCategoryHeightSetPower',
        0, 1),
       (3, 1, '客户管理员', '21,34,22,36,24,14,11,38,33,26,30,31,13,15,16',
        'changeCategoryBaseSetPower,changeContentBaseSetPower,changeCategorySEOPower,changeContentHeightSetPower', 0,
        1),
       (4, 2, '禁止访问', '', '', 1, 1),
       (5, 2, '普通会员', '', '', 0, 1);