insert into `pk_category`(`id`, `siteId`, `modelId`, `parentId`, `parentIdList`, `childrenIdList`, `categoryType`,
                          `name`, `info`, `image`, `seo`, `url`, `template`, `power`, `listSort`, `countContent`,
                          `isShow`, `isDeleted`)
values (1, 1, 0, 0, '', NULL, 'link', '网站首页', '', '', NULL, '/', NULL, 'N;', 0, 0, 1, 0),
       (2, 1, 0, 0, '', '3', 'link', '关于我们', '', '', NULL, '/about.htm', NULL, 'N;', 0, 0, 1, 0),
       (3, 1, 0, 2, '2', '', 'page', '企业简介', '', '', NULL, NULL,
        'a:3:{s:7:\"pageTpl\";s:8:\"page.tpl\";s:8:\"pageHtml\";s:1:\"1\";s:7:\"pageUrl\";s:9:\"about.htm\";}',
        'a:1:{s:9:\"EditGroup\";s:0:\"\";}', 0, 0, 1, 0),
       (4, 1, 3, 0, '', NULL, 'list', '企业新闻', '', '',
        'a:3:{s:5:\"title\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:11:\"description\";s:0:\"\";}', NULL,
        'a:8:{s:11:\"categoryTpl\";s:17:\"category_news.tpl\";s:10:\"contentTpl\";s:13:\"show_news.tpl\";s:12:\"listDataSize\";s:2:\"20\";s:9:\"orderList\";s:0:\"\";s:12:\"categoryHtml\";s:1:\"1\";s:11:\"contentHtml\";s:1:\"1\";s:11:\"categoryUrl\";s:23:\"category[id]-[page].htm\";s:10:\"contentUrl\";s:23:\"content[catId]-[id].htm\";}',
        'a:2:{s:9:\"EditGroup\";s:0:\"\";s:11:\"ReadMeGroup\";s:0:\"\";}', 0, 0, 1, 0),
       (5, 1, 4, 0, '', NULL, 'list', '产品中心', '', '',
        'a:3:{s:5:\"title\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:11:\"description\";s:0:\"\";}', NULL,
        'a:8:{s:11:\"categoryTpl\";s:20:\"category_product.tpl\";s:10:\"contentTpl\";s:16:\"show_product.tpl\";s:12:\"listDataSize\";s:2:\"20\";s:9:\"orderList\";s:0:\"\";s:12:\"categoryHtml\";s:1:\"1\";s:11:\"contentHtml\";s:1:\"1\";s:11:\"categoryUrl\";s:23:\"category[id]-[page].htm\";s:10:\"contentUrl\";s:23:\"content[catId]-[id].htm\";}',
        'a:2:{s:9:\"EditGroup\";s:0:\"\";s:11:\"ReadMeGroup\";s:0:\"\";}', 0, 0, 1, 0),
       (6, 1, 0, 0, '', '', 'page', '联系方式', '', '', NULL, NULL,
        'a:3:{s:7:\"pageTpl\";s:8:\"page.tpl\";s:8:\"pageHtml\";s:1:\"1\";s:7:\"pageUrl\";s:11:\"contact.htm\";}',
        'a:1:{s:9:\"EditGroup\";s:0:\"\";}', 0, 0, 1, 0)