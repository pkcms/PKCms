<?php


namespace PKApp\Install;

use PKApp\DataBase\Classes\TraitDataBase;
use PKApp\Install\Classes\InstallController;

class ApiDataBase extends InstallController
{
    use TraitDataBase;

    public function Main()
    {
        $this->modelByDB();
        $this->checkLink($this->params_post);
        cache()->Disk()->WriteByConfig('DataBase', [$this->params_post]);
        $this->json();
    }

    public function GetNameIsEqual()
    {
        $this->modelByDB();
        $this->checkLink($this->params_post);
        $this->json([
            'is_exists' => $this->serviceInstall()->GetIsExistsByDB(
                $this->params_post['name']
            ),
        ]);
    }

    public function CreateDB()
    {
        $this->modelByDB();
        $this->checkLink($this->params_post);
        $db_name = $this->params_post['name'];
        if ($this->serviceInstall()->GetIsExistsByDB($db_name)) {
            $this->serviceOfDataBase()->RemoveOfDataBaseByAll();
        } else {
            $this->serviceInstall()->InstallByCreateDB($db_name);
        }
        $this->json([]);
    }

}