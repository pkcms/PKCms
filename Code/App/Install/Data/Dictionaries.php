<?php
return [
    // 表单数据的状态
    'php_extension' => [
        'must' => [
            'curl', 'gd', 'mbstring', 'mysqli', 'pdo_mysql', 'iconv', 'json'
        ],
        'select' => [
            'pdo_sqlite', 'sqlite3', 'sockets', 'openssl',
            'imagick', 'newrelic', 'pcntl', 'posix',
        ],
    ],
];
