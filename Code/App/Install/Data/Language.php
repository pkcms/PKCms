<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return array(
    'appNameEn_empty' => array(
        'zh-cn' => '没有检测到 app 的 name',
        'en' => 'No name detected for app'
    ),
    'app_exists' => array(
        'zh-cn' => '要安装的 app 已经存在，无须再执行安装操作！',
        'en' => 'The app to be installed already exists, there is no need to perform the installation operation again!'
    ),
    'app_noExists' => array(
        'zh-cn' => '要升级的 app 不存在，无法再执行升级操作！',
        'en' => 'The app to be upgraded does not exist and cannot be upgraded again!'
    ),
    'app_physicalStructure_err' => array(
        'zh-cn' => '要安装的 app 的物理结构的路径出错！请检查路径：',
        'en' => 'The path of the physical structure of the app to be installed is incorrect! Please check the path:'
    ),
    'app_structureJson_error' => array(
        'zh-cn' => '要安装的 app 的物理结构的说明文件不存在，或者里面的数据为空，请检查！',
        'en' => 'The physical structure description file of the app to be installed does not exist, or the data inside is empty. Please check!'
    ),
    'php_extension_listEmpty' => array(
        'zh-cn' => 'PHP 扩展列表数据找不到了，请检查程序文件的完整性',
        'en' => 'The PHP extension list data cannot be found. Please check the integrity of the program file'
    ),
    'php_extension_listEmpty_must' => [
        'zh-cn' => 'PHP 必须扩展列表数据找不到了，请检查程序文件的完整性',
        'en' => 'PHP must extend the list data and cannot be found. Please check the integrity of the program file'
    ],
    'db_name_empty' => array(
        'zh-cn' => '检查数据库名是否重复时，数据库名不能为空',
        'en' => 'When checking for duplicate database names, the database name cannot be empty'
    ),
    'db_sqlFile_empty' => array(
        'zh-cn' => '安装所需的 SQL 文件不存在，请核实程序的目录及文件的完整性后再尝试！',
        'en' => 'The SQL file required for installation does not exist. Please verify the integrity of the program directory and files before attempting again!'
    ),
    'setParams_siteId' => array(
        'zh-cn' => '站点的ID不能为空',
        'en' => 'The ID of the site cannot be empty'
    ),
    'setParams_userName_admin' => array(
        'zh-cn' => '超级管理员的名不能为空',
        'en' => 'The name of the super administrator cannot be empty'
    ),
    'setParams_userName_site' => array(
        'zh-cn' => '站点管理员的名不能为空',
        'en' => 'The name of the site administrator cannot be empty'
    ),
    'setParams_userName_customer' => array(
        'zh-cn' => '客户管理员的名不能为空',
        'en' => 'The name of the customer administrator cannot be empty'
    ),
    'zipFile_noExists' => array(
        'zh-cn' => '执行的压缩包没有找到，路径：',
        'en' => 'The compressed package executed was not found, path:'
    ),
);