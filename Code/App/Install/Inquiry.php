<?php
/**
 * 安装前的询问
 */

namespace PKApp\Install;


use PKFrame\Controller;

class Inquiry extends Controller
{

    public function Main()
    {
        if (file_exists(PATH_CACHE)) {
            $this->fetch('Inquiry');
        } else {
            $this->fetch('Install');
        }
    }

    public function ApiByChangePath()
    {
        fileHelper()->PutContents(PATH_CACHE, SYSTEM_LOCK, request()->ServerFingerprint());
        $this->json();
    }

}