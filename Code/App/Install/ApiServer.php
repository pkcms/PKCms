<?php


namespace PKApp\Install;


use PKFrame\Controller;
use PKFrame\DataHandler\Arrays;

class ApiServer extends Controller
{

    private $_result;

    public function Main()
    {
        $this->_result = [
            'OS' => request()->ServerByOS(),
            'web_soft' => request()->server('SERVER_SOFTWARE'),
            'php_version' => PHP_VERSION,
            'php_extension' => [],
        ];
        $this->_getPHPExtension();
        $this->json($this->_result);
    }

    private function _getPHPExtension()
    {
        $list_extension = dict('php_extension');
        if (Arrays::Is($list_extension)) {
            $php_extension = ['must' => [], 'select' => []];
            foreach (Arrays::GetKey('must', $list_extension, 'php_extension_listEmpty_must') as $item) {
                $php_extension['must'][] = [
                    'name' =>$item,
                    'isLoaded' =>extension_loaded($item),
                ];
            }
            if ($list_extension_select = Arrays::GetKey('select', $list_extension)) {
                foreach ($list_extension_select as $item) {
                    $php_extension['select'][] = [
                        'name' =>$item,
                        'isLoaded' =>extension_loaded($item),
                    ];
                }
            }
            $this->_result['php_extension'] = $php_extension;
        } else {
            $this->noticeByJson('php_extension_listEmpty');
        }
    }

}