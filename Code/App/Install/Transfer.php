<?php


namespace PKApp\Install;


use PKApp\Install\Classes\InstallController;

class Transfer extends InstallController
{

    public function Main()
    {
        $this->fetch(isInstall() ? 'transfer' : 'done');
    }

    public function Done()
    {
        fileHelper()->PutContents(PATH_CACHE, SYSTEM_LOCK, request()->ServerFingerprint());
        $this->json();
    }

}