<?php
/**
 * 控制器 - 安装
 */

namespace PKApp\Install;

use PKApp\Install\Classes\InstallController;
use PKApp\Member\Classes\MemberService;
use PKApp\Model\Classes\FieldService;
use PKApp\Site\Classes\TraitSite;
use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\MatchHelper;
use PKFrame\DataHandler\Str;

class Install extends InstallController
{

    use TraitSite;

    public function Main()
    {
        $this->fetch(isInstall() ? 'Install' : 'done');
    }

    public function ApiByCreateTable()
    {
        $this->modelByDB();
        $this->checkLink($this->params_post['db']);
        $this->serviceInstall()->InstallByCreateTable(
            $this->params_post['db']['name'],
            $this->params_post['db']['prefix']
        );
        $this->json([]);
    }

    public function ApiByImportData()
    {
        $this->modelByDB();
        $this->checkLink($this->params_post['db']);
        $this->serviceInstall()->InstallByImportData();
        $list_model = $this->serviceField()->ServiceByModel()->GetListByType('content');
        if (Arrays::Is($list_model)) {
            foreach ($list_model as $item) {
                $this->serviceField()->saveCache($item['id']);
            }
        }
        $this->json([]);
    }

    public function ApiByCreateSite()
    {
        $this->modelByDB();
        $params_post_site = $this->params_post['site'];
        MatchHelper::checkUrlOfScheme($params_post_site['siteDomain']) ?: $this->noticeByJson('Site_Domain_Error', 'site');
        $this->json(['siteId' => $this->serviceOfSite()->Add([
            'Site_Name' => $params_post_site['siteName'],
            'Site_title' => $params_post_site['siteName'],
            'Site_Keyword' => $params_post_site['siteName'],
            'Site_Description' => $params_post_site['siteName'],
            'setting' => serialize([
                'Default_Lang' => 'zh-cn',
                'searchItemSize' => 18,
                'Site_Themes' => 'default',
                'MobileStaticsPath' => '',
            ]),
            'Site_Domain' => $params_post_site['siteDomain'],
        ])]);
    }

    public function ApiByCreateAdmin()
    {
        $this->modelByDB();
        $params_post_site = $this->params_post['site'];
        $id_site = Arrays::GetKey('siteId', $params_post_site, 'setParams_siteId');
        $list_admin = [
            'admin' => [
                'userName' => Arrays::GetKey('userName_admin', $params_post_site, 'setParams_userName_admin'),
                'modelId' => 1,
                'groupId' => 1,
                'createTime' => TIMESTAMP,
            ],
            'site' => [
                'userName' => Arrays::GetKey('userName_site', $params_post_site, 'setParams_userName_site'),
                'modelId' => 1,
                'groupId' => 2,
                'siteIdByAdmin' => $id_site,
                'createTime' => TIMESTAMP,
            ],
            'customer' => [
                'userName' => Arrays::GetKey('userName_customer', $params_post_site, 'setParams_userName_customer'),
                'modelId' => 1,
                'groupId' => 3,
                'siteIdByAdmin' => $id_site,
                'createTime' => TIMESTAMP,
            ],
        ];
        $service = new MemberService();
        $result = [];
        foreach ($list_admin as $index => $item) {
            $tmp_pwd = Arrays::GetKey('password', $item) ? $item['password'] : Str::Random(16);
            $service->Add(array_merge($item, $service->createNewPassWord($tmp_pwd)));
            $result = array_merge($result, [
                'userName_' . $index => $item['userName'],
                'pwd_' . $index => $tmp_pwd,
            ]);
        }
        fileHelper()->PutContents(PATH_CACHE, SYSTEM_LOCK, request()->ServerFingerprint());
        request()->session(SESSION_AdminKey, null, true);
        $this->json($result);
    }

    protected function serviceField(): FieldService
    {
        static $cls;
        !empty($cls) ?: $cls = new FieldService();
        return $cls;
    }

}