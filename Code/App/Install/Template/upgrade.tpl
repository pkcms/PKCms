<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
    <style>
        .ok {
            color: white;
            font-size: 3px;
            font-weight: bold;
            background: #52C41A;
            padding: 5px;
            line-height: 3px;
        }

        .error {
            color: white;
            font-size: 3px;
            font-weight: bold;
            background: red;
            padding: 5px;
            line-height: 3px;
        }
    </style>
</head>

<body>

<div class="layui-fluid">

    <div class="layui-fluid">

        <div class="layui-card">
            <div class="layui-card-header layuiadmin-card-header-auto layui-bg-blue">
                <fieldset class="layui-elem-field layui-field-title">
                    <legend>程序升级向导</legend>
                </fieldset>
            </div>

            <div class="layui-card-body layui-card-body-pb">
                <div class="layui-carousel" id="stepForm" lay-filter="stepForm" style="margin: 0 auto;">
                    <div carousel-item>
                        <div>
                            <form class="layui-form" style="margin: 0 auto;max-width: 800px;padding-top: 40px;">
                                <p>程序检查到新的版本，将执行版本升级的操作，过程中会进行以下的操作</p>
                                <div id="view-tips"></div>
                                <div class="layui-form-item">
                                    <div class="layui-input-block">
                                        <button class="layui-btn layui-hide" id="btn-u"
                                                lay-submit lay-filter="formStep">
                                            &emsp;确定执行升级操作&emsp;
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div>
                            <div style="margin: 0 auto;max-width: 460px;padding-top: 40px;">
                                <div>备份进度：</div>
                                <div class="layui-progress layui-progress-big" lay-filter="backup_db"
                                     lay-showpercent="true">
                                    <div class="layui-progress-bar layui-bg-blue" lay-percent="0%">
                                        <span class="layui-progress-text"></span>
                                    </div>
                                </div>
                                <div>升级进度：</div>
                                <div class="layui-progress layui-progress-big" lay-filter="upgrade-db"
                                     lay-showpercent="true">
                                    <div class="layui-progress-bar layui-bg-blue" lay-percent="0%">
                                        <span class="layui-progress-text"></span>
                                    </div>
                                </div>
                                <div class="layui-border-box" style="height: 300px;padding: 5px;overflow: auto;">
                                    <ul id="view-log"></ul>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div style="text-align: center;margin-top: 90px;">
                                <i class="layui-icon layui-circle"
                                   style="color: white;font-size:30px;font-weight:bold;background: #52C41A;padding: 20px;line-height: 80px;">&#xe605;</i>
                                <div style="font-size: 24px;color: #333;font-weight: 500;margin-top: 30px;">
                                    升级完成，感谢您的使用！
                                </div>
                                <div id="view-done" style="font-size: 14px;color: #666;margin-top: 20px;">
                                </div>
                            </div>
                            <div style="text-align: center;margin-top: 50px;">
                                <a class="layui-btn next" href="/index.php/Admin/Index">进入后台</a>
                                <a class="layui-btn layui-btn-primary" href="/index.php/Site/Index?siteId=1">查看前台</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/html" id="tpl-tips">
    <ul style="padding: 20px;">
        <li>1、备份数据库</li>
        <li>2、本次升级的版本跨度有：{{ d.range }}个版本</li>
        <li>3、要涉及修改的数据表：{{ d.change_table }}</li>
    </ul>
</script>

<script type="text/html" id="tpl-log">
    <li>
        完成进度： -&gt;【{{ d.text }}】
    </li>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/install.js"></script>

<script type="text/javascript">
    var data = {
        steps_transfer: [
            {
                title: "阅读须知"
            },
            {
                title: "执行升级"
            },
            {
                title: "完成升级"
            }
        ],
        version: {
            new: '<{$version_new}>',
            now: '<{$version_now}>'
        },
        api: '<{SERVICE_API}>',
        upgrade: {
            range: 0,
            change_table: '',
            change_list: [],
            index: 0
        },
        backup: {
            api: 'DataBase/AdminSetBackup',
            list: null,
            index: 0,
            count: 0
        },
        file: {
            list: [],
            count: 0,
            index: 0
        },
        step: '',
        element_speed: ''
    };

    function backup_speed(element_speed) {
        setTimeout(function () {
            element_speed.progress('backup_db', PKCms.Percentage(data.backup.index, data.backup.count) + '%');
            PKLayer.apiPost(data.backup.api, data.backup, function (params_result, count) {
                PKLayer.useTplPrepend({text:data.backup.list[data.backup.index]}, 'tpl-log', 'view-log');
                data.backup.index += 1;
                if (data.backup.index < data.backup.count) {
                    backup_speed(element_speed);
                } else {
                    element_speed.progress('backup_db', PKCms.Percentage(data.backup.index, data.backup.count) + '%');
                    PKLayer.useTplPrepend({text:'任务已完成'}, 'tpl-log', 'view-log');
                    upgrade_downLoad();
                }
            });
        }, 1000);
    }

    function getBackup() {
        layui.use(['element'], function () {
            var element_speed = layui.element;
            PKLayer.apiPost('DataBase/AdminSetBackup/GetListByTableName',{}, function (result, count) {
                PKLayer.useTplPrepend({text:'获取所有的数据表名'}, 'tpl-log', 'view-log');
                data.backup.list = result;
                data.backup.count = count;
                data.backup.index = 0;
                backup_speed(element_speed);
            });
        });
    }

    function upgrade() {
        setTimeout(function () {
            if (data.file.index < data.file.count) {
                var item_file = data.file.list[data.file.index];
                PKLayer.apiPost(
                    'Install/Upgrade/Change', item_file,
                    function (apiResult, count) {
                        PKLayer.useTplPrepend({text:item_file.file_name}, 'tpl-log', 'view-log');
                        data.file.index += 1;
                        data.element_speed.progress('upgrade_db', PKCms.Percentage(data.file.index, data.file.count) + '%');
                        upgrade();
                    });
            } else {
                PKLayer.apiPost('Install/Upgrade/Done', data.version, function (apiResult, count) {
                    PKLayer.useTplPrepend({text:'升级完毕'}, 'tpl-log', 'view-log');
                    data.step.next('#stepForm');
                });
            }
        }, 1000);
    }

    function upgrade_file() {
        PKLayer.apiPost('Install/Upgrade/GetChangeFile', data.version, function (apiResult, count) {
            PKLayer.useTplPrepend({text:'获取所有的数据结构更新文件'}, 'tpl-log', 'view-log');
            console.log(count);
            if (count > 0) {
                data.file.count = count;
                data.file.list = apiResult;
                data.element_speed.progress('upgrade_db', PKCms.Percentage(data.file.index, data.file.count) + '%');
                upgrade();
            } else {
                PKLayer.apiPost('Install/Upgrade/Done', data.version, function (apiResult, count) {
                    PKLayer.useTplPrepend({text:'升级完毕'}, 'tpl-log', 'view-log');
                    data.step.next('#stepForm');
                });
            }
        });
    }

    function upgrade_releaseZip() {
        PKLayer.apiPost('Install/Upgrade/ReleaseZip', data.version, function (apiResult, count) {
            PKLayer.useTplPrepend({text:'释放数据结构更新包'}, 'tpl-log', 'view-log');
            upgrade_file();
        });
    }

    function upgrade_downLoad() {
        PKLayer.apiPost('Install/Upgrade/DownLoad', data.version, function (apiResult, count) {
            PKLayer.useTplPrepend({text:'获取数据结构更新包'}, 'tpl-log', 'view-log');
            upgrade_releaseZip();
        });
    }

    PKInstall.ready(function () {
        PKCms.apiPost(data.api + 'Version/getVersionRange', data.version, function (error, apiResult) {
            if (error) {
                PKLayer.tipsMsg('服务接口出错！');
            } else {
                PKLayer.apiResultHandler(apiResult, function (res, count) {
                    console.log(res);
                    if (res.hasOwnProperty('is_upgrade') && res.is_upgrade) {
                        if (res.hasOwnProperty('range_list') && res.hasOwnProperty('range_table')) {
                            data.upgrade.change_table = res.range_table.toString();
                            data.upgrade.range = res.range_list.length;
                            data.upgrade.change_list = res.range_list;
                            PKLayer.useTpl(data.upgrade, 'tpl-tips', 'view-tips');
                            $('#btn-u').removeClass('layui-hide');
                        }
                    } else {
                        window.location.href = '/admin.php';
                    }
                });
            }
        });


        PKLayer.useFormByStep(data.steps_transfer, '700px', function (form, step, element_speed) {
            data.step = step;
            data.element_speed = element_speed;
            form.on('submit(formStep)', function (params_form) {
                data.step.next('#stepForm');
                getBackup();
                return false;
            });
        });

    });

</script>
</body>
</html>
