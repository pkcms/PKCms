<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
    <style>
        .ok {
            color: white;
            font-size: 3px;
            font-weight: bold;
            background: #52C41A;
            padding: 5px;
            line-height: 3px;
        }

        .error {
            color: white;
            font-size: 3px;
            font-weight: bold;
            background: red;
            padding: 5px;
            line-height: 3px;
        }
    </style>
</head>

<body>

<div class="layui-fluid">

    <div class="layui-fluid">

        <div class="layui-card">
            <div class="layui-card-header layuiadmin-card-header-auto layui-bg-blue">
                <fieldset class="layui-elem-field layui-field-title">
                    <legend>转移网站向导</legend>
                </fieldset>
            </div>

            <div class="layui-card-body layui-card-body-pb">
                <div class="layui-carousel" id="stepForm" lay-filter="stepForm" style="margin: 0 auto;">
                    <div carousel-item>
                        <div>
                            <form class="layui-form" style="margin: 0 auto;max-width: 800px;padding-top: 40px;">
                                <p>操作前的提示及准备工作</p>
                                <ul style="padding: 20px;">
                                    <li>1、确认转移目标机器（或空间）的环境适合程序运行的基本需求</li>
                                    <li>2、转移前做好重要程序及数据（包括数据库）的备份</li>
                                    <li>3、备份数据库可通过超级管理员的后台的【数据库管理】》【备份/导入】界面操作</li>
                                    <li>4、将程序根目录下所有目录及文件（Tmp目录及其下的目录文件可除外）整个打包上传要目标地，<b
                                                style="color: red">在此前做好第2、3步操作</b>
                                    </li>
                                </ul>
                                <div class="layui-form-item">
                                    <div class="layui-input-block">
                                        <button class="layui-btn" lay-submit
                                                lay-filter="formStep">
                                            &emsp;下一步&emsp;
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div>
                            <form class="layui-form" style="margin: 0 auto;width: 800px;padding-top: 40px;">
                                <div id="show-server"></div>
                                <div class="layui-form-item">
                                    <div class="layui-input-block">
                                        <button type="button" class="layui-btn layui-btn-primary pre">上一步</button>
                                        <button id="stepBtn-service" class="layui-btn" disabled
                                                lay-submit lay-filter="formStep2">
                                            &emsp;下一步&emsp;
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div>
                            <form class="layui-form" style="margin: 0 auto;max-width: 560px;padding-top: 40px;">
                                <div style="color: red;font-weight: bold;font-size: 16px;">
                                    信息（除密码）都必须填写！在线上则按照服务器商给配置的填写。
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">数据库访问服务器地址:</label>
                                    <div class="layui-input-block">
                                        <input type="text" class="layui-input" name="host"
                                               value="127.0.0.1" lay-verify="required" autocomplete="off"/>
                                        <div class="layui-form-mid layui-word-aux">
                                            如数据库与站点在一台机子（像宝塔）建议写 127.0.0.1。
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">数据库访问端口号:</label>
                                    <div class="layui-input-block">
                                        <input type="text" class="layui-input" name="port"
                                               value="3306" lay-verify="required" autocomplete="off"/>
                                        <div class="layui-form-mid layui-word-aux">
                                            默认端口：3306。
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">数据库访问数据库名:</label>
                                    <div class="layui-input-block">
                                        <div>
                                            <div class="layui-inline">
                                                <input type="text" class="layui-input" name="name"
                                                       value="cms_" lay-verify="required" autocomplete="off"/>
                                            </div>
                                            <div class="layui-inline">
                                                <button class="layui-btn layui-btn-normal" lay-submit
                                                        lay-filter="checkDBExists">
                                                    &emsp;检查是否存在同名&emsp;
                                                </button>
                                            </div>
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">
                                            在本地可【cms_[xxx]】。
                                            <b style="color: red">
                                                如数据库中有存在相同的库名，则原来会被删除并重新创建，请做好对原来的备份
                                            </b>
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">数据库访问用户名:</label>
                                    <div class="layui-input-block">
                                        <input type="text" class="layui-input" name="user"
                                               value="root" lay-verify="required" autocomplete="off"/>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">数据库访问用户密码:</label>
                                    <div class="layui-input-block">
                                        <input type="text" class="layui-input" name="pass"/>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <div class="layui-input-block">
                                        <button type="button" class="layui-btn layui-btn-primary pre">上一步</button>
                                        <button class="layui-btn" lay-submit lay-filter="formStep3">
                                            &emsp;下一步&emsp;
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div>
                            <div style="margin: 0 auto;max-width: 460px;padding-top: 40px;">
                                <div>导入进度：</div>
                                <div class="layui-progress layui-progress-big" lay-filter="demo" lay-showpercent="true">
                                    <div class="layui-progress-bar layui-bg-blue" lay-percent="0%">
                                        <span class="layui-progress-text"></span>
                                    </div>
                                </div>
                                <div class="layui-border-box" style="height: 300px;padding: 5px;overflow: auto;">
                                    <ul id="view-log"></ul>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div style="text-align: center;margin-top: 90px;">
                                <i class="layui-icon layui-circle"
                                   style="color: white;font-size:30px;font-weight:bold;background: #52C41A;padding: 20px;line-height: 80px;">&#xe605;</i>
                                <div style="font-size: 24px;color: #333;font-weight: 500;margin-top: 30px;">
                                    转移完成，感谢您的使用！
                                </div>
                                <div id="view-done" style="font-size: 14px;color: #666;margin-top: 20px;">
                                </div>
                            </div>
                            <div style="text-align: center;margin-top: 50px;">
                                <a class="layui-btn next" href="/index.php/Admin/Index">进入后台</a>
                                <a class="layui-btn layui-btn-primary" href="/index.php/Site/Index?siteId=1">查看前台</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/html" id="tpl-server">
    <table class="layui-table" id="table-field-model" lay-filter="table-field-model">
        <thead>
        <tr>
            <th>检查项目</th>
            <th>要求配置</th>
            <th>当前配置</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th>操作系统</th>
            <td>windows / linux（推荐 linux）</td>
            <td>
                {{# if(d.isOK_os){ }}
                <i class="layui-icon layui-circle ok">&#xe605;</i>
                {{# }else{ }}
                <i class="layui-icon layui-circle error">&#x1006;</i>
                {{# } }}
                {{ d.OS }}
            </td>
        </tr>
        <tr>
            <th>WEB服务器软件</th>
            <td>IIS / APACHE / NGIUX（推荐 APACHE / NGIUX）</td>
            <td>
                {{# if(d.isOK_os){ }}
                <i class="layui-icon layui-circle ok">&#xe605;</i>
                {{# }else{ }}
                <i class="layui-icon layui-circle error">&#x1006;</i>
                {{# } }}
                {{ d.web_soft }}
            </td>
        </tr>
        <tr>
            <th>数据库服务器</th>
            <td>MySQL 5.5 （及以上）</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <th>PHP 运行库（版本）</th>
            <td>7.2.x ~ 7.4.x</td>
            <td>{{ d.php_version }}</td>
        </tr>
        <tr>
            <th>PHP 必须要打开的扩展</th>
            <td colspan="2">
                <div class="layui-row">
                    <ul>
                        {{#  layui.each(d.php_extension.must, function(index, item){ }}
                        <li class="layui-col-md2" style="margin-bottom: 10px;">
                            {{# if(item.isLoaded){ }}
                            <i class="layui-icon layui-circle ok">&#xe605;</i>
                            {{# }else{ }}
                            <i class="layui-icon layui-circle error">&#x1006;</i>
                            {{# } }}
                            {{ item.name }}
                        </li>
                        {{#  }); }}
                    </ul>
                </div>
            </td>
        </tr>
        <tr>
            <th>
                PHP 可选择性打开的扩展 <br>
                (可在后期有需要用时再打开)
            </th>
            <td colspan="2">
                <div class="layui-row">
                    <ul>
                        {{#  layui.each(d.php_extension.select, function(index, item){ }}
                        <li class="layui-col-md2" style="margin-bottom: 10px;">
                            {{# if(item.isLoaded){ }}
                            <i class="layui-icon layui-circle ok">&#xe605;</i>
                            {{# }else{ }}
                            <i class="layui-icon layui-circle error">&#x1006;</i>
                            {{# } }}
                            {{ item.name }}
                        </li>
                        {{#  }); }}
                    </ul>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</script>

<script type="text/html" id="tpl-log">
    <li>
        完成进度： -&gt;【{{ d.text }}】
    </li>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/install.js"></script>

<script type="text/javascript">
    var data = {
        steps_transfer: [
            {
                title: "阅读须知"
            },
            {
                title: "环境检测"
            },
            {
                title: "配置数据库信息"
            },
            {
                title: "执行数据导入"
            },
            {
                title: "完成转移向导"
            }
        ]
    };

    PKInstall.ready(function () {
        PKLayer.useFormByStep(data.steps_transfer, '700px', function (form, step, element_speed) {

            form.on('submit(formStep)', function (params_form) {
                step.next('#stepForm');
                PKInstall.api('ApiServer',{}, function (res_params, count) {
                    PKInstall.serviceHandler(res_params);
                });
                return false;
            });

            form.on('submit(checkDBExists)', function (params_form) {
                PKInstall.checkDB(params_form.field);
                return false;
            });

            form.on('submit(formStep2)', function (params_form) {
                step.next('#stepForm');
                return false;
            });

            form.on('submit(formStep3)', function (params_form) {
                PKInstall.params_api.db = params_form.field;
                PKInstall.api('ApiDataBase', PKInstall.params_api, function (params_res, count) {
                    PKInstall.api('ApiDataBase/CreateDB', PKInstall.params_api, function (params_result, count) {
                        step.next('#stepForm');
                        PKLayer.apiPost('DataBase/AdminImportBackup/CountBackupFile',{}, function (result, count) {
                            window.speed.list = result;
                            window.speed.count = count;
                            window.speed.index = 0;
                            PKLayer.useTplPrepend({text:'获取所有的备份SQL文件'}, 'tpl-log', 'view-log');
                            PKInstall.goToSpeed(element_speed, 'DataBase/AdminImportBackup', function () {
                                PKLayer.apiPost('Install/Transfer/Done',{}, function (res, count) {
                                    step.next('#stepForm');
                                });
                            });
                        });
                    });
                });
                return false;
            });

        });

    });

</script>
</body>
</html>
