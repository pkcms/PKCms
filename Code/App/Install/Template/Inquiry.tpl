<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
</head>

<body>

<div class="layui-fluid">

    <div class="layui-fluid">

        <div class="layui-card">
            <div class="layui-card-header layuiadmin-card-header-auto layui-bg-blue">
                <fieldset class="layui-elem-field layui-field-title">
                    <legend>程序所在环境发生变动</legend>
                </fieldset>
            </div>

            <div class="layui-card-body">
                <p>出现以下现象时会出现此提示</p>
                <ul style="padding: 20px;">
                    <li>1、程序所在机器的操作系统等环境发生了改变</li>
                    <li>2、程序所在磁盘的路径发生了改变</li>
                    <li>3、新的网站程序的部署</li>
                    <li>4、重要的配置数据的丢失，影响程序的正常运行</li>
                </ul>
                <p>根据以上提示，请选择以下操作!</p>
            </div>
            <div class="layui-card-body layui-card-body-pb">
                <div class="layui-inline">
                    <a class="layui-btn layui-btn"
                       lay-tips="恒心要重新安装，操作此项" href="/index.php/Install/Install">
                        直接重新安装
                    </a>
                    <a class="layui-btn layui-btn-normal"
                       lay-tips="程序的所在环境发生改变，操作此项" href="/index.php/Install/Transfer">
                        转移网站，重配环境参数（包含数据库）
                    </a>
                    <button type="button" class="layui-btn layui-btn-normal" id="btnChangePath"
                            lay-tips="只是路径发生改变，操作此项">
                        现有配置不变
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/install.js"></script>

<script type="text/javascript">
    var data = {};

    PKInstall.ready(function () {
        $('#btnChangePath').one('click',function () {
            PKLayer.apiGet('Install/Inquiry/ApiByChangePath',{}, function (res, count) {
                PKLayer.tipsMsg('操作成功，系统将自动刷新，请稍候!');
                setTimeout(function () {
                    window.location.reload();
                }, 3000);
            });
        });
    });

</script>
</body>
</html>
