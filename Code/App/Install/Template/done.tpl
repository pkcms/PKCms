<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
</head>

<body>

<div class="layui-fluid">

    <div class="layui-fluid">

        <div class="layui-card">
            <div class="layui-card-header layuiadmin-card-header-auto layui-bg-blue">
                <fieldset class="layui-elem-field layui-field-title">
                    <legend>请谨慎操作！</legend>
                </fieldset>
            </div>

            <div class="layui-card-body layui-card-body-pb">
                <div style="text-align: center;margin-top: 90px;">
                    <i class="layui-icon layui-circle"
                       style="color: white;font-size:30px;font-weight:bold;background: #52C41A;padding: 20px;line-height: 80px;">&#xe605;</i>
                    <div style="font-size: 24px;color: #333;font-weight: 500;margin-top: 30px;">
                        程序无须重置，感谢您的使用！
                    </div>
                    <div id="view-done" style="font-size: 14px;color: #666;margin-top: 20px;">
                    </div>
                </div>
                <div style="text-align: center;margin-top: 50px;">
                    <a class="layui-btn next" href="/index.php/Admin/Index" target="_blank">进入后台</a>
                    <a class="layui-btn layui-btn-primary" href="/index.php/Site/Index?siteId=1"
                       target="_blank">查看前台</a>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/install.js"></script>

<script type="text/javascript">
    var data = {};

    PKInstall.ready(function () {
        $('#btnChangePath').on('click', function () {
            PKLayer.apiGet('Install/Inquiry/ApiByChangePath',{}, function (res, count) {
                PKLayer.tipsMsg('操作成功，系统将自动刷新，请稍候!');
                setTimeout(function () {
                    window.location.reload();
                }, 3000);
            });
        });
    });

</script>
</body>
</html>
