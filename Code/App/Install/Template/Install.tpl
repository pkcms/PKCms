<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/steps.css" media="all">
    <link rel="stylesheet" href="/statics/pkcms/icons/iconfont.css" media="all">
    <style>
        .ok {
            color: white;
            font-size: 3px;
            font-weight: bold;
            background: #52C41A;
            padding: 5px;
            line-height: 3px;
        }

        .error {
            color: white;
            font-size: 3px;
            font-weight: bold;
            background: red;
            padding: 5px;
            line-height: 3px;
        }
    </style>
</head>

<body>

<div class="layui-fluid">

    <div class="layui-fluid">

        <div class="layui-card">
            <div class="layui-card-header layuiadmin-card-header-auto layui-bg-blue">
                <fieldset class="layui-elem-field layui-field-title">
                    <legend>PKCMS安装向导</legend>
                </fieldset>
            </div>

            <div class="layui-card-body">
                <div class="layui-carousel" id="stepForm" lay-filter="stepForm" style="margin: 0 auto;">
                    <div carousel-item>
                        <div>
                            <form class="layui-form" style="margin: 0 auto;max-width: 800px;padding-top: 40px;">
                                <div id="show-server"></div>
                                <div class="layui-form-item">
                                    <div class="layui-input-block">
                                        <button id="stepBtn-service" disabled class="layui-btn" lay-submit
                                                lay-filter="formStep">
                                            &emsp;下一步&emsp;
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div>
                            <form class="layui-form" style="margin: 0 auto;max-width: 560px;padding-top: 40px;">
                                <div style="color: red;font-weight: bold;font-size: 16px;">
                                    信息（除密码）都必须填写！在线上则按照服务器商给配置的填写。
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">数据库访问服务器地址:</label>
                                    <div class="layui-input-block">
                                        <input type="text" class="layui-input" name="host"
                                               value="127.0.0.1" lay-verify="required" autocomplete="off"/>
                                        <div class="layui-form-mid layui-word-aux">
                                            如数据库与站点在一台机子（像宝塔）建议写 127.0.0.1。
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">数据库访问端口号:</label>
                                    <div class="layui-input-block">
                                        <input type="text" class="layui-input" name="port"
                                               value="3306" lay-verify="required" autocomplete="off"/>
                                        <div class="layui-form-mid layui-word-aux">
                                            默认端口：3306。
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">数据库访问数据库名:</label>
                                    <div class="layui-input-block">
                                        <div>
                                            <div class="layui-inline">
                                                <input type="text" class="layui-input" name="name"
                                                       value="cms_" lay-verify="required" autocomplete="off"/>
                                            </div>
                                            <div class="layui-inline">
                                                <button class="layui-btn layui-btn-normal" lay-submit
                                                        lay-filter="checkDBExists">
                                                    &emsp;检查是否存在同名&emsp;
                                                </button>
                                            </div>
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">
                                            在本地可【cms_[xxx]】。
                                            <b style="color: red">
                                                如数据库中有存在相同的库名，则原来会被删除并重新创建，请做好对原来的备份
                                            </b>
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">数据库访问用户名:</label>
                                    <div class="layui-input-block">
                                        <input type="text" class="layui-input" name="user"
                                               value="root" lay-verify="required" autocomplete="off"/>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">数据库访问用户密码:</label>
                                    <div class="layui-input-block">
                                        <input type="text" class="layui-input" name="pass"/>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <div class="layui-input-block">
                                        <button type="button" class="layui-btn layui-btn-primary pre">上一步</button>
                                        <button class="layui-btn" lay-submit lay-filter="formStep2">
                                            &emsp;下一步&emsp;
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div>
                            <form class="layui-form" style="margin: 0 auto;width: 460px;padding-top: 40px;">
                                <div style="color: red;font-weight: bold;font-size: 16px;">所有信息都必须填写！</div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">超级管理员用户名:</label>
                                    <div class="layui-input-block">
                                        <input type="text" class="layui-input" name="userName_admin"
                                               value="sAdmin" lay-verify="required" autocomplete="off"/>
                                        <div class="layui-form-mid layui-word-aux">
                                            为提高安全性，密码由系统随机产生
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">开发管理员用户名:</label>
                                    <div class="layui-input-block">
                                        <input type="text" class="layui-input" name="userName_site"
                                               value="dAdmin" lay-verify="required" autocomplete="off"/>
                                        <div class="layui-form-mid layui-word-aux">
                                            为提高安全性，密码由系统随机产生
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">客户管理员用户名:</label>
                                    <div class="layui-input-block">
                                        <input type="text" class="layui-input" name="userName_customer"
                                               value="cAdmin" lay-verify="required" autocomplete="off"/>
                                        <div class="layui-form-mid layui-word-aux">
                                            为提高安全性，密码由系统随机产生
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">网站名称:</label>
                                    <div class="layui-input-block">
                                        <input type="text" class="layui-input" name="siteName"
                                               value="中国XXX有限公司" lay-verify="required" autocomplete="off"/>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">网站域名:</label>
                                    <div class="layui-input-block">
                                        <input type="text" class="layui-input" name="siteDomain"
                                               value="http://www.xxx.com" lay-verify="required" autocomplete="off"/>
                                        <div class="layui-form-mid layui-word-aux">
                                            系统部分功能，需要核实域名才能正常使用，请认真填写
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <div class="layui-input-block">
                                        <button type="button" class="layui-btn layui-btn-primary pre">上一步</button>
                                        <button class="layui-btn" lay-submit lay-filter="formStep3">
                                            &emsp;确认入库&emsp;
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div>
                            <div style="margin: 0 auto;max-width: 460px;padding-top: 40px;">
                                <div>安装进度：</div>
                                <div class="layui-progress layui-progress-big" lay-filter="demo" lay-showpercent="true">
                                    <div class="layui-progress-bar layui-bg-blue" lay-percent="0%">
                                        <span class="layui-progress-text"></span>
                                    </div>
                                </div>
                                <div class="layui-border-box" style="height: 300px;padding: 5px;overflow: auto;">
                                    <ul id="view-log"></ul>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div style="text-align: center;margin-top: 90px;">
                                <i class="layui-icon layui-circle"
                                   style="color: white;font-size:30px;font-weight:bold;background: #52C41A;padding: 20px;line-height: 80px;">&#xe605;</i>
                                <div style="font-size: 24px;color: #333;font-weight: 500;margin-top: 30px;">
                                    安装完成，感谢您的使用！
                                </div>
                                <div id="view-done" style="font-size: 14px;color: #666;margin-top: 20px;">
                                </div>
                            </div>
                            <div style="text-align: center;margin-top: 50px;">
                                <a class="layui-btn next" href="/index.php/Admin/Index">进入后台</a>
                                <a class="layui-btn layui-btn-primary" href="/index.php/Site/Index?siteId=1">查看前台</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-card-body">
                &copy; <a href="https://pkcms.cn" target="_blank">pkcms.cn</a> 版权所有
            </div>
        </div>
    </div>
</div>


<script type="text/html" id="tpl-server">
    <table class="layui-table" id="table-field-model" lay-filter="table-field-model">
        <thead>
        <tr>
            <th>检查项目</th>
            <th>要求配置</th>
            <th>当前配置</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th>操作系统</th>
            <td>windows / linux（推荐 linux）</td>
            <td>
                {{# if(d.isOK_os){ }}
                <i class="layui-icon layui-circle ok">&#xe605;</i>
                {{# }else{ }}
                <i class="layui-icon layui-circle error">&#x1006;</i>
                {{# } }}
                {{ d.OS }}
            </td>
        </tr>
        <tr>
            <th>WEB服务器软件</th>
            <td>IIS / APACHE / NGIUX（推荐 APACHE / NGIUX）</td>
            <td>
                {{# if(d.isOK_os){ }}
                <i class="layui-icon layui-circle ok">&#xe605;</i>
                {{# }else{ }}
                <i class="layui-icon layui-circle error">&#x1006;</i>
                {{# } }}
                {{ d.web_soft }}
            </td>
        </tr>
        <tr>
            <th>数据库服务器</th>
            <td>MySQL 5.5 （及以上）</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <th>PHP 运行库（版本）</th>
            <td>7.2.x ~ 7.4.x</td>
            <td>{{ d.php_version }}</td>
        </tr>
        <tr>
            <th>PHP 必须要打开的扩展</th>
            <td colspan="2">
                <div class="layui-row">
                    <ul>
                        {{#  layui.each(d.php_extension.must, function(index, item){ }}
                        <li class="layui-col-md2" style="margin-bottom: 10px;">
                            {{# if(item.isLoaded){ }}
                            <i class="layui-icon layui-circle ok">&#xe605;</i>
                            {{# }else{ }}
                            <i class="layui-icon layui-circle error">&#x1006;</i>
                            {{# } }}
                            {{ item.name }}
                        </li>
                        {{#  }); }}
                    </ul>
                </div>
            </td>
        </tr>
        <tr>
            <th>
                PHP 可选择性打开的扩展 <br>
                (可在后期有需要用时再打开)
            </th>
            <td colspan="2">
                <div class="layui-row">
                    <ul>
                        {{#  layui.each(d.php_extension.select, function(index, item){ }}
                        <li class="layui-col-md2" style="margin-bottom: 10px;">
                            {{# if(item.isLoaded){ }}
                            <i class="layui-icon layui-circle ok">&#xe605;</i>
                            {{# }else{ }}
                            <i class="layui-icon layui-circle error">&#x1006;</i>
                            {{# } }}
                            {{ item.name }}
                        </li>
                        {{#  }); }}
                    </ul>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</script>

<script type="text/html" id="tpl-done">
    <b style="color: red;">请记录以下的信息！</b><br/>
    超级管理员，用户名： <b>{{ d.userName_admin }}</b> 、密码： <b>{{ d.pwd_admin }}</b> 。<br/>
    开发管理员，用户名： <b>{{ d.userName_site }}</b> 、密码： <b>{{ d.pwd_site }}</b> 。<br/>
    客户管理员，用户名： <b>{{ d.userName_customer }}</b> 、密码： <b>{{ d.pwd_customer }}</b> 。<br/>
</script>

<script type="text/html" id="tpl-log">
    <li>
        完成进度： -&gt;【{{ d.text }}】
    </li>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/install.js"></script>

<script type="text/javascript">
    var data = {
        steps: [
            {
                title: "环境检测"
            },
            {
                title: "配置数据库信息"
            },
            {
                title: "配置站点信息"
            },
            {
                title: "执行数据安装"
            },
            {
                title: "完成安装向导"
            }
        ],
        is_next: true,
        params_api: {
            db:{},
            site:{}
        },
        steps_install: [
            {
                text: '数据库的初始化',
                action: 'ApiByCreateDB'
            },
            {
                text: '数据表的初始化',
                action: 'ApiByCreateTable'
            },
            {
                text: '导入部分数据',
                action: 'ApiByImportData'
            },
            {
                text: '创建第一个站点',
                action: 'ApiByCreateSite'
            },
            {
                text: '创建后台管理员',
                action: 'ApiByCreateAdmin'
            }
        ],
        steps_install_index: 0
    };

    PKInstall.ready(function () {
        PKLayer.useFormByStep(data.steps, '700px', function (form, step, element_speed) {

            function goToInstall() {
                setTimeout(function () {
                    var tmp_length = data.steps_install.length;
                    element_speed.progress('demo', PKCms.Percentage(data.steps_install_index, tmp_length) + '%');
                    var tmp_step = data.steps_install[data.steps_install_index];
                    var api_path = 'Install/' + tmp_step.action;
                    if (tmp_step.action === 'ApiByCreateDB') {
                        api_path = 'ApiDataBase/CreateDB';
                    }
                    PKInstall.api(api_path, data.params_api, function (params_result, count) {
                        PKLayer.useTplPrepend(tmp_step, 'tpl-log', 'view-log');
                        if (typeof params_result == "object") {
                            data.params_api.site = Object.assign(data.params_api.site, params_result);
                        }
                        data.steps_install_index += 1;
                        if (data.steps_install_index < tmp_length) {
                            setTimeout(function () {
                                goToInstall();
                            }, 2000);
                        } else {
                            PKLayer.useTplCall(params_result, 'tpl-done', 'view-done', function (tpl) {
                                step.next('#stepForm');
                            });
                        }
                    });
                }, 1000)
            }

            form.on('submit(formStep)', function (params_form) {
                step.next('#stepForm');
                return false;
            });

            form.on('submit(checkDBExists)', function (params_form) {
                PKInstall.checkDB(params_form.field);
                return false;
            });

            form.on('submit(formStep2)', function (params_form) {
                data.params_api.db = Object.assign(data.params_api.db, params_form.field);
                PKInstall.api('ApiDataBase', data.params_api, function (params_res, count) {
                    step.next('#stepForm');
                });
                return false;
            });

            form.on('submit(formStep3)', function (params_form) {
                step.next('#stepForm');
                data.params_api.site = Object.assign(data.params_api.site, params_form.field);
                goToInstall();
                return false;
            });

            PKInstall.api('ApiServer',{}, function (res_params, count) {
                PKInstall.serviceHandler(res_params);
            });
        });

    });

</script>
</body>
</html>
