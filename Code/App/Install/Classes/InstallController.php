<?php


namespace PKApp\Install\Classes;

use PKFrame\Controller;

abstract class InstallController extends Controller
{

    protected $params_post;

    protected function serviceInstall(): InstallService
    {
        static $service;
        !empty($service) ?: $service = new InstallService();
        return $service;
    }

    protected function checkLink($params_config)
    {
        $this->serviceInstall()->CheckDriverByMySQL($params_config);
    }

    protected function modelByDB()
    {
        $this->params_post = request()->post();
        $this->params_post['db'] = array_merge(
            $this->params_post['db'],
            ['prefix' => 'pk_', 'type' => 'mysql']
        );
        (request()->controller() != 'ApiDataBase') ?: $this->params_post = $this->params_post['db'];
    }

}