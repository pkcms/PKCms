<?php

namespace PKApp\Area\Classes;

trait TraitArea
{

    protected function serviceOfArea(): ServiceArea
    {
        static $cls;
        !empty($cls) ?: $cls = new ServiceArea();
        return $cls;
    }

}