<?php

namespace PKApp\Area\Classes;

use PKFrame\Service;

class ServiceArea extends Service
{

    public function interface_getEntityById($id, $viewField = '*')
    {
    }

    protected function db()
    {
        static $cls;
        !empty($cls) ?: $cls = new DBArea();
        return $cls;
    }

    public function GetList($viewParams = [], $viewField = '*')
    {
        return $this->db()->Where($viewParams)
            ->OrderBy('code', 'ASC')
            ->Select($viewField)->ToList();
    }

    public function GetEntity($viewParams = [], $viewField = '*')
    {
    }
}