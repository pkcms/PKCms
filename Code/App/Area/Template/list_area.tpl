<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/pkcms/icons/iconfont.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
</head>

<body>

<div class="layui-fluid">

    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <fieldset class="layui-elem-field layui-field-title">
                <legend>地区数据</legend>
            </fieldset>
        </div>

        <div class="layui-card-body">
            <table class="layui-hide PK-tableTree" id="table_adminList" lay-filter="table_adminList"
                   data-get="area/AdminGetArea/GetListOfArea"
                   data-set="Content/AdminSetCategory"></table>
        </div>
    </div>
</div>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">
    var data = {
    };

    function renderTable() {
        PKAdmin.tableTree('code', 'parentCode', {
            cols: [[
                {field:'code', title:'邮编'},
                {field:'name', title:'地区名'},
                {field:'phoneCode', title:'电话区号'},
                {field:'level', title:'编辑'},
                {field:'parentName', title:'管辖'},
                {field:'fullName', title:'详细描述'}
            ]],
            initSort: {
                field: 'code',
                type: 'asc'
            },
            even: true
        }, 0);
    }

    PKAdmin.ready(function () {
        renderTable();
    });
</script>
</body>
</html>
