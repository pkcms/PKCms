<?php


namespace PKApp\Links;

use PKApp\Links\Classes\LinksDataBase;
use PKApp\Links\Classes\LinkService;
use PKApp\Links\Classes\CategoryDataBase;
use PKCommon\Controller\AdminGetDetailController;
use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\Numbers;

class AdminGetLinks extends AdminGetDetailController
{

    public function __construct()
    {
        parent::__construct();
        $this->service = new LinkService();
    }

    public function Main()
    {
        $this->assign(array(
            'categoryId' => Numbers::To(request()->get('category')),
        ))->fetch('data_lists');
    }

    public function ApiByLists()
    {
        $categoryId = request()->get('categoryId');
        Numbers::IsId($categoryId) ?: $this->noticeByJson('Links_CategoryID_Empty');
        $list = $this->service->GetListByParentId($this->loginUser()->SiteId, $categoryId);
        $this->json($list, count($list));
    }

    public function ApiBySelect()
    {
        // TODO: Implement ApiBySelect() method.
    }

    public function ApiGetDetail()
    {
        $this->json(
            $this->service->interface_getEntityById(
                request()->get('id')
            )
        );
    }
}