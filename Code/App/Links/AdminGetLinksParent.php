<?php


namespace PKApp\Links;

use PKApp\Links\Classes\CategoryService;
use PKCommon\Controller\AdminGetDetailController;

class AdminGetLinksParent extends AdminGetDetailController
{

    public function __construct()
    {
        parent::__construct();
        $this->service = new CategoryService();
    }

    public function Main()
    {
        $this->fetch('category_lists');
    }

    public function ApiByLists()
    {
        $list = $this->service->GetList();
        $this->json($list, count($list));
    }

    public function ApiBySelect()
    {
        $list = $this->service->GetList();
        $this->json($list, count($list));
    }

    public function ApiGetDetail()
    {
        $this->json(
            $this->service->interface_getEntityById(
                request()->get('id')
            )
        );
    }
}