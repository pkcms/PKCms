<?php


namespace PKApp\Links;

use PKApp\Links\Classes\TraitLInks;
use PKCommon\Controller\AdminController;
use PKFrame\DataHandler\Numbers;

class AdminSetLinksParent extends AdminController
{

    use TraitLInks;

    public function Main()
    {
        $this->callOfSellFunc();
    }

    protected function apiByCreate()
    {
        $post = $this->model();
        unset($post['id']);
        $this->serviceOfLinkCategory()->Add($post);
        $this->inputSuccess();
    }

    protected function apiByChange()
    {
        $post = $this->model();
        Numbers::IsId($post['id']) ?: $this->noticeByJson('Links_CategoryID_Empty');
        $this->serviceOfLinkCategory()->UpdateById($post['id'], $post);
        $this->inputSuccess();
    }

    protected function apiByDel()
    {
        $id = request()->post('id');
        Numbers::IsId($id) ?: $this->noticeByPage('Links_CategoryID_Empty');
        $this->serviceOfLinkCategory()->HideById($id);
        $this->inputSuccess();
    }

    protected function model(): array
    {
        $post = $this->checkModel(['id', 'name']);
        $post['name'] = $this->checkPostFieldIsEmpty('name', 'Links_CategoryName_Empty');
        $post['isDeleted'] = 0;
        return $post;
    }
}