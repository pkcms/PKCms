<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return array(
    'Links_Category_NoExists' => array(
        'zh-cn' => '链接分类不存在',
        'en' => 'Link classification does not exist'
    ),
    'Links_CategoryID_Empty' => array(
        'zh-cn' => '链接分类ID不能为空',
        'en' => 'Link classification ID cannot be empty'
    ),
    'Links_CategoryName_Empty' => array(
        'zh-cn' => '链接分类名称不能为空',
        'en' => 'Link classification ID cannot be empty'
    ),
    'Links_NoExists' => array(
        'zh-cn' => '链接的内容信息不存在',
        'en' => 'The linked content information does not exist'
    ),
    'Links_IdEmpty' => array(
        'zh-cn' => '链接的内容信息ID不能为空',
        'en' => 'The content information ID of the link cannot be empty'
    ),
    'Links_SiteNameEmpty' => array(
        'zh-cn' => '链接站点名称不能为空',
        'en' => 'Link site name cannot be empty'
    ),
    'Links_SiteUrlEmpty' => array(
        'zh-cn' => '链接地址不能为空',
        'en' => 'Link address cannot be empty'
    ),
    'Format_timestamp' => array(
        'zh-cn' => '创建时间的格式不正常',
        'en' => 'The format of the creation time is incorrect'
    ),
);