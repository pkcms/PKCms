<?php


namespace PKApp\Links\Classes;

use PKFrame\Lib\DataBase;

class CategoryDataBase extends DataBase
{

    protected $table = 'links_category';

}