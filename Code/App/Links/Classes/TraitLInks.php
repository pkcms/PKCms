<?php

namespace PKApp\Links\Classes;

trait TraitLInks
{

    protected function serviceOfLinks()
    {
        static $cls;
        !empty($cls) ?: $cls = new LinkService();
        return $cls;
    }

    protected function serviceOfLinkCategory()
    {
        static $cls;
        !empty($cls) ?: $cls = new CategoryService();
        return $cls;
    }

}