<?php


namespace PKApp\Links\Classes;


use PKFrame\Service;

class LinkService extends Service
{

    protected function db()
    {
        return new LinksDataBase();
    }

    public function GetList($viewParams = [], $viewField = '*')
    {
        return $this->db()->GetList($viewParams, $viewField);
    }

    public function GetEntity($viewParams = [], $viewField = '*')
    {
        return $this->db()->Where($viewParams)->Select($viewField)->First();
    }

    public function GetListByParentId($id_site, $id_category)
    {
        return $this->GetList([
            'siteId' => $id_site,
            'categoryId' => $id_category
        ]);
    }

    public function interface_getEntityById($id, $viewField = '*')
    {
        return $this->getEntityById($id,'Links_IdEmpty','Links_NoExists', $viewField);
    }
}