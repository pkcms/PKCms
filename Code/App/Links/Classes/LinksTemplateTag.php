<?php

namespace PKApp\Links\Classes;

use PKApp\Site\Classes\SiteController;
use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\Numbers;

class LinksTemplateTag extends SiteController
{

    public function lists($params_tpl): array
    {
        $id_category = Arrays::GetKey('category', $params_tpl, 'Links_CategoryID_Empty');
        Numbers::IsId($id_category) ?: $this->noticeByPage('Links_CategoryID_Empty');
        $assign = Arrays::GetKey('assign', $params_tpl);
        !empty($assign) ?: $assign = 'linkList';
        $service = new LinkService();
        return [
            $assign => $service->GetListByParentId($this->getSiteEntity()->Id, $id_category),
        ];
    }

    public function Main()
    {
        return [];
    }

}

?>
