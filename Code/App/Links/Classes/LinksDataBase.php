<?php


namespace PKApp\Links\Classes;

use PKFrame\DataHandler\Arrays;
use PKFrame\Lib\DataBase;

class LinksDataBase extends DataBase
{

    protected $table = 'links_site';
    protected $table_category = 'links_category';

    public function GetList($option = [], $lineNum = 0, $index = 0)
    {
        if ($lineNum > 0) {
            $this->Limit($lineNum, $index);
        }
        $this->join($this->table_category, 'categoryId');
        if (Arrays::Is($option)) {
            foreach ($option as $key => $value) {
                $this->WhereJoin($key, $value);
            }
        }
        return $this->OrderBy('listSort')->OrderBy('id')->Select([
            'id', 'categoryId', 'listSort', 'siteName', 'siteLogo', 'siteInfo', 'siteUrl', 'isIndex',
            'createTime', 'categoryName' => [$this->table_category, 'name'],
        ])->ToList();
    }

}