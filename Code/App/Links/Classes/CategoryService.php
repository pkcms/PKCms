<?php


namespace PKApp\Links\Classes;


use PKFrame\Service;

class CategoryService extends Service
{

    protected function db()
    {
        return new CategoryDataBase();
    }

    public function GetList($viewParams = [], $viewField = '*')
    {
        array_key_exists('isDeleted',$viewParams) ?: $viewParams['isDeleted'] = 0;
        return $this->db()->Where($viewParams)->Select()->ToList();
    }

    public function GetEntity($viewParams = [], $viewField = '*')
    {
        array_key_exists('isDeleted',$viewParams) ?: $viewParams['isDeleted'] = 0;
        return $this->db()->Where($viewParams)->Select($viewField)->First();
    }

    public function interface_getEntityById($id, $viewField = '*')
    {
        return $this->getEntityById($id,'Links_CategoryID_Empty','Links_Category_NoExists', $viewField);
    }
}