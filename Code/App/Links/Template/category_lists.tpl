<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
</head>

<body>

<div class="layui-fluid">

    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <fieldset class="layui-elem-field layui-field-title">
                <legend>链接分类管理</legend>
            </fieldset>
        </div>

        <div class="layui-card-body">
            <table class="layui-hide PK-table" id="table_adminList" lay-filter="table_adminList"
                   data-get="links/AdminGetLinksParent" data-set="links/AdminSetLinksParent"></table>
        </div>
    </div>
</div>

<script type="text/html" id="table_toolBar">
    <div class="layui-btn-container">
        <button class="layui-btn layui-btn-sm" lay-event="add|tpl-change">添加</button>
    </div>
</script>

<script type="text/html" id="table_tool">
    <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="edit|tpl-change">
        <i class="layui-icon layui-icon-edit"></i>修改</a>
    <a class="layui-btn layui-btn-primary layui-btn-xs"
       lay-href="/index.php/links/AdminGetLinks/?category={{ d.id }}">
        <i class="layui-icon layui-icon-list"></i>链接列表</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">
        <i class="layui-icon layui-icon-delete"></i>删除</a>
</script>

<script type="text/html" id="tpl-change" data-title="编辑链接的分类">
    <div class="layui-card">
        <div class="layui-card-body">
            <form class="layui-form" lay-filter="form-frame">
                <div class="layui-form-item">
                    <label class="layui-form-label">分类名：</label>
                    <div class="layui-input-block">
                        <input type="text" name="name" class="layui-input" lay-verify="required" autocomplete="off"
                               value="{{#  if(d.name){ }}{{  d.name  }}{{#  } }}"/>
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <div class="layui-footer">
                            <button class="layui-btn" lay-submit lay-filter="form-frame">立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">

    function renderTable() {
        PKAdmin.table({
            cols: [[
                {field:'id', title:'ID'},
                {field:'name', title:'分类名'},
                {
                    title: "操作",
                    align: "center",
                    width: 300,
                    fixed: "right",
                    toolbar: "#table_tool"
                }
            ]]
        });
    }

    PKAdmin.ready(function () {
        renderTable();
    });
</script>
</body>
</html>
