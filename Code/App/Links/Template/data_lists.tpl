<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
</head>

<body>

<div class="layui-fluid">

    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <fieldset class="layui-elem-field layui-field-title">
                <legend>链接列表
        </div>
        </legend>
        </fieldset>
    </div>

    <div class="layui-card-body">
        <table class="layui-hide PK-table" id="table_adminList" lay-filter="table_adminList"
               data-get="links/AdminGetLinks" data-set="links/AdminSetLinks"></table>
    </div>
</div>

<script type="text/html" id="table_toolBar">
    <div class="layui-btn-container">
        <button class="layui-btn layui-btn-sm" lay-tips="添加新的链接信息"
                lay-event="add|tpl-change">
            <i class="layui-icon layui-icon-add-1"></i>添加链接
        </button>
        <button class="layui-btn layui-btn-sm" lay-tips="添加内容链接"
                lay-event="add|tpl-content">
            <i class="layui-icon layui-icon-add-1"></i>添加内容链接
        </button>
    </div>
</script>

<script type="text/html" id="table_tool">
    <a class="layui-btn layui-btn-normal layui-btn-xs"
       lay-event="edit|tpl-change">
        <i class="layui-icon layui-icon-edit"></i>修改</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs"
       lay-event="del">
        <i class="layui-icon layui-icon-delete"></i>删除</a>
</script>

<script type="text/html" id="isIndexTpl">
    {{#  if(d.isIndex == 1){ }}
    <button class="layui-btn layui-btn-xs">是</button>
    {{#  } else { }}
    <button class="layui-btn layui-btn-primary layui-btn-xs">否</button>
    {{#  } }}
</script>

<script type="text/html" id="tpl-change" data-title="编辑链接的内容">
    <div class="layui-card">
        <div class="layui-card-body">
            <form class="layui-form" lay-filter="form-frame">
                <div class="layui-form-item">
                    <label class="layui-form-label">链接名：</label>
                    <div class="layui-input-block">
                        <input type="text" name="siteName" class="layui-input" lay-verify="required" autocomplete="off"
                               value="{{#  if(d.siteName){ }}{{  d.siteName  }}{{#  } }}"/>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">logo地址：</label>
                    <div class="layui-input-block">
                        <input type="text" class="layui-input" style="width: 60%;float: left;"
                               id="siteLogo" name="siteLogo"
                               value="{{#  if(d.siteLogo){ }}{{  d.siteLogo  }}{{#  } }}"/>
                        <button type="button" class="layui-btn pkcms_upload"
                                id="uploadBtn_siteLogo" data-field="siteLogo">
                            <i class="layui-icon layui-icon-upload"></i>选择
                        </button>
                        <button type="button" class="layui-btn" onclick="PreViewImage('siteLogo')">预览
                        </button>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">链接地址：</label>
                    <div class="layui-input-block">
                        <input type="text" name="siteUrl" class="layui-input"
                               value="{{#  if(d.siteUrl){ }}{{  d.siteUrl  }}{{#  } }}"
                               lay-verify="required" autocomplete="off"/>
                        <div class="layui-form-mid layui-word-aux">
                            请输入完整的地址，带上 HTTP 或 HTTPS 协议。
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">链接描述：</label>
                    <div class="layui-input-block">
                <textarea name="siteInfo"
                          class="layui-textarea">{{#  if(d.siteInfo){ }}{{  d.siteInfo  }}{{#  } }}</textarea>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">是否在首页显示：</label>
                    <div class="layui-input-block">
                        <input type="checkbox" lay-skin="switch" id="is_index" lay-filter="is_index"
                               name="isIndex"{{#  if(d.isIndex){ }} value="1" checked{{#  } }}/>
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <div class="layui-footer">
                            <button class="layui-btn" lay-submit lay-filter="form-frame">立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</script>

<script type="text/html" id="tpl-content">

    <div class="layui-fluid">
        <div class="layui-card">
            <div class="layui-card-body">
                <form class="layui-form" lay-filter="form-search">
                    <div class="layui-form-item">
                        <label class="layui-form-label">内容模型</label>
                        <div class="layui-input-inline">
                            <div class="pkcms_select" id="modelId" data-name="modelId"
                                 data-api="Model/AdminGetModel" data-params="modelType=content"></div>
                        </div>
                        <div class="layui-input-inline">
                            <input type="text" name="query" class="layui-input" placeholder="请输入标题的搜索词"/>
                        </div>
                        <div class="layui-input-inline">
                            <button class="layui-btn layui-btn-sm layui-btn-normal searchContent"
                                    onclick="searchContent()"
                                    lay-submit lay-filter="form-search" lay-tips="按模型搜索内容，关键词作模糊搜索">
                                <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                            </button>
                        </div>
                        <div class="layui-form-mid layui-word-aux">
                        </div>
                    </div>
                </form>
                <div id="view-table-content"></div>
            </div>
        </div>
    </div>

</script>

<script type="text/html" id="tpl-table-content">
    <ul>
        {{#  layui.each(d.list, function(index, item){ }}
        <li>
            <input type="radio" class="layui-form-radio" name="select_content" value="{{ item.id }}">
            {{ item.title }}
        </li>
        {{#  }); }}
        {{#  if(d.list.length === 0){ }}
        无数据
        {{#  } }}
    </ul>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">
    var data = {
        init: {
            categoryId: '<{$categoryId}>'
        },
        init_content: {
            query: '',
            modelId: 0,
            recovery: 0
        },
        params_add: {},
    };

    function searchContent() {
        PKLayer.useFormControlByParams(function (params) {
            if (params.modelId === '') {
                PKLayer.tipsMsg('请选择模型！');
                return false;
            }
            var params_api = Object.assign(data.init_content, params, {recovery: 0});
            PKLayer.apiGet('Content/AdminGetContent/ApiByLists', params_api, function (res) {
                PKLayer.useTplCall({list:res}, 'tpl-table-content', 'view-table-content', function () {
                    $('input[name="select_content"]').on('change', function () {
                        var sel_v = $('input[name="select_content"]:checked').val();
                        // console.log('sel_v', sel_v);
                        for (var re of res) {
                            // console.log('re.id', re.id);
                            if (re.id == sel_v) {
                                // console.log('re.id=', re.id);
                                data.params_add = Object.assign(data.init, {
                                    siteName: re.title,
                                    siteLogo: re.thumb,
                                    siteUrl: re.url,
                                    siteInfo: re.seoDescription,
                                    createTime: re.createTime
                                });
                                PKLayer.tipsConfirmByTwoBtn('确定要选择：' + re.title, null,
                                    function () {
                                        PKLayer.apiPost('Links/AdminSetLinks/apiByCreate', data.params_add, function () {
                                            layer.closeAll();
                                            renderTable();
                                        });
                                    }, function () { });
                                break;
                            }
                        }
                    });
                });
            });
            return false;
        });
    }

    function renderTable() {
        PKAdmin.table({
            cols: [[
                {field:'id', title:'ID',width:60},
                {field:'listSort', title:'排序', width:60, edit:true},
                {field:'siteName', title:'网站名称'},
                {field:'siteUrl', title:'网站链接'},
                {field:'categoryName', title:'类别'},
                {field:'createTime', title:'创建时间',templet:"<div>{{dateFormat(d.createTime)}}</div>"},
                {
                    title: "操作",
                    align: "center",
                    width: 250,
                    fixed: "right",
                    toolbar: "#table_tool"
                }
            ]]
        });
    }

    PKAdmin.ready(function () {
        renderTable();
    });
</script>
</body>
</html>
