<?php
/**
 * 模块语言包
 * User: Administrator
 * Date: 2019/5/27
 * Time: 9:28
 */

return [
    'poster_NotExists' => [
        'zh-cn' => '广告位不存在',
        'en' => 'Advertising space does not exist'
    ],
    'posterIdEmpty' => [
        'zh-cn' => '广告位ID不能为空',
        'en' => 'Advertising slot ID cannot be empty'
    ],
    'poster_NameEmpty' => [
        'zh-cn' => '广告位名称不能为空',
        'en' => 'Advertising space name cannot be empty'
    ],
    'poster_TypeEmpty' => [
        'zh-cn' => '广告位的类型不能为空',
        'en' => 'The type of advertising space cannot be empty'
    ],
    'poster_TypeError' => [
        'zh-cn' => '广告位的类型系统找不到该类型的数据，请检查数据的正确性！',
        'en' => 'The Type system of the advertising position cannot find the data of this type, please check the correctness of the data!'
    ],
    'poster_DelError' => [
        'zh-cn' => '您无法删除该广告位！其下有（包含其他站点的）广告内容的存在，请先清空内容后再尝试。',
        'en' => 'You cannot delete this advertising space! There is advertising content under it (including other sites), please clear the content first before trying again.'
    ],
    'image_srcEmpty' => [
        'zh-cn' => '广告的图片地址不能为空',
        'en' => 'The image address of the advertisement cannot be empty'
    ],
    'text_Empty' => [
        'zh-cn' => '广告的文字不能为空',
        'en' => 'The text of the advertisement cannot be empty'
    ],
    'url_Empty' => [
        'zh-cn' => '链接地址不能为空',
        'en' => 'Link address cannot be empty'
    ],
    'timeRange_Empty' => [
        'zh-cn' => '投放时间不能为空',
        'en' => 'The launch time cannot be empty'
    ],
    'timeFormat_Error' => [
        'zh-cn' => '时间格式不正确',
        'en' => 'Incorrect time format'
    ],
    'posterContent_IdEmpty' => [
        'zh-cn' => '广告内容的ID不能为空',
        'en' => 'The ID of advertising content cannot be empty'
    ],
    'posterContent_notExists' => [
        'zh-cn' => '广告内容的ID不能为空',
        'en' => 'The ID of advertising content cannot be empty'
    ],
    'text_fontColorFormat_Error' => [
        'zh-cn' => '文字的（十六进制）颜色代码不对',
        'en' => 'The (hexadecimal) color code of the text is incorrect'
    ],
];