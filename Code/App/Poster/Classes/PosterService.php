<?php


namespace PKApp\Poster\Classes;


use PKFrame\Service;

class PosterService extends Service
{
    protected $moduleName = 'Poster';

    protected function db()
    {
        return new PosterDataBase();
    }

    public function GetList($viewParams = [], $viewField = '*')
    {
        return $this->db()->Where($viewParams)->Select($viewField)->ToList();
    }

    public function GetEntity($viewParams = [], $viewField = '*')
    {
        return $this->db()->Where($viewParams)->Select($viewField)->First();
    }

    public function interface_getEntityById($id, $viewField = '*')
    {
        return $this->getEntityById($id,'posterIdEmpty','poster_NotExists', $viewField);
    }

}