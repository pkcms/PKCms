<?php

namespace PKApp\Poster\Classes;

trait TraitPoster
{

    protected function serviceOfPoster()
    {
        static $cls;
        !empty($cls) ?: $cls = new PosterService();
        return $cls;
    }

    protected function servicePosterOfImage()
    {
        static $cls;
        !empty($cls) ?: $cls = new ImageService();
        return $cls;
    }

    protected function servicePosterOfText()
    {
        static $cls;
        !empty($cls) ?: $cls = new TextService();
        return $cls;
    }

}