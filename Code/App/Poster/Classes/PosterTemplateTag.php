<?php


namespace PKApp\Poster\Classes;


use PKApp\Site\Classes\SiteController;
use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\Numbers;

class PosterTemplateTag extends SiteController
{
    // _id_site 是用户指定
    protected $id_poster, $id_site;

    public function Main()
    {
    }

    protected function params(array $params)
    {
        $this->id_poster = Numbers::To($params['id']);
        Numbers::IsId($this->id_poster) ?: $this->noticeByJson('posterIdEmpty');

    }

    public function Img(array $params): array
    {
        $this->params($params);
        $service_img = new ImageService();
        $list_img = $service_img->GetList([
            'siteId' => array_key_exists('siteId',$params) ? Numbers::To($params['siteId']) : $this->getSiteEntity()->Id,
            'posterId' => $this->id_poster,
        ], ['image_src', 'image_alt', 'image_url']);
        return ['listPosterOfImg' => $list_img];
    }

    public function Text(array $params): array
    {
        $this->params($params);
        $service_text = new TextService();
        $list_text = $service_text->GetList([
            'siteId' => array_key_exists('siteId',$params) ? Numbers::To($params['siteId']) : $this->getSiteEntity()->Id,
            'posterId' => $this->id_poster,
        ], ['text', 'fontColor', 'url']);
        return ['listPosterOfText' => $list_text];
    }

}