<?php


namespace PKApp\Poster\Classes;


use PKFrame\Lib\DataBase;

class TextDataBase extends DataBase
{

    protected $table = 'pk_poster_text';

}