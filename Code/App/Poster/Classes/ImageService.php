<?php


namespace PKApp\Poster\Classes;


use PKFrame\Service;

class ImageService extends Service
{

    public function interface_getEntityById($id, $viewField = '*')
    {
        return $this->getEntityById($id,'posterContent_IdEmpty','posterContent_notExists', $viewField);
    }

    protected function db()
    {
        return new ImageDataBase();
    }

    public function GetList($viewParams = [], $viewField = '*')
    {
        return $this->db()->Where($viewParams)->Select($viewField)->ToList();
    }

    public function GetEntity($viewParams = [], $viewField = '*')
    {
        return $this->db()->Where($viewParams)->Select($viewField)->First();
    }

}