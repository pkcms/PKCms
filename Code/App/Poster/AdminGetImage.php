<?php


namespace PKApp\Poster;

use PKApp\Poster\Classes\ImageService;
use PKCommon\Controller\AdminGetDetailController;
use PKFrame\DataHandler\Date;
use PKFrame\DataHandler\Numbers;

class AdminGetImage extends AdminGetDetailController
{

    public function __construct()
    {
        parent::__construct();
        $this->service = new ImageService();
    }

    public function ApiByLists()
    {
        $options = [
            'siteId' => $this->loginUser()->SiteId,
            'posterId' => request()->get('posterId'),
        ];
        Numbers::IsId($options['posterId']) ?: $this->noticeByJson('posterIdEmpty');
        $this->json(
            $this->service->GetList(
                $options,
                ['id', 'image_alt', 'image_src', 'image_url', 'startTime', 'stopTime', 'createTime', 'listSort']
            )
        );
    }

    public function ApiBySelect()
    {
        // TODO: Implement ApiBySelect() method.
    }

    public function Main()
    {
        $this->assign([
            'posterId'=>Numbers::To(request()->get('id')),
        ])->fetch('image_lists');
    }

    public function ApiGetDetail()
    {
        $params = $this->service->interface_getEntityById(
            request()->get('id')
        );
        $params['timeRange'] = Date::Format('Y-m-d H:i:s', $params['startTime']) . ' - '
            . Date::Format('Y-m-d H:i:s',$params['stopTime']);
        $this->json($params);
    }
}