<?php


namespace PKApp\Poster;

use PKApp\Poster\Classes\PosterService;
use PKCommon\Controller\AdminGetDetailController;

class AdminGetSpace extends AdminGetDetailController
{

    public function __construct()
    {
        parent::__construct();
        $this->service = new PosterService();
    }

    public function Main()
    {
        $this->fetch('space_init');
    }

    public function ApiByLists()
    {
        $list = $this->service->GetList();
        $posterTypeList = dict('PosterType');
        foreach ($list as $index => $item) {
            $item['posterTypeName'] = $posterTypeList[$item['posterType']];
            $list[$index] = $item;
        }
        $this->json($list, count($list));
    }

    public function ApiBySelect()
    {
        $this->json(
            $this->service->GetList()
        );
    }

    public function ApiGetDetail()
    {
        $this->json(
            $this->service->interface_getEntityById(
                request()->get('id')
            )
        );
    }
}