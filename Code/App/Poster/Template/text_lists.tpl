<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
</head>

<body>

<div class="layui-fluid" lay-filter="tpl-element">
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <fieldset class="layui-elem-field layui-field-title" id="view-table">
            </fieldset>
        </div>

        <div class="layui-card-body">
            <table class="layui-hide PK-table" id="table_adminList" lay-filter="table_adminList"
                   data-get="Poster/AdminGetText" data-set="poster/AdminSetText"></table>
        </div>
    </div>
</div>

<script type="text/html" data-id="<{$posterId}>" data-api="Poster/AdminGetSpace" id="tplDetail-table">
    <legend>广告位：{{  d.name  }} 内容列表</legend>
</script>

<script type="text/html" id="table_toolBar">
    <div class="layui-btn-container">
        <button class="layui-btn layui-btn-sm" lay-event="add|tpl-change">添加
        </button>
    </div>
</script>

<script type="text/html" id="table_tool">
    <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="edit|tpl-change">
        <i class="layui-icon layui-icon-edit"></i>修改</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">
        <i class="layui-icon layui-icon-delete"></i>删除</a>
</script>

<script type="text/html" id="tpl-change" data-title="编辑文字广告的信息">
    <div class="layui-card">
        <div class="layui-card-body">
            <form class="layui-form" lay-filter="form-frame">
                <div class="layui-form-item">
                    <label class="layui-form-label">投放时间：</label>
                    <div class="layui-input-block">
                        <input type="text" id="timeRange" name="timeRange" data-range="true"
                               value="{{#  if(d.timeRange){ }}{{  d.timeRange  }}{{#  }else{ }}{{  dateFormat(d.startTime)  }} - {{  dateFormat(d.stopTime)  }}{{#  } }}"
                               class="layui-input pkcms_date" lay-verify="required" autocomplete="off"/>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">文字：</label>
                    <div class="layui-input-block">
                        <input type="text" name="text" class="layui-input" lay-verify="required" autocomplete="off"
                               value="{{#  if(d.text){ }}{{  d.text  }}{{#  } }}"/>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">文字颜色：</label>
                    <div class="layui-input-block">
                        <div class="pkcms_colorpicker" data-id="fontColor" data-name="fontColor"
                             data-value="{{#  if(d.fontColor){ }}{{  d.fontColor  }}{{#  } }}"></div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">链接地址：</label>
                    <div class="layui-input-block">
                        <input type="text" name="url" class="layui-input" lay-verify="required" autocomplete="off"
                               value="{{#  if(d.url){ }}{{  d.url  }}{{#  } }}"/>
                        <div class="layui-form-mid layui-word-aux">
                            请输入完整的地址，带上 HTTP 或 HTTPS 协议。
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <div class="layui-footer">
                            <button class="layui-btn" lay-submit lay-filter="form-frame">立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>
<script type="text/javascript">
    var data = {
        init:{
            posterId:'<{$posterId}>'
        }
    };

    function renderTable() {
        PKAdmin.table({
            cols: [[
                {field:'id', title:'ID', width:60},
                {field:'listSort', title:'排序', width:100, edit:'text'},
                {field:'text', title:'文字'},
                {field:'url', title:'链接地址'},
                {field:'startTime', title:'开始时间',templet:"<div>{{dateFormat(d.startTime)}}</div>"},
                {field:'stopTime', title:'结束时间',templet:"<div>{{dateFormat(d.stopTime)}}</div>"},
                {field:'createTime', title:'创建时间',templet:"<div>{{dateFormat(d.createTime)}}</div>"},
                {
                    title: "操作",
                    align: "center",
                    width: 160,
                    fixed: "right",
                    toolbar: "#table_tool"
                }
            ]],
            initSort: {
                field: 'listSort',
                type: 'asc'
            }
        });
    }

    PKAdmin.ready(function () {
    });
</script>
</body>
</html>
