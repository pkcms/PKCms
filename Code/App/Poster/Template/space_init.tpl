<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
    <link rel="stylesheet" href="/statics/pkcms/icons/iconfont.css" media="all">
</head>

<body>

<div class="layui-fluid">

    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <fieldset class="layui-elem-field layui-field-title">
                <legend>广告位管理</legend>
                <div class="layui-field-box">
                    <p>广告位与广告内容的区分：</p>
                    <p>广告位是多个广告内容的集合，通常用于前台页面某个区域的多个广告，比如：首页横幅、某个栏目的侧边广告等。</p>
                </div>
            </fieldset>
        </div>

        <div class="layui-card-body">
            <table class="layui-hide PK-table" id="table_adminList" lay-filter="table_adminList"
                   data-get="Poster/AdminGetSpace" data-set="poster/adminSetSpace"></table>
        </div>
    </div>
</div>

<script type="text/html" id="table_toolBar">
    <div class="layui-btn-container">
        <button class="layui-btn layui-btn-sm layui-btn-normal" lay-tips="添加新的广告位"
                lay-event="add|tpl-change">
            <i class="layui-icon layui-icon-add-1"></i>
        </button>
    </div>
</script>

<script type="text/html" id="table_tool">
    <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="edit|tpl-change">
        <i class="layui-icon layui-icon-edit"></i>修改</a>
    {{#  if(d.posterType=='image') {  }}
    <a class="layui-btn layui-btn-primary layui-btn-xs"
       lay-href="/index.php/poster/AdminGetImage?id={{ d.id }}">
        <i class="layui-icon layui-icon-list"></i>图片广告列表</a>
    {{#  }  }}
    {{#  if(d.posterType=='text') {  }}
    <a class="layui-btn layui-btn-primary layui-btn-xs"
       lay-href="/index.php/poster/AdminGetText?id={{ d.id }}">
        <i class="layui-icon layui-icon-list"></i>文字广告列表</a>
    {{#  }  }}
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">
        <i class="layui-icon layui-icon-delete"></i>删除</a>
</script>

<script type="text/html" id="tpl-change" data-title="编辑广告位的信息">
    <div class="layui-card">
        <div class="layui-card-body">
            <form class="layui-form" lay-filter="form-frame">
                <div class="layui-form-item">
                    <label class="layui-form-label">广告位名：</label>
                    <div class="layui-input-block">
                        <input type="text" name="name" class="layui-input" lay-verify="required" autocomplete="off"
                               value="{{#  if(d.name){ }}{{  d.name  }}{{#  } }}"/>
                        <div class="layui-form-mid layui-word-aux">
                            可描述为广告在前台页面的某一位置，如：首页横幅、某个栏目的侧边广告等。
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">广告类型：</label>
                    <div class="layui-input-block">
                        <div class="pkcms_select" id="posterType"
                             data-name="posterType" data-value="{{#  if(d.posterType){ }}{{  d.posterType  }}{{#  } }}"
                             data-api="Poster/AdminApi/GetPosterType"></div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <div class="layui-footer">
                            <button class="layui-btn" lay-submit lay-filter="form-frame">立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>

<script type="text/javascript">

    function renderTable() {
        PKAdmin.table({
            cols: [[
                {field:'id', title:'ID'},
                {field:'name', title:'广告位名'},
                {field:'posterTypeName', title:'广告位类型'},
                {
                    title: "操作",
                    align: "center",
                    width: 300,
                    fixed: "right",
                    toolbar: "#table_tool"
                }
            ]]
        });
    }

    PKAdmin.ready(function () {
        renderTable();
    });
</script>
</body>
</html>
