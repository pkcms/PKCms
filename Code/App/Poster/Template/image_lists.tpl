<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <title>系统后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/statics/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
</head>

<body>

<div class="layui-fluid" lay-filter="tpl-element">
    <div class="layui-fluid">

        <div class="layui-card">
            <div class="layui-card-header layuiadmin-card-header-auto">
                <fieldset id="view-table" class="layui-elem-field layui-field-title">
                </fieldset>
            </div>

            <div class="layui-card-body">
                <table class="layui-hide PK-table" id="table_adminList" lay-filter="table_adminList"
                       data-get="Poster/AdminGetImage" data-set="poster/AdminSetImage"></table>
            </div>
        </div>
    </div>
</div>

<script type="text/html" data-id="<{$posterId}>" data-api="Poster/AdminGetSpace" id="tplDetail-table">
    <legend>广告位：{{  d.name  }} 内容列表</legend>
</script>

<script type="text/html" id="table_toolBar">
    <div class="layui-btn-container">
        <button class="layui-btn layui-btn-sm" lay-tips="添加普通广告"
                lay-event="add|tpl-change">
            <i class="layui-icon layui-icon-add-1"></i>
            添加普通广告
        </button>
        <button class="layui-btn layui-btn-sm" lay-tips="添加内容广告"
                lay-event="add|tpl-content">
            <i class="layui-icon layui-icon-add-1"></i>添加内容广告
        </button>
    </div>
</script>

<script type="text/html" id="table_tool">
    <button class="layui-btn layui-btn-normal layui-btn-xs" lay-event="edit|tpl-change">
        <i class="layui-icon layui-icon-edit"></i>修改</button>
    <button class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">
        <i class="layui-icon layui-icon-delete"></i>删除</button>
</script>

<script type="text/html" id="tpl-change" data-title="编辑图片广告的信息">
    <div class="layui-card">
        <div class="layui-card-body">
            <form class="layui-form" lay-filter="form-frame">
                <div class="layui-form-item">
                    <label class="layui-form-label">投放时间：</label>
                    <div class="layui-input-block">
                        <input type="text" id="timeRange" name="timeRange" data-range="true"
                               value="{{#  if(d.timeRange){ }}{{  d.timeRange  }}{{#  }else{ }}{{  dateFormat(d.startTime)  }} - {{  dateFormat(d.stopTime)  }}{{#  } }}"
                               class="layui-input pkcms_date" lay-verify="required" autocomplete="off"/>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">图片描述：</label>
                    <div class="layui-input-block">
                        <input type="text" name="image_alt" class="layui-input" lay-verify="required" autocomplete="off"
                               value="{{#  if(d.image_alt){ }}{{  d.image_alt  }}{{#  } }}" />
                        <div class="layui-form-mid layui-word-aux">
                            请输入对图片的描述，方便日后的管理。
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">链接地址：</label>
                    <div class="layui-input-block">
                        <input type="text" name="image_url" class="layui-input" lay-verify="required" autocomplete="off"
                               value="{{#  if(d.image_url){ }}{{  d.image_url  }}{{#  } }}" />
                        <div class="layui-form-mid layui-word-aux">
                            请输入完整的地址，带上 HTTP 或 HTTPS 协议。
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">图片地址：</label>
                    <div class="layui-input-block">
                        <input type="text" class="layui-input" style="width: 60%;float: left;"
                               id="image_src" name="image_src" value="{{#  if(d.image_src){ }}{{  d.image_src  }}{{#  } }}"/>
                        <button type="button" class="layui-btn pkcms_upload" id="uploadBtn_image_src" data-field="image_src">
                            <i class="layui-icon layui-icon-upload"></i>选择
                        </button>
                        <button type="button" class="layui-btn" onclick="PreViewImage('image_src')">预览
                        </button>
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <div class="layui-footer">
                            <button class="layui-btn" lay-submit lay-filter="form-frame">立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</script>

<script type="text/html" id="tpl-content">

    <div class="layui-fluid">
        <div class="layui-card">
            <div class="layui-card-body">
                <form class="layui-form" lay-filter="form-search">
                    <div class="layui-form-item">
                        <label class="layui-form-label">内容模型</label>
                        <div class="layui-input-inline">
                            <div class="pkcms_select" id="modelId" data-name="modelId"
                                 data-api="Model/AdminGetModel" data-params="modelType=content"></div>
                        </div>
                        <div class="layui-input-inline">
                            <input type="text" name="query" class="layui-input" placeholder="请输入标题的搜索词"/>
                        </div>
                        <div class="layui-input-inline">
                            <button class="layui-btn layui-btn-sm layui-btn-normal searchContent"
                                    onclick="searchContent()"
                                    lay-submit lay-filter="form-search" lay-tips="按模型搜索内容，关键词作模糊搜索">
                                <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                            </button>
                        </div>
                        <div class="layui-form-mid layui-word-aux">
                        </div>
                    </div>
                </form>
                <div id="view-table-content"></div>
            </div>
        </div>
    </div>

</script>

<script type="text/html" id="tpl-table-content">
    <ul>
        {{#  layui.each(d.list, function(index, item){ }}
        <li>
            <input type="radio" class="layui-form-radio" name="select_content" value="{{ item.id }}">
            {{ item.title }}
        </li>
        {{#  }); }}
        {{#  if(d.list.length === 0){ }}
        无数据
        {{#  } }}
    </ul>
</script>

<script type="text/javaScript" src="/statics/layui/layui.js"></script>
<script type="text/javaScript" src="/statics/pkcms/admin.js"></script>
<script type="text/javascript">
    var data = {
        init:{
            posterId:'<{$posterId}>'
        },
        init_content: {
            query: '',
            modelId: 0,
            recovery: 0
        },
        params_add: {}
    };

    function searchContent() {
        PKLayer.useFormControlByParams(function (params) {
            if (params.modelId === '') {
                PKLayer.tipsMsg('请选择模型！');
                return false;
            }
            var params_api = Object.assign(data.init_content, params, {recovery: 0});
            PKLayer.apiGet('Content/AdminGetContent/ApiByLists', params_api, function (res) {
                PKLayer.useTplCall({list:res}, 'tpl-table-content', 'view-table-content', function () {
                    $('input[name="select_content"]').on('change', function () {
                        var sel_v = $('input[name="select_content"]:checked').val();
                        // console.log('sel_v', sel_v);
                        for (var re of res) {
                            // console.log('re.id', re.id);
                            if (re.id == sel_v) {
                                // console.log('re.id=', re.id);
                                data.params_add = Object.assign(data.init, {
                                    image_alt: re.title,
                                    image_src: re.thumb,
                                    image_url: re.url
                                });
                                PKLayer.tipsConfirmByTwoBtn('确定要选择：' + re.title, null,
                                    function () {
                                        PKLayer.apiPost('Poster/AdminSetImage/ApiByCreateOfContent', data.params_add, function () {
                                            layer.closeAll();
                                            renderTable();
                                        });
                                    }, function () { });
                                break;
                            }
                        }
                    });
                });
            });
            return false;
        });
    }

    function renderTable() {
        PKAdmin.table({
            cols: [[
                {field:'id', title:'ID', width:60},
                {field:'listSort', title:'排序', width:100, edit:'text'},
                {field:'image_src', title:'广告图片'},
                {field:'image_alt', title:'图片描述'},
                {field:'startTime', title:'开始时间',templet:"<div>{{dateFormat(d.startTime)}}</div>"},
                {field:'stopTime', title:'结束时间',templet:"<div>{{dateFormat(d.stopTime)}}</div>"},
                {field:'createTime', title:'上传时间',templet:"<div>{{dateFormat(d.createTime)}}</div>"},
                {
                    title: "操作",
                    align: "center",
                    width: 160,
                    fixed: "right",
                    toolbar: "#table_tool"
                }
            ]],
            initSort: {
                field: 'listSort',
                type: 'asc'
            }
        });
    }

    PKAdmin.ready(function () {
        // renderTable();
    });
</script>
</body>
</html>
