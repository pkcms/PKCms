<?php


namespace PKApp\Poster;


use PKApp\Poster\Classes\TextService;
use PKCommon\Controller\AdminGetDetailController;
use PKFrame\DataHandler\Date;
use PKFrame\DataHandler\Numbers;

class AdminGetText extends AdminGetDetailController
{

    public function __construct()
    {
        parent::__construct();
        $this->service = new TextService();
    }

    public function ApiByLists()
    {
        $options = [
            'siteId' => $this->loginUser()->SiteId,
            'posterId' => request()->get('posterId'),
        ];
        Numbers::IsId($options['posterId']) ?: $this->noticeByJson('posterIdEmpty');
        $this->json(
            $this->service->GetList(
                $options,
                ['id', 'text', 'url', 'startTime', 'stopTime', 'createTime', 'listSort']
            )
        );
    }

    public function ApiBySelect()
    {
        // TODO: Implement ApiBySelect() method.
    }

    public function Main()
    {
        $this->assign([
            'posterId'=>Numbers::To(request()->get('id')),
        ])->fetch('text_lists');
    }

    public function ApiGetDetail()
    {
        $params = $this->service->interface_getEntityById(
            request()->get('id')
        );
        $params['timeRange'] = Date::Format('Y-m-d H:i:s', $params['startTime']) . ' - '
            . Date::Format('Y-m-d H:i:s',$params['stopTime']);
        $this->json($params);
    }
}