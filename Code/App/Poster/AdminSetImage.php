<?php

namespace PKApp\Poster;

use PKApp\Poster\Classes\TraitPoster;
use PKCommon\Controller\AdminController;
use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\Date;
use PKFrame\DataHandler\Numbers;

class AdminSetImage extends AdminController
{

    use TraitPoster;

    public function Main()
    {
        $this->callOfSellFunc();
        if (request()->action() == 'ApiByCreateOfContent') {
            $this->apiByCreate();
        }
    }

    protected function model(): array
    {
        switch (request()->action()) {
            case 'ApiByCreate':
            case 'ApiByChange':
            case 'ApiByCreateOfContent':
                $post = $this->checkModel(['posterId', 'image_alt', 'image_src', 'image_url', 'timeRange']);
                $db = [
                    'posterId' => $this->postId('posterId', 'posterIdEmpty'),
                    'image_alt' => Arrays::GetKey('image_alt', $post, 'image_altEmpty'),
                    'image_src' => Arrays::GetKey('image_src', $post, 'image_srcEmpty'),
                    'image_url' => Arrays::GetKey('image_url', $post, 'url_Empty'),
                ];
                if (request()->action() == 'ApiByCreateOfContent') {
                    // 添加内容广告
                    $timeRange = [
                        'startTime' => TIMESTAMP,
                        'stopTime' => strtotime("+10 year"),
                    ];
                } else {
                    $timeRange = Date::TimeRangeToArray(
                        $this->checkPostFieldIsEmpty('timeRange', 'timeRange_Empty')
                    );
                }
                return array_merge($db, $timeRange, [
                    'siteId' => $this->loginUser()->SiteId,
                ]);
            case 'ApiByDel':
                return $this->checkModel(['id', 'posterId']);
            case 'ApiBySortIndex':
                return $this->checkModel(['id', 'sortIndex']);
        }
        return [];
    }

    protected function apiByCreate()
    {
        $this->servicePosterOfImage()->Add(array_merge(
            $this->model(),
            [
                'createTime' => time(),
                'listSort' => 0
            ]
        ));
        $this->_inputSuccess();
    }

    protected function apiByChange()
    {
        $this->servicePosterOfImage()->UpdateById($this->checkPostFieldIsEmpty('id', 'posterContent_IdEmpty'),
            $this->model());
        $this->_inputSuccess();
    }

    protected function apiByDel()
    {
        $post = $this->model();
        $this->servicePosterOfImage()->DeleteById(
            $this->checkPostFieldIsEmpty('id', 'posterContent_IdEmpty')
        );
        $this->_inputSuccess();
    }

    private function _inputSuccess()
    {
        $path_dir = PATH_TMP . dict('cache_dir');
        $list_cache = fileHelper()->OpenFolder($path_dir, 'json');
        if (Arrays::Is($list_cache)) {
            foreach ($list_cache as $item) {
                cache()->Tmp()->RemoveDir($path_dir . DS . $item['file_name']);
            }
        }
        $this->inputSuccess();
    }

    protected function apiBySortIndex()
    {
        $post = $this->model();
        $id = $this->checkPostFieldIsEmpty('id', 'posterIdEmpty');
        $sortIndex = Numbers::To($post['sortIndex']);
        $this->servicePosterOfImage()->UpdateById($id, ['listSort' => $sortIndex]);
        $this->inputSuccess();
    }
}