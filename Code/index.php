<?php

// +----------------------------------------------------------------------
// | Pkcms [ Pk Content Management System ]
// +----------------------------------------------------------------------
// | Copyright (c) 2012-now http://pkcms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: zhangh <zhangh@jishuzai.com>
// +----------------------------------------------------------------------
// Pk Framework 入口文件

define('DS', DIRECTORY_SEPARATOR);
define('ASK_PHP_VER', '7.2.0');
// 判断PHP版本号
if (!version_compare(PHP_VERSION, ASK_PHP_VER, '>')) {
    exit('Your current PHP version (' . PHP_VERSION . ') is less than the minimum requirement (' . ASK_PHP_VER . ')');
}

require_once dirname(__FILE__) . DS . 'AutoLoad.php';

//Session保存路径
if (PHP_OS == 'WINNT') {
    // 解决 windows 下的默认 TEMP 不能读写
    $tmpPath_upload =  @ini_get('upload_tmp_dir');
    if (!is_writeable($tmpPath_upload) || !is_readable($tmpPath_upload)) {
        $tmpPath_upload = PATH_TMP . 'TEMP' . DS;
        file_exists($tmpPath_upload) ?: mkdir($tmpPath_upload);
        @ini_set(
            'upload_tmp_dir',
            $tmpPath_upload
        );
    }
    $tmpPath_session = session_save_path();
    if (!is_writeable($tmpPath_session) || !is_readable($tmpPath_session)) {
        $tmpPath_session = PATH_TMP . "Sessions" . DS;
        file_exists($tmpPath_session) ?: mkdir($tmpPath_session);
        session_save_path($tmpPath_session);
    }
}

eval('run();');
