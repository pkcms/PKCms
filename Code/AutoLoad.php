<?php
/**
 * 自动加载
 * User: Administrator
 * Date: 2019/5/24
 * Time: 9:53
 */


/**
 * @param $class
 */
function autoLoad($class)
{
    $root_name = strstr($class, '\\', true);
//    print_r($root_name);
    if (stristr($root_name, 'PK')) {
        $class = str_replace('\\', DS, ltrim($class, 'PK'));
        switch ($root_name) {
            case 'PKFrame':
            case 'PKCommon':
                loader(PATH_PK . $class);
                break;
            case 'PKApp':
                $class = substr($class, strlen('App' . DS));
                loader(PATH_APP . $class);
                break;
            case 'PKExtApp':
                $class = substr($class, strlen('ExtApp' . DS));
                loader(PATH_ExtAPP . $class);
                break;
        }
    }
}

require_once dirname(__FILE__) . DS . 'Config.php';
spl_autoload_register('autoLoad');
require_once PATH_Frame . 'Helper.php';
loader(PATH_PK . 'PlugIn' . DS . 'vendor' . DS . 'autoload.php');
loader(PATH_PK . 'Common' . DS . 'Config.php');
