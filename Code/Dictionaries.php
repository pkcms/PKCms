<?php
defined('PATH_PK') or die();
// 系统字典
return [
    'logFile_blackList' => 'blacklist_ip.log',
    'Params_Field' => [
        'MySQL' => [
            'prefix' => '数据表的前缀',
            'host' => 'MySQL数据库服务器地址',
            'name' => 'MySQL数据库名',
            'user' => 'MySQL数据库访问用户名',
            'pass' => 'MySQL数据库访问密码',
            'port' => 'MySQL数据库访问端口',
        ],
        'SQLite' => [
            'file' => 'SQLite数据库文件'
        ],
    ],
];
