<?php


namespace PKFrame\DataHandler;
defined('PATH_PK') or die();

class Pages
{

    /**
     * URL 格式的检查
     * @param string $url 文件在网络的链接地址
     * @return boolean
     */
    public static function IsUrl(string $url = '')
    {
        $url = trim($url);
        if (empty($url)) {
            return false;
        }
        $url_parts = parse_url($url);
        $scheme = $url_parts['scheme'];
        $host = $url_parts['host'];
        $path = $url_parts['path'];
        $port = !empty($url_parts['port']) ? ':' . $url_parts['port'] : '';
        return (!empty($scheme) ? $scheme . '://' . $host : (!empty($host) ? 'http://' . $host : 'http://' . $path)) . $port . '/';
    }

    /**
     * 从文章正文中提取图片路径
     * @param string $html_code
     * @return bool|mixed
     */
    public static function GetImgsInArticle($html_code = '')
    {
        $temp = [];
        preg_match_all("/(src|SRC)=[\"|'| ]{0,}((.*).(gif|jpg|jpeg|bmp|png))/isU", $html_code, $temp);
        if (is_array($temp[0]) == FALSE) {
            return FALSE;
        }
        foreach ($temp[0] as $key => $value) {
            $temp[0][$key] = substr($value, 5);
        }
        return $temp[0];
    }

    /**
     * 对 HTML 代码进行压缩
     * @param string $data 要处理的内容信息
     * @return string
     */
    public static function Zip(string $data): string
    {
        if (strlen($data) > 2) {
            $data = preg_replace("/[\r|\n|\t|\r\n]/", "", $data);
            $data = preg_replace("/\/\/[\S\f\t\v ]*?;[\r|\n]/", "", $data); // js注释请以";"号结尾
            // $data=preg_replace("/\<\!\-\-[\s\S]*?\-\-\>/","",$data);//去掉html里的<!--注释-->
            $data = preg_replace("/\>[\s]+\</", "><", $data);
            $data = preg_replace("/;[\s]+/", ";", $data);
            $data = preg_replace("/[\s]+\}/", "}", $data);
            $data = preg_replace("/}[\s]+/", "}", $data);
            $data = preg_replace("/\{[\s]+/", "{", $data);
            $data = preg_replace("/([\s]){2,}/", "$1", $data);
            $data = preg_replace("/[\s]+\=[\s]+/", "=", $data);
            $data = str_replace('	', '', $data);
            return $data;
        } else {
            return "";
        }
    }

    /**
     * 分页的处理
     * @param int $data_count 总数据量
     * @param int $line_number 返回列的行数
     * @param int $page_code_index 当前的页码
     * @param string $link 翻页
     * @param false $isClosePageByFirst
     * @return array
     */
    public static function PageNavs(int $data_count = 0, int $line_number = 0, int $page_code_index = 0,
                                    string $link = '', bool $isClosePageByFirst = false): array
    {
        $page_code_index = max($page_code_index, 1);
        // 基本页码总数
        $base_page_size = 5;
        //最后页，也是总页数
        $page_count = ceil($data_count / $line_number);
        //上一页
        $pre_page_code = max($page_code_index - 1, 1);
        // 下一页
        $next_page_code = $page_code_index + 1;
        // 判断下一页是否为最后一页
        $next_page_code <= $page_count ?: $next_page_code = $page_count;
        // 产生页码区间
        // 产生最小和最大的值
        $min = max($page_code_index - 2, 1);
        $max = $page_code_index + 2;
        if ($page_count <= $base_page_size) {
            // 要显示页数 小等于 要求基本总数
            $new_min = $page_count - $base_page_size + 1;
            $min = $new_min < 1 ? $min : min($new_min, $min);
            $max = $page_count;
        } else {
            // 要显示页数 大于 要求基本总数
            $max <= $page_count ?: $max = min($max, $page_count);
        }
        $page_section = range($min, $max);
        // 产生 HTML
        if ($page_count == 0) return [];
        $result = array(
            'index' => $page_code_index,
            'pageCount' => $page_count,
            'dataCount' => $data_count,
            'start' => self::_replaceStartPageCode(1, $link, $isClosePageByFirst . true),
            'pre' => self::_replaceStartPageCode($pre_page_code, $link, $isClosePageByFirst)
        );
        foreach ($page_section as $number) {
            $result['sizeList'][$number] = self::_replaceStartPageCode($number, $link, $isClosePageByFirst);
        }
        $result['next'] = self::_replaceStartPageCode($next_page_code, $link, $isClosePageByFirst);
        $result['end'] = self::_replaceStartPageCode($page_count, $link, $isClosePageByFirst);
        return $result;
    }

    private static function _replaceStartPageCode(int $pageCode, string $link, bool $isClosePageByFirst = false, bool $isHome = false)
    {
        $pregStr = '(_\[page\]|-\[page\]|\/\[page\]|\[page\])';
        preg_match($pregStr, $link, $preg);
        if (count($preg) == 0) {
            return $link;
        }
        $parts = preg_split('#\?#i', $link, 2);
        if (count($parts) == 2) {
            return str_replace('[page]', $pageCode, $link);
        }
        if ($isHome || ($pageCode == 1 && $isClosePageByFirst)) {
            return preg_replace($pregStr, '', $link);
        } else {
            return str_replace('[page]', $pageCode, $link);
        }
    }

}
