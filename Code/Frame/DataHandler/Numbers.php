<?php


namespace PKFrame\DataHandler;
defined('PATH_PK') or die();


class Numbers
{
    /**
     * 产生随机数字字符串
     * @param int $length 输出长度
     * @param string $chars 可选的 ，默认为 0123456789
     * @return   string     字符串
     */
    public static function Random(int $length, string $chars = '0123456789'): string
    {
        $hash = '';
        $max = strlen($chars) - 1;
        for ($i = 0; $i < $length; $i++) {
            $hash .= $chars[mt_rand(0, $max)];
        }
        return $hash;
    }

    /**
     * 检查变量是否为整形
     * @param $expression -要检查的变量
     * @return int|string
     */
    public static function Is($expression)
    {
        return !\is_numeric($expression) ? 0 : $expression;
    }

    public static function IsId($expression): bool
    {
        return is_numeric($expression) && $expression > 0;
    }

    public static function IsIds(array &$list_id, string $tips, string $app = null)
    {
        foreach ($list_id as $index => $item_id) {
            if (!self::IsId($item_id)) {
                unset($list_id[$index]);
            }
        }
        Arrays::Is($list_id) ?: out()->noticeByLJson($tips, $app);
    }

    /**
     * 检查变量是否为整形
     * @param $expression -要检查的变量
     * @return int|string
     */
    public static function To($expression)
    {
        return !\is_numeric($expression) ? 0 : $expression;
    }

    /**
     * 检查变量是否为浮点型
     * @param float $expression 要检查的变量
     * @return boolean
     */
    public static function IsFloat($expression)
    {
        $expression = (\is_float($expression) || \is_numeric($expression)) ? \number_format($expression, 2) : number_format(0, 2);
        return str_replace(',', '', $expression);
    }

}