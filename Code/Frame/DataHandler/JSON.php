<?php


namespace PKFrame\DataHandler;
defined('PATH_PK') or die();


class JSON
{

    /**
     * JSON 格式压缩，针对 版本5.6
     * @param array $data
     * @param bool $isNumber
     * @return false|string
     */
    public static function EnCode(array $data = [], bool $isNumber = true)
    {
        return json_encode($data, $isNumber ? JSON_NUMERIC_CHECK | JSON_UNESCAPED_UNICODE : JSON_UNESCAPED_UNICODE);
    }

    /**
     * 检查是否为 JSON 格式
     * @param null $vars
     * @return bool
     */
    public static function Is($vars = null)
    {
        if (Arrays::Is($vars) || empty($vars)) {
            return false;
        }
        $json_data = json_decode($vars);
        return json_last_error() == JSON_ERROR_NONE ? $json_data : false;
    }
}
