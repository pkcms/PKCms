<?php


namespace PKFrame\DataHandler;
defined('PATH_PK') or die();


class Code
{

    /**
     * 将 HTML 转换成 ASCII 码
     * @param string $html_code HTML 代码
     * @return mixed|string
     */
    public static function ToASCII($html_code = '')
    {
        if ($html_code != "") {
            $html_code = str_replace(">", ">", $html_code);
            $html_code = str_replace("<", "<", $html_code);
            $html_code = str_replace(" ", chr(32), $html_code);
            $html_code = str_replace("", chr(13), $html_code);
            $html_code = str_replace("<br>", chr(10) & chr(10), $html_code);
            $html_code = str_replace("<BR>", chr(10), $html_code);
        }
        return $html_code;
    }

    /**
     * UTF-8 编码转换成中文字符
     * @param string $utf8_str UTF-8 编码
     * @return mixed
     */
    public static function Utf8ToChinese($utf8_str = '')
    {
        return preg_replace('/[\x{10000}-\x{10FFFF}]/u', '', $utf8_str);
    }

    /**
     * utf8字符转换成Unicode字符
     * @param string $utf8_str Utf-8字符
     * @return string Unicode字符
     */
    public static function Utf8ToUnicode($utf8_str = '')
    {
        $unicode = (\ord($utf8_str[0]) & 0x1F) << 12;
        $unicode |= (\ord($utf8_str[1]) & 0x3F) << 6;
        $unicode |= (\ord($utf8_str[2]) & 0x3F);
        return \dechex($unicode);
    }

    /**
     * 以下方法可以将Unicode编码的中文转换成utf8编码的中文，且对原来就是utf8编码的中文没影响：
     * @param $str
     * @return null|string|string[]
     */
    public static function DecodeUnicode($str)
    {
        return preg_replace_callback('/\\\\u([0-9a-f]{4})/i', \create_function('$matches', 'return iconv("UCS-2BE","UTF-8",pack("H*", $matches[1]));'), $str);
    }

    /**
     * @param $str -原始中文字符串
     * @param string $encoding 原始字符串的编码，默认GBK
     * @param string $prefix 编码后的前缀，默认"&#"
     * @param string $postfix 编码后的后缀，默认";"
     * @return string
     */
    public static function UnicodeEncode($str, $encoding = 'GBK', $prefix = '&#', $postfix = ';')
    {
        $str = iconv($encoding, 'UCS-2', $str);
        $arrstr = str_split($str, 2);
        $unistr = '';
        for ($i = 0, $len = count($arrstr); $i < $len; $i++) {
            $dec = hexdec(bin2hex($arrstr[$i]));
            $unistr .= $prefix . $dec . $postfix;
        }
        return $unistr;
    }

    /**
     * @param string $unistr Unicode编码后的字符串
     * @param string $encoding 原始字符串的编码，默认GBK
     * @param string $prefix 编码字符串的前缀，默认"&#"
     * @param string $postfix 编码字符串的后缀，默认";"
     * @return string
     */
    public static function UnicodeDecode($unistr, $encoding = 'GBK', $prefix = '&#', $postfix = ';')
    {
        $arruni = explode($prefix, $unistr);
        $unistr = '';
        for ($i = 1, $len = count($arruni); $i < $len; $i++) {
            if (strlen($postfix) > 0) {
                $arruni[$i] = substr($arruni[$i], 0, strlen($arruni[$i]) - strlen($postfix));
            }
            $temp = intval($arruni[$i]);
            $unistr .= ($temp < 256) ? chr(0) . chr($temp) : chr($temp / 256) . chr($temp % 256);
        }
        return iconv('UCS-2', $encoding, $unistr);
    }

    /**
     * Unicode字符转换成utf8字符
     * @param String $unicode_str
     * @return string
     */
    public static function UnicodeToUtf8($unicode_str = '')
    {
        $code = \intval(\hexdec($unicode_str));
        //这里注意转换出来的code一定得是整形，这样才会正确的按位操作
        $ord_1 = \decbin(0xe0 | ($code >> 12));
        $ord_2 = \decbin(0x80 | (($code >> 6) & 0x3f));
        $ord_3 = \decbin(0x80 | ($code & 0x3f));
        $utf8_str = \chr(\bindec($ord_1)) . \chr(\bindec($ord_2)) . \chr(\bindec($ord_3));
        return $utf8_str;
    }

    /**
     * 自动判断把gbk或gb2312编码的字符串转为utf8
     * 能自动判断输入字符串的编码类，如果本身是utf-8就不用转换，否则就转换为utf-8的字符串
     * 支持的字符编码类型是：utf-8,gbk,gb2312
     * @param string $string - 被转换字符串
     * @return string - 输出后的 utf-8 字符串
     */
    public static function GbkToUtf8($string = '')
    {
        $charset = \mb_detect_encoding($string, array('UTF-8', 'GBK', 'GB2312'));
        $charset = \strtolower($charset);
        if ('cp936' == $charset) {
            $charset = 'GBK';
        }
        if ('utf-8' != $charset) {
            $string = \iconv($charset, 'UTF-8//IGNORE', $string);
        }
        return $string;
    }

}