<?php


namespace PKFrame\DataHandler;
defined('PATH_PK') or die();


class Date
{

    public static function isTimeStamp(int $timestamp, $msgStr, $module = '', $tips = '', $stateCode = 500)
    {
        if (strtotime(date('Y-m-d H:i:s', $timestamp)) != $timestamp) {
            out()->noticeByLJson($msgStr, $module, $tips, $stateCode);
        }
    }

    /**
     * 返回固定的日期时间格式
     * @param string $format 想要的格式
     * @param mixed $time 被处理的时间字符
     * @return false|string
     */
    public static function Format($format = 'Y-m-d H:i:s', $time = SYS_TIME)
    {
        return date($format, is_numeric($time) ? $time : strtotime($time));
    }

    /**
     * 时间戳转日期格式（精确到毫秒，x代表毫秒）
     * @param $time
     * @return mixed
     */
    public static function TimeStampByMillisecondToDate($time)
    {
        if (strstr($time, '.')) {
            sprintf("%01.3f", $time); //小数点。不足三位补0
            list($usec, $sec) = explode(".", $time);
            $sec = str_pad($sec, 3, "0", STR_PAD_RIGHT); //不足3位。右边补0
        } else {
            $usec = $time;
            $sec = "000";
        }
        $date = date("Y-m-d H:i:s.x", $usec);
        return str_replace('x', $sec, $date);
    }

    /**
     * 时间日期转时间戳格式（精确到毫秒）
     * @param $time
     * @return string
     */
    public static function DateTimeToTimeStampByMillisecond($time): string
    {
        list($usec, $sec) = explode(".", $time);
        $date = strtotime($usec);
        //不足13位。右边补0
        return str_pad($date . $sec, 13, "0", STR_PAD_RIGHT);
    }

    /**
     * 获取当前时间戳,精确到毫秒
     * @return float
     */
    public static function getMicrotimeFloat(): float
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }

    /**
     * 返回本周的开始日期到结束日期
     * @return array
     */
    public static function GetWeekDates(): array
    {
        //当前日期
        $default_date = date("Y-m-d");
        //$first =1 表示每周星期一为开始日期 0表示每周日为开始日期
        $first = 1;
        //获取当前周的第几天 周日是 0 周一到周六是 1 - 6
        $w = date('w', strtotime($default_date));
        //获取本周开始日期，如果$w是0，则表示周日，减去 6 天
        $week_start = date('Y-m-d 00:00:00', strtotime("$default_date -" . ($w ? $w - $first : 6) . ' days'));
        //本周结束日期
        $week_end = date('Y-m-d 23:59:59', strtotime("$week_start +6 days"));
        return array($week_start, $week_end);
    }

    /*
     * 返回本月的开始日期到结束日期
     * @return array
     */
    public static function GetMonthDates(): array
    {
        $month_start = date('Y-m-01 00:00:00', TIMESTAMP);
        $month_end = date('Y-m-d 23:59:59', strtotime("$month_start +1 month -1 day"));
        return array($month_start, $month_end);
    }

    public static function UNIXToUTC(int $unix)
    {
        return date('Y-m-d\TH:i:s.000\Z', $unix);
    }

    public static function UTCToDateTime(string $utc)
    {
        return str_replace(array('T', 'Z'), ' ', $utc);
    }

    public static function UTCToUNIX(string $utc)
    {
        return strtotime(self::UTCToDateTime($utc));
    }

    public static function DateTimeToUTC(string $dateTime)
    {
        return self::UNIXToUTC(strtotime($dateTime));
    }

    public static function TimeRangeToArray(string $timeRange): array
    {
        $timeRange = explode(' - ', $timeRange);
        if (Arrays::Is($timeRange) == 2) {
            foreach ($timeRange as $item) {
                MatchHelper::MatchFormat('dateTime', $item, 'timeFormat_Error');
            }
        } else {
            out()->noticeByLJson('timeRange_Empty');
        }
        return [
            'startTime' => strtotime($timeRange[0]),
            'stopTime' => strtotime($timeRange[1]),
        ];
    }

}