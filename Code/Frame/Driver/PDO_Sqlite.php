<?php


namespace PKFrame\Driver;
defined('PATH_PK') or die();

use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\JSON;

class PDO_Sqlite
{

    private $_path;

    public function __construct($path)
    {
        try {
            if (!extension_loaded('pdo_sqlite')) {
                throw new \Exception(language('PHPExtension_PDOSqlite_NoExists'));
            }
            $this->_path = $path;
        } catch (\Exception $e) {
            $this->_notice($e);
        }
    }

    private function _notice(\Exception $exception)
    {
        out()->noticeByJson($exception->getMessage());
    }

    private function _conn(): \PDO
    {
        try {
            if (!file_exists($this->_path)) {
                throw new \Exception(language('DataBase_SqliteFile_NoExists') . $path);
            }
        } catch (\Exception $exception) {
            $this->_notice($exception);
        }
        $conn = new \PDO('sqlite:' . $this->_path);
        $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        return $conn;
    }

    private function _errorLog(\PDO $conn, $sqlStr)
    {
        logger()->LOGS(JSON::EnCode($conn->errorInfo()), array('sql' => $sqlStr, 'ErrorCode' => $conn->errorCode()));
    }

    public function sqlQuery($sqlStr)
    {
        !empty($sqlStr) ?: out()->noticeByJson('do sql is empty');
        $conn = $this->_conn();
        try {
            $conn->exec($sqlStr);
        } catch (\PDOException $exception) {
            $this->_errorLog($conn, $sqlStr);
            $this->_notice($exception);
        }
    }

    public function fetchAssoc($sqlStr, $toList = false)
    {
        !empty($sqlStr) ?: out()->noticeByJson('do sql is empty');
        $result = '';
        $conn = $this->_conn();
        try {
            if ($toList) {
                $sth = $conn->query($sqlStr);
                $result = $sth->fetchAll(\PDO::FETCH_ASSOC);
            } else {
                $sth = $conn->query($sqlStr);
                $result = $sth->fetch(\PDO::FETCH_ASSOC);
            }
        } catch (\PDOException $exception) {
            $this->_errorLog($conn, $sqlStr);
            $this->_notice($exception);
        }
        return $result;
    }

    public function insert($sqlStr)
    {
        !empty($sqlStr) ?: out()->noticeByJson('do sql is empty');
        $conn = $this->_conn();
        try {
            if (Arrays::Is($sqlStr)) {
                $conn->exec(implode(';', $sqlStr));
            }
        } catch (\PDOException $exception) {
            $this->_errorLog($conn, $sqlStr);
            $this->_notice($exception);
        }
    }

    public function insertId($sqlStr)
    {
        !empty($sqlStr) ?: out()->noticeByJson('do sql is empty');
        $id = 0;
        $conn = $this->_conn();
        try {
            $conn->exec($sqlStr);
            $id = $conn->lastInsertId();
        } catch (\PDOException $exception) {
            $this->_errorLog($conn, $sqlStr);
            $this->_notice($exception);
        }
        return $id;
    }
}
