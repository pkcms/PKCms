<?php


namespace PKFrame;
defined('PATH_PK') or die();


use PKFrame\DataHandler\Arrays;

class Base
{

    final protected function noticeByJson($str, $modules = '', $tips = '', $stateCode = 50000)
    {
        $modules = !empty($modules) ? ucfirst($modules) : request()->module();
        out()->noticeByLJson($str, $modules, $tips, $stateCode);
    }

    final protected function noticeByPage($str, $goto = null, $modules = '')
    {
        $modules = !empty($modules) ? ucfirst($modules) : request()->module();
        out()->notice(language($str, $modules), $goto);
    }

    protected function getDictToArray($key): array
    {
        $array = dict($key);
        $result = [];
        if (Arrays::Is($array)) {
            foreach ($array as $key => $item) {
                $result[] = array('id' => $key, 'name' => $item);
            }
        }
        return $result;
    }

    protected function arrayToIdAndName($array, $fieldById = '', $fieldByName = ''): array
    {
        $result = [];
        if (Arrays::Is($array)) {
            foreach ($array as $item) {
                $result[] = [
                    'id' => empty($fieldById) ? $item : $item[$fieldById],
                    'name' => empty($fieldByName) ? $item : $item[$fieldByName],
                ];
            }
        }
        return $result;
    }

}