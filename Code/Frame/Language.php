<?php
defined('PATH_PK') or die();
/**
 * 底层语言包
 * User: Administrator
 * Date: 2019/5/24
 * Time: 9:08
 */

return [
    'Is_Empty' => [
        'zh-cn' => '是空的',
        'en' => 'is empty',
    ],
    'path_notExists' => [
        'zh-cn' => '该路径不存在，请检查磁盘物理路径：',
        'en' => 'The path does not exist. Please check the physical path of the disk:',
    ],
    'PostJSON_isEmpty' => [
        'zh-cn' => '没有接收请求参数',
        'en' => 'Request parameters not received',
    ],
    'MatchFormat_DataEmpty' => [
        'zh-cn' => '被校验数据格式的数据为空，或不是字符类型！',
        'en' => 'Request parameters not received',
    ],
    'MatchFormat_TypeNoExists' => [
        'zh-cn' => '本系统不支持该数据格式的校验，您输入的数据格式校验类型为：',
        'en' => 'This system does not support verification of this data format. The data format verification type you entered is:',
    ],
    'Date_StartStop_Error' => [
        'zh-cn' => '检索中的开始时间与结束时间的先后错误',
        'en' => 'Wrong sequence of start time and end time in retrieval',
    ],
    'DataBase_ConfigEmpty' => [
        'zh-cn' => '数据库的配置信息为空，或者是数组类型的配置信息',
        'en' => 'The configuration information of the database is empty or array type configuration information',
    ],
    'DataBase_Connect_Error' => [
        'zh-cn' => '数据库的连接失败！请检查一下数据库的访问地址、端口、用户名和密码是否填写正确，或者远程访问被限制了。',
        'en' => 'Database connection failed! Please check if the access address, port, username, and password of the database are filled in correctly, or if remote access is restricted.',
    ],
    'DataBase_Config_IndexEmpty' => [
        'zh-cn' => '数据库的配置信息的索引为空',
        'en' => "The index of the database's configuration information is empty",
    ],
    'Redis_ConfigEmpty' => [
        'zh-cn' => 'Redis 的配置信息为空',
        'en' => "Redis' configuration information is empty",
    ],
    'PHPExtension_FileInfo_NoExists' => [
        'zh-cn' => '当前服务器的 PHP 运行库（版本）中的 php_fileinfo 扩展没有开启，请联系服务器商开启，否则无法使用与该扩展有关的功能模块。',
        'en' => "PHP in the current server's PHP runtime (version)_ The fileinfo extension is not enabled. Please contact the server vendor to enable it, otherwise the functional modules related to this extension cannot be used.",
    ],
    'PHPExtension_MBString_NoExists' => [
        'zh-cn' => '当前服务器的 PHP 运行库（版本）中的 mbstring 扩展没有开启，请联系服务器商开启，否则无法使用与该扩展有关的功能模块。',
        'en' => 'The mbstring extension in the PHP runtime (version) of the current server is not enabled. Please contact the server vendor to enable it, otherwise the functional modules related to this extension cannot be used.',
    ],
    'PHPExtension_Sqlite_NoExists' => [
        'zh-cn' => '当前服务器的 PHP 运行库（版本）中的 Sqlite 扩展没有开启，请联系服务器商开启，否则无法使用与该扩展有关的功能模块。',
        'en' => 'The Sqlite extension in the PHP runtime (version) of the current server is not enabled. Please contact the server vendor to enable it, otherwise the functional modules related to this extension cannot be used.',
    ],
    'PHPExtension_PDOSqlite_NoExists' => [
        'zh-cn' => '当前服务器的 PHP 运行库（版本）中的 pdo_sqlite 扩展没有开启，请联系服务器商开启，否则无法使用与该扩展有关的功能模块。',
        'en' => 'PDO in the PHP runtime (version) of the current server_ The sqlite extension is not enabled. Please contact the server vendor to enable it, otherwise the functional modules related to this extension cannot be used.',
    ],
    'PHPExtension_zip_NoExists' => [
        'zh-cn' => '当前服务器的 PHP 运行库（版本）中的 zip 扩展没有开启，请联系服务器商开启，否则无法使用与该扩展有关的功能模块。',
        'en' => 'The zip extension in the PHP runtime (version) of the current server is not enabled. Please contact the server vendor to enable it, otherwise the functional modules related to this extension cannot be used.',
    ],
    'PHPSetting_allow_url_fopen_NoExists' => [
        'zh-cn' => '当前服务器的 PHP 运行库（版本）中的 allow_url_fopen 配置没有开启，无法读取远程文件，请联系服务器商开启，否则无法使用与该扩展有关的功能模块。',
        'en' => 'Allow in the PHP runtime (version) of the current server_ URL_ The fopen configuration is not enabled and cannot read remote files. Please contact the server vendor to enable it, otherwise the functional modules related to this extension cannot be used.',
    ],
    'DataBase_Prefix_Empty' => [
        'zh-cn' => '数据库配置信息中的数据表名的前缀参数不存在，或者为空',
        'en' => 'The prefix parameter for the data table name in the database configuration information does not exist or is empty',
    ],
    'DataBase_SqliteFile_NoExists' => [
        'zh-cn' => '当前项目中的 Sqlite 数据库文件找不到或者不存在，请将数据库文件放在：',
        'en' => 'The Sqlite database file in the current project cannot be found or does not exist. Please place the database file in:',
    ],
    'DataBase_Sql_NumError' => [
        'zh-cn' => 'SQL 执行出错',
        'en' => 'SQL execution error',
    ],
    'SystemFile_IncludeError' => [
        'zh-cn' => '系统在引用时找不到文件路径，路径是：',
        'en' => 'The system cannot find the file path when referencing, the path is:',
    ],
    'TempLate_File_NoExists' => [
        'zh-cn' => '模板文件找不到或不存在，路径是：',
        'en' => 'The template file cannot be found or does not exist. The path is:',
    ],
    'Template_TagModule_NoExists' => [
        'zh-cn' => '模板的模块标签找不到或不存在，模块标签名是：',
        'en' => 'The module label of the template cannot be found or does not exist. The module label name is:',
    ],
    'Template_TagFunc_Error' => [
        'zh-cn' => '模板函数返回的数据异常，模块名：[module]，操作名：[action]',
        'en' => 'The template function returned abnormal data, module name: [module], operation name: [action]',
    ],
    'Template_TagFunc_NoExists' => [
        'zh-cn' => '模板的操作标签找不到或不存在，操作标签名是：',
        'en' => 'The action label for the template cannot be found or does not exist. The action label name is:',
    ],
    'Data_Input_Success' => [
        'zh-cn' => '数据入库成功',
        'en' => 'Data entry successful',
    ],
    'Module_InstallOk' => [
        'zh-cn' => '扩展模块安装成功',
        'en' => 'Successfully installed the expansion module',
    ],
    'App_Error' => [
        'zh-cn' => '没有正确安装，请重新安装',
        'en' => 'Not installed correctly, please reinstall'
    ],
];
