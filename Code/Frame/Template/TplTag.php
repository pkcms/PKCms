<?php
defined('PATH_PK') or die();
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/5/25
 * Time: 11:56
 */

/**
 * 引入
 * @param $file
 * @param null $dir
 * @param string $theme
 * @return false|string
 */
function tplTag_Template($file, $dir = NULL, $theme = '')
{
    return out()->tpl(strtolower($file))->SetTplThemes($theme)->SetTplDir($dir)->TplDisplay($file);
}

function htmlDeCode(string $html) {
    return \PKFrame\DataHandler\Auth::HtmlspecialcharsDeCode($html);
}

function thumb($image, $width = 135, $height = 135, $narrow = 0)
{
    $thumb = new \PKApp\Attachment\Classes\Thumb();
    return $thumb->Main($image, $width, $height, $narrow);
}

function GuestMsg($isFloat = true)
{
    // 查询IP
    $apiUrl_ip = '/index.php/GuestMsg/PublicAjax';
    // 初始正文
    $style = \request()->param('siteContact_guestmsgstyle');
    $site_id = \request()->param('site_id');
    $tel = \PKFrame\DataHandler\JSON::EnCode(\request()->param('siteContact_tel'));
    $qq = \PKFrame\DataHandler\JSON::EnCode(\request()->param('siteContact_qq'));
    $weChat = \PKFrame\DataHandler\JSON::EnCode(\request()->param('siteContact_wechat'));
    $float = (bool)$isFloat;
    $id = $float ? 'float' : '';
    $domain = \request()->domain();
    $apiUrl = API_GuestMsg;
    $time = time();
    return <<<html
<div id="GuestMsg_{$id}"></div>
<link rel="stylesheet" href="/statics/helloweba_tncode/style.css" />
<script type="text/javascript" src="/statics/helloweba_tncode/tn_code.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        let params = {
          siteId: "{$site_id}",
          style: "{$style}",
          domain: "{$domain}",
          float: "{$float}",
          tel: {$tel},
          qq: {$qq},
          weChat: {$weChat}
      };
      $.get('{$apiUrl_ip}',function(res) {
          if ((typeof res === "string")) {
              res = JSON.parse(res);
          }
        if (res.hasOwnProperty('StateCode') && (res.StateCode === 200) && res.hasOwnProperty('Result')) {
            var result = JSON.parse(res.Result);
            if (result.hasOwnProperty('isHyperNumber')) {
                params['isHyperNumber'] = result.isHyperNumber;
                $.post('{$apiUrl}index.php?_={$time}', params, function(body) {
                    $('#GuestMsg_{$id}').html(body);
                });
            }
        }
      });
    });
</script>
html;
}
