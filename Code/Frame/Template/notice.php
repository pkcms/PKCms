<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<?php
defined('PATH_PK') or die();
if (isset($isLoadJS) && $isLoadJS) { ?>
    <link href="/statics/layui/css/layui.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="/statics/admin/style/admin.css" media="all">
    <link rel="stylesheet" href="/statics/admin/style/login.css" media="all">
    <link rel="stylesheet" href="/statics/pkcms/pkcms_login.css" media="all">
<?php } ?>

<body class="text-middle">
<div class="layadmin-user-login layadmin-user-display-show">

    <div class="layadmin-user-login-main login-box">
        <div class="layadmin-user-login-box layadmin-user-login-header">
            <h1>消息提示</h1>
        </div>
        <div class="layadmin-user-login-box layadmin-user-login-body">
            <div class="layui-card">
                <div class="layui-card-body" style="word-break: break-word;">
                    <?php echo isset($msg) ? $msg : ''; ?>
                </div>
                <?php if (isset($url) && !empty($url)) { ?>
                <div class="layui-card-body-mb">
                    <?php
                    if (stristr($url,'login')) {
                        $url = "javascript:window.parent.location.href='{$url}'";
                    }
                    ?>
                    <a href="<?php echo $url; ?>" class="layui-btn">
                        点这里进行跳转
                    </a>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

