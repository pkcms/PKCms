<?php

/**
 * Created by PhpStorm.
 * User: wz_zh
 * Date: 2019/5/4
 * Time: 12:59
 */

namespace PKFrame\Lib;
defined('PATH_PK') or die();

use \Monolog\Handler\StreamHandler;
use \Monolog\Logger;

class Log
{
    private static $_name = 'PKFrame';
    private $log_dir;

    public function __construct(string $_log_dir = null)
    {
        $this->log_dir = is_null($_log_dir) ? 'Log' : ucfirst($_log_dir);
    }

    public static function instance(string $_log_dir = null): Log
    {
        return new static($_log_dir);
    }

    private function _fileName(): string
    {
        $path = PATH_TMP . $this->log_dir . DS;
        file_exists($path) ?: fileHelper()->MKDir($path);
        return $path . date('Ymd') . '.log';
    }

    public function WARNING($message, $errNo, $file, $lineNum)
    {
        try {
            $log = new Logger(self::$_name);
            $log->pushHandler(new StreamHandler($this->_fileName(), Logger::WARNING));
            $log->warning($message, array('errNo' => $errNo, 'file' => $file, 'lineNum' => $lineNum));
        } catch (\Exception $ex) {
            exit('system WARNING');
        }
    }

    public function ERROR($message, $errNo, $file, $lineNum)
    {
        try {
            $log = new Logger(self::$_name);
            $log->pushHandler(new StreamHandler($this->_fileName(), Logger::ERROR));
            $log->error($message, array('errNo' => $errNo, 'file' => $file, 'lineNum' => $lineNum));
        } catch (\Exception $ex) {
            exit('system ERROR');
        }
    }

    public function INFO($message, array $context = [])
    {
        try {
            $log = new Logger(self::$_name);
            $log->pushHandler(new StreamHandler($this->_fileName(), Logger::NOTICE));
            $log->info($message, $context);
        } catch (\Exception $ex) {
            exit('system INFO');
        }
    }

    public function LOGS($message, array $context = [])
    {
        try {
            $log = new Logger(self::$_name);
            $log->pushHandler(new StreamHandler($this->_fileName(), Logger::NOTICE));
            $log->notice($message, $context);
        } catch (\Exception $ex) {
            exit('system LOGS');
        }
    }
}
