<?php


namespace PKFrame\Lib;
defined('PATH_PK') or die();

use PKFrame\DataHandler\JSON;
use PKFrame\Template\Tpl;

class Out
{

    private static $instance;
    private static $tpl;

    /**
     * @return Out
     */
    public static function getInstance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new static;
        }

        return self::$instance;
    }

    public function json($result, $count = 0, $isSuccess = true, $stateCode = 0)
    {
        header("Content-type:application/json; charset=UTF-8");
        list($result, $msgStr) = $this->_handlerResult($result, $isSuccess);
        $result = array(
            'isSuccess' => $isSuccess,
            'code' => $stateCode,
            'count' => (int)$count,
            'data' => $result,
            'msg' => $msgStr);
        die(JSON::EnCode($result));
    }

    public function html($code)
    {
        header("Content-type:text/html;charset=utf-8");
        exit($code);
    }

    public function notice($msgStr, $goto = '', $module = '')
    {
        $tpl = $this->tpl('Notice');
        empty($module) ?: $msgStr = language($msgStr, $module);
        $this->html($tpl->Notice($msgStr, $goto));
    }

    public function noticeByLJson($msgStr, $module = '', $tips = '', $stateCode = 500)
    {
        $this->json(language($msgStr, $module) . $tips, 0, false, $stateCode);
    }

    public function noticeByJson($tips, $stateCode = 500)
    {
        $this->json($tips, 0, false, $stateCode);
    }

    /**
     * 模板引擎
     * @param string $tagName
     * @return Tpl
     */
    public function tpl($tagName = 'page')
    {
        return Tpl::getInstance($tagName);
    }

    private function _handlerResult($result, $isSuccess): array
    {
        if ($isSuccess) {
            if (!is_array($result)) {
                $msgStr = language($result);
                $result = '';
            } else {
                $msgStr = '';
            }
        } else {
            $msgStr = $result;
            $result = '';
        }
        return [$result, $msgStr];
    }

    /**
     * 对http协议的状态设定，跳转页面中需要经常使用的函数
     * @param string $code 状态码
     */
    public function HttpResponseCode($code = '')
    {
        static $_status = array(
            // Informational 1xx
            100 => 'Continue',
            101 => 'Switching Protocols',
            // Success 2xx
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            // Redirection 3xx
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Moved Temporarily ', // 1.1
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            // 306 is deprecated but reserved
            307 => 'Temporary Redirect',
            // Client Error 4xx
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            // Server Error 5xx
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported',
            509 => 'Bandwidth Limit Exceeded'
        );
        if (array_key_exists($code, $_status)) {
            header('HTTP/1.1 ' . $code . ' ' . $_status[$code]);
        }
    }

}