<?php
/**
 * 黑名单 IP
 */

namespace PKFrame\Lib;


class BlackListIP
{

    private static $instance;

    /**
     * @return BlackListIP
     */
    public static function getInstance(): BlackListIP
    {
        if (is_null(static::$instance)) {
            static::$instance = new static;
        }

        return self::$instance;
    }

    public function IsExists(string $guest_ip)
    {
        $blackList_log = \cache()->Tmp()->Read($this->fileName());
        $blackList_ip = explode("\r\n", $blackList_log);
        !in_array($guest_ip, $blackList_ip) ?: die('blacklist');
    }

    public function WriteNewIP()
    {
        fileHelper()->PutContents(
            PATH_TMP, $this->fileName(),
            request()->GuestIP() . "\r\n", false
        );
    }

    protected function fileName(): string
    {
        return dict('logFile_blackList');
    }

}