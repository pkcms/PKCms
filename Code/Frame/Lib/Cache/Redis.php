<?php


namespace PKFrame\Lib\Cache;
defined('PATH_PK') or die();


class Redis
{

    private static $instance;

    /**
     * @return Redis
     */
    public static function getInstance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new static;
        }

        return self::$instance;
    }

}