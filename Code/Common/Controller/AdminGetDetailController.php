<?php


namespace PKCommon\Controller;


abstract class AdminGetDetailController extends AdminGetController
{

    abstract public function ApiGetDetail();

}