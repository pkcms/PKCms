<?php

/**
 * 后台控制器
 * User: hui zhang
 * Date: 2019/5/29
 * Time: 9:43
 */

namespace PKCommon\Controller;

use PKApp\Admin\Model\LoginUserInfo;
use PKApp\Site\Classes\TraitSite;
use PKFrame\Controller;
use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\MatchHelper;
use PKFrame\DataHandler\Numbers;
use PKFrame\DataHandler\Str;

abstract class AdminController extends Controller
{
    use TraitSite;
    protected $siteEntity;
    protected $service;

    public function __construct()
    {
        $isCheck_online = true;
        if (stristr(\request()->url(), 'Admin/Index')) {
            $this->_checkVersion();
        }
        switch (\request()->controller()) {
            case 'AdminImportBackup':
                if ($this->_isFromLocal('Install/Transfer')) {
                    $isCheck_online = false;
                }
                break;
            case 'AdminSetBackup':
                if ($this->_isFromLocal('Install/Upgrade')) {
                    $isCheck_online = false;
                }
                break;
        }
        if ($isCheck_online) {
            $modulePower_isRoot = in_array(\request()->controller(), array('Install', 'AdminModule'));
            $admin = \request()->session(SESSION_AdminKey);
            if (empty($admin) || !is_array($admin)) {
                // 没有检测到在线管理员的信息
                \request()->isAjax()
                    ? $this->noticeByJson('Error_onOnline', 'member', '', 500401)
                    : $this->noticeByPage('Error_onOnline', '/index.php/member/AdminLogin', 'member');
            }
            $this->loginUser($admin);
            if ($modulePower_isRoot && $this->loginUser()->GroupId != 1) {
                $this->noticeByJson('UserLogin_Power_Error');
            }
            loader(PATH_Frame . 'Template' . DS . 'tplTag');
            loader(PATH_Frame . 'Template' . DS . 'tplTag_admin');
        }
    }

    private function _isFromLocal($referer): bool
    {
        $isLocal = stristr(\request()->origin(), \request()->host());
        $isLocal_referer = stristr(\request()->referer(), '/index.php/' . $referer);
        return $isLocal && $isLocal_referer;
    }

    /**
     * @param null $loginUser
     * @return LoginUserInfo
     */
    protected function loginUser($loginUser = null): LoginUserInfo
    {
        static $m;
        if (!is_null($loginUser)) {
            $m = new LoginUserInfo();
            $m->Id = Arrays::GetKey('userId', $loginUser);
            $m->SiteId = Arrays::GetKey('siteId', $loginUser);
            $m->UserName = Arrays::GetKey('userName', $loginUser);
            $m->GroupId = Arrays::GetKey('groupId', $loginUser);
            $m->PowerByAction = explode(',', Arrays::GetKey('powerByAction', $loginUser));
        }
        return $m;
    }

    protected function getIdOfSiteByAdmin()
    {
        Numbers::IsId($this->loginUser()->SiteId) ?: $this->noticeByJson('Site_IdEmpty');
        return $this->loginUser()->SiteId;
    }

    private function _checkVersion()
    {
        $version_local = loader(PATH_CACHE . VERSION_FILE, true);
        $version_code = loader(PATH_PK . VERSION_FILE, true);
        if ($version_local < $version_code) {
            header('location:' . self::ToMeUrl('Install/Upgrade'));
        }
    }

    protected function inputSuccess()
    {
        $this->json('Data_Input_Success');
    }

    protected function tagsPOfTranslate(string $code_html): array
    {
        $list_p = $listP_translate = [];
        MatchHelper::GetPTagInHTMLCode($code_html, $list_p, false);
        foreach ($list_p as $index => $item) {
            if (Str::IsExistsInChinese($item) && !stristr($item, '<img')
                && !stristr($item,'<hr/>')) {
                $listP_translate['p_' . $index] = MatchHelper::FilterSpecialGraphicStr(strip_tags($item));
            }
        }
        return $listP_translate;
    }
}
