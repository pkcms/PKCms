<?php


namespace PKCommon\Controller;


abstract class AdminGetController extends AdminController
{
    abstract public function ApiByLists();

    abstract public function ApiBySelect();

}