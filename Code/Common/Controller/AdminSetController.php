<?php


namespace PKCommon\Controller;


abstract class AdminSetController extends AdminController
{

    abstract protected function model();

    abstract public function ApiByCreate();

    abstract public function ApiByChange();

    abstract public function ApiByDel();

}