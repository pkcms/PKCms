<?php


namespace PKCommon\Controller;

use PKFrame\DataHandler\Arrays;
use PKFrame\DataHandler\MatchHelper;

abstract class AdminSetSortController extends AdminSetController
{

    abstract public function ApiBySortIndex();

    protected function handlerTimeRange($timeRange): array
    {
        $timeRange = explode(' - ', $timeRange);
        if (Arrays::Is($timeRange) == 2) {
            foreach ($timeRange as $item) {
                MatchHelper::MatchFormat('dateTime', $item, 'timeFormat_Error');
            }
        } else {
            $this->noticeByJson('timeRange_Empty');
        }
        return [
            'startTime' => strtotime($timeRange[0]),
            'stopTime' => strtotime($timeRange[1]),
        ];
    }

}