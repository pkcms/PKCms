<?php

namespace PKCommon;

use PKFrame\DataHandler\Date;
use PKFrame\Lib\Curl;

trait TraitDingTalk
{

    protected $api_dingTalk = 'https://dingrobot.lian-xin.com';

    protected function pushDingTalk(array $params)
    {
        $curl = new Curl($this->api_dingTalk, 'json');
        $curl->Param($params)->Request();
    }

    protected function dingOfLogin(string $userName, string $content)
    {
        $domain = request()->domain();
        $dateTime = Date::Format();
        $ip = request()->GuestIP();
        $ipArea = '';
        try {
            $ipArea = getLocationByIp($ip);
        } catch (\Exception $e) {
            handlerException($e);
        }
        $msg = <<<eof
来自站点 {$domain} 的安全预警信息，请留意：
预警事件：登陆管理后台
发生时间：{$dateTime}
使用用户：{$userName}
预警内容：{$content}
来访地区：{$ipArea}（{$ip}）
---------------------------------------------
如非工作人员或客户的操作，请引起重视。
请做好用户名或密码的定期变更，建议使用强度在四星级及以上的密码。
eof;
        $params = ['toUse' => 'default', 'text' => str_replace("\n", "\r", $msg)];
        $this->pushDingTalk($params);
    }

}