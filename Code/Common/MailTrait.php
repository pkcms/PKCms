<?php

namespace PKCommon;

use PKFrame\Lib\Curl;

trait MailTrait
{

    private $_api_url = 'http://func.lian-xin.com/';

    protected function sendMail(string $email, string $html)
    {
        $postArray = [
            'toMail' => $email,
            'mailBody' => $html,
            'fromDomain' => request()->domain(),
        ];
        $curl = new Curl($this->_api_url . 'mail', 'json');
        $result = $curl->Param($postArray)->Request();
        logger()->LOGS('sendMail', json_decode($result, true));
    }

}