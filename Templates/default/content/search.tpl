<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="<{$site_keyword}>" />
		<meta name="description" content="<{$site_description}>" />
		<title>搜索-<{$site_name}></title>
		<!-- 包含 js_css 脚本样式引用模板文件 -->
		<{template "js_css","block"}>
	</head>

	<body class="container">
		<{template "header","block"}>

		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4">
				<{template "sidebar","block"}>

				<div class="panel panel-default">
					<div class="panel-heading">
						按模型检索：
					</div>
					<div class="panel-body">
						<{pk:model action="model"}>
						<div class="bs-example" data-example-id="simple-ol">
							<ol>
								<{loop $modelList $id $name}>
								<li>
									<a href="javascript:PKSite.getSearchUrlByModel(<{$id}>);"><{$name}></a>
								</li>
								<{/loop}>
							</ol>
						</div>
						<{/pk}>
					</div>
				</div>
			</div>

			<div class="col-lg-8 col-md-8 col-sm-8">

				<div class="panel panel-default">
					<div class="panel-heading">
						当前检索词：<{$query}>
					</div>
					<div class="panel-body">
						<div class="bs-example" data-example-id="simple-ol">
							<ol>
								<{loop $searchData $item}>
								<li>
									<a href="<{$item[url]}>"><{str_cut($item[title],46)}></a>
								</li>
								<{/loop}>
							</ol>
						</div>
					</div>
					<div class="panel-footer">
						<div class="Page navigation">
							<ul class="pagination">
								<li>
									<span>总条数：<{$pageNavList['dataCount']}> </span>
								</li>
								<li>
									<span>第 <{$pageNavList['index']}>&nbsp;/&nbsp;<{$pageNavList['pageCount']}> 页</span>
								</li>
								<li>
									<a href="<{$pageNavList['start']}>">首页</a>
								</li>
								<li>
									<a href="<{$pageNavList['pre']}>">前页</a>
								</li>
								<{loop $pageNavList['sizeList'] $pageNumber $url}>
								<li>
									<a href="<{$url}>"><{$pageNumber}></a>
								</li>
								<{/loop}>
								<li>
									<a href="<{$pageNavList['next']}>">后页</a>
								</li>
								<li>
									<a href="<{$pageNavList['end']}>">尾页</a>
								</li>
							</ul>
						</div>
					</div>
				</div>

			</div>
		</div>

		<{template "footer","block"}>
	</body>
</html>
