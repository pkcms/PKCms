<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="<{$seokeywords}>"/>
    <meta name="description" content="<{$seodescription}>"/>
    <title><{$seoTitle}>-<{$site_name}></title>
    <{template "js_css","block"}>
</head>

<body class="container">
<{template "header","block"}>

<div class="panel panel-default">
    <div class="panel-heading">
        <a href="/" class="home">首页</a>/
        <a href="<{$category[url]}>"><{$category[name]}></a> 移动
    </div>
    <div class="panel-title text-center">
        <h1><{$title}></h1>
    </div>
    <div class="panel-body">
        <{$content}>
    </div>
</div>

<{template "footer","block"}>

</body>
</html>

