<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="<{$seoKeywords}>"/>
    <meta name="description" content="<{$seoDescription}>"/>
    <title><{$seoTitle}>-<{$site_name}></title>
    <{template "js_css","block"}>
    <script src="/statics/pkcms/pkcms.js" type="text/javascript"></script>
</head>

<body class="container">
<{template "header","block"}>

<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-4">
        <{template "sidebar","block"}>

        <div class="panel panel-default">
            <div class="panel-heading">
                相关内容
            </div>
            <div class="panel-body">
                <{pk:content action="related" catId="3" num="10"}>
                <div class="bs-example" data-example-id="simple-ol">
                    <ol>
                        <{loop $contentList $item}>
                        <li>
                            <a href="<{$item[url]}>"><{$item[title]}></a>
                        </li>
                        <{/loop}>
                    </ol>
                </div>
                <{/pk}>
            </div>
        </div>
    </div>

    <div class="col-lg-8 col-md-8 col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="/" class="home">首页</a>/
                <{loop $categoryParent $parent}>
                <a href="<{$parent[url]}>"><{$parent[name]}></a> /
                <{/loop}>
                <a href="<{$category[url]}>"><{$category[name]}></a> 移动版
            </div>
            <div class="panel-title text-center">
                <h1><{$title}></h1>
            </div>
            <div class="panel-heading">
                <i class="glyphicon glyphicon-time">&nbsp;<{date('Y-m-d',$createTime)}></i>
                <i class="glyphicon glyphicon-eye-open">
                    &nbsp;<span id="view_size"></span>
                    <script type="text/javascript">
                        PKSite.ready(function () {
                            PKSite.hits('view_size', '<{$siteId}>', '<{$catId}>','<{$id}>');
                        });
                    </script>
                </i>
            </div>
            <div class="panel-body">
                <div id="content" class="content"> <{$content}></div>
                <script type="text/javascript">
                    PKSite.ready(function () {
                        PKSite.getContentByPower('<{$siteId}>', '<{$catId}>','<{$id}>', function (result_json) {
                            $('#content').html(result_json.content);
                        });
                    });
                </script>
            </div>
            <div class="panel-footer">
                上一条新闻：<a href="<{$previous[url]}>" title="<{$previous[title]}>"><{$previous[title]}></a><br/>
                下一条新闻：<a href="<{$next[url]}>" title="<{$next[title]}>"><{$next[title]}></a>
            </div>
        </div>
    </div>
</div>

<{template "footer","block"}>
</body>
</html>