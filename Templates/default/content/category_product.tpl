<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="<{$seo[keyword]}>"/>
    <meta name="description" content="<{$seo[description]}>"/>
    <title><{$seo[title]}>- 第<{$nowPage}>页-<{$site_name}></title>
    <{template "js_css","block"}>
</head>

<body class="container">
<{template "header","block"}>

<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-4">
        <{template "sidebar","block"}>
    </div>

    <div class="col-lg-8 col-md-8 col-sm-8">

        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="/" class="home">首页</a>/
                <{loop $categoryParent $parent}>
                <a href="<{$parent[url]}>"><{$parent[name]}></a> /
                <{/loop}>
                <a href="<{$url}>"><{$name}></a>
            </div>
            <div class="panel-body">
                <{pk:content action="categoryContentList"}>
                <div class="list-group">
                    <ul class="row">
                        <{loop $contentListData $item}>
                        <li class="col-md-3 col-sm-4 text-center" style="list-style: none;">
                            <dd>
                                <img src="<{thumb($item[thumb],120,120)}>" style="max-width: 120px;max-height: 120px;" alt="<{$item[title]}>">
                            </dd>
                            <dt style="padding: 5px;">
                                <a href="<{$item[url]}>"><{str_cut($item[title],46)}></a>
                            </dt>
                        </li>
                        <{/loop}>
                    </ul>
                </div>
                <div class="Page navigation">
                    <ul class="pagination">
                        <li>
                            <span>总条数：<{$pageNavList['dataCount']}> </span>
                        </li>
                        <li>
                            <span>第 <{$pageNavList['index']}> / <{$pageNavList['pageCount']}> 页</span>
                        </li>
                        <li>
                            <a href="<{$pageNavList['start']}>">首页</a>
                        </li>
                        <li>
                            <a href="<{$pageNavList['pre']}>">前页</a>
                        </li>
                        <{loop $pageNavList['sizeList'] $pageNumber $url}>
                        <li>
                            <a href="<{$url}>"><{$pageNumber}></a>
                        </li>
                        <{/loop}>
                        <li>
                            <a href="<{$pageNavList['next']}>">后页</a>
                        </li>
                        <li>
                            <a href="<{$pageNavList['end']}>">尾页</a>
                        </li>
                    </ul>
                </div>
                <{/pk}>
            </div>
        </div>

    </div>
</div>

<{template "footer","block"}>

</body>
</html>