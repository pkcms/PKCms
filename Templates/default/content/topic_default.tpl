<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="<{$seoKeywords}>"/>
    <meta name="description" content="<{$seoDescription}>"/>
    <title><{$seoTitle}></title>
    <{template "js_css","block"}>
</head>

<body class="container">
<div class="panel panel-default">
    <div class="banner">
        <img src="<{$banner}>">
    </div>
    <div class="panel-heading">
        <a href="/" class="home">首页</a>/
        <a href="<{$url}>"><{$title}></a>
    </div>
    <div class="panel-title text-center">
        <h1><{$title}></h1>
    </div>
    <div class="panel-body">
        <{loop $list_topicType $item_topicType}>
        <a href="#<{$item_topicType[id]}>"><{$item_topicType[name]}></a>
        <{/loop}>
    </div>
    <div class="panel-body">
        <{$content}>
    </div>
</div>

<{template "footer","block"}>

</body>
</html>

