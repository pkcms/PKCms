<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="<{$site_keyword}>"/>
    <meta name="description" content="<{$site_description}>"/>
    <title><{$site_title}></title>
    <!-- 包含 js_css 脚本样式引用模板文件 -->
    <{template "js_css","block"}>
</head>

<body class="container">
<{template "header","block"}>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 text-center">
        <div class="panel panel-default">
            <div class="panel-heading">
                图片广告列表
            </div>
            <div class="panel-body">
                <div class="bs-example" id="ads_img" data-example-id="simple-ol">
                    <ol>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    PKSite.ready(function () {
        PKCms.useAppJs('user',function () {
            PKUser.getInfoOFUser(function (res) {
                // 这里写注册成功之后执行的代码
                // console.log(res);
            });
        });
    });
</script>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 text-center">
        <div class="panel panel-default">
            <div class="panel-heading">
                头条列表
            </div>
            <div class="panel-body">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                推荐列表
            </div>
            <div class="panel-body">
                
            </div>
        </div>
    </div>

    <div class="col-lg-6 col-md-6 col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                置顶列表
            </div>
            <div class="panel-body">
            </div>
        </div>
    </div>

    <div class="col-lg-6 col-md-6 col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                常规列表
            </div>
            <div class="panel-body">
                
            </div>
        </div>
    </div>

    <div class="col-lg-6 col-md-6 col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                地图
            </div>
            <div class="panel-body">
                <div id="allmap" style="height: 460px;"></div>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
    PKSite.ready(function () {
        PKSite.BaiDuMapsBySiteId('allmap', '<{$Site_Id}>', 'title', 'text');
    });
</script>

<{template "footer","block"}>
</body>
</html>