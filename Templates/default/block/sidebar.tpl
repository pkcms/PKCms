<div class="panel panel-default">
    <div class="panel-heading">
        新闻资讯
    </div>
    <div class="panel-body">
        <{pk:content action="category" parentId="3"}>
        <div class="bs-example" data-example-id="simple-ol">
            <ol>
                <{loop $categoryList $item}>
                <li>
                    <a href="<{$item[url]}>"><{$item[name]}></a>
                </li>
                <{/loop}>
            </ol>
        </div>
        <{/pk}>
    </div>
</div>
