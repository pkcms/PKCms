<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 text-center">
        <div class="panel panel-default">
            <div class="panel-heading">
                友情链接
            </div>
            <div class="panel-body">
                <{pk:links action="lists" category="1"}>
                <div class="bs-example" data-example-id="simple-ol">
                    <ol class="row">
                        <{loop $linkList $item}>
                        <li class="col-md-2">
                            <a href="<{$item[siteUrl]}>" title="<{$item[siteName]}>"><{$item[siteName]}></a>
                        </li>
                        <{/loop}>
                    </ol>
                </div>
                <{/pk}>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6">

    </div>
</div>

<div style="height: 300px"></div>