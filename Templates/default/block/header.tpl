<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><{$site_name}></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <{pk:content action="category" siteId="1"}>
        <{loop $categoryList $item}>
        <li><a href="<{$item[url]}>"><{$item[name]}></a></li>
        <{/loop}>
        <{/pk}>
      </ul>
      <form class="navbar-form navbar-left" method="get" action="/index.php/content/Search">
        <div class="form-group">
          <input type="hidden" name="siteId" value="1" />
          <{pk:model action="model"}>
          <select name="modelId">
            <{loop $modelList $id $name}>
            <option value="<{$id}>"><{$name}></option>
            <{/loop}>
          </select>
          <{/pk}>
          <input type="text" name="query" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
      <ul class="nav navbar-nav navbar-right">

      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>